initial.plt: Plant community initialization - Little River Experimental Watershed
NAME   PLANTS_COM             CPNM       IGRO     PHU    LAI	 BIOMS   PHUACC      POP     YRMAT     RSDIN   
frst            1     
                              frst          1  5340.3   0.00      0.00     0.00     0.00      0.00  10000.00
past            1
                              past          0  1996.0   0.00      0.00     0.00     0.00      0.00   3000.00  							  
agrl            1        
                              agrl          0  1826.0   0.00      0.00     0.00     0.00      0.00   1000.00
watr            1
                              watr          0  2256.5   0.00      0.00     0.00     0.00      0.00      0.00
utrn            1  
                              berm          0  1996.0   0.00      0.00     0.00     0.00      0.00   3000.00
csoy            2    
                              corn          0  1800.0   0.00      0.00     0.00     0.00      0.00   2000.00       
                              soyb          0  1800.0   0.00      0.00     0.00     0.00      0.00   2000.00 
ryeg            1     
                              ryeg          1  2500.0   1.00    500.00     0.00     0.00      0.00   2000.00