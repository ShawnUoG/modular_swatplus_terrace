reservoir.res: Reservoir properties - LREW Subbasin March 2016
    RES_NUMB     RES_NAME        RES_INI       RES_HYD      RELEASE            RES_SED      RES_NUT        RES_PST
           1 wetland             res_ini_1a    res_hyd_1a   wetland_iowa       res_sed_1a   res_nut_1a     res_pst_1a
           2 reservoir_2a        res_ini_2a    res_hyd_2a   corps_med_res      res_sed_2a   res_nut_2a     res_pst_2a