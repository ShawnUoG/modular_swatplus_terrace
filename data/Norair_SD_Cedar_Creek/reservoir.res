reservoir.res: Reservoir properties - Little River Experimental Watershed
   1
NAME       INIT  WQ  OUTFLO  IFLOD1R  IFLOD2R  IRES1  IRES2  IRESCO  IYRES  MORES     PSA   PVOL     ESA    EVOL     K    NSED   D50     RR  EVRSV  SED_STLR  NDTARGR STARG_FPS  VELSETLR
Example       1   1       0        5       10      5     10       0      0      0   120.0   60.0   400.0   150.0   0.5   200.0   3.0  500.0    0.7       1.0       10       0.9      10.0