initial.res - Reservoir_Data_File
NUMB   NAME         HYDCO  WQCO   VOL    SED  ORGN  NO3  NO2  NH3 ORGP SOLP SECI  SAN  SIL  CLA  SAG  LAG  GRA CHLA PST_MASS SPST_MASS BACTLP  BACTP  
   1   Lake_Belton      1     1   0.9  200.0  10.0  2.0  0.2  0.1  1.0  0.1  1.0  0.0  1.0  9.0  0.0  0.0  0.0  0.1     0.01       0.0    0.0    0.0
   2   Stillhouse       2     1   0.9  200.0  10.0  2.0  0.2  0.1  1.0  0.1  1.0  0.0  1.0  9.0  0.0  0.0  0.0  0.1     0.01       0.0    0.0    0.0