      module terrace_parm

    
    integer :: mnr, mcut, mapp
!     parameters added by Shao.H for the terrace simulation
      ! slope variables
      integer :: tflag
      real :: trc_ha, trc_slp, trc_sll, trc_n, trc_cn, laiday_trc
      real :: canmx_trc, blai_trc, canstor_trc, trc_uslek, trc_p
      real :: tsol_cov, trc_vmax, tcanev, tep_max, tep_day
      real :: trc_cn1, trc_cn2, trc_cn3, cnday_trc
      real :: hru_fn, hru_fp, hru_yn, hru_yp
      real :: hru_rn, hru_vn, hru_fxn, hru_dnt
      real :: bssprev
      real :: bssnprev  ! store previous days' latno3
      real :: bssedpre, bsorgnpre, bsno3pre  ! surface nutrients lag 2013/8/28
      real :: bsorgppre, bssolppre, bsminpapre, bsminpspre  ! surface nutrients lag 2013/8/28
      real :: tfactor   ! sub-daily adjustment factor
      real, dimension (:,:), allocatable :: trcmono,trcyro,trcaao
      integer, dimension (:), allocatable :: trc_release, itrc1, itrc2, trc_psetlp1, trc_psetlp2, trc_nsetlp1, trc_nsetlp2
      integer, dimension (:), allocatable :: trc_type
      integer, dimension (:), allocatable :: tdr_flag
      integer, dimension (:), allocatable :: trc_chtype
      real, dimension (:), allocatable :: trc_fr, trc_sax, trc_volx
      real, dimension (:), allocatable :: trc_htx
      real, dimension (:), allocatable :: trci_fr, trc_drmm, trc_drx
      real, dimension (:), allocatable :: trc_no, trc_chl, trc_chs
      real, dimension (:), allocatable :: trc_wth
      real, dimension (:), allocatable :: trfqeo_30d, teo_30d
      real, dimension (:), allocatable :: pre_wbal, pre_nbal, pre_pbal  ! store value for balance test
      ! runoff variables
      real :: trcq, trcflwi, tpeakr, prec_trc, twtab, trcbtm, trclatq
      real :: ovflwi, inftrc, twltrc, trc_sumsep, tes_day, trc_sumev
      real :: tsmx, tsci
      real :: trcu_cnday, trcr_cnday, trcb_cnday
      real :: trcu_esday, trcr_esday, trcb_esday
      real :: trcu_epday, trcr_epday, trcb_epday
      real, dimension (:), allocatable :: trc_vol
      !real, dimension (:), allocatable :: trc_hafrtl    ! 2012/8/29 by Shao.H
      real, dimension (2) :: twrt
      ! sediment & nutrients parameters
      real :: trcsed, trcsan, trcsil, trccla, trcsag, trclag, trcqsolp
      real :: tsedminpa, tsedminps, tsedorgp, tsedorgn, trcqno3, tlatno3
      real, dimension (:), allocatable :: trc_sed, trc_san, trc_sil
      real, dimension (:), allocatable :: trc_cla, trc_sag, trc_lag
      real, dimension (:), allocatable :: trc_pseds, trc_orgp, trc_orgn
      real, dimension (:), allocatable :: trc_no3, trc_no3s, trc_nsed
      real, dimension (:), allocatable :: trc_stl, trc_solp
      real, dimension (:), allocatable :: trcsa_stl, trcht_stl
      real, dimension (:), allocatable :: tsed_stl, trc_stlr_co
      real, dimension (:), allocatable :: trc_pseda
      ! soil variables
      real :: tsol_sw
      real, dimension (:), allocatable :: tsol_st, tsol_mp
      real, dimension (:), allocatable :: tsol_solp, tsol_orgp
      real, dimension (:), allocatable :: tsol_actp, tsol_stap, tsol_fop
      real, dimension (:), allocatable :: tsol_n, tsol_fon, tsol_mn
      real, dimension (:), allocatable :: tsol_no3, tsol_nh3 
      real, dimension (:), allocatable :: tsol_prk, tsol_flat
      real, dimension (:), allocatable :: tsol_rsd, tsol_mc
      real, dimension (:), allocatable :: tsol_aorgn, tsol_cbn
      real, dimension (:), allocatable :: tsol_orgn
      real :: tsepcrk, tsepcrktot, tcrk, tvoltot !! calculate crack flow 2014-12-28 20:46:05
      real, dimension (:), allocatable :: tsol_volcr
      ! undisturbed segment variables
      real, dimension (:), allocatable :: trcu_fr
      real, dimension (:), allocatable :: tusol_sw, trcu_p, trcu_slp
      real, dimension (:), allocatable :: trcu_sll, trcu_n, trcu_cn 
      real, dimension (:), allocatable :: laiday_trcu, canmx_trcu 
      real, dimension (:), allocatable :: canstor_trcu, trcu_uslek
      real, dimension (:), allocatable :: tusol_cov, tuwtab
      real, dimension (:), allocatable :: trcu_cn1, trcu_cn2, trcu_cn3
      real, dimension (:), allocatable :: trcu_sci, trcu_smx
      real, dimension (:,:), allocatable :: trcu_wrt
      real, dimension (:,:), allocatable :: tusol_st, tusol_prk
      real, dimension (:,:), allocatable :: tusol_flat, tusol_solp
      real, dimension (:,:), allocatable :: tusol_orgp, tusol_mp
      real, dimension (:,:), allocatable :: tusol_stap, tusol_fop
      real, dimension (:,:), allocatable :: tusol_fon, tusol_mn
      real, dimension (:,:), allocatable :: tusol_actp, tusol_no3
      real, dimension (:,:), allocatable :: tusol_nh3, tusol_n
      real, dimension (:,:), allocatable :: tusol_rsd, tusol_mc
      real, dimension (:,:), allocatable :: tusol_aorgn, tusol_cbn
      real, dimension (:,:), allocatable :: tusol_orgn
      real, dimension (:,:), allocatable :: turfqeo_30d, tueo_30d
      real, dimension (:,:), allocatable :: tusol_volcr !! calculate crack flow 2014-12-28 20:46:05
      ! rised segment variables
      real, dimension (:), allocatable :: trcr_fr
      real, dimension (:), allocatable :: trsol_sw, trcr_p, trcr_slp
      real, dimension (:), allocatable :: trcr_sll, trcr_n, trcr_cn
      real, dimension (:), allocatable :: laiday_trcr, canmx_trcr
      real, dimension (:), allocatable :: canstor_trcr, trcr_uslek
      real, dimension (:), allocatable :: trsol_cov, trwtab
      real, dimension (:), allocatable :: trcr_cn1, trcr_cn2, trcr_cn3
      real, dimension (:), allocatable :: trcr_sci, trcr_smx
      real, dimension (:,:), allocatable :: trcr_wrt
      real, dimension (:,:), allocatable :: trsol_st, trsol_prk
      real, dimension (:,:), allocatable :: trsol_flat, trsol_solp
      real, dimension (:,:), allocatable :: trsol_orgp, trsol_mp
      real, dimension (:,:), allocatable :: trsol_stap, trsol_fop
      real, dimension (:,:), allocatable :: trsol_fon, trsol_mn
      real, dimension (:,:), allocatable :: trsol_actp, trsol_no3
      real, dimension (:,:), allocatable :: trsol_nh3, trsol_n
      real, dimension (:,:), allocatable :: trsol_rsd, trsol_mc
      real, dimension (:,:), allocatable :: trsol_aorgn, trsol_cbn
      real, dimension (:,:), allocatable :: trsol_orgn
      real, dimension (:,:), allocatable :: trrfqeo_30d, treo_30d
      real, dimension (:,:), allocatable :: trsol_volcr !! calculate crack flow 2014-12-28 20:46:05
      ! bed segment variables
      real, dimension (:), allocatable :: trcb_fr
      real, dimension (:), allocatable :: tbsol_sw, trcb_p, trcb_slp
      real, dimension (:), allocatable :: trcb_sll, trcb_n, trcb_cn
      real, dimension (:), allocatable :: laiday_trcb, canmx_trcb
      real, dimension (:), allocatable :: canstor_trcb, trcb_uslek
      real, dimension (:), allocatable :: tbsol_cov, tbwtab
      real, dimension (:), allocatable :: trcb_ht
      real, dimension (:), allocatable :: trcb_cn1, trcb_cn2, trcb_cn3
      real, dimension (:), allocatable :: trcb_sci, trcb_smx
      real, dimension (:,:), allocatable :: trcb_wrt
      real, dimension (:,:), allocatable :: tbsol_st, tbsol_prk
      real, dimension (:,:), allocatable :: tbsol_flat, tbsol_solp
      real, dimension (:,:), allocatable :: tbsol_orgp, tbsol_mp
      real, dimension (:,:), allocatable :: tbsol_stap, tbsol_fop
      real, dimension (:,:), allocatable :: tbsol_fon, tbsol_mn
      real, dimension (:,:), allocatable :: tbsol_actp, tbsol_no3
      real, dimension (:,:), allocatable :: tbsol_nh3, tbsol_n
      real, dimension (:,:), allocatable :: tbsol_rsd, tbsol_mc
      real, dimension (:,:), allocatable :: tbsol_aorgn, tbsol_cbn
      real, dimension (:,:), allocatable :: tbsol_orgn
      real, dimension (:,:), allocatable :: tbrfqeo_30d, tbeo_30d
      real, dimension (:,:), allocatable :: tbsol_volcr !! calculate crack flow 2014-12-28 20:46:05
      ! output variables
      real :: trcflwo_ov, trcno3o_ov, trcorgno_ov, trcsolpo_ov
      real :: trcpsedso_ov, trcorgpo_ov, trcsedo_ov, trcsano_ov
      real :: trcsilo_ov, trcclao_ov, trcsago_ov, trclago_ov
      real :: trcno3o_dr, trcorgno_dr, trcsolpo_dr, trcpsedso_dr
      real :: trcorgpo_dr, trcsedo_dr, trcsano_dr, trcsilo_dr
      real :: trcclao_dr, trcsago_dr, trclago_dr, trcflwo_dr
      real :: trcpsedao_ov, trcpsedao_dr
      ! plant growth variables(compute)
      real :: trc_phuacc, trc_bioday, trc_bioms, trc_rd
      real :: trc_rwt, trc_laimxfr, trc_cht, trc_olai
      real :: thvstiadj, trc_pltfrn, trc_pltfrp, tstrsn
      real :: tuno3d, trc_pltn, trc_pltp, tpplnt, tstrsp
      real :: trc_fixn, imgt_trc, tnplnt
      real :: trc_bioyrms, trc_yldanu, trc_laiyrmx
      real :: tsumix, trc_pltet, trc_pltpet
      real :: tsol_rd, tstrsw, tstrsa, tstrstmp
      integer :: tnro, tncut, ticr, ticnop, tntil, trc_cyrmat
      integer :: igro_trc, idorm_trc, tnrot
      real, dimension (:), allocatable :: trc_tillagef
      real, dimension (:,:), allocatable :: trc_biotarg, phup_trc
      real, dimension (:,:), allocatable :: phuho_trc, phuk_trc
      real, dimension (:,:), allocatable :: phut_nocr_trc, phut_trc
      real, dimension (:,:), allocatable :: trc_inilai, trc_inibio
      real, dimension (:,:), allocatable :: trc_cnop, trc_biohv
      real, dimension (:,:), allocatable :: trc_hitarg, trc_phuplt
      real, dimension (:,:), allocatable :: trc_tnyld, trc_frharvk
      real, dimension (:,:), allocatable :: trc_hiovr, trc_yldkg
      real, dimension (:,:), allocatable :: tidtill, tharveff
      real, dimension (:,:), allocatable :: phuh_trc, trc_tnylda
      integer, dimension (:,:), allocatable :: iplant_trc, ihv_trc
      integer, dimension (:,:), allocatable :: ihvo_trc, ihv_gbm_trc
      integer, dimension (:,:), allocatable :: ikill_trc, trc_ncrops
      integer, dimension (:,:), allocatable :: idplt_trc, iop_trc
      ! plant growth variables(undisturbed)
      real, dimension (:), allocatable :: trcu_phuacc, trcu_bioday
      real, dimension (:), allocatable :: trcu_rwt, trcu_rd
      real, dimension (:), allocatable :: trcu_laimxfr, trcu_cht
      real, dimension (:), allocatable :: trcu_olai, trcu_pltfrn
      real, dimension (:), allocatable :: trcu_pltfrp, imgt_trcu
      real, dimension (:), allocatable :: trcu_pltn, trcu_pltp
      real, dimension (:), allocatable :: trcu_fixn, trcu_laiyrmx
      real, dimension (:), allocatable :: trcu_bioyrms
      real, dimension (:), allocatable :: trcu_yldanu, trcu_bioms
      real, dimension (:), allocatable :: tusumix, trcu_pltet
      real, dimension (:), allocatable :: trcu_pltpet, tunplnt
      real, dimension (:), allocatable :: tuhvstiadj, tupplnt
      real, dimension (:), allocatable :: trcu_bioaams, trcu_laiaamx
      real, dimension (:), allocatable :: trcu_yldaa
      real, dimension (:), allocatable :: tustrsw, tustrsa, tustrstmp
      real, dimension (:), allocatable :: tustrsn, tustrsp
      integer, dimension (:), allocatable :: idorm_trcu
      integer, dimension (:), allocatable :: tuicnop, tunro, tunrot
      integer, dimension (:), allocatable :: tuncut, tuicr, tuntil
      integer, dimension (:), allocatable :: trcu_cyrmat, igro_trcu
      real, dimension (:,:), allocatable :: trcu_tillagef
      real, dimension (:,:,:), allocatable :: trcu_biotarg, phuho_trcu 
      real, dimension (:,:,:), allocatable :: phup_trcu, phut_trcu
      real, dimension (:,:,:), allocatable :: phut_nocr_trcu, phuk_trcu
      real, dimension (:,:,:), allocatable :: trcu_inilai, trcu_inibio
      real, dimension (:,:,:), allocatable :: trcu_cnop, trcu_biohv
      real, dimension (:,:,:), allocatable :: trcu_hitarg, trcu_phuplt
      real, dimension (:,:,:), allocatable :: trcu_tnyld, trcu_frharvk
      real, dimension (:,:,:), allocatable :: trcu_hiovr, trcu_yldkg
      real, dimension (:,:,:), allocatable :: tuidtill, tuharveff
      real, dimension (:,:,:), allocatable :: phuh_trcu, trcu_tnylda
      real, dimension (:,:,:), allocatable :: trcu_yldn, trcu_bioaahv
      integer, dimension (:,:,:), allocatable :: ihvo_trcu, ihv_gbm_trcu
      integer, dimension (:,:,:), allocatable :: trcu_ncrops, ihv_trcu
      integer, dimension (:,:,:), allocatable :: iop_trcu, idplt_trcu
      integer, dimension (:,:,:), allocatable :: iplant_trcu, ikill_trcu
      ! plant growth variables(raiser)
      real, dimension (:), allocatable :: trcr_phuacc, trcr_bioday
      real, dimension (:), allocatable :: trcr_rwt, trcr_rd
      real, dimension (:), allocatable :: trcr_laimxfr, trcr_cht
      real, dimension (:), allocatable :: trcr_olai, trcr_pltfrn
      real, dimension (:), allocatable :: trcr_pltfrp, imgt_trcr
      real, dimension (:), allocatable :: trcr_pltn, trcr_pltp
      real, dimension (:), allocatable :: trcr_fixn, trcr_laiyrmx
      real, dimension (:), allocatable :: trcr_bioyrms
      real, dimension (:), allocatable :: trcr_yldanu, trcr_bioms
      real, dimension (:), allocatable :: trsumix, trcr_pltet
      real, dimension (:), allocatable :: trcr_pltpet, trnplnt
      real, dimension (:), allocatable :: trhvstiadj, trpplnt
      real, dimension (:), allocatable :: trcr_bioaams, trcr_laiaamx
      real, dimension (:), allocatable :: trcr_yldaa
      real, dimension (:), allocatable :: trstrsw, trstrsa, trstrstmp
      real, dimension (:), allocatable :: trstrsn, trstrsp
      integer, dimension (:), allocatable :: idorm_trcr
      integer, dimension (:), allocatable :: tricnop, trnro, trnrot
      integer, dimension (:), allocatable :: trncut, tricr, trntil
      integer, dimension (:), allocatable :: trcr_cyrmat, igro_trcr
      real, dimension (:,:), allocatable :: trcr_tillagef
      real, dimension (:,:,:), allocatable :: trcr_biotarg, phuho_trcr 
      real, dimension (:,:,:), allocatable :: phup_trcr, phut_trcr
      real, dimension (:,:,:), allocatable :: phut_nocr_trcr, phuk_trcr
      real, dimension (:,:,:), allocatable :: trcr_inilai, trcr_inibio
      real, dimension (:,:,:), allocatable :: trcr_cnop, trcr_biohv
      real, dimension (:,:,:), allocatable :: trcr_hitarg, trcr_phuplt
      real, dimension (:,:,:), allocatable :: trcr_tnyld, trcr_frharvk
      real, dimension (:,:,:), allocatable :: trcr_hiovr, trcr_yldkg
      real, dimension (:,:,:), allocatable :: tridtill, trharveff
      real, dimension (:,:,:), allocatable :: phuh_trcr, trcr_tnylda
      real, dimension (:,:,:), allocatable :: trcr_yldn, trcr_bioaahv
      integer, dimension (:,:,:), allocatable :: ihvo_trcr, ihv_gbm_trcr
      integer, dimension (:,:,:), allocatable :: trcr_ncrops, ihv_trcr
      integer, dimension (:,:,:), allocatable :: iop_trcr, idplt_trcr
      integer, dimension (:,:,:), allocatable :: iplant_trcr, ikill_trcr
      ! plant growth variables(bed)
      real, dimension (:), allocatable :: trcb_phuacc, trcb_bioday
      real, dimension (:), allocatable :: trcb_rwt, trcb_rd
      real, dimension (:), allocatable :: trcb_laimxfr, trcb_cht
      real, dimension (:), allocatable :: trcb_olai, trcb_pltfrn
      real, dimension (:), allocatable :: trcb_pltfrp, imgt_trcb
      real, dimension (:), allocatable :: trcb_pltn, trcb_pltp
      real, dimension (:), allocatable :: trcb_fixn, trcb_laiyrmx
      real, dimension (:), allocatable :: trcb_bioyrms
      real, dimension (:), allocatable :: trcb_yldanu, trcb_bioms
      real, dimension (:), allocatable :: tbsumix, trcb_pltet
      real, dimension (:), allocatable :: trcb_pltpet, tbnplnt
      real, dimension (:), allocatable :: tbhvstiadj, tbpplnt
      real, dimension (:), allocatable :: trcb_bioaams, trcb_laiaamx
      real, dimension (:), allocatable :: trcb_yldaa
      real, dimension (:), allocatable :: tbstrsw, tbstrsa, tbstrstmp
      real, dimension (:), allocatable :: tbstrsn, tbstrsp
      integer, dimension (:), allocatable :: idorm_trcb
      integer, dimension (:), allocatable :: tbicnop, tbnro, tbnrot
      integer, dimension (:), allocatable :: tbncut, tbicr, tbntil
      integer, dimension (:), allocatable :: trcb_cyrmat, igro_trcb
      real, dimension (:,:), allocatable :: trcb_tillagef
      real, dimension (:,:,:), allocatable :: trcb_biotarg, phuho_trcb 
      real, dimension (:,:,:), allocatable :: phup_trcb, phut_trcb
      real, dimension (:,:,:), allocatable :: phut_nocr_trcb, phuk_trcb
      real, dimension (:,:,:), allocatable :: trcb_inilai, trcb_inibio
      real, dimension (:,:,:), allocatable :: trcb_cnop, trcb_biohv
      real, dimension (:,:,:), allocatable :: trcb_hitarg, trcb_phuplt
      real, dimension (:,:,:), allocatable :: trcb_tnyld, trcb_frharvk
      real, dimension (:,:,:), allocatable :: trcb_hiovr, trcb_yldkg
      real, dimension (:,:,:), allocatable :: tbidtill, tbharveff
      real, dimension (:,:,:), allocatable :: phuh_trcb, trcb_tnylda
      real, dimension (:,:,:), allocatable :: trcb_yldn, trcb_bioaahv
      integer, dimension (:,:,:), allocatable :: ihvo_trcb, ihv_gbm_trcb
      integer, dimension (:,:,:), allocatable :: trcb_ncrops, ihv_trcb
      integer, dimension (:,:,:), allocatable :: iop_trcb, idplt_trcb
      integer, dimension (:,:,:), allocatable :: iplant_trcb, ikill_trcb
      ! soil nutrient variables(compute)
      real :: thmntl, trwntl, thmptl, trmn2tl, trmptl, twdntl
      real :: troctl, trmp1tl, tcmup_kgh, tcmtot_kgh
      ! soil nutrient variables(undisturbed)
      real, dimension (:), allocatable :: tuhmntl, turwntl, tuhmptl
      real, dimension (:), allocatable :: turmn2tl, turmptl, tuwdntl
      real, dimension (:), allocatable :: turoctl, turmp1tl
      real, dimension (:), allocatable :: tucmup_kgh, tucmtot_kgh
      ! soil nutrient variables(raiser)
      real, dimension (:), allocatable :: trhmntl, trrwntl, trhmptl
      real, dimension (:), allocatable :: trrmn2tl, trrmptl, trwdntl
      real, dimension (:), allocatable :: trroctl, trrmp1tl
      real, dimension (:), allocatable :: trcmup_kgh, trcmtot_kgh
      ! soil nutrient variables(bed)
      real, dimension (:), allocatable :: tbhmntl, tbrwntl, tbhmptl
      real, dimension (:), allocatable :: tbrmn2tl, tbrmptl, tbwdntl
      real, dimension (:), allocatable :: tbroctl, tbrmp1tl
      real, dimension (:), allocatable :: tbcmup_kgh, tbcmtot_kgh
      ! fertilization variables(compute)
      real :: trc_tfertn, trc_tfertp, trc_fertn, trc_fertp
      real :: tauto_napp, trc_anano3, tauto_nyr, tafrt_surface
      real :: tauto_nstrs, trc_tauton, trc_tautop, trc_auton
      real :: trc_autop, trc_cfertn, trc_cfertp, trc_tcfrtn
      real :: trc_tcfrtp
      integer :: tnfert, trc_iafrttyp, tncf, trc_icfrt
      integer :: tiday_fert, trc_ndcfrt
      real, dimension (:,:), allocatable :: tfrt_surface, tfrt_kg
      real, dimension (:,:), allocatable :: tcfrt_kg, phucf_trc
      real, dimension (:,:), allocatable :: phun_nocrop_trc, phun_trc
      integer, dimension (:,:), allocatable :: trc_ifrttyp, trc_ifert
      integer, dimension (:,:), allocatable :: trc_icfert, tifrt_freq
      integer, dimension (:,:), allocatable :: tcfrt_id, tfert_days
      ! fertilization variables(undisturbed)
      real :: trcu_cfertn, trcu_cfertp, trcu_auton, trcu_autop
      real :: trcu_fertn, trcu_fertp
      real, dimension (:), allocatable :: trcu_tfertn, trcu_tfertp
      real, dimension (:), allocatable :: trcu_tcfrtn, trcu_tcfrtp
      real, dimension (:), allocatable :: tuauto_napp, trcu_anano3
      real, dimension (:), allocatable :: tuauto_nyr, tuafrt_surface
      real, dimension (:), allocatable :: tuauto_nstrs, trcu_tauton
      real, dimension (:), allocatable :: trcu_tautop
      integer, dimension (:), allocatable :: tunfert, trcu_iafrttyp
      integer, dimension (:), allocatable :: tuncf, trcu_icfrt
      integer, dimension (:), allocatable :: tuiday_fert, trcu_ndcfrt
      real, dimension (:,:,:), allocatable :: tufrt_surface, tufrt_kg
      real, dimension (:,:,:), allocatable :: tucfrt_kg, phucf_trcu
      real, dimension (:,:,:), allocatable :: phun_nocrop_trcu,phun_trcu
      integer, dimension (:,:,:), allocatable :: trcu_ifrttyp, tucfrt_id
      integer, dimension (:,:,:), allocatable :: trcu_icfert
      integer, dimension (:,:,:), allocatable :: tuifrt_freq
      integer, dimension (:,:,:), allocatable :: trcu_ifert, tufert_days
      ! fertilization variables(raiser)
      real :: trcr_cfertn, trcr_cfertp, trcr_auton, trcr_autop
      real :: trcr_fertn, trcr_fertp
      real, dimension (:), allocatable :: trcr_tfertn, trcr_tfertp
      real, dimension (:), allocatable :: trcr_tcfrtn, trcr_tcfrtp
      real, dimension (:), allocatable :: trauto_napp, trcr_anano3
      real, dimension (:), allocatable :: trauto_nyr, trafrt_surface
      real, dimension (:), allocatable :: trauto_nstrs, trcr_tauton
      real, dimension (:), allocatable :: trcr_tautop
      integer, dimension (:), allocatable :: trnfert, trcr_iafrttyp
      integer, dimension (:), allocatable :: trncf, trcr_icfrt
      integer, dimension (:), allocatable :: triday_fert, trcr_ndcfrt
      real, dimension (:,:,:), allocatable :: trfrt_surface, trfrt_kg
      real, dimension (:,:,:), allocatable :: trcfrt_kg, phucf_trcr
      real, dimension (:,:,:), allocatable :: phun_nocrop_trcr,phun_trcr
      integer, dimension (:,:,:), allocatable :: trcr_ifrttyp, trcfrt_id
      integer, dimension (:,:,:), allocatable :: trcr_icfert
      integer, dimension (:,:,:), allocatable :: trifrt_freq
      integer, dimension (:,:,:), allocatable :: trcr_ifert, trfert_days
      ! fertilization variables(bed)
      real :: trcb_cfertn, trcb_cfertp, trcb_auton, trcb_autop
      real :: trcb_fertn, trcb_fertp
      real, dimension (:), allocatable :: trcb_tfertn, trcb_tfertp
      real, dimension (:), allocatable :: trcb_tcfrtn, trcb_tcfrtp
      real, dimension (:), allocatable :: tbauto_napp, trcb_anano3
      real, dimension (:), allocatable :: tbauto_nyr, tbafrt_surface
      real, dimension (:), allocatable :: tbauto_nstrs, trcb_tauton
      real, dimension (:), allocatable :: trcb_tautop
      integer, dimension (:), allocatable :: tbnfert, trcb_iafrttyp
      integer, dimension (:), allocatable :: tbncf, trcb_icfrt
      integer, dimension (:), allocatable :: tbiday_fert, trcb_ndcfrt
      real, dimension (:,:,:), allocatable :: tbfrt_surface, tbfrt_kg
      real, dimension (:,:,:), allocatable :: tbcfrt_kg, phucf_trcb
      real, dimension (:,:,:), allocatable :: phun_nocrop_trcb,phun_trcb
      integer, dimension (:,:,:), allocatable :: trcb_ifrttyp, tbcfrt_id
      integer, dimension (:,:,:), allocatable :: trcb_icfert
      integer, dimension (:,:,:), allocatable :: tbifrt_freq
      integer, dimension (:,:,:), allocatable :: trcb_ifert, tbfert_days
      ! rainfall harvest tank storage variables
      real, dimension (:), allocatable :: trc_tvol, trc_tvolx
      real, dimension (:), allocatable :: tnk_eff, trc_tnkmm, tankirr
      ! irrigation variables(compute)
      real :: trc_aird, trc_qird, trc_tankfr, tsq_rto
      integer :: tnair, tnirr
      real, dimension (:,:), allocatable :: tauto_wstr, tirr_mx
      real, dimension (:,:), allocatable :: tirr_asq, tirr_sq
      real, dimension (:,:), allocatable :: tirr_efm, tirr_amt
      real, dimension (:,:), allocatable :: phuirr_nocrop_trc
      real, dimension (:,:), allocatable :: phuirr_trc, tirr_eff
      integer, dimension (:,:), allocatable :: twstrs_id, trc_iir
      ! irrigation variables(undisturbed)
      real, dimension (:), allocatable :: trcu_aird, trcu_qird
      real, dimension (:), allocatable :: trcu_tankfr
      integer, dimension (:), allocatable :: tunair, tunirr
      real, dimension (:,:,:), allocatable :: tuauto_wstr, tuirr_mx
      real, dimension (:,:,:), allocatable :: tuirr_asq, tuirr_sq
      real, dimension (:,:,:), allocatable :: tuirr_efm, tuirr_amt
      real, dimension (:,:,:), allocatable :: phuirr_nocrop_trcu
      real, dimension (:,:,:), allocatable :: phuirr_trcu, tuirr_eff
      real, dimension (:,:,:), allocatable :: phuai_trcu 
      integer, dimension (:,:,:), allocatable :: tuwstrs_id, trcu_iir
      integer, dimension (:,:,:), allocatable :: tuiairr
      ! irrigation variables(raiser)
      real, dimension (:), allocatable :: trcr_aird, trcr_qird
      real, dimension (:), allocatable :: trcr_tankfr
      integer, dimension (:), allocatable :: trnair, trnirr
      real, dimension (:,:,:), allocatable :: trauto_wstr, trirr_mx
      real, dimension (:,:,:), allocatable :: trirr_asq, trirr_sq
      real, dimension (:,:,:), allocatable :: trirr_efm, trirr_amt
      real, dimension (:,:,:), allocatable :: phuirr_nocrop_trcr
      real, dimension (:,:,:), allocatable :: phuirr_trcr, trirr_eff
      real, dimension (:,:,:), allocatable :: phuai_trcr
      integer, dimension (:,:,:), allocatable :: trwstrs_id, trcr_iir
      integer, dimension (:,:,:), allocatable :: triairr
      ! irrigation variables(bed)
      real, dimension (:), allocatable :: trcb_aird, trcb_qird
      real, dimension (:), allocatable :: trcb_tankfr
      integer, dimension (:), allocatable :: tbnair, tbnirr
      real, dimension (:,:,:), allocatable :: tbauto_wstr, tbirr_mx
      real, dimension (:,:,:), allocatable :: tbirr_asq, tbirr_sq
      real, dimension (:,:,:), allocatable :: tbirr_efm, tbirr_amt
      real, dimension (:,:,:), allocatable :: phuirr_nocrop_trcb
      real, dimension (:,:,:), allocatable :: phuirr_trcb, tbirr_eff
      real, dimension (:,:,:), allocatable :: phuai_trcb
      integer, dimension (:,:,:), allocatable :: tbwstrs_id, trcb_iir
      integer, dimension (:,:,:), allocatable :: tbiairr

      end module terrace_parm
