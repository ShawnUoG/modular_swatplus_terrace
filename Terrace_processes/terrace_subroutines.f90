      subroutine alph_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine computes alpha, a dimensionless parameter that
!!    expresses the fraction of total rainfall that occurs during 0.5h
!!    on terrace segment.

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units       |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
!!    amp_r(:,:)  |none        |alpha factor for rain(mo max 0.5h rain)
!!    idg(:)      |none        |array location of random number seed
!!                             |used for a given process
!!    ihru        |none        |HRU number
!!    ovflwi      |m^3 H2O     |volume of water flow in terrace segment
!!    prec_trc    |mm H2O      |amount of water reaching soil surface in terrace segment
!!    rndseed(:,:)|none        |random number generator seed
!!    snomlt      |mm H2O      |amount of snow melt in HRU on current day
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units       |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
!!    al5         |none        |fraction of total rainfall on day that occurs
!!                             |during 0.5h highest intensity rainfall
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units       |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
!!    ab          |mm H2O      |lowest value al5 can have
!!    ajp         |mm H2O      |highest value al5 can have
!!    j           |none        |HRU number
!!    ovflwimm    |mm H2O      |volume of water flow of terrace segment in mm
!!    preceff     |mm H2O      |amount of rainfall on day in HRU
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    SWAT: Expo, Atri

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use climate_parms
      use basin_module
      use hydrograph_module
      use time_module
      integer :: j
      real :: ab, ajp, preceff, ovflwimm, expo, atri

      j = 0
      j = ihru
      ab = 0.02083
      ovflwimm = ovflwi / trc_ha / 10.

          preceff = 0.
            if (prec_trc > snomlt) then
              preceff = prec_trc - snomlt
            else
              preceff = 0.
            endif

            if (preceff > ovflwimm) then
              preceff = preceff - ovflwimm
            else
              preceff = 0.
            endif
         
          ajp = 0.
          ajp = 1. - Expo(-125. / (preceff + 5.))
          !if (ised_det == 0) then
          !al5 = Atri(ab, amp_r(i_mo,hru_sub(j)), ajp, rndseed(idg(6),j))
          !else
          !al5 = amp_r(i_mo,hru_sub(j))
          !end if
          if (bsn_cc%sed_det == 0) then
            !iwst = ob(icmd)%wst
          iwgn = wst(iwst)%wco%wgn
          al5 = Atri(ab, wgn_pms(iwgn)%amp_r(time%mo),                      &
                   ajp,rndseed(idg(6),iwgn))
          else
              iwgn = wst(iwst)%wco%wgn
            al5 = wgn_pms(iwgn)%amp_r(time%mo)
          end if

      return
      end

      subroutine anfert_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine automatically applies Nitrogen and Phosphorus to terrace
!!    segment when Nitrogen stress exceeds a user input threshhold.  

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    time%yrs       |none          |current year of simulation
!!    fminn(:)    |kg minN/kg frt|fraction of fertilizer which is mineral
!!                               |nitrogen (NO3 + NH3)
!!    fminp(:)    |kg minP/kg frt|fraction of fertilizer which is mineral
!!                               |phosphorus
!!    fnh3n(:)    |kg NH3-N/kg N |fraction of mineral N content of
!!                               |fertilizer which is NH3
!!    forgn(:)    |kg orgN/kg frt|fraction of fertilizer which is organic
!!                               |nitrogen
!!    forgp(:)    |kg orgP/kg frt|fraction of fertilizer which is organic
!!                               |phosphorus
!!    hru_dafr(:) |km**2/km**2   |fraction of watershed area in HRU
!!    ihru        |none          |HRU number
!!    pco%nyskip      |none          |number of years of output summarization
!!                               |and printing to skip
!!    sol_nly(:)  |none          |number of layers in soil profile
!!    tafrt_surface |none          |fraction of fertilizer which is applied
!!                               |to top 10 mm of soil (the remaining
!!                               |fraction is applied to first soil
!!                               |layer)
!!    tauto_napp  |kg NO3-N/ha   |maximum NO3-N content allowed in one
!!                               |fertilizer application
!!    tauto_nstrs |none          |nitrogen stress factor which triggers
!!                               |auto fertilization
!!    tauto_nyr   |kg NO3-N/ha   |maximum NO3-N content allowed to be
!!                               |applied in one year by auto-fertilization
!!    ticr        |none          |sequence number of crop grown within the
!!                               |current year
!!    tnro        |none          |sequence number of year in rotation
!!    trc_auton   |kg N/ha       |amount of nitrogen applied in auto-fert
!!                               |application
!!    trc_autop   |kg P/ha       |amount of phosphorus applied in auto-fert
!!                               |application
!!    trc_phuacc  |none          |fraction of plant heat units accumulated
!!    trc_pltn    |kg N/ha       |amount of nitrogen in plant biomass
!!    trc_tnylda(:,:)|kg N/kg yield|estimated/target nitrogen content of
!!                               |yield used in autofertilization
!!    tsol_aorgn(:)|kg N/ha     |amount of nitrogen stored in the active
!!                               |organic (humic) nitrogen pool in soil layer
!!    tsol_fon(:) |kg N/ha       |amount of nitrogen stored in the fresh
!!                               |organic (residue) pool in soil layer
!!    tsol_fop(:)|kg P/ha       |amount of phosphorus stored in the fresh
!!                               |organic (residue) pool in soil layer
!!    tsol_nh3(:)|kg N/ha       |amount of nitrogen stored in the ammonium
!!                               |pool in soil layer
!!    tsol_no3(:) |kg N/ha       |amount of nitrogen stored in the
!!                               |nitrate pool in soil layer
!!    tsol_orgp(:)|kg P/ha       |amount of phosphorus stored in the organic
!!                               |P pool in soil layer
!!    tsol_solp(:)|kg P/ha       |amount of phosohorus in solution
!!                               |in soil layer
!!    tstrsn      |none          |fraction of potential plant growth achieved on
!!                               |the day where the reduction is caused by
!!                               |nitrogen stress
!!    tstrsp      |none          |fraction of potential plant growth achieved on
!!                               |the day where the reduction is caused by
!!                               |phosphorus stress
!!    wshd_fminp  |kg P/ha       |average annual amount of mineral P applied
!!                               |in watershed
!!    wshd_fnh3   |kg N/ha       |average annual amount of NH3-N applied in
!!                               |watershed
!!    wshd_fno3   |kg N/ha       |average annual amount of NO3-N applied in
!!                               |watershed
!!    wshd_forgn  |kg N/ha       |average annual amount of organic N applied
!!                               |in watershed
!!    wshd_forgp  |kg P/ha       |average annual amount of organic P applied
!!                               |in watershed
!!    wshd_ftotn  |kg N/ha       |average annual amount of N (mineral &
!!                               |organic) applied in watershed
!!    wshd_ftotp  |kg P/ha       |average annual amount of P (mineral &
!!                               |organic) applied in watershed
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    trc_anano3  |kg N/ha       |total amount of nitrogen applied during the
!!                               |year in auto-fertilization
!!    trc_auton   |kg N/ha       |amount of nitrogen applied in auto-fert
!!                               |application
!!    trc_autop   |kg P/ha       |amount of phosphorus applied in auto-fert
!!                               |application
!!    trc_tauton  |kg N/ha       |amount of N applied in autofert operation in
!!                               |year
!!    trc_tautop  |kg P/ha       |amount of P applied in autofert operation in
!!                               |year
!!    tsol_aorgn(:)|kg N/ha      |amount of nitrogen stored in the active
!!                               |organic (humic) nitrogen pool in soil layer
!!    tsol_fon(:) |kg N/ha       |amount of nitrogen stored in the fresh
!!                               |organic (residue) pool in soil layer
!!    tsol_fop(:) |kg P/ha       |amount of phosphorus stored in the fresh
!!                               |organic (residue) pool in soil layer
!!    tsol_nh3(:) |kg N/ha       |amount of nitrogen stored in the ammonium
!!                               |pool in soil layer
!!    tsol_no3(:) |kg N/ha       |amount of nitrogen stored in the
!!                               |nitrate pool in soil layer
!!    tsol_orgp(:) |kg P/ha      |amount of phosphorus stored in the organic
!!                               |P pool in soil layer
!!    tsol_solp(:) |kg P/ha      |amount of phosohorus stored in solution
!!                               |in soil layer
!!    wshd_fminp  |kg P/ha       |average annual amount of mineral P applied
!!                               |in watershed
!!    wshd_fnh3   |kg N/ha       |average annual amount of NH3-N applied in
!!                               |watershed
!!    wshd_fno3   |kg N/ha       |average annual amount of NO3-N applied in
!!                               |watershed
!!    wshd_forgn  |kg N/ha       |average annual amount of organic N applied
!!                               |in watershed
!!    wshd_forgp  |kg P/ha       |average annual amount of organic P applied
!!                               |in watershed
!!    wshd_ftotn  |kg N/ha       |average annual amount of N (mineral &
!!                               |organic) applied in watershed
!!    wshd_ftotp  |kg P/ha       |average annual amount of P (mineral &
!!                               |organic) applied in watershed
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    dwfert      |kg fert/ha    |amount of fertilizer to be applied to meet
!!                               |nitrogen requirement
!!    j           |none          |HRU number
!!    ly          |none          |counter (soil layers)
!!    nstresslocal     |none          |code for approach used to determine amount
!!                               |of nitrogen to terrace segment
!!                               |0 nitrogen target approach
!!                               |1 annual max approach
!!    rtoaf       |none          |weighting factor used to partition the
!!                               |organic N & P content of the fertilizer
!!                               |between the fresh organic and the active
!!                               |organic pools
!!    targn       |kg N/ha       |target mineral N application
!!    tfp         |kg minP/kg frt|fraction of mineral P to be applied
!!    tpno3       |
!!    tsno3       |
!!    xx          |none          |fraction of total amount of fertilizer to
!!                               |be applied to layer
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      implicit none
      integer, parameter :: nstresslocal = 1 
      real, parameter :: rtoaf = 0.50
      integer :: j, ly, ifrt
      real :: tsno3, tpno3, dwfert, xx, targn, tfp

      j = 0
      j = ihru

      ifrt = 0
      ifrt = trc_iafrttyp

!! determine amount of mineral N to be applied
      if (tstrsn < tauto_nstrs) then
        targn = 0.
        if (nstresslocal == 0) then                !! n target approach
         tsno3 = 0.
         tpno3 = 0.
         do ly = 1, soil(j)%nly
           tsno3 = tsno3 + tsol_no3(ly) + tsol_nh3(ly)
         end do
         tpno3 = trc_pltn

         targn = trc_tnylda(tnro,ticr) - tsno3 - tpno3
         if (targn > tauto_napp) targn = tauto_napp
         if (targn < 0.) targn = 0.

         trc_anano3 = trc_anano3 + targn
         if (trc_anano3 >= tauto_nyr) then
           targn = tauto_nyr - (trc_anano3 - targn)
           if (targn < 0.) targn = 0.
           trc_anano3 = tauto_nyr
         endif

        else                                  !! annual max approach
          targn = tauto_napp * (1. - trc_phuacc)
          if (targn > tauto_napp) targn = tauto_napp

          trc_anano3 = trc_anano3 + targn
          if (trc_anano3 >= tauto_nyr) then
            targn = tauto_nyr - (trc_anano3 - targn)
            trc_anano3 = tauto_nyr
          endif
        endif
        if (targn <= 1.e-6) return


!! add nutrients to soil based on nitrogen need
        dwfert = 0.
        if (fertdb(ifrt)%fminn > 0.0001) then
          dwfert = targn / fertdb(ifrt)%fminn
        else
          dwfert = 0.
        endif

        do ly = 1, 2
          xx = 0.
          if (ly == 1) then
            xx = tafrt_surface
          else
            xx = 1. - tafrt_surface
          endif
  
          tsol_no3(ly) = tsol_no3(ly) + xx * dwfert * fertdb(ifrt)%fminn *     &
     &                    (1. - fertdb(ifrt)%fnh3n)
          tsol_nh3(ly) = tsol_nh3(ly) + xx * dwfert * fertdb(ifrt)%fminn *     &
     &                    fertdb(ifrt)%fnh3n

          if (bsn_cc%cswat == 0) then
		tsol_fon(ly) = tsol_fon(ly) + rtoaf * xx * dwfert             &
     &                    * fertdb(ifrt)%forgn
            tsol_aorgn(ly) = tsol_aorgn(ly) + (1. - rtoaf) * xx         &
     &                    * dwfert * fertdb(ifrt)%forgn
            tsol_fop(ly) = tsol_fop(ly) + rtoaf * xx * dwfert           &
     &                    * fertdb(ifrt)%forgp
            tsol_orgp(ly) = tsol_orgp(ly) + (1. - rtoaf) * xx *         &
     &                    dwfert* fertdb(ifrt)%forgp
	    else
            tsol_mc(ly) = tsol_mc(ly) + xx * dwfert * fertdb(ifrt)%forgn*10.
            tsol_mn(ly) = tsol_mn(ly) + xx * dwfert * fertdb(ifrt)%forgn
            tsol_mp(ly) = tsol_mp(ly) + xx * dwfert * fertdb(ifrt)%forgp
	    end if


          !! check for P stress
          tfp = 0.
          if (tstrsp <= 0.75) then
            tfp = fertdb(ifrt)%fminn / 7.
          else
            tfp = fertdb(ifrt)%fminp
          end if
          tsol_solp(ly) = tsol_solp(ly) + xx * dwfert * tfp
        end do

!! summary calculations
          trc_auton = trc_auton + dwfert * (fertdb(ifrt)%fminn + fertdb(ifrt)%forgn)
          trc_autop = trc_autop + dwfert * (tfp + fertdb(ifrt)%forgp)
          trc_tauton = trc_tauton + trc_auton
          trc_tautop = trc_tautop + trc_autop
        if (time%yrs > pco%nyskip) then
  !       wshd_ftotn = wshd_ftotn + dwfert * (fertdb(ifrt)%fminn +              &
  !   &               fertdb(ifrt)%forgn)* hru_dafr(j) * trc_ha / (hru(j)%km*100)
  !       wshd_forgn = wshd_forgn + dwfert * fertdb(ifrt)%forgn * hru_dafr(j) * &
  !   &                                           trc_ha / (hru(j)%km*100)   
  !       wshd_fno3 = wshd_fno3 + dwfert * fertdb(ifrt)%fminn *                 &
  !   &               (1. - fertdb(ifrt)%fnh3n) * hru_dafr(j) *                 &
  !   &                trc_ha / (hru(j)%km*100)
  !       wshd_fnh3 = wshd_fnh3 + dwfert * fertdb(ifrt)%fminn * fertdb(ifrt)%fnh3n *   &
  !   &               hru_dafr(j) * trc_ha / (hru(j)%km*100)
  !       wshd_fminp = wshd_fminp + dwfert * tfp * hru_dafr(j) *         &
  !   &                                            trc_ha / (hru(j)%km*100)
  !       wshd_forgp = wshd_forgp + dwfert * fertdb(ifrt)%forgp * hru_dafr(j) * &
  !   &                                            trc_ha / (hru(j)%km*100)
        hru_fn = hru_fn + dwfert * (fertdb(ifrt)%fminn + fertdb(ifrt)%forgn)          &
     &                                             * trc_ha / (hru(j)%km*100)   !! for terraced hru balance test
        hru_fp = hru_fp + dwfert * (fertdb(ifrt)%fminp + fertdb(ifrt)%forgp)          &
     &                                             * trc_ha / (hru(j)%km*100)   !! for terraced hru balance test
        end if

      endif

      return
    end subroutine
    

      subroutine autoirr_trc

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine performs the auto-irrigation operation on terrace segment

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name           |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tankirr(:)     |mm H2O        |amount of water removed from rainfall harvest tank
!!    hru_ha(:)      |ha            |area of HRU in hectares
!!    ihru           |none          |HRU number
!!    sol_sumfc(:)   |mm H2O        |amount of water held in soil profile at
!!                                  |field capacity
!!    tauto_wstr(:,:)|none or mm    |water stress factor which triggers auto
!!                                  |irrigation
!!    ticr           |none          |sequence number of crop grown within the
!!                                  |current year
!!    tirr_asq       |none          |surface runoff ratio
!!    tirr_mx        |mm            |maximum irrigation amount per auto application
!!    tnro           |none          |sequence number of year in rotation
!!    trc_tvol(:)    |m^3           |volume of rainfall harvest tank water storage 
!!    trc_fr(:)      |none          |fraction of terrace/HRU area
!!    trc_ha         |ha            |terrace segment area in hectare
!!    tsol_sw        |mm H2O        |amount of water stored in soil profile on
!!                                  |any given day
!!    tstrsw         |none          |fraction of potential plant growth achieved
!!                                  |on the day where the reduction is caused by
!!                                  |water stress
!!    twstrs_id(:,:) |none          |water stress identifier
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tankirr(:)  |mm H2O        |amount of water removed from rainfall harvest tank
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    cnv         |none          |conversion factor (mm/ha => m^3)
!!    j           |none          |HRU number
!!    k           |none          |counter
!!    vmm         |mm H2O        |maximum amount of water to be applied
!!    vmma        |mm H2O        |amount of water in source
!!    vol         |mm H2O        |volume of water applied to HRU
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Min
!!    SWAT: irrigate_trc

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j, k
      real :: vmma, vmm, cnv, vol

      j = 0
      j = ihru
  

      if ((twstrs_id(tnro,tnair) == 1 .and.                             &
     &                     tstrsw < tauto_wstr(tnro,tnair)) .or.        &
     &    (twstrs_id(tnro,tnair) == 2 .and.                             &
     &     soil(j)%sumfc - tsol_sw > tauto_wstr(tnro,tnair))) then

      !! determine available amount of water in source
      !! ie upper limit on water removal on day
        vmma = 0.
        vmm = 0.

        cnv = 0.
        cnv = trc_ha * 10.
        vmma = trc_tvol(j) * tirr_eff(tnro,tnair)
        vmma = vmma / cnv

      !! if water available from terrace rainfall harvest tank, proceed with irrigation
        if (vmma > 0.) then
          tsq_rto = tirr_asq(tnro,tnair)
          vmm = (soil(j)%sumfc - tsol_sw) / (1. - tsq_rto)
          vmm = Min(vmm, vmma)
          vmm = Min(vmm,tirr_mx(tnro,tnair))
          call irrigate_trc(j,vmm)

        !! subtract irrigation from terrace rainfall harvest tank
          vol = 0.
          cnv = 0.
          cnv = trc_ha * 10.
          vol = vmm * cnv
          trc_tvol(j) = trc_tvol(j) - vol
          if (trc_tvol(j) < 0.) then
            vol = vol + trc_tvol(j)
            trc_tvol(j) = 0.
          end if
          cnv = 0.
          cnv = (hru(j)%km*100) * trc_fr(j) * 10.
          tankirr(j) = tankirr(j) + vol / cnv

        endif
      end if 


      return
      end

      subroutine canopyint_trc

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine computes canopy interception of rainfall
!!    used for methods other than curve number on terrace segment

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    blai_trc     |none          |maximum (potential) leaf area index
!!    canmx_trc    |mm H2O        |maximum canopy storage
!!    canstor_trc  |mm H2O        |amount of water held in canopy storage
!!    laiday_trc   |m**2/m**2     |leaf area index
!!    prec_trc     |mm H2O        |precipitation for the day in terrace segment
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    canstor_trc  |mm H2O        |amount of water held in canopy storage
!!    prec_trc     |mm H2O        |precipitation for the day in terrace segment
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    canmxl      |mm H2O        |maximum canopy storage at current day's leaf
!!                               |area
!!    canstori    |mm H2O        |initial canopy storage water content
!!    xx          |mm H2O        |precipitation prior to canopy interception 
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~


      use parm; use terrace_parm; use basin_module
      implicit none
      real :: xx, canmxl

          xx = 0.
          xx = prec_trc
          canmxl = 0.
          canmxl = canmx_trc * laiday_trc / blai_trc
          prec_trc = prec_trc - (canmxl - canstor_trc)

          if (prec_trc < 0.) then
            canstor_trc = canstor_trc + xx
            prec_trc = 0.
          else
            canstor_trc = canmxl
          endif

      return
      end

      subroutine carbon_trc
      !! This code simulates organic C, N, and P cycling in the terrace segment soil
      !! The code was developed by Armen R. Kemanian and Stefan Julich
      !! It has been adapted from Kemanian and Stockle (2010) (European Journal of Agronomy 32:22-29)
      !! and crafted to accomodate to SWAT conventions
      !! Plant residues and manure residues are decomposed separately
      !! For convenience, the denitrification subroutine is called from here
      !! March 2009: testing has been minimal and further adjustments are expected
      !! manuscript describing this subroutine to be submitted to Ecological Modelling (September, 2010)
      !! use with caution and report anomalous results to akemanian@psu.edu, and jeff.arnold@ars.usda.edu, stefan.julich@tudor.lu
      
    
!!!!!!!
!      cmup_kgh       kg/ha    current soil carbon for first soil layer
!      cmtot_kgh      kg/ha    current soil carbon integrated - aggregating 
!                                 all soil layers 
 
      !! local variables
      !! cx = saturated soil carbon concentration (%) (Hassink and Whitmore, 1997)
      !! decf = decomposition factor  
      !! net_N = nitrogen net mineralization
      !! net_P = phosphorus net mineralization
      !! rnet_N, rnet_P, mnet_N, mnet_P for residue and manure respectively
      !! sol_cdec = carbon decomposition (CO2)
      !! tilf = tillage factor
      !! resc_hum = humified carbon residue
      !! manc_hum = humified carbon manure

      use parm; use terrace_parm; use basin_module
      !implicit none
      
    !! private variables
      real :: cx, decf, rhc, mhc, sol_cdec, tilf
      real :: resc_hum, manc_hum
      real :: xx, xx1, xx2, xx3, xx4, csf
      real :: rdc, mdc, wdn, cdg, sut
      real :: CNsoil, CPsoil, NPsoil
      real :: CNres, CPres, CNman, CPman, rCNnew, mCNnew
      real :: sol_thick, sol_mass, sol_cmass, sol_nmass
      real :: net_N, net_P, rnet_N, rnet_P, mnet_N, mnet_P
      real :: wc, fc, wf, of, void

    !! mass balance variables
!$$!      real :: sum_c_i, sum_n_i, sum_p_i
!$$!      real :: sum_c_f, sum_n_f, sum_p_f
!$$!      real :: bal_c, bal_n, bal_p

    !! output variables for soil profile
!$$!      real :: cmass_pro, nmass_pro, sol_orgp_pro
!$$!      real :: sol_rsd_pro, sol_fon_pro, sol_fop_pro
!$$!      real :: sol_mc_pro, sol_mn_pro, sol_mp_pro
!$$!      real :: sol_no3_pro, sol_solp_pro, sol_nh3_pro
!$$!      real :: sol_cdec_pro, wdn_pro, net_N_pro, solN_net_min
!$$!      real :: solN_net_min_pro 

      integer :: j, k, kk

    !! functions
      real :: fwf_trc, fof_trc, fcdg_trc, ftilf_trc,fcx_trc, fCNnew_trc
      real :: fhc_trc, fnetmin_trc


      j = 0
      j = ihru
  
    !! initialize
!$$!      cmass_pro = 0.
!$$!      nmass_pro = 0.
!$$!     sol_orgp_pro = 0.
!$$!      sol_rsd_pro = 0.
!$$!      sol_fon_pro = 0.
!$$!      sol_fop_pro = 0.
!$$!      sol_mc_pro = 0.
!$$!      sol_mn_pro = 0.
!$$!      sol_mp_pro = 0.
!$$!      sol_no3_pro = 0.
!$$!      sol_nh3_pro = 0.
!$$!      sol_solp_pro = 0. 
!$$!      sol_cdec_pro = 0.
!$$!      wdn_pro = 0.
!$$!      net_N_pro = 0.
!$$!      solN_net_min = 0.
!$$!      solN_net_min_pro=0.
!!    zero new carbon variables for output.hru
      tcmup_kgh = 0.
      tcmtot_kgh = 0.
!!    zero new carbon variables for output.hru


      if (tsol_cbn(1) == 0.) return	
 
      do k = 1, soil(j)%nly

!$$!      sum_c_i = 0.
!$$!      sum_n_i = 0.
!$$!      sum_p_i = 0.    

      rdc = 0.
      mdc = 0.
      rhc  = 0.
      mhc = 0.
      sol_cdec = 0.    
      resc_hum = 0.
      manc_hum = 0.
      rnet_N = 0.
      rnet_P = 0.
      mnet_N = 0.
      mnet_P = 0.
      net_N = 0.
      net_P = 0.

      rCNnew = 0.
      mCNnew = 0.

      wc = 0.
      fc = 0.
      wf = 0.
      of = 0.
      void = 0.
      cdg = 0.
      sut = 0.
      csf = 0.

      ffres = 0.
      ffres1 = 0.
      ffres2 = 0.

      ffman = 0.
      ffman1 = 0.
      ffman2 = 0.

	if (k == 1) then
	    sol_thick = soil(j)%phys(k)%d
	else	
	    sol_thick = soil(j)%phys(k)%d - soil(j)%phys(k-1)%d
	end if

      !! soil carbon and nitrogen mass
      sol_mass = (sol_thick / 1000.) * 10000. * soil(j)%phys(k)%bd    &
     &     * 1000. * (1- soil(j)%phys(k)%rock / 100.)
      sol_cmass = sol_mass * (tsol_cbn(k) / 100.) 
      sol_nmass = sol_mass * (tsol_n(k) / 100.) 

	!! sum initial C,N,P for mass balance
!$$!	sum_c_i = sol_cmass + 0.43 * tsol_rsd (k) + tsol_mc(k)
!$$!	sum_n_i = sol_nmass + tsol_no3(k) + tsol_nh3(k) + tsol_fon(k)
!$$!     &	+ tsol_mn(k)
!$$!	sum_p_i = tsol_orgp(k) + tsol_solp(k) + tsol_fop(k) 
!$$!     &	+ tsol_mp(k)

      kk = k 
      if (k == 1) kk = 2

	if (soil(j)%phys(kk)%tmp > 0. .and. tsol_st(kk) > 0.) then
		!! microbial processes if temp > 0 C
		!! microbial processes if water > pwp
	
		!!compute soil water factor - sut
		fc = soil(j)%phys(kk)%fc + soil(j)%phys(kk)%wpmm  ! units mm
		wc = tsol_st(kk) + soil(j)%phys(kk)%wpmm  ! units mm
		sat = soil(j)%phys(kk)%ul + soil(j)%phys(kk)%wpmm ! units mm
		void = soil(j)%phys(kk)%por * (1. - wc / sat)	

        wf = fwf_trc(fc,wc,soil(j)%phys(kk)%wpmm)
        of = fof_trc(void,soil(j)%phys(kk)%por)
        sut = wf * of

        !! compute soil temperature factor - cdg
        cdg = fcgd_trc(soil(j)%phys(kk)%tmp)

		!! compute combined factor
		xx = 0.
!!		xx = sqrt(cdg * sut)
		xx = (cdg * sut) ** cf(j)
		if (xx < 0.) xx = 0.
		if (xx > 1.) xx = 1.
		csf = xx
	
		!! call denitrification (to use void and cdg factor)
		wdn = 0.
		if (cdg > 0. .and. void <= 0.1) then
			call ndenit_trc(k,j,cdg,wdn,void)
		end if
!		wshd_dnit = wshd_dnit + wdn * hru_dafr(j) * trc_ha / (hru(j)%km*100)
		twdntl = twdntl + wdn
        hru_dnt = hru_dnt + wdn * trc_ha / (hru(j)%km*100)

		!! calculate soil carbon 'decomposition'
		!! tillage factor
		tilf = ftilf_trc(trc_tillagef(k), wc, sat)	
		!! saturated carbon concentration (%)
		cx = fcx_trc(soil(j)%phys(k)%clay)	
		!! soil carbon decomposition
		sol_cdec=fsol_cdec_trc(tsol_cbn(k),cx,cfdec(j),              &
     & 		tilf,csf,sol_cmass)

        !! residue and manure decomposition and N and P mineralization    
        CNsoil = tsol_cbn(k) / tsol_n(k)
        NPsoil = (sol_mass * tsol_n(k) / 100.)/ tsol_orgp(k)
        CPsoil = CNsoil * NPsoil
    
        if (tsol_rsd(k) > 0.00001) then

            ! This IF STATEMENT preserved in case residues with different properties are considered            
            !if (idplt(nro(j),icr(j),j) > 0) then
            !    decr = rsdco_pl(idplt(nro(j),icr(j),j)) * csf
            !else
            !    decr = 0.05 *  csf
            !end if

            !! residue decomposition
            rdc = 0.05 * csf * tsol_rsd(k)

			!! humification factor
			rhc = fhc_trc(soil(j)%phys(k)%clay,tsol_cbn(k),cx) * cfh(j)
            
            CNres = 0.43 * tsol_rsd(k) / tsol_fon(k)
            CPres = 0.43 * tsol_rsd(k) / tsol_fop(k)
            
            !! CN of new organic matter (humified residue)
            rCNnew = fCNnew_trc(tsol_no3(k),sol_mass,CNres, 110.)

			!! N mineralization / immobilization			
			xx1 = tsol_rsd(k)
			xx2 = tsol_no3(k) + tsol_nh3(k)
			rnet_N = fnetmin_trc(rdc,CNres,rCNnew,rhc,ffres1,xx1,xx2,0.43)

            !! P mineralization / immobilization
          xx2 = tsol_solp(k)
          rnet_P = fnetmin_trc(rdc,CPres,CPsoil,rhc,ffres2,xx1,xx2,0.43)
  
            !! check if P limits N mineralization and re-adjust
            if (ffres2 < ffres1) then
                  rnet_N = rdc * 0.43 * (1. / CNres - rhc / rCNnew)
                ffres = ffres2
            else
                ffres = ffres1
            end if
        end if

        ! manure decomposition and N and P mineralization
        if (tsol_mc(k) > 0.00001) then

            !! residue decomposition
            !! decomposition rate about 1/2 of residue
            mdc = 0.025 * csf * tsol_mc(k)

            !! humification factor
			mhc = 1.6 * fhc_trc(soil(j)%phys(k)%clay,tsol_cbn(k),cx)
			
			CNman = tsol_mc(k) / tsol_mn(k)
			CPman = tsol_mc(k) / tsol_mp(k)
			
			!! CN of new organic matter (humified manure)
			mCNnew = fCNnew_trc(tsol_no3(k),sol_mass,CNman, 55.)

			!! N mineralization / immobilization			
			xx1 = tsol_mc(k)
			xx2 = tsol_no3(k) + tsol_nh3(k)
			mnet_N = fnetmin_trc(mdc,CNman,mCNnew,mhc,ffman1,xx1,xx2,1.0)

            !! P mineralization / immobilization
          xx2 = tsol_solp(k)
          mnet_P = fnetmin_trc(mdc,CPman,CPsoil,mhc,ffman2,xx1,xx2,1.0)
  
            !! check if P or N limit mineralization and re-adjust
            if (ffman2 < ffman1) then
                  mnet_N = mdc * (1. / CNman - mhc / mCNnew)
                ffman = ffman1
            else
                ffman = ffman2
            end if
        end if

        !! check if sufficient mineral N for both residue and manure decomposition
		if ((tsol_no3(k) + tsol_nh3(k)) > 0.) then
			xx = (rnet_N + mnet_N) / (tsol_no3(k) + tsol_nh3(k))
            if (xx < -1.) then
                rdc = -rdc / xx
                rnet_N = -rnet_N / xx
                rnet_P = -rnet_P / xx
                ffres = -ffres / xx

                mdc = -mdc / xx
                mnet_N = -mnet_N / xx
                mnet_P = -mnet_P / xx
                ffman = -ffman / xx
            end if
        end if
        
        !! check if sufficient mineral P for both residue and manure decomposition
        if (tsol_solp(k) > 0.) then
            xx = (rnet_P + mnet_P) / tsol_solp(k)
            if (xx < -1.) then
                rdc = -rdc / xx
                rnet_N = -rnet_N / xx
                rnet_P = -rnet_P / xx
                ffres = -ffres / xx

                mdc = -mdc / xx
                mnet_N = -mnet_N / xx
                mnet_P = -mnet_P / xx
                ffman = -ffman / xx
            end if
        end if

        resc_hum = rhc * rdc * 0.43
        manc_hum = mhc * mdc
        net_N = rnet_N + mnet_N
        net_P = rnet_P + mnet_P
        if (resc_hum == 0.) rCNnew = 1000.
        if (manc_hum == 0.) mCNnew = 1000.

		!! C N P pools update
		sol_cmass = sol_cmass + resc_hum + manc_hum - sol_cdec
 		tsol_cbn(k) = 100. * sol_cmass / sol_mass
		
		sol_nmass = sol_nmass - sol_cdec / CNsoil          &
     &       	  + resc_hum / rCNnew + manc_hum / mCNnew

!! april 2010 output net_min
		solN_net_min=-(- sol_cdec / CNsoil                 &
     &             + resc_hum / rCNnew + manc_hum / mCNnew)
 		tsol_n(k) = 100. * sol_nmass / sol_mass
		tsol_orgn(k) = sol_nmass ! for output
        tsol_orgp(k) = tsol_orgp(k) - sol_cdec / CPsoil       &
     &         + (resc_hum + manc_hum) / CPsoil

                if (ffres > 1.) ffres = 1.

        tsol_rsd(k) = tsol_rsd(k) * (1. - ffres)
        tsol_fon(k) = tsol_fon(k) * (1. - ffres)
        tsol_fop(k) = tsol_fop(k) * (1. - ffres)
     
                if (ffman > 1.) ffman = 1.

        tsol_mc(k) = tsol_mc(k) * (1. - ffman)
        tsol_mn(k) = tsol_mn(k) * (1. - ffman)
        tsol_mp(k) = tsol_mp(k) * (1. - ffman)

		!tsol_no3(k) = tsol_no3(k) + net_N + 
     &	!	sol_cdec * (1. / CNsoil)
		
		! add positive n-mineralization to ammonia pool in the layer
		if (rnet_N>0.) tsol_nh3(k) = tsol_nh3(k) + rnet_N 
		if (mnet_N>0.) tsol_nh3(k) = tsol_nh3(k) + mnet_N

		if (rnet_N<0.) then
			if (abs(rnet_N) < tsol_nh3(k)) then
				tsol_nh3(k) = tsol_nh3(k) + rnet_N
			else
				xx4 = tsol_nh3(k) + rnet_N
				tsol_nh3(k) = 0.
				tsol_no3(k) = tsol_no3(k) + xx4
			end if
		end if
	
		if (mnet_N<0.) then
			if (abs(mnet_N) < tsol_nh3(k)) then
				tsol_nh3(k) = tsol_nh3(k) + mnet_N
			else
				xx4 = tsol_nh3(k) + mnet_N
				tsol_nh3(k) = 0.
				tsol_no3(k) = tsol_no3(k) + xx4
			end if
		end if
	
		tsol_nh3(k) = tsol_nh3(k) + sol_cdec * (1. / CNsoil)
		
		tsol_solp(k) = tsol_solp(k) + net_P +                &
     &		sol_cdec * (1. / CPsoil)

!		wshd_rmn = wshd_rmn + net_N * hru_dafr(j) * trc_ha / (hru(j)%km*100)
!		wshd_rmp = wshd_rmp + net_P * hru_dafr(j) * trc_ha / (hru(j)%km*100)
	 
		If (tsol_rsd(k) < 1e-10) tsol_rsd(k) = 1.e-10
		If (tsol_fon(k) < 1e-11) tsol_fon(k) = 1.e-11
		If (tsol_fop(k) < 1e-12) tsol_fop(k) = 1.e-12
		If (tsol_mc(k) < 1e-10) tsol_mc(k) = 1.e-10
		If (tsol_mn(k) < 1e-11) tsol_mn(k) = 1.e-11
		If (tsol_mp(k) < 1e-12) tsol_mp(k) = 1.e-12
		If (tsol_no3(k) < 1e-12) tsol_no3(k) = 1.e-12

	end if

	!! balance file cswat_balance
!$$!	sum_c_f = sol_cmass + 0.43 * tsol_rsd(k) + sol_cdec
!$$!     &	+ (1. - rhc) * rdc * 0.43 + tsol_mc(k) + (1. - mhc) * mdc
!$$!	sum_n_f = sol_nmass + tsol_no3(k) + tsol_nh3(k) + tsol_fon(k)  
!$$!     &	+ tsol_mn(k) + wdn
!$$!	sum_p_f = tsol_orgp(k) + tsol_solp(k) + tsol_fop(k)
!$$!     &	+ tsol_mp(k)
		  
!$$!	bal_c = sum_c_i - sum_c_f
!$$!	bal_n = sum_n_i - sum_n_f
!$$!	bal_p = sum_p_i - sum_p_f

	!! writing daily output by layer for testing purposes of the routine SJ and AK 2010
	!!if (i==365) then
!!	   write (98,9000) iyr, i, k, j, sol_cmass, tsol_cbn(k),
!!     &	sol_nmass, tsol_n(k), tsol_orgp(k), tsol_rsd(k),
!!     &	tsol_fon(k), tsol_fop(k), tsol_solp(k), tsol_mc(k),
!!     &	tsol_mn(k), tsol_mp(k), tsol_no3(k),
!!     &	sol_cmass/sol_nmass, sol_nmass/tsol_orgp(k), tsol_nh3(k),
!!     &    tilf, sol_cdec, wdn, net_N
!!      	 write (99,9001) iyr, i, k, j, bal_c, sum_c_i, sum_c_f, 
!!     &     bal_n,sum_n_i, sum_n_f, bal_p, sum_p_i, sum_p_f
	!!end if


!!    carbon outputs for .hru file
      if (k == 1) tcmup_kgh = sol_cmass
      tcmtot_kgh = tcmtot_kgh + sol_cmass
!!    carbon outputs for .hru file


!$$!	cmass_pro = cmass_pro + sol_cmass
!$$!	nmass_pro = nmass_pro + sol_nmass
!$$!	sol_rsd_pro = sol_rsd_pro + tsol_rsd(k)
!$$!	sol_cdec_pro = sol_cdec_pro + sol_cdec
!$$!	sol_fon_pro = sol_fon_pro + tsol_fon(k)
!$$!	sol_no3_pro = sol_no3_pro + tsol_no3(k)
!$$!	sol_nh3_pro = sol_nh3_pro + tsol_nh3(k)
!$$!	sol_orgp_pro = sol_orgp_pro + tsol_orgp(k)
!$$!	sol_fop_pro = sol_fop_pro + tsol_fop(k)
!$$!	sol_solp_pro = sol_solp_pro + tsol_solp(k)
!$$!	sol_mc_pro = sol_mc_pro + tsol_mc(k)
!$$!	sol_mn_pro = sol_mn_pro + tsol_mn(k)
!$$!	sol_mp_pro = sol_mp_pro + tsol_mp(k)
!$$!	wdn_pro = wdn_pro + wdn
!$$!	net_N_pro = net_N_pro + net_N
!$$!	solN_net_min_pro = solN_net_min_pro + solN_net_min

      end do
	
	!! writing daily profile output
      !!if (i==365) then
      !!write (100,9002) iyr, i, j, cmass_pro, sol_rsd_pro, sol_mc_pro  


      !!end if

!9000  format(i4,';',i3,';',i1,';',i4,20(';',f10.3))
!9001  format(i4,';',i3,';',i1,';',i4,10(';',f10.3))
!&&!9002  format(i4,';',i3,';',i4,';',f11.3,';',f11.3,';',f11.3)

      return
      end subroutine

	
	!! LOCAL FUNCTIONS
	Function fwf_trc(fc,wc,pwp)
		xx2 = 0.
		if (wc <= pwp) then
			xx2 = 0.4 * wc / pwp
		else if (wc <= fc) then
			xx2 = 0.4 + 0.6 * (wc - pwp)/(fc - pwp)
		else
			xx2 = 1.
		end if

        !!fwf_trc = (1. + (1. - xx2) / (1. - 0.75)) * (xx2 / 1.) ** (1./ (1. - 0.75))    
        fwf_trc = (1. + (1. - xx2) / 0.25) * (xx2) ** 4.
      End function

      Function fof_trc(void,por)
        xx3 = 0.
        if (void >= 0.1) then
            xx3 = 0.2 + 0.8 * (void - 0.1) / (por - 0.1)
        else
            xx3 = 0.2 * void / 0.1
        end if
        fof_trc = 0.5 + 0.5 * xx3 / (xx3 + Exp(-20. * xx3))
      End function

	
	Function fcgd_trc(xx)
		tn = -5.
	  top = 35.
		tx = 50.
		qq = (tn - top)/(top - tx)
	  fcgd_trc = ((xx-tn)**qq)*(tx-xx)/(((top-tn)**qq)*(tx-top))
		if (fcgd_trc < 0.) fcgd_trc = 0.
	End function

      Function ftilf_trc(tillage, wc, sat)
        !! tillage factor effect on decomposition
        !! tillage factor returns to baseline (=1) based on WC 
        tillage = tillage * (1. - 0.02 * wc/sat) 
        if (tillage < 0.) tillage = 0.
        ftilf_trc = 1. + tillage 
      End function
    

      Function fcx_trc(pclay)
        !! saturated soil carbon concentration (%) from Hassink and Whitmore 1997
        fcx_trc = 2.11 + 0.0375 * pclay
      End function


	Function fsol_cdec_trc(pcarbon, cx, cfdec, tilf, csf, sol_cmass)
		!! decomposition adjustment by current SOC 
		decf = (pcarbon / cx) ** 0.5	
		! if (decf > 1.) decf = 1. 
		!! maximum soil carbon decomposition = 0.045 kg C / kg C per year
		fsol_cdec_trc = cfdec / 365. * decf * tilf * csf * sol_cmass
	End function


      Function fCNnew_trc(yy1,yy2,CNpool,yy5)
      !! CN ratio of newly formed organic matter
      !! based on CN or decomposing residue and nitrate in soil
      !! the same approach used for crop residues and manure
      !! CNpool = the CN of decomposing pool
      !! yy1 = the layer nitrate mass
      !! yy2 = the layer soil mass
      !! yy3 = nitrate concentration g/g
      !! yy4 = correction factor based on CN of decomposing pool
      !! yy5 = input-dependent constant to correct CN ratio

        yy3 = yy1 / yy2
        yy4 = 5.5 * (1. - 1. / (1. + (CNpool / yy5)**3.))
        fCNnew_trc = 8.5 + yy4 * (0.5 + 0.5 / (1. + (yy3 / 8e-6)**3.)) 
      End function


	Function fhc_trc(pclay, pcarbon, cx) 		 
	!! maximum and actual humification factor 
	!! hx = maximum humification factor
	!! hf = humification adjustment factor 
	!! pclay = %clay
	!! pcarbon = %carbon
	!! cx = saturated soil carbon, %
	
	real :: hx, hf, pclay, pcarbon
			 
		hx = 0.09 + 0.09 * (1. - Exp(-5.5 * pclay / 100.))
		!! humification adjustment by current SOC
		if (pcarbon > cx) then
			hf = 0.
		else
			hf = 1. - (pcarbon / cx) ** 6.
		end if
		fhc_trc = hx * hf		  
	End function

	Function fnetmin_trc(poold, R1, R2, hc, dummy, poolm, xinorg, cc1)
	!! This function computes net mineralization
	!! R1 = CN or CP ratio of decomposing pool
	!! R2 = CN or CP ratio of receiving pool
	!! hc = humification rate
	!! dummy = fraction of pool being decomposed
	!! poolm = current mass of pool being decomposed
	!! poold = mass of pool decomposed
	!! xinorg = mass of NO3 or P in solution
	!! xx = net mineralization of N or P
	!! cc1 = pool's carbon fraction

		xx = 0.
		xx = poold * cc1 * (1. / R1 - hc / R2) 
		
		if (xx > 0.) then
			dummy = poold / poolm
		else if (abs(xx)< xinorg) then 
			!! net mineralization is positive or
			!! immobilization not consuming all mineral N or P
			dummy = poold / poolm
		else
			!! immobilization, consuming all mineral N or P
			xx = -xinorg
			poold = xx / cc1 * 1. / (1. / R1 - hc / R2)
			dummy = poold / poolm
    		end if

        fnetmin_trc = xx
      End function

      subroutine confert_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine simulates a continuous fertilizer operation on terrace segment

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name         |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    time%yrs        |none          |current year of simulation
!!    fminn(:)     |kg minN/kg frt|fraction of mineral N (NO3 + NH3) in 
!!                                |fertilizer/manure
!!    fminp(:)     |kg minP/kg frt|fraction of mineral P in fertilizer/manure
!!    fnh3n(:)     |kg NH3-N/kg minN|fraction of NH3-N in mineral N in 
!!                                |fertilizer/manure
!!    forgn(:)     |kg orgN/kg frt|fraction of organic N in fertilizer/manure
!!    forgp(:)     |kg orgP/kg frt|fraction of organic P in fertilizer/manure
!!    hru_dafr(:)  |km**2/km**2   |fraction of watershed area in HRU
!!    ihru         |none          |HRU number
!!    iida         |julian date   |day being simulated (current julian day
!!    laiday_trc   |m**2/m**2     |leaf area index
!!    pco%nyskip       |none          |number of years to skip output summarization
!!                                |and printing
!!    phucf_trc(:,:)|none         |fraction of plant heat units at which
!!                                |continuous fertilization begins
!!    sol_bd(:,:)  |Mg/m**3       |bulk density of the soil
!!    sol_z(:,:)   |mm            |depth to bottom of soil layer
!!    tcfrt_id(:,:)|none          |manure (fertilizer) identification
!!                                |number from fert.dat
!!    tcfrt_kg(:,:)|(kg/ha)/day   |dry weight of fertilizer/manure deposited
!!                                |on terrace segment daily
!!    tfert_days(:,:)|none        |number of days continuous fertilization
!!                                |will be simulated
!!    tncf         |none          |sequence number of continuous fertilizer
!!                                |operation within the year
!!    tnro         |none          |sequence number of year in rotation
!!    trc_cfertn   |kg N/ha       |total amount of nitrogen applied to soil
!!                                |during continuous fertilizer operation in 
!!                                |terrace segment on day
!!    trc_cfertp   |kg P/ha       |total amount of phosphorus applied to soil
!!                                |during continuous fertilizer operation in 
!!                                |terrace segment on day
!!    trc_icfert(:,:)|julian date |date continuous fertilizer operation begins
!!    trc_icfrt    |none          |continuous fert flag for terrace segment:
!!                                |0 terrace segment currently not continuously fertilized
!!                                |1 terrace segment currently continuously fertilized
!!    trc_ndcfrt   |days          |number of days terrace segment has been continuously
!!                                |fertilized
!!    trc_phuacc   |none          |fraction of plant heat units accumulated
!!    tsol_fon(:)  |kg N/ha       |amount of nitrogen stored in the fresh
!!                                |organic (residue) pool
!!    tsol_fop(:)  |kg P/ha       |amount of phosphorus stored in the fresh
!!                                |organic (residue) pool
!!    tsol_nh3(:)  |kg N/ha       |amount of nitrogen stored in the ammonium
!!                                |pool in soil layer
!!    tsol_no3(:)  |kg N/ha       |amount of nitrogen stored in the nitrate pool
!!                                |in soil layer
!!    tsol_solp(:) |kg P/ha       |amount of phosohorus stored in solution
!!    wshd_fminp   |kg P/ha       |average annual amount of mineral P applied
!!                                |in watershed
!!    wshd_fnh3    |kg N/ha       |average annual amount of NH3-N applied in
!!                                |watershed
!!    wshd_fno3    |kg N/ha       |average annual amount of NO3-N applied in
!!                                |watershed
!!    wshd_ftotn   |kg N/ha       |average annual amount of N (mineral &
!!                                |organic) applied in watershed
!!    wshd_ftotp   |kg P/ha       |average annual amount of P (mineral &
!!                                |organic) applied in watershed
!!    wshd_orgn    |kg N/ha       |average annual amount of organic N applied
!!                                |in watershed
!!    wshd_orgp    |kg P/ha       |average annual amount of organic P applied
!!                                |in watershed
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tncf        |none          |sequence number of continuous fertilizer
!!                               |operation within the year
!!    trc_cfertn  |kg N/ha       |amount of nitrogen added to soil in continuous
!!                               |fertilizer operation on day
!!    trc_cfertp  |kg P/ha       |amount of phosphorus added to soil in continuous
!!                               |fertilizer operation on day
!!    trc_icfrt   |none          |continuous fertilizer flag for terrace segment:
!!                               |0 terrace segment currently not continuously fertilized
!!                               |1 terrace segment currently continuously fertilized
!!    trc_ndcfrt  |days          |number of days HRU has been continuously
!!                               |fertilized
!!    trc_tcfertn |kg N/ha       |total amount of nitrogen applied to soil
!!                               |during continuous fertilizer operation in 
!!                               |terrace segment on day
!!    trc_tcfertp |kg P/ha       |total amount of phosphorus applied to soil
!!                               |during continuous fertilizer operation in 
!!                               |terrace segment on day
!!    tsol_fon(:) |kg N/ha       |amount of nitrogen stored in the fresh
!!                               |organic (residue) pool
!!    tsol_fop(:) |kg P/ha       |amount of phosphorus stored in the fresh
!!                               |organic (residue) pool
!!    tsol_nh3(:) |kg N/ha       |amount of nitrogen stored in the ammonium
!!                               |pool in soil layer
!!    tsol_no3(:) |kg N/ha       |amount of nitrogen stored in the nitrate pool
!!                               |in soil layer
!!    tsol_solp(:)|kg P/ha       |amount of phosohorus stored in solution
!!    wshd_fminp  |kg P/ha       |average annual amount of mineral P applied
!!                               |in watershed
!!    wshd_fnh3   |kg N/ha       |average annual amount of NH3-N applied in
!!                               |watershed
!!    wshd_fno3   |kg N/ha       |average annual amount of NO3-N applied in
!!                               |watershed
!!    wshd_ftotn  |kg N/ha       |average annual amount of N (mineral &
!!                               |organic) applied in watershed
!!    wshd_ftotp  |kg P/ha       |average annual amount of P (mineral &
!!                               |organic) applied in watershed
!!    wshd_orgn   |kg N/ha       |average annual amount of organic N applied
!!                               |in watershed
!!    wshd_orgp   |kg P/ha       |average annual amount of organic P applied
!!                               |in watershed
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    frt_t       |
!!    gc          |
!!    gc1         |
!!    it          |none          |manure/fertilizer id number from fert.dat
!!    j           |none          |HRU number
!!    l           |none          |number of soil layer that manure is applied
!!    swf         |
!!    xx          |
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Max
!!    SWAT: 

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      implicit none
      integer :: j, l, it

      j = 0
      j = ihru

!! if continuous fertilization not currently on, check to see if it is time
!! to initialize continuous fertilization
      if (trc_icfrt == 0) then
        if (trc_icfert(tnro,tncf) > 0 .and.                             &
     &                             iida >= trc_icfert(tnro,tncf)) then
          trc_icfrt = 1
          trc_ndcfrt = 1
          tiday_fert = tifrt_freq(tnro,tncf)
        else if (trc_phuacc > phucf_trc(tnro,tncf)) then
          trc_icfrt = 1
          trc_ndcfrt = 1
          tiday_fert = tifrt_freq(tnro,tncf)
        else
          return
        end if
      else
        !! if not first day of continuous fert increment total days of 
        !! continuous fert by one
        trc_ndcfrt = trc_ndcfrt + 1
      end if

      if (tiday_fert == tifrt_freq(tnro,tncf)) then
        !! apply manure
        it = 0
        it = tcfrt_id(tnro,tncf)
        if (tcfrt_kg(tnro,tncf) > 0.) then
          l = 1

          tsol_no3(l) = tsol_no3(l) + tcfrt_kg(tnro,tncf) *             &
     &                 (1. - fertdb(it)%fnh3n) * fertdb(it)%fminn
          tsol_fon(l) = tsol_fon(l) + tcfrt_kg(tnro,tncf) * fertdb(it)%forgn
          tsol_nh3(l) = tsol_nh3(l) + tcfrt_kg(tnro,tncf) *             &
     &                 fertdb(it)%fnh3n * fertdb(it)%fminn
          tsol_solp(l) = tsol_solp(l) + tcfrt_kg(tnro,tncf) * fertdb(it)%fminp
          tsol_fop(l) = tsol_fop(l) + tcfrt_kg(tnro,tncf) * fertdb(it)%forgp

        endif
 
        !! reset frequency counter
        tiday_fert = 1

        !! summary calculations
        trc_cfertn = trc_cfertn + tcfrt_kg(tnro,tncf) *                 &
     &               (fertdb(it)%fminn + fertdb(it)%forgn)
        trc_cfertp = trc_cfertp + tcfrt_kg(tnro,tncf) *                 &
     &               (fertdb(it)%fminp + fertdb(it)%forgp)
        trc_tcfrtn = trc_tcfrtn + trc_cfertn
        trc_tcfrtp = trc_tcfrtp + trc_cfertp

        if (time%yrs > pco%nyskip) then
 !         wshd_ftotn = wshd_ftotn + tcfrt_kg(tnro,tncf) *               &
 !    &                 hru_dafr(j) * (fertdb(it)%fminn + fertdb(it)%forgn) *          &
 !    &                 trc_ha / (hru(j)%km*100)
 !         wshd_forgn = wshd_forgn + tcfrt_kg(tnro,tncf) *               &
 !    &                 hru_dafr(j) * fertdb(it)%forgn * trc_ha / (hru(j)%km*100)
 !         wshd_fno3 = wshd_fno3 + tcfrt_kg(tnro,tncf) *                 &
 !    &                hru_dafr(j) * fertdb(it)%fminn * (1. - fertdb(it)%fnh3n) *      &
 !    &                trc_ha / (hru(j)%km*100)
 !         wshd_fnh3 = wshd_fnh3 + tcfrt_kg(tnro,tncf) * hru_dafr(j)     &
 !    &               * fertdb(it)%fminn * fertdb(it)%fnh3n * trc_ha / (hru(j)%km*100)
 !         wshd_ftotp = wshd_ftotp + tcfrt_kg(tnro,tncf) *               &
 !    &                 hru_dafr(j) * (fertdb(it)%fminp + fertdb(it)%forgp) *          &
 !    &                 trc_ha / (hru(j)%km*100)
 !         wshd_fminp = wshd_fminp + tcfrt_kg(tnro,tncf) *               &
 !    &                 hru_dafr(j) * fertdb(it)%fminp * trc_ha / (hru(j)%km*100)
 !         wshd_forgp = wshd_forgp + tcfrt_kg(tnro,tncf) *               &
 !    &                 hru_dafr(j) * fertdb(it)%forgp * trc_ha / (hru(j)%km*100)
        hru_fn = hru_fn + tcfrt_kg(tnro,tncf) * (fertdb(it)%fminn +            &
     &                              fertdb(it)%forgn) * trc_ha / (hru(j)%km*100)   !! for terraced hru balance test
        hru_fp = hru_fp + tcfrt_kg(tnro,tncf) * (fertdb(it)%fminp +            &
     &                              fertdb(it)%forgp) * trc_ha / (hru(j)%km*100)   !! for terraced hru balance test
        end if
      else
        tiday_fert = tiday_fert + 1
      end if

!! check to set if continuous fertilizer period is over
      if (trc_ndcfrt == tfert_days(tnro,tncf)) then
        trc_icfrt = 0
        trc_ndcfrt = 0
        tiday_fert = 0
        tncf = tncf + 1
      end if


      return
      end

      subroutine crackflow_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this surboutine modifies surface runoff to account for crack flow

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    ihru        |none          |HRU number
!!    trcq        |mm H2O        |surface runoff in the HRU for the day
!!    tvoltot     |mm            |total volume of cracks expressed as depth
!!                               |per unit area
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    trcq        |mm H2O        |surface runoff in the terrace segment for the day
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    j           |none          |HRU number
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
 
      integer :: j

      j = 0
      j = ihru

      !! subtract crack flow from surface runoff
      if (surfq(j) > voltot) then
        trcq = trcq - tvoltot
      else
        trcq = 0.
      endif

      return
      end

      subroutine crackvol_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this surboutine computes total crack volume for the soil profile and 
!!    modifies surface runoff to account for crack flow

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    ihru        |none          |HRU number
!!    sol_fc(:,:) |mm H2O        |amount of water available to plants in soil
!!                               |layer at field capacity (fc - wp)
!!    sol_nly(:)  |none          |number of soil layers in HRU
!!    sol_st(:,:) |mm H2O        |amount of water stored in the soil layer on
!!                               |any given day (less wp water)
!!    sol_sumfc(:)|mm H2O        |amount of water held in soil profile at
!!                               |field capacity
!!    sol_sw(:)   |mm H2O        |amount of water stored in soil profile on
!!                               |any given day
!!    crdep(:,:)  |mm            |maximum or potential crack volume
!!    volcr(:,:)  |mm            |crack volume for soil layer
!!    volcrmin    |mm            |minimum crack volume allowed in any soil layer
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    volcr(:,:)  |mm            |crack volume for soil layer
!!    voltot      |mm            |total volume of cracks expressed as depth
!!                               |per unit area
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    crlag       |none          |lag factor for day
!!    crlagdry    |none          |lag in crack development when soil is dry
!!    crlagwet    |none          |lag in crack development when soil is wet
!!    j           |none          |HRU number
!!    l           |none          |counter
!!    volcrnew    |mm            |crack volume for soil layer based on new 
!!                               |moisture conditions
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
 
      integer :: l, j
      real :: volcrnew, crlag, crlagdry = .99, crlagwet = 0.

      j = 0
      j = ihru
      tvoltot = 0.

      !! calculate volume of cracks in soil
      do l = 1, soil(j)%nly
        volcrnew = 0.
        crlag = 0.
        volcrnew = soil(j)%phys(l)%crdep * (soil(j)%phys(l)%fc - tsol_st(l)) /        &
     &       (soil(j)%phys(l)%fc)
        if (tsol_sw < .90 * soil(j)%sumfc) then
          if (volcrnew > tsol_volcr(l)) then
            crlag = crlagdry
          else
            crlag = crlagwet
          end if
        else
          crlag = crlagwet
        end if
        tsol_volcr(l) = crlag * tsol_volcr(l) + (1. - crlag) * volcrnew
        if (tsol_volcr(l) < 0.) tsol_volcr(l) = 0.
        tvoltot = tvoltot + tsol_volcr(l) + volcrmin
      end do

      return
      end

      subroutine curno_trc(cnn,h)

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine determines the curve numbers for moisture conditions
!!    I and III and calculates coefficents and shape parameters for the 
!!    water retention curve on terrace segment
!!    the coefficents and shape parameters are calculated by one of two methods:
!!    the default method is to make them a function of soil water, the 
!!    alternative method (labeled new) is to make them a function of 
!!    accumulated PET, precipitation and surface runoff.

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    cnn         |none          |SCS runoff curve number for moisture
!!                               |condition II
!!    h           |none          |HRU number
!!    sol_sumfc(:)|mm H2O        |amount of water held in soil profile at
!!                               |field capacity
!!    sol_sumul(:)|mm H2O        |amount of water held in soil profile at
!!                               |saturation
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!
!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    trc_cn1     |none          |SCS runoff curve number for moisture
!!                               |condition I
!!    trc_cn3     |none          |SCS runoff curve number for moisture
!!                               |condition III
!!    tsci        |none          |retention coefficient for cn method based on
!!                               |plant ET in terrace
!!    tsmx        |none          |retention coefficient for cn method based on
!!                               |soil moisture in terrace
!!    twrt(1)     |none          |1st shape parameter for calculation of 
!!                               |water retention
!!    twrt(2)     |none          |2nd shape parameter for calculation of 
!!                               |water retention
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    
!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition   
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    c2          |none          |variable used to hold calculated value
!!    rto3        |none          |fraction difference between CN3 and CN1 
!!                               |retention parameters
!!    rtos        |none          |fraction difference between CN=99 and CN1 
!!                               |retention parameters
!!    s3          |none          |retention parameter for CN3
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Exp, Max
!!    SWAT: ascrv

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      integer, intent (in) :: h
      real, intent (in) :: cnn
      real :: c2, s3, rto3, rtos, smxold
   
      trc_cn2 = cnn
      if (trc_cn1 > 1.e-6) smxold = 254.* (100. / trc_cn1 - 1.)
      c2 = 0.
      trc_cn3 = 0.
      s3 = 0.
      trc_cn1 = 0.

!! calculate moisture condition I and III curve numbers
      c2 = 100. - cnn
      trc_cn1 = cnn - 20. * c2 / (c2 + Exp(2.533 - 0.0636 * c2))
      trc_cn1 = Max(trc_cn1, .4 * cnn)
      trc_cn3 = cnn * Exp(.006729 * c2)

!! calculate maximum retention parameter value
      tsmx = 254. * (100. / trc_cn1 - 1.)

!! calculate retention parameter value for CN3
      s3 = 254. * (100. / trc_cn3 - 1.)

!! calculate fraction difference in retention parameters
      rto3 = 0.
      rtos = 0.
      rto3 = 1. - s3 / tsmx
      rtos = 1. - 2.54 / tsmx
!! calculate shape parameters
      !call ascrv(rto3,rtos,soil(h)%sumfc,soil(h)%sumul,twrt(1),twrt(2))
      sumul = soil(h)%sumul
      sumfc = soil(h)%sumfc + hru(h)%hyd%cn3_swf * (sumul -          &
                                                     soil(h)%sumfc)
!! calculate shape parameters
      call ascrv(rto3,rtos,sumfc,sumul,twrt(1),twrt(2))

      if (time%yrs == 1 .and. iida == time%idaf) then
	  tsci = 0.9 * tsmx
	else
        tsci = (1. - ((smxold - tsci) / smxold)) * tsmx      !! plant ET
	end if

      return
      end

      subroutine dailycn_trc

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    Calculates curve number for the day in terrace segment 

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    ihru        |none          |HRU number
!!    tsci        |none          |retention coefficient for cn method based on
!!                               |plant ET in terrace
!!    tsmx        |none          |retention coefficient for cn method based on
!!                               |soil moisture
!!    sol_tmp(2,:)|deg C         |daily average temperature of second soil layer
!!    tsol_sw     |mm H2O        |amount of water stored in soil profile on
!!                               |any given day
!!    twrt(1)     |none          |1st shape parameter for calculation of
!!                               |water retention
!!    twrt(2)     |none          |2nd shape parameter for calculation of
!!                               |water retention
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    cnday_trc   |none          |curve number for current day, terrace segment 
!!                               |and at current soil moisture
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    j           |none          |HRU number
!!    r2          |none          |retention parameter in CN equation
!!    xx          |none          |variable used to store intermediate
!!                               |calculation result
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Exp


!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~


      use parm; use terrace_parm; use basin_module

      integer :: j   

      real :: xx, r2

      j = 0
      j = ihru


      xx = 0.
      r2 = 0.
      xx = twrt(1) - twrt(2) * tsol_sw
      if (xx < -20.) xx = -20.
      if (xx > 20.) xx = 20.

      if (icn <= 0) then
        !! traditional CN method (function of soil water)
        if ((tsol_sw + Exp(xx)) > 0.001) then
          r2 = tsmx * (1. - tsol_sw / ( tsol_sw + Exp(xx)))
        end if
      else                                                     
        !! alternative CN method (function of plant ET)
        r2 = amax1(3., tsci)
      end if      

      if (soil(j)%phys(2)%tmp <= 0.) r2 = tsmx * (1. - Exp(- bsn_prm%cn_froz * r2))
      r2 = amax1(3.,r2)

      cnday_trc = 25400. / (r2 + 254.)

      return
      end

      subroutine dormant_trc

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine checks the dormant status of the different plant types

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name           |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    alai_min(:)    |m**2/m**2     |minimum LAI during winter dormant period
!!    bio_leaf(:)    |none          |fraction of biomass that drops during
!!                                  |dormancy (for trees only)
!!    dayl(:)        |hours         |day length for current day
!!    daylmn(:)      |hours         |shortest daylength occurring during the
!!                                  |year
!!    dormhr(:)      |hour          |time threshold used to define dormant
!!                                  |period for plant (when daylength is within
!!                                  |the time specified by dormhr from the minimum
!!                                  |daylength for the area, the plant will go
!!                                  |dormant)
!!    icr(:)         |none          |sequence number of crop grown within the
!!                                  |current year
!!    idc(:)         |none          |crop/landcover category:
!!                                  |1 warm season annual legume
!!                                  |2 cold season annual legume
!!                                  |3 perennial legume
!!                                  |4 warm season annual
!!                                  |5 cold season annual
!!                                  |6 perennial
!!                                  |7 trees
!!    idorm_trc      |none          |dormancy status code:
!!                                  |0 land cover growing
!!                                  |1 land cover dormant
!!    idplt_trc(:,:) |none          |land cover code from crop.dat
!!    ihru           |none          |HRU number
!!    tnro           |none          |sequence number for year in rotation
!!    trc_bioms      |kg/ha         |land cover/crop biomass (dry weight)
!!    trc_bioyrms    |metric tons/ha|annual biomass (dry weight) in the terrace segment
!!    trc_phuacc     |none          |fraction of plant heat units accumulated
!!    trc_pltfrn     |none          |fraction of plant biomass that is nitrogen
!!    trc_pltfrp     |none          |fraction of plant biomass that is phosphorus
!!    trc_pltn       |kg N/ha       |amount of nitrogen in plant biomass
!!    trc_pltp       |kg P/ha       |amount of phosphorus in plant biomass
!!    tsol_fon(:)    |kg N/ha       |amount of nitrogen stored in the fresh
!!                                  |organic (residue) pool
!!    tsol_fop(:)    |kg P/ha       |amount of phosphorus stored in the fresh
!!                                  |organic (residue) pool
!!    tsol_rsd(:)    |kg/ha         |amount of organic matter in the soil
!!                                  |classified as residue
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    idorm_trc   |none          |dormancy status code:
!!                               |0 land cover growing
!!                               |1 land cover dormant
!!    laiday_trc  |m**2/m**2     |leaf area index
!!    trc_bioms   |kg/ha         |land cover/crop biomass (dry weight)
!!    trc_bioyrms |metric tons/ha|annual biomass (dry weight) in the terrace segment
!!    trc_phuacc  |none          |fraction of plant heat units accumulated
!!    trc_pltn    |kg N/ha       |amount of nitrogen in plant biomass
!!    trc_pltp    |kg P/ha       |amount of phosphorus in plant biomass
!!    tsol_fon(:) |kg N/ha       |amount of nitrogen stored in the fresh
!!                               |organic (residue) pool
!!    tsol_fop(:) |kg P/ha       |amount of phosphorus stored in the fresh
!!                               |organic (residue) pool
!!    tsol_rsd(:) |kg/ha         |amount of organic matter in the soil
!!                               |classified as residue
!!    tstrsw      |none          |fraction of potential plant growth achieved
!!                               |on the day where the reduction is caused by
!!                               |water stress
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    j           |none          |HRU number
!!    resnew      |
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Max

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm
      use climate_parms
      use basin_module
      use hydrograph_module
      use jrw_datalib_module
      implicit none
      real :: resnew
      integer :: j, iwgn

      j = ihru
      iwgn = wst(ob(hru(j)%obj_no)%wst)%wco%wgn


!! check for beginning of dormant season
      if (idorm_trc == 0 .and.                                    &
     &           wgn_pms(iwgn)%daylth-dormhr(j)<wgn_pms(iwgn)%daylmn)     then

        select case (pldb(idplt_trc(tnro,ticr))%idc)

          !! beginning of forest dormant period
          case (7)
            idorm_trc = 1
            resnew = 0.
            resnew = trc_bioms * pcom(j)%plg(idplt_trc(tnro,ticr))%bio_leaf
            tsol_rsd(1) = tsol_rsd(1) + resnew
            tsol_rsd(1) = Max(tsol_rsd(1),0.)
            tsol_fon(1) = resnew * trc_pltn / trc_bioms + tsol_fon(1)
            tsol_fop(1) = resnew * trc_pltp / trc_bioms + tsol_fop(1)
            trc_biohv(tnro,ticr) = trc_bioms + trc_biohv(tnro,ticr)
            trc_bioyrms = trc_bioyrms + trc_bioms / 1000.
            trc_bioms = trc_bioms * (1. -                               &
     &                              pcom(j)%plg(idplt_trc(tnro,ticr))%bio_leaf)
            trc_pltn = trc_pltn - resnew * trc_pltn / trc_bioms
            trc_pltp = trc_pltp - resnew * trc_pltp / trc_bioms
            tstrsw = 1.
            laiday_trc = pldb(idplt_trc(tnro,ticr))%alai_min
            trc_phuacc = 0.

          !! beginning of perennial (pasture/alfalfa) dormant period
          case (3, 6)
            idorm_trc = 1
            resnew = 0.
            resnew = pldb(idplt_trc(tnro,ticr))%bm_dieoff * trc_bioms
            tsol_rsd(1) = tsol_rsd(1) + resnew
            tsol_rsd(1) = Max(tsol_rsd(1),0.)
            tsol_fon(1) = tsol_fon(1) + pldb(idplt_trc(tnro,ticr))%bm_dieoff &
     &                                                       * trc_pltn
            tsol_fop(1) = tsol_fop(1) + pldb(idplt_trc(tnro,ticr))%bm_dieoff &
     &                                                       * trc_pltp
            trc_biohv(tnro,ticr) = trc_bioms *                          &
     &            pldb(idplt_trc(tnro,ticr))%bm_dieoff + trc_biohv(tnro,ticr)
            trc_bioyrms = trc_bioyrms + trc_bioms *                     &
     &                     pldb(idplt_trc(tnro,ticr))%bm_dieoff / 1000.
            trc_bioms = (1. - pldb(idplt_trc(tnro,ticr))%bm_dieoff) *        &
     &                                                         trc_bioms
            trc_pltn = (1. - pldb(idplt_trc(tnro,ticr))%bm_dieoff) * trc_pltn
            trc_pltp = (1. - pldb(idplt_trc(tnro,ticr))%bm_dieoff) * trc_pltp
            tstrsw = 1.
            laiday_trc = pldb(idplt_trc(tnro,ticr))%alai_min
            trc_phuacc = 0.

          !! beginning of cool season annual dormant period
          case (2, 5)
            if (trc_phuacc < 0.75) then
              idorm_trc = 1
              tstrsw = 1.
            end if

        end select
     end if

!! check if end of dormant period
     iwgn = wst(iwst)%wco%wgn
      if (idorm_trc == 1 .and. wgn_pms(iwgn)%daylth-dormhr(j) >= wgn_pms(iwgn)%daylmn) &
     &                                                              then

          select case (pldb(idplt_trc(tnro,ticr))%idc)
          
            !! end of perennial dormant period
            case (3, 6, 7)
              idorm_trc = 0

            !! end of cool season annual dormant period
            case (2, 5)
              idorm_trc = 0

          end select

        end if

      return
      end

      subroutine etact_trc

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine calculates potential plant transpiration and potential 
!!    and actual soil evaporation using Penman-Monteith method. NO3 movement 
!!    into surface soil layer due to evaporation is also calculated.


!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name         |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    esco(:)      |none          |soil evaporation compensation factor
!!    ihru         |none          |HRU number
!!    ipet         |none          |code for potential ET method
!!                                |0 Priestley-Taylor method
!!                                |1 Penman/Monteith method
!!                                |2 Hargreaves method
!!    laiday_trc   |m**2/m**2     |leaf area index on terrace segment
!!    pet_day      |mm H2O        |potential evapotranspiration on current day
!!                                |in HRU
!!    sol_fc(:,:)  |mm H2O        |amount of water available to plants in soil
!!                                |layer at field capacity (fc - wp water)
!!    sol_nly(:)   |none          |number of soil layers in profile
!!    sol_z(:,:)   |mm            |depth to bottom of soil layer
!!    tmpav(:)     |deg C         |average air temperature on current day for
!!                                |HRU
!!    tsol_cov     |kg/ha         |amount of residue on soil surface
!!    tsol_no3(:)  |kg N/ha       |amount of nitrogen stored in the nitrate
!!                                |pool 
!!    tsol_st(:)   |mm H2O        |amount of water stored in the soil layer on
!!                                |current day
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name         |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    sno3up       |kg N/ha       |amount of nitrate moving upward in the soil
!!                                |profile in watershed
!!    tcanev       |mm H2O        |amount of water evaporated from canopy
!!                                |storage
!!    tep_max      |mm H2O        |maximum amount of transpiration (plant et)
!!                                |that can occur on current day in terrace segment
!!    tes_day      |mm H2O        |actual amount of evaporation (soil et) that
!!                                |occurs on day in terrace segment
!!    tsol_st(:)   |mm H2O        |amount of water stored in the soil layer on
!!                                |current day
!!    tsol_sw      |mm H2O        |amount of water stored in the soil profile
!!                                |on current day
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name         |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    cej          |
!!    dep          |mm            |soil depth from which evaporation will occur
!!                                |in current soil layer
!!    eaj          |none          |weighting factor to adjust PET for impact of
!!                                |plant cover
!!    effnup       |
!!    eos1         |none          |variable to hold intermediate calculation
!!                                |result
!!    eosl         |mm H2O        |maximum amount of evaporation that can occur
!!                                |from soil profile
!!    esd          |mm            |maximum soil depth from which evaporation
!!                                |is allowed to occur
!!    esleft       |mm H2O        |potenial soil evap that is still available
!!    etco         |
!!    evz          |
!!    evzp         |
!!    ib           |none          |counter
!!    j            |none          |HRU number
!!    ly           |none          |counter
!!    no3up        |kg N/ha       |amount of nitrate moving upward in profile
!!    pet          |mm H2O        |amount of PET remaining after water stored
!!                                |in canopy is evaporated
!!    sev          |mm H2O        |amount of evaporation from soil layer
!!    sumsnoeb     |mm H2O        |amount of snow in elevation bands whose air
!!                                |temperature is greater than 0 degrees C
!!    tes_max      |mm H2O        |maximum amount of evaporation (soil et) that can
!!                                |occur on current day in terrace segment
!!    xx           |none          |variable to hold intermediate calculation 
!!                                |result
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Exp, Min, Max
!!    SWAT: Expo

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      implicit none
      integer :: j, ib, ly
!!    real, parameter :: esd = 500., etco = 0.80, effnup = 0.1
      real :: esd, etco, effnup
      real :: no3up, tes_max, eos1, xx, cej, eaj, pet, esleft, yy
      real :: evzp, eosl, dep, evz, sev, et_trc
      real :: tk, pb, gma, xl, ea, ed, dlt, ralb1
      real :: rbo, rto, rn, uzz, zz, zom, zov, rv, fvpd
      real :: rc, rho, rout, d, chz, gsi_adj, ee, expo

      j = 0
      j = ihru
      pet = 0.
      pet = pet_day
!!    added statements for test of real statement above
	esd = 500.
	etco = 0.80
	effnup = 0.1
      tep_max = 0.
      tes_max = 0.

      tk = 0.
      tk = tmpav(j) + 273.15

      !! calculate mean barometric pressure
      pb = 0.
      pb = 101.3 - hru(j)%topo%elev *                               &
     &                       (0.01152 - 0.544e-6 * hru(j)%topo%elev)

      !! calculate latent heat of vaporization
      xl = 0.
      xl = 2.501 - 2.361e-3 * tmpav(j)

      !! calculate psychrometric constant
      gma = 0.
      gma = 1.013e-3 * pb / (0.622 * xl)

      !! calculate saturation vapor pressure, actual vapor pressure and
      !! vapor pressure deficit
      ea = 0.
      ed = 0.
      vpd = 0.
      ea = Ee(tmpav(j))
      ed = ea * rhd(j)
      vpd = ea - ed

      !!calculate the slope of the saturation vapor pressure curve
      dlt = 0.
      dlt = 4098. * ea / (tmpav(j) + 237.3)**2
	
      pet = pet - canstor_trc
      if (pet < 0.) then
        canstor_trc = -pet
        tcanev = pet_day
        pet = 0.
        tep_max = 0.
        tes_max = 0.
      else
        tcanev = canstor_trc
        canstor_trc = 0.
      endif

      if (pet > 1.0e-6) then

!! evaporate canopy storage first
!! canopy storage is calculated by the model only if the Green & Ampt
!! method is used to calculate surface runoff. The curve number methods
!! take canopy effects into account in the equations. For either of the
!! CN methods, canstor will always equal zero.

        !! compute potential plant evap for methods other that Penman-Monteith
        if (bsn_cc%pet /= 1) then
          if (laiday_trc <= 3.0) then
            tep_max = laiday_trc * pet / 3.
          else
            tep_max = pet
          end if
          else
!! PENMAN-MONTEITH POTENTIAL EVAPOTRANSPIRATION METHOD
       !! net radiation
         !! calculate net short-wave radiation for max plant ET
          ralb1 = 0.
          ralb1 = hru_ra(j) * (1.0 - albday) 

         !! calculate net long-wave radiation
          !! net emissivity  equation 2.2.20 in SWAT manual
          rbo = 0.
          rbo = -(0.34 - 0.139 * Sqrt(ed))

          !! cloud cover factor equation 2.2.19
          rto = 0.
            if (hru_rmx(j) < 1.e-4) then
		    rto = 0.
            else
              rto = 0.9 * (hru_ra(j) / hru_rmx(j)) + 0.1
            end if

          !! net long-wave radiation equation 2.2.21
          rout = 0.
          rout = rbo * rto * 4.9e-9 * (tk**4)

          !! calculate net radiation
          rn = 0.
          rn = ralb1 + rout
       !! net radiation

          rho = 0.
          rho = 1710. - 6.85 * tmpav(j)

          if (u10(j) < 0.01) u10(j) = 0.01
 
        !! maximum plant ET
          if (igro_trc <= 0) then
            tep_max = 0.0
          else
            !! determine wind speed and height of wind speed measurement
            !! adjust to 100 cm (1 m) above canopy if necessary
            uzz = 0.
            zz = 0.
            if (trc_cht <= 1.0) then
              zz = 170.0
            else
              zz = trc_cht * 100. + 100.
            end if
            uzz = u10(j) * (zz/1000.)**0.2

            !! calculate canopy height in cm
            chz = 0.
            if (trc_cht < 0.01) then
              chz = 1.
            else
              chz = trc_cht * 100.
            end if

            !! calculate roughness length for momentum transfer
            zom = 0.
            if (chz <= 200.) then
              zom = 0.123 * chz
            else
              zom = 0.058 * chz**1.19
            end if
 
            !! calculate roughness length for vapor transfer
            zov = 0.
            zov = 0.1 * zom

            !! calculate zero-plane displacement of wind profile
            d = 0.
            d = 0.667 * chz

            !! calculate aerodynamic resistance
            rv = Log((zz - d) / zom) * Log((zz - d) / zov)
            rv = rv / ((0.41)**2 * uzz)

            !! adjust stomatal conductivity for low vapor pressure
            !! this adjustment will lower maximum plant ET for plants
            !! sensitive to very low vapor pressure
            xx = 0.
            fvpd = 0.
            xx = vpd - 1.
            if (xx > 0.0) then
              fvpd = Max(0.1,1.0 - plcp(idplt_trc(tnro,ticr))%vpd2 * xx)
            else
              fvpd = 1.0
            end if
            gsi_adj = 0.
            gsi_adj = pldb(idplt_trc(tnro,ticr))%gsi * fvpd
            
            !! calculate canopy resistance
            rc = 0.
            rc = 1. / gsi_adj                    !single leaf resistance
            rc = rc / (0.5 * (laiday_trc + 0.01)                        &
     &                    * (1.4 - 0.4 * hru(j)%parms%co2 / 330.))

            !! calculate maximum plant ET
           tep_max = (dlt * rn + gma * rho * vpd / rv) /                &
     &                               (xl * (dlt + gma * (1. + rc / rv)))
            if (tep_max < 0.) tep_max = 0.
            tep_max = Min(tep_max, pet_day)
          end if
        end if

        !! compute potential soil evaporation
        cej = -5.e-5
        eaj = 0.
        tes_max = 0.
        eos1 = 0.
        if (sno_hru(j) >= 0.5) then
          eaj = 0.5
        else
          eaj = Exp(cej * (tsol_cov+ 0.1))
        end if
        tes_max = pet * eaj
        eos1 = pet / (tes_max + tep_max + 1.e-10)
        eos1 = tes_max * eos1
        tes_max = Min(tes_max, eos1)
        tes_max = Max(tes_max, 0.)

        !! make sure maximum plant and soil ET doesn't exceed potential ET
        if (pet_day < tes_max + tep_max) then
          tes_max = pet_day - tep_max
          if (pet < tes_max + tep_max) then
            tes_max = pet * tes_max / (tes_max + tep_max)
            tep_max = pet * tep_max / (tes_max + tep_max)
          end if
        end if         
        
        !! adjust maximum potential soil ET considering the stored water in terrace
        et_trc = 0.
        if (trc_vol(j) > 1.e-4 .and. tflag == 3) then
            if (trc_vol(j) < trc_vmax) then
                xx = 0.
                yy = 0.
                yy = tes_max * (trc_vol(j) / trc_vmax) ** .5
                xx = yy * trc_ha * 10.
                if (xx < trc_vol(j)) then
                  trc_vol(j) = trc_vol(j) - xx
                  xx = 0.
                else
                  trc_vol(j) = 0
                  xx = xx - trc_vol(j)
                end if
                et_trc = yy - xx / trc_ha / 10.
            else
                xx = 0.
                yy = 0.
                yy = tes_max
                xx = tes_max * trc_ha * 10.
                if (xx < trc_vol(j)) then
                  trc_vol(j) = trc_vol(j) - xx
                  xx = 0.
                else
                  trc_vol(j) = 0
                  xx = xx - trc_vol(j)
                end if
                et_trc = yy - xx / trc_ha / 10.
            end if
        end if
        
        !! initialize soil evaporation variables
        esleft = 0.
        esleft = tes_max - et_trc
        
        !! compute sublimation
        if (esleft > snoev) then
          esleft = esleft - snoev
        else
          esleft = 0.
        endif

!! take soil evap from each soil layer
      evzp = 0.
      eosl = 0.
      eosl = esleft
      do ly = 1, soil(j)%nly

        !! depth exceeds max depth for soil evap (esd)
        dep = 0.
        if (ly == 1) then
          dep = soil(j)%phys(1)%d
        else
          dep = soil(j)%phys(ly-1)%d
        endif
        
        if (dep < esd) then
          !! calculate evaporation from soil layer
          evz = 0.
          sev = 0.
          xx = 0.
          evz = eosl * soil(j)%phys(ly)%d / (soil(j)%phys(ly)%d + Exp(2.374 -         &
     &       .00713 * soil(j)%phys(ly)%d))
          sev = evz - evzp * hru(j)%hyd%esco
          evzp = evz
          if (tsol_st(ly) < soil(j)%phys(ly)%fc) then
            xx =  2.5 * (tsol_st(ly) - soil(j)%phys(ly)%fc) / soil(j)%phys(ly)%fc
            sev = sev * Expo(xx)
          end if
          sev = Min(sev, tsol_st(ly) * etco)

          if (sev < 0.) sev = 0.
          if (sev > esleft) sev = esleft

          !! adjust soil storage, potential evap
          if (tsol_st(ly) > sev) then
            esleft = esleft - sev
            tsol_st(ly) = Max(1.e-6, tsol_st(ly) - sev)
          else
            esleft = esleft - tsol_st(ly)
            tsol_st(ly) = 0.
          endif
        endif

        !! compute no3 flux from layer 2 to 1 by soil evaporation
        if (ly == 2) then
          no3up = 0.
          no3up = effnup * sev * tsol_no3(2) / (tsol_st(2) + 1.e-6)
          no3up = Min(no3up, tsol_no3(2))
          sno3up = sno3up + no3up * hru_dafr(j) * trc_ha / (hru(j)%km*100)
          tsol_no3(2) = tsol_no3(2) - no3up
          tsol_no3(1) = tsol_no3(1) + no3up
        endif

        end do
        
      tes_day = tes_max - esleft
      if (tes_day < 0.) then
        tes_day = 0.
      end if

      !! update total soil water content
      tsol_sw = 0.
      do ly = 1, soil(j)%nly
        tsol_sw = tsol_sw + tsol_st(ly)
      end do

      end if

      return
      end

      subroutine fert_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine applies N and P specified by date and amount in 
!!    the terrace segment management files (.mtu, .mtr, .mtb) 

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name          |units         |definition                  
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    time%yrs         |none          |current year of simulation
!!    fminn(:)      |kg minN/kg frt|fraction of fertilizer that is mineral N
!!                                 |(NO3 + NH4)
!!    fminp(:)      |kg minP/kg frt|fraction of fertilizer that is mineral P
!!    fnh3n(:)      |kgNH3-N/kgminN|fraction of mineral N in fertilizer that
!!                                 |is NH3-N
!!    forgn(:)      |kg orgN/kg frt|fraction of fertilizer that is organic N
!!    forgp(:)      |kg orgP/kg frt|fraction of fertilizer that is organic P
!!    hru_dafr(:)   |km2/km2       |fraction of watershed area in HRU
!!    ihru          |none          |HRU number
!!    laiday_trc    |m**2/m**2     |leaf area index
!!    pco%nyskip        |none          |number of years to not print/summarize output
!!    sol_bd(1,:)   |Mg/m^3        |bulk density of top soil layer in HRU
!!    sol_z(:,:)    |mm            |depth to bottom of soil layer
!!    tfrt_kg(:,:)  |kg/ha         |amount of fertilizer applied to terrace segment
!!    tfrt_surface(:,:)|none       |fraction of fertilizer which is applied to
!!                                 |the top 10 mm of soil (the remaining
!!                                 |fraction is applied to first soil layer)
!!    tnfert        |none          |sequence number of fertilizer application
!!                                 |within the year
!!    tnro          |none          |sequence number of year in rotation
!!    trc_fertn     |kg N/ha       |total amount of nitrogen applied to soil
!!                                 |in terrace segment on day
!!    trc_fertp     |kg P/ha       |total amount of phosphorus applied to soil
!!                                 |in terrace segment on day
!!    tsol_aorgn(:) |kg N/ha       |amount of nitrogen stored in the active
!!                                 |organic (humic) nitrogen pool
!!    tsol_fon(:)   |kg N/ha       |amount of nitrogen stored in the fresh
!!                                 |organic (residue) pool
!!    tsol_fop(:)   |kg P/ha       |amount of phosphorus stored in the fresh
!!                                 |organic (residue) pool
!!    tsol_nh3(:)   |kg N/ha       |amount of nitrogen stored in the ammonium
!!                                 |pool in soil layer
!!    tsol_no3(:)   |kg N/ha       |amount of nitrogen stored in the nitrate pool
!!                                 |in soil layer
!!    tsol_orgp(:)  |kg P/ha       |amount of phosphorus stored in the organic
!!                                 |P pool
!!    tsol_solp(:)  |kg P/ha       |amount of inorganic phosohorus stored in
!!                                 |solution
!!    wshd_fminp    |kg P/ha       |average annual amount of mineral P applied
!!                                 |in watershed
!!    wshd_fnh3     |kg N/ha       |average annual amount of NH3-N applied in
!!                                 |watershed
!!    wshd_fno3     |kg N/ha       |average annual amount of NO3-N applied in
!!                                 |watershed
!!    wshd_ftotn    |kg N/ha       |average annual amount of N (mineral & 
!!                                 |organic) applied in watershed
!!    wshd_ftotp    |kg P/ha       |average annual amount of P (mineral &
!!                                 |organic) applied in watershed
!!    wshd_orgn     |kg N/ha       |average annual amount of organic N applied
!!                                 |in watershed
!!    wshd_orgp     |kg P/ha       |average annual amount of organic P applied
!!                                 |in watershed
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name          |units        |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tnfert        |none         |sequence number of fertilizer application
!!                                |within the year
!!    trc_fertn     |kg N/ha      |total amount of nitrogen applied to soil
!!                                |in terrace segment on day
!!    trc_fertp     |kg P/ha      |total amount of phosphorus applied to soil
!!                                |in terrace segment on day
!!    tsol_aorgn(:) |kg N/ha      |amount of nitrogen stored in the active
!!                                |organic (humic) nitrogen pool
!!    tsol_fon(:)   |kg N/ha      |amount of nitrogen stored in the fresh
!!                                |organic (residue) pool
!!    tsol_fop(:)   |kg P/ha      |amount of phosphorus stored in the fresh
!!                                |organic (residue) pool
!!    tsol_nh3(:)   |kg N/ha      |amount of nitrogen stored in the ammonium
!!                                |pool in soil layer
!!    tsol_no3(:)   |kg N/ha      |amount of nitrogen stored in the nitrate pool
!!                                |in soil layer
!!    tsol_orgp(:)  |kg P/ha      |amount of phosphorus stored in the organic
!!                                |P pool
!!    tsol_solp(:)  |kg P/ha      |amount of inorganic phosohorus stored in
!!                                |solution
!!    wshd_fminp    |kg P/ha      |average annual amount of mineral P applied
!!                                |in watershed
!!    wshd_fnh3     |kg N/ha      |average annual amount of NH3-N applied in
!!                                |watershed
!!    wshd_fno3     |kg N/ha      |average annual amount of NO3-N applied in
!!                                |watershed
!!    wshd_ftotn    |kg N/ha      |average annual amount of N (mineral & 
!!                                |organic) applied in watershed
!!    wshd_ftotp    |kg P/ha      |average annual amount of P (mineral &
!!                                |organic) applied in watershed
!!    wshd_orgn     |kg N/ha      |average annual amount of organic N applied
!!                                |in watershed
!!    wshd_orgp     |kg P/ha      |average annual amount of organic P applied
!!                                |in watershed
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name         |units        |definition                  
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    frt_t        |
!!    gc           |
!!    gc1          |
!!    j            |none         |HRU number
!!    l            |none         |counter (soil layer #)
!!    rtof         |none         |weighting factor used to partition the 
!!                               |organic N & P content of the fertilizer
!!                               |between the fresh organic and the active 
!!                               |organic pools
!!    xx           |none         |fraction of fertilizer applied to layer
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    SWAT: Erfc

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      implicit none
      real, parameter :: rtof=0.5
      integer :: j, l, ifrt
      real :: xx, gc, gc1, swf, frt_t

      j = 0
      j = ihru

      ifrt = 0
      ifrt = trc_ifrttyp(tnro,tnfert)

      do l = 1, 2
        xx = 0.
        if (l == 1) then
          xx = tfrt_surface(tnro,tnfert)
        else
          xx = 1. - tfrt_surface(tnro,tnfert)
        endif

        tsol_no3(l) = tsol_no3(l) + xx * tfrt_kg(tnro,tnfert) *         &
     &      (1. - fertdb(ifrt)%fnh3n) * fertdb(ifrt)%fminn

        if (bsn_cc%cswat == 0) then
        tsol_fon(l) = tsol_fon(l) + rtof * xx *                         &
     &     tfrt_kg(tnro,tnfert) * fertdb(ifrt)%forgn                    
	  tsol_aorgn(l) = tsol_aorgn(l) + (1. - rtof) * xx *              &
     &     tfrt_kg(tnro,tnfert) * fertdb(ifrt)%forgn
        tsol_fop(l) = tsol_fop(l) + rtof * xx *                         &
     &      tfrt_kg(tnro,tnfert) * fertdb(ifrt)%forgp
        tsol_orgp(l) = tsol_orgp(l) + (1. - rtof) * xx *                &
     &      tfrt_kg(tnro,tnfert) * fertdb(ifrt)%forgp
	  else
	  tsol_mc(l) = tsol_mc(l) + xx * tfrt_kg(tnro,tnfert) *           &
     &		fertdb(ifrt)%forgn * 10.
	  tsol_mn(l) = tsol_mn(l) + xx * tfrt_kg(tnro,tnfert) *           &
     &		fertdb(ifrt)%forgn
	  tsol_mp(l) = tsol_mp(l) + xx * tfrt_kg(tnro,tnfert) *           &
     &		fertdb(ifrt)%forgp
	  end if

        tsol_nh3(l) = tsol_nh3(l) + xx * tfrt_kg(tnro,tnfert) *         &
     &      fertdb(ifrt)%fnh3n * fertdb(ifrt)%fminn

        tsol_solp(l) = tsol_solp(l) + xx * tfrt_kg(tnro,tnfert) *       &
     &      fertdb(ifrt)%fminp

      end do 

!! summary calculations
      trc_fertn = trc_fertn + (tfrt_kg(tnro,tnfert) + cfertn) *         &
     &   (fertdb(ifrt)%fminn + fertdb(ifrt)%forgn)

      trc_fertp = trc_fertp + (tfrt_kg(tnro,tnfert) + cfertp) *         &
     &   (fertdb(ifrt)%fminp + fertdb(ifrt)%forgp)

      trc_tfertn = trc_tfertn + trc_fertn
      trc_tfertp = trc_tfertp + trc_fertp

      if (time%yrs > pco%nyskip) then
!      wshd_ftotn = wshd_ftotn + tfrt_kg(tnro,tnfert) * hru_dafr(j)      &
!     &   * (fertdb(ifrt)%fminn + fertdb(ifrt)%forgn) * trc_ha / (hru(j)%km*100)
!
!      wshd_forgn = wshd_forgn + tfrt_kg(tnro,tnfert) * hru_dafr(j)      &
!     &   * fertdb(ifrt)%forgn * trc_ha / (hru(j)%km*100)
!
!      wshd_fno3 = wshd_fno3 + tfrt_kg(tnro,tnfert) * hru_dafr(j) *      &
!     &   fertdb(ifrt)%fminn * (1. - fertdb(ifrt)%fnh3n) * trc_ha / (hru(j)%km*100)

!      wshd_fnh3 = wshd_fnh3 + tfrt_kg(tnro,tnfert) * hru_dafr(j) *      &
!     &   fertdb(ifrt)%fminn * fertdb(ifrt)%fnh3n * trc_ha / (hru(j)%km*100)

!      wshd_ftotp = wshd_ftotp + tfrt_kg(tnro,tnfert) * hru_dafr(j)      &
!     &   * (fertdb(ifrt)%fminp + fertdb(ifrt)%forgp) * trc_ha / (hru(j)%km*100)

!      wshd_fminp = wshd_fminp + tfrt_kg(tnro,tnfert) * hru_dafr(j)      &
!     &   * fertdb(ifrt)%fminp * trc_ha / (hru(j)%km*100)

!      wshd_forgp = wshd_forgp + tfrt_kg(tnro,tnfert) * hru_dafr(j)      &
!     &   * fertdb(ifrt)%forgp * trc_ha / (hru(j)%km*100)
        hru_fn = hru_fn + tfrt_kg(tnro,tnfert) * (fertdb(ifrt)%fminn +         &
     &                              fertdb(ifrt)%forgn) * trc_ha / (hru(j)%km*100)   !! for terraced hru balance test
        hru_fp = hru_fp + tfrt_kg(tnro,tnfert) * (fertdb(ifrt)%fminp +         &
     &                              fertdb(ifrt)%forgp) * trc_ha / (hru(j)%km*100)   !! for terraced hru balance test

      end if
      
!! increase fertilizer sequence number by one
      tnfert = tnfert + 1

      return
      end

      subroutine grow_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine adjusts plant biomass, leaf area index, and canopy height
!!    taking into account the effect of water, temperature and nutrient stresses
!!    on the terrace segment plant

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units            |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    bio_e(:)    |(kg/ha)/(MJ/m**2)|biomass-energy ratio
!!                                  |The potential (unstressed) growth rate per
!!                                  |unit of intercepted photosynthetically
!!                                  |active radiation.
!!    blai(:)     |none             |maximum (potential) leaf area index
!!    chtmx(:)    |m                |maximum canopy height
!!    co2(:)      |ppmv             |CO2 concentration
!!    time%yrs       |none             |current year of simulation
!!    dlai(:)     |none             |fraction of growing season when leaf
!!                                  |area declines
!!    hru_dafr(:) |km**2/km**2      |fraction of watershed area in HRU
!!    hru_ra(:)   |MJ/m^2           |solar radiation for the day in HRU
!!    hvsti(:)    |(kg/ha)/(kg/ha)  |harvest index: crop yield/aboveground
!!                                  |biomass
!!    idc(:)      |none             |crop/landcover category:
!!                                  |1 warm season annual legume
!!                                  |2 cold season annual legume
!!                                  |3 perennial legume
!!                                  |4 warm season annual
!!                                  |5 cold season annual
!!                                  |6 perennial
!!                                  |7 trees
!!    idorm_trc   |none             |dormancy status code:
!!                                  |0 land cover growing (not dormant)
!!                                  |1 land cover dormant
!!    idplocallt_trc(:,:)|none           |land cover code from crop.dat
!!    ihru        |none             |HRU number
!!    laiday_trc  |m**2/m**2        |leaf area index
!!    leaf1(:)    |none             |1st shape parlocalameter for leaf area
!!                                  |development equation.
!!    leaf2(:)    |none             |2nd shape parlocalameter for leaf area
!!                                  |development equation.
!!    pco%nyskip      |none             |number of years output summarization
!!                                  |and printing is skipped
!!    pet_day     |mm H2O           |potential evapotranspiration on current day
!!                                  |in HRU
!!    t_base(:)   |deg C            |minimum temperature for plant growth
!!    tauto_nstrs |none             |nitrogen stress factor which triggers
!!                                  |auto fertilization
!!    tbio_targ(:,:)|kg/ha          |biomass target
!!    tep_day     |mm H2O           |actual amount of transpiration that occurs
!!                                  |on day in terrace segment
!!    tes_day     |mm H2O           |actual amount of evaporation (soil et) that
!!                                  |occurs on day in terrace segment
!!    ticr        |none             |sequence number of crop grown within the
!!                                  |current year
!!    tmpav(:)    |deg C            |average air temperature on current day in 
!!                                  |HRU
!!    tnro        |none             |sequence number of year in rotation
!!    trc_bioms   |kg/ha            |land cover/crop biomass (dry weight)
!!    trc_laimxfr |
!!    trc_laiyrmx |none             |maximum leaf area index for the year in the
!!                                  |terrace segment
!!    trc_olai    |
!!    trc_phuacc  |none             |fraction of plant heat units accumulated
!!    trc_phuplt(:,:)|heat units       |total number of heat units to bring plant
!!                                  |to maturity
!!    trc_pltet   |mm H2O           |actual ET simulated during life of plant
!!    trc_pltpet  |mm H2O           |potential ET simulated during life of plant
!!    tstrsn      |none             |fraction of potential plant growth achieved 
!!                                  |on the day where the reduction is caused by
!!                                  |nitrogen stress
!!    tstrsp      |none             |fraction of potential plant growth achieved
!!                                  |on the day where the reduction is caused by
!!                                  |phosphorus stress
!!    tstrstmp    |none             |fraction of potential plant growth achieved
!!                                  |on the day in terrace segment where the reduction is
!!                                  |caused by temperature stress
!!    tstrsw      |none             |fraction of potential plant growth achieved
!!                                  |on the day where the reduction is caused by
!!                                  |water stress
!!    vpd         |kPa              |vapor pressure deficit
!!    wac21(:)    |none             |1st shape parlocalameter for radiation use
!!                                  |efficiency equation.
!!    wac22(:)    |none             |2nd shape parlocalameter for radiation use
!!                                  |efficiency equation.
!!    wavp(:)     |none             |Rate of decline in radiation use efficiency
!!                                  |as a function of vapor pressure deficit
!!    wshd_nstrs  |stress units     |average annual number of nitrogen stress
!!                                  |units in watershed
!!    wshd_pstrs  |stress units     |average annual number of phosphorus stress
!!                                  |units in watershed
!!    wshd_tstrs  |stress units     |average annual number of temperature stress
!!                                  |units in watershed
!!    wshd_wstrs  |stress units     |average annual number of water stress units
!!                                  |in watershed
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    thvstiadj   |none          |harvest index adjusted for water stress
!!    trc_bioday  |kg            |biomass generated on current day in terrace segment
!!    trc_bioms   |kg/ha         |land cover/crop biomass (dry weight)
!!    trc_cht     |m             |canopy height
!!    trc_laimxfr |
!!    trc_laiyrmx |none          |maximum leaf area index for the year in the
!!                               |terrace segment
!!    trc_olai    |
!!    trc_phuacc  |none          |fraction of plant heat units accumulated
!!    trc_pltet   |mm H2O        |actual ET simulated during life of plant
!!    trc_pltpet  |mm H2O        |potential ET simulated during life of plant
!!    trc_rwt     |none          |fraction of total plant biomass that is
!!                               |in roots
!!    wshd_nstrs  |stress units  |average annual number of nitrogen stress
!!                               |units in watershed
!!    wshd_pstrs  |stress units  |average annual number of phosphorus stress
!!                               |units in watershed
!!    wshd_tstrs  |stress units  |average annual number of temperature stress
!!                               |units in watershed
!!    wshd_wstrs  |stress units  |average annual number of water stress units
!!                               |in watershed
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units            |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    beadj       |(kg/ha)/(MJ/m**2)|radiation-use efficiency for a given CO2
!!                                  |concentration
!!    delg        |
!!    deltalai    |
!!    f           |none             |fraction of plant's maximum leaf area index
!!                                  |corresponding to a given fraction of
!!                                  |potential heat units for plant
!!    ff          |
!!    j           |none             |HRU number
!!    laimax      |none             |maximum leaf area index
!!    parlocal         |MJ/m^2           |photosynthetically active radiation
!!    reg         |none             |stress factor that most limits plant growth
!!                                  |on current day
!!    ruedecl     |none             |decline in radiation use efficiency for the
!!                                  |plant
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Exp, Max, Min, Sqrt
!!    SWAT: tstr_trc, nup_trc, npup_trc, anfert_trc

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      implicit none
      integer :: j, idplocal
      real :: delg, parlocal, ruedecl, beadj, reg, f, ff, deltalai
      real :: laimax, rto, biomxyr, min

      j = 0
      j = ihru


        !! plant will not undergo stress if dormant
        if (idorm_trc == 1) return
        idplocal = idplt_trc(tnro,ticr)

        !! update accumulated heat units for the plant
        delg = 0.
        if (trc_phuplt(tnro,ticr) > 0.1) then
          delg = (tmpav(j) - pldb(idplocal)%t_base) / trc_phuplt(tnro,ticr)
        end if
        if (delg < 0.) delg = 0.
        trc_phuacc = trc_phuacc + delg  


        !! if plant hasn't reached maturity
        if (trc_phuacc <= 1.) then

         !! compute temperature stress - tstrstmp   
          call tstr_trc

         !! calculate optimal biomass

          !! calculate photosynthetically active radiation
          parlocal = 0.
          parlocal = .5 * hru_ra(j) * (1. - Exp(-pldb(idplocal)%ext_coef *             &
     &          (laiday_trc + .05)))

          !! adjust radiation-use efficiency for CO2
          beadj = 0.
          if (hru(j)%parms%co2 > 330.) then
            beadj = 100. * hru(j)%parms%co2 / (hru(j)%parms%co2 +         &
     &              Exp(plcp(idplocal)%ruc1 - hru(j)%parms%co2 * plcp(idplocal)%ruc2))     
          else
            beadj = pldb(idplocal)%bio_e
          end if

          !! adjust radiation-use efficiency for vapor pressure deficit
          !!assumes vapor pressure threshold of 1.0 kPa
          if (vpd > 1.0) then
            ruedecl = 0.
            ruedecl = vpd - 1.0
            beadj = beadj - pldb(idplocal)%wavp * ruedecl
            beadj = Max(beadj, 0.27 * pldb(idplocal)%bio_e)
          end if

          beadj = pldb(idplocal)%bio_e

          trc_bioday = beadj * parlocal
          if (trc_bioday < 0.) trc_bioday = 0.

          !! calculate plant uptake of nitrogen and phosphorus
          call nup_trc
          call npup_trc

          !! auto fertilization-nitrogen demand (non-legumes only)
          select case (pldb(idplocal)%idc)
            case (4, 5, 6, 7)
            if (tauto_nstrs > 0.) call anfert_trc
          end select

          !! reduce predicted biomass due to stress on plant
          reg = 0.
          reg = Min(tstrsw, tstrstmp, tstrsn, tstrsp, tstrsa)
          if (reg < 0.) reg = 0.
          if (reg > 1.) reg = 1.

          if (trc_biotarg(tnro,ticr) > 1.e-2) then
          trc_bioday = trc_bioday * (trc_biotarg(tnro,ticr) - trc_bioms)&
     &                           / trc_biotarg(tnro,ticr)
            reg = 1.
          end if
 
          trc_bioms = trc_bioms + trc_bioday * reg
          if (pldb(idplocal)%idc == 7) then
            if (pldb(idplocal)%mat_yrs > 0) then
              rto = float(trc_cyrmat) / float(pldb(idplocal)%mat_yrs)
              biomxyr = rto * pldb(idplocal)%bmx_peren * 1000.  !t/ha -> kg/ha
              trc_bioms = Min (trc_bioms, biomxyr)
            else
              rto = 1.
            end if
          end if

          trc_bioms = Max(trc_bioms,0.)
          
      !! calculate fraction of total biomass that is in the roots
      trc_rwt = pldb(idplocal)%rsr1 - pldb(idplocal)%rsr2 * trc_phuacc
          f = 0.
          ff = 0.
          f = trc_phuacc / (trc_phuacc + Exp(plcp(idplocal)%leaf1 - plcp(idplocal)%leaf2 * trc_phuacc))
          ff = f - trc_laimxfr
          trc_laimxfr = f

          !! calculate new canopy height
          if (pldb(idplocal)%idc == 7) then
            trc_cht = rto * pldb(idplocal)%chtmx
          else
            trc_cht = pldb(idplocal)%chtmx * Sqrt(f)
          end if

          !! calculate new leaf area index
          if (trc_phuacc <= pldb(idplocal)%dlai) then
            laimax = 0.
            deltalai = 0.
            if (pldb(idplocal)%idc == 7) then
              laimax = rto * pldb(idplocal)%dlai
            else
              laimax = pldb(idplocal)%dlai
            end if
            if (laiday_trc > laimax) laiday_trc = laimax
            deltalai = ff * laimax * (1.0 - Exp(5.0 * (laiday_trc -     &
     &                                             laimax))) * Sqrt(reg)
            laiday_trc = laiday_trc + deltalai
            if (laiday_trc > laimax) laiday_trc = laimax
            trc_olai = laiday_trc
            if (laiday_trc > trc_laiyrmx) then
                trc_laiyrmx = laiday_trc
            end if
          else
            laiday_trc = trc_olai * (1. - trc_phuacc) / (1. - pldb(idplocal)%dlai)
          end if
          if (laiday_trc < 0.) laiday_trc = 0.

          !! calculate plant ET values
          if (trc_phuacc > 0.5 .and. trc_phuacc < pldb(idplocal)%dlai) then
            trc_pltet = trc_pltet + tep_day + tes_day
            trc_pltpet = trc_pltpet + pet_day
          end if

          thvstiadj = pldb(idplocal)%hvsti * 100. * trc_phuacc / (100.*trc_phuacc &
     &                 + Exp(11.1 - 10. * trc_phuacc))

          if (time%yrs > pco%nyskip) then
!            wshd_wstrs = wshd_wstrs + (1.-tstrsw) * hru_dafr(j) *       &
!     &                                      trc_ha / (hru(j)%km*100)
!            wshd_tstrs = wshd_tstrs + (1.-tstrstmp) * hru_dafr(j) *     &
!     &                                      trc_ha / (hru(j)%km*100)
!            wshd_nstrs = wshd_nstrs + (1.-tstrsn) * hru_dafr(j) *       &
!     &                                      trc_ha / (hru(j)%km*100)
!            wshd_pstrs = wshd_pstrs + (1.-tstrsp) * hru_dafr(j) *       &
!     &                                      trc_ha / (hru(j)%km*100)
          end if

        end if
      return
      end

      subroutine harvestop_trc

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine performs the harvest operation (no kill) on terrace segment

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units          |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    auto_eff(:) |none           |fertilizer application efficiency calculated
!!                                |as the amount of N applied divided by the
!!                                |amount of N removed at harvest
!!    cnyld(:)    |kg N/kg yieldlocal  |fraction of nitrogen in yieldlocal
!!    cpyld(:)    |kg P/kg yieldlocal  |fraction of phosphorus in yieldlocal
!!    time%yrs       |none           |current year in simulation
!!    hru_dafr(:) |km2/km2        |fraction of watershed area in HRU
!!    hvsti(:)    |(kg/ha)/(kg/ha)|harvest index: crop yieldlocal/aboveground
!!                                |biomass
!!    idc(:)      |none           |crop/landcover category:
!!                                |1 warm season annual legume
!!                                |2 cold season annual legume
!!                                |3 perennial legume
!!                                |4 warm season annual
!!                                |5 cold season annual
!!                                |6 perennial
!!                                |7 trees
!!    idplt_trc(:,:)|none         |land cover code from crop.dat
!!    ihru        |none           |HRU number
!!    laiday_trc  |none           |leaf area index
!!    pco%nyskip      |none           |number of years output is not printed/
!!                                |summarized
!!    tharveff(:,:) |none         |harvest efficiency: fraction of harvested 
!!                                |yieldlocal that is removed from terrace segment; the 
!!                                |remainder becomes residue on the soil
!!                                |surface
!!    thvstiadj   |(kg/ha)/(kg/ha)|optimal harvest index for specific time 
!!                                |during growing season
!!    ticr        |none           |sequence number of crop grown within the
!!                                |current year
!!    tncut       |none           |sequence number of harvest operation within
!!                                |a year
!!    tnro        |none           |sequence number of year in rotation
!!    trc_biohv(:,:)|kg/ha          |harvested biomass (dry weight)
!!    trc_bioms   |kg/ha          |land cover/crop biomass (dry weight)
!!    trc_bioyrms |metric tons/ha |annual biomass (dry weight) in the terrace segment
!!    trc_hiovr(:,:)|(kg/ha)/(kg/ha)|harvest index target specified at
!!                                |harvest
!!    trc_phuacc  |none           |fraction of plant heat units accumulated
!!    trc_pltet   |mm H2O         |actual ET simulated during life of plant
!!    trc_pltfrn  |none           |fraction of plant biomass that is nitrogen
!!    trc_pltfrp  |none           |fraction of plant biomass that is phosphorus
!!    trc_pltn    |kg N/ha        |amount of nitrogen in plant biomass
!!    trc_pltp    |kg P/ha        |amount of phosphorus in plant biomass
!!    trc_pltpet  |mm H2O         |potential ET simulated during life of plant
!!    trc_rwt     |none           |fraction of total plant biomass that is
!!                                |in roots
!!    trc_yldanu   |metric tons/ha|annual yieldlocal (dry weight) in the terrace segment
!!    trc_yldkg(:,:)|kg/ha        |yieldlocal (dry weight) by crop type in the terrace segment
!!    tsol_fon(:) |kg N/ha        |amount of nitrogen stored in the fresh
!!                                |organic (residue) pool
!!    tsol_fop(:) |kg P/ha        |amount of phosphorus stored in the fresh
!!                                |organic (residue) pool
!!    tsol_rsd(:) |kg/ha          |amount of organic matter in the soil
!!                                |classified as residue
!!    wshd_yldn   |kg N/ha        |amount of nitrogen removed from soil in
!!                                |watershed in the yieldlocal
!!    wshd_yldp   |kg P/ha        |amount of phosphorus removed from soil in
!!                                |watershed in the yieldlocal
!!    wsyf(:)     |(kg/ha)/(kg/ha)|Value of harvest index between 0 and HVSTI
!!                                |which represents the lowest value expected
!!                                |due to water stress
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units          |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    laiday_trc  |none           |leaf area index
!!    trc_biohv(:,:)|kg/ha          |harvested biomass (dry weight)
!!    trc_bioms   |kg/ha          |land cover/crop biomass (dry weight)
!!    trc_bioyrms |metric tons/ha |annual biomass (dry weight) in the terrace segment
!!    trc_phuacc  |none           |fraction of plant heat units accumulated
!!    trc_pltn    |kg N/ha        |amount of nitrogen in plant biomass
!!    trc_pltp    |kg P/ha        |amount of phosphorus in plant biomass
!!    trc_tnyld(:,:)|kg N/kg yieldlocal  |modifier for autofertilization target
!!                                |nitrogen content for plant
!!    trc_yldanu  |metric tons/ha |annual yieldlocal (dry weight) in the terrace segment
!!    trc_yldkg(:,:)|kg/ha        |yieldlocal (dry weight) by crop type in the terrace segment
!!    tsol_fon(:) |kg N/ha        |amount of nitrogen stored in the fresh
!!                                |organic (residue) pool
!!    tsol_fop(:) |kg P/ha        |amount of phosphorus stored in the fresh
!!                                |organic (residue) pool
!!    tsol_rsd(:) |kg/ha          |amount of organic matter in the soil
!!                                |classified as residue
!!    wshd_yldn   |kg N/ha        |amount of nitrogen removed from soil in
!!                                |watershed in the yieldlocal
!!    wshd_yldp   |kg P/ha        |amount of phosphorus removed from soil in
!!                                |watershed in the yieldlocal
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units          |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    clip        |kg/ha          |yieldlocal lost during harvesting
!!    clipn       |kg N/ha        |nitrogen in clippings
!!    clipp       |kg P/ha        |phosphorus in clippings
!!    clippst     |kg pst/ha      |pesticide in clippings
!!    hiad1       |none           |actual harvest index (adj for water/growth)
!!    j           |none           |HRU number
!!    k           |none           |counter
!!    wur         |none           |water deficiency factor
!!    yieldlocal       |kg             |yieldlocal (dry weight)
!!    yieldlocaln      |kg N/ha        |nitrogen removed in yieldlocal
!!    yieldlocalp      |kg P/ha        |phosphorus removed in yieldlocal
!!    yldpst      |kg pst/ha      |pesticide removed in yieldlocal
!!    xx          |
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Exp, Min

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~
      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      implicit none
      integer :: j, k, l
      real :: hiad1, wur, yieldlocal, clip, yieldlocaln, yieldlocalp, clipn, clipp
      real :: yldpst, clippst, rtresnew, ssb, ssabg, ssr, ssn
      real :: ssp, ff3, nssr, ff4, rtresn, rtresp

      j = 0
      j = ihru

      ssb = 0.
      ssabg = 0.
      ssr = 0.
      ssn = 0.
      ssp = 0.
      ssb = trc_bioms                            ! Armen 16 Jan 2009 storing info
      ssabg = trc_bioms * (1.- trc_rwt)	    ! Armen 16 Jan 2009 storing info
      ssr = ssb * trc_rwt                         ! Armen 16 Jan 2009 storing info
      ssn = trc_pltn                            ! Armen 20 May 2006 storing info
      ssp = trc_pltp                            ! Armen 20 May 2006 storing info
	
	
!! calculate modifier for autofertilization target nitrogen content
      trc_tnyld(tnro,ticr) = 0.
      trc_tnyld(tnro,ticr) = (1. - trc_rwt) * trc_bioms *               &
     &                                      trc_pltfrn * auto_eff(j)
      if (ticr > 1) then
        trc_tnyld(tnro,ticr-1) = trc_tnyld(tnro,ticr)
      else
        trc_tnyld(tnro,ticr+1) = trc_tnyld(tnro,ticr)
      end if

      hiad1 = 0.
      if (trc_hiovr(tnro,tncut) > 0.) then
        hiad1 = trc_hiovr(tnro,tncut)
      else
        if (trc_pltpet < 10.) then
          wur = 100.
        else
          wur = 0.
          wur = 100. * trc_pltet / trc_pltpet
        endif
        hiad1 = (thvstiadj - pldb(idplt_trc(tnro,ticr))%wsyf) *              &
     &      (wur / (wur + Exp(6.13 - .0883 * wur))) +                   &
     &      pldb(idplt_trc(tnro,ticr))%wsyf
        if (hiad1 > pldb(idplt_trc(tnro,ticr))%hvsti) then
          hiad1 = pldb(idplt_trc(tnro,ticr))%hvsti
        end if
      end if


!! check if yieldlocal is from above or below ground
      yieldlocal = 0.
      if (pldb(idplt_trc(tnro,ticr))%hvsti > 1.001) then
        yieldlocal = trc_bioms * (1. - 1. / (1. + hiad1))
      else
        yieldlocal = (1.-trc_rwt) * trc_bioms * hiad1
      endif
      if (yieldlocal < 0.) yieldlocal = 0.

!! determine clippings (biomass left behind) and update yieldlocal
      clip = 0.
      clip = yieldlocal * (1. - tharveff(tnro,tncut))
      yieldlocal = yieldlocal * tharveff(tnro,tncut)
      if (yieldlocal < 0.) yieldlocal = 0.
      if (clip < 0.) clip = 0.

      if (trc_hiovr(tnro,tncut) > 0.) then
        !! calculate nutrients removed with yieldlocal
        yieldlocaln = 0.
        yieldlocalp = 0.
        yieldlocaln = yieldlocal * trc_pltn / trc_bioms
        yieldlocalp = yieldlocal * trc_pltp / trc_bioms
        yieldlocaln = Min(yieldlocaln, 0.80 * trc_pltn) ! note Armen changed .80 for 0.9
        yieldlocalp = Min(yieldlocalp, 0.80 * trc_pltp) ! note Armen changed .80 for 0.9
        !! calculate nutrients removed with clippings
        clipn = 0.
        clipp = 0.
        clipn = clip * trc_pltn / trc_bioms
        clipp = clip * trc_pltp / trc_bioms
        clipn = Min(clipn,trc_pltn-yieldlocaln)
        clipp = Min(clipp,trc_pltp-yieldlocalp)
      else
        !! calculate nutrients removed with yieldlocal
        yieldlocaln = 0.
        yieldlocalp = 0.
        yieldlocaln = yieldlocal * pldb(idplt_trc(tnro,ticr))%cnyld
        yieldlocalp = yieldlocal * pldb(idplt_trc(tnro,ticr))%cpyld
        yieldlocaln = Min(yieldlocaln, 0.80 * trc_pltn) ! note Armen changed .80 for 0.9
        yieldlocalp = Min(yieldlocalp, 0.80 * trc_pltp) ! note Armen changed .80 for 0.9
        !! calculate nutrients removed with clippings
        clipn = 0.
        clipp = 0.
        clipn = clip * pldb(idplt_trc(tnro,ticr))%cnyld
        clipp = clip * pldb(idplt_trc(tnro,ticr))%cpyld
        clipn = Min(clipn,trc_pltn-yieldlocaln)
        clipp = Min(clipp,trc_pltp-yieldlocalp)
      endif

      yieldlocaln = Max(yieldlocaln,0.)
      yieldlocalp = Max(yieldlocalp,0.)
      clipn = Max(clipn,0.)
      clipp = Max(clipp,0.)

      !! add clippings to residue and organic n and p
      tsol_rsd(1) = tsol_rsd(1) + clip
      tsol_fon(1) = clipn + tsol_fon(1)
      tsol_fop(1) = clipp + tsol_fop(1)

	!! Calculation for dead roots allocations, resetting phenology, updating other pools

      ff3 = 0.
      if (ssabg > 1.e-6) then
        ff3 = (yieldlocal + clip) / ssabg	! Armen 20 May 2008 and 16 Jan 2009
      else
        ff3 = 1.
      endif 
      if (ff3 > 1.0) ff3 = 1.0


	! nssr is the new mass of roots
      nssr = trc_rwt * ssabg * (1. - ff3) / (1. - trc_rwt)  
      rtresnew = ssr - nssr
      if (ssr > 1.e-6) then
       ff4 = rtresnew / ssr
      else
	   ff4 = 0.
      end if
      rtresn = ff4 * ssn
      rtresp = ff4 * ssp


      !! reset leaf area index and fraction of growing season
      if (ssb > 0.001) then
        laiday_trc = laiday_trc * (1. - ff3)  
        if (trc_phuacc < .999) trc_phuacc = trc_phuacc * (1. - ff3)  
!!        trc_rwt = .4 - .2 * trc_phuacc
      trc_rwt = pldb(idplt_trc(tnro,ticr))%rsr1 -                            &
     &          pldb(idplt_trc(tnro,ticr))%rsr2 * trc_phuacc        
      else
        trc_bioms = 0.
        laiday_trc = 0.
        trc_phuacc = 0.
      endif

	!! remove n and p in harvested yieldlocal, clipped biomass, and dead roots 
      trc_bioms = trc_bioms - yieldlocal - clip - rtresnew ! Armen 20 May 2008
      trc_pltn = trc_pltn - yieldlocaln - clipn - rtresn
      trc_pltp = trc_pltp - yieldlocalp - clipp - rtresp 
      if (trc_bioms < 0.) then
        rtresnew = rtresnew + trc_bioms
        trc_bioms = 0.
      end if
      if (trc_pltn < 0.) then
        rtresn = rtresn + trc_pltn
        trc_pltn = 0.
      end if
      if (trc_pltp < 0.) then
        rtresp = rtresp + trc_pltp
        trc_pltp = 0.
      end if

      !! compute fraction of roots in each layer	! Armen 20 May 2008
      call rootfr_trc

      !! allocate roots, N, and P to soil pools	! Armen 20 May 2008
      do l=1, soil(j)%nly
            tsol_rsd(l) = tsol_rsd(l) + soil(j)%ly(l)%rtfr * rtresnew
            tsol_fon(l) = tsol_fon(l) + soil(j)%ly(l)%rtfr * rtresn
            tsol_fop(l) = tsol_fop(l) + soil(j)%ly(l)%rtfr * rtresp
      end do

      !rtfr = 0.

	!! summary calculations
      if (time%yrs > pco%nyskip) then
 !       wshd_yldn = wshd_yldn + yieldlocaln * hru_dafr(j) * trc_ha /(hru(j)%km*100)
 !       wshd_yldp = wshd_yldp + yieldlocalp * hru_dafr(j) * trc_ha /(hru(j)%km*100)
        hru_yn = hru_yn + yieldlocaln * trc_ha /(hru(j)%km*100)        !! for terraced hru balance test
        hru_yp = hru_yp + yieldlocalp * trc_ha /(hru(j)%km*100)        !! for terraced hru balance test
        trc_yldkg(tnro,ticr) = trc_yldkg(tnro,ticr) + yieldlocal
        trc_yldanu = trc_yldanu + yieldlocal  / 1000.    
        trc_biohv(tnro,ticr) = (yieldlocal + clip + rtresnew) +           &
     &                                        trc_biohv(tnro,ticr)      !! Jeff, is this the intention
        trc_bioyrms = trc_bioyrms + (yieldlocal + clip + rtresnew) / 1000.		            !! Jeff, is this the intention
       ! end select
      endif
      
       tncut = tncut + 1

      return
      end subroutine

      subroutine harvgrainop_trc

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine performs the harvest grain only operation on terrace segment

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units          |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    auto_eff(:) |none           |fertilizer application efficiency calculated
!!                                |as the amount of N applied divided by the
!!                                |amount of N removed at harvest
!!    cnyld(:)    |kg N/kg yieldLocal |fraction of nitrogen in yield
!!    cpyld(:)    |kg P/kg yieldLocal |fraction of phosphorus in yield
!!    time%yrs       |none           |current year in simulation
!!    hru_dafr(:) |km2/km2        |fraction of watershed in HRU
!!    hvsti(:)    |(kg/ha)/(kg/ha)|harvest index: crop yield/aboveground
!!                                |biomass
!!    idplt_trc(:,:)|none         |land cover code from crop.dat
!!    ihru        |none           |HRU number
!!    pco%nyskip      |none           |number of years to not summarize/print output
!!    thvstiadj   |(kg/ha)/(kg/ha)|optimal harvest index for current time during
!!                                |growing season
!!    ticr        |none           |sequence number of crop grown within the
!!                                |current year
!!    tnro        |none           |sequence number for year in rotation
!!    trc_biohv(:,:)|kg/ha          |harvested biomass (dry weight)
!!    trc_bioms   |kg/ha          |land cover/crop biomass (dry weight)
!!    trc_bioyrms |metric tons/ha |annual biomass (dry weight) in the terrace segment
!!    trc_cnop(:,:)|none          |SCS runoff curve number for moisture
!!                                |condition II
!!    trc_hitarg(:,:)|(kg/ha)/(kg/ha)|harvest index target of cover defined at
!!                                |planting
!!    trc_pltet   |mm H2O         |actual ET simulated during life of plant
!!    trc_pltfrn  |none           |fraction of plant biomass that is nitrogen
!!    trc_pltn    |kg N/ha        |amount of nitrogen in plant biomass
!!    trc_pltp    |kg P/ha        |amount of phosphorus in plant biomass
!!    trc_pltpet  |mm H2O         |potential ET simulated during life of plant
!!    trc_rwt     |none           |fraction of total plant biomass that is
!!                                |in roots
!!    trc_yldanu  |metric tons/ha |annual yieldLocal(dry weight) in the terrace segment
!!    trc_yldkg(:,:)|kg/ha        |yieldLocal(dry weight) by crop type in the terrace segment
!!    tsol_fon(:) |kg N/ha        |amount of nitrogen stored in the fresh
!!                                |organic (residue) pool
!!    tsol_fop(:) |kg P/ha        |amount of phosphorus stored in the fresh
!!    tsol_rsd(:) |kg/ha          |amount of organic matter in the soil
!!                                |classified as residue
!!    wshd_yldn   |kg N/ha        |amount of nitrogen removed from soil in
!!                                |watershed in the yield
!!    wshd_yldp   |kg P/ha        |amount of phosphorus removed from soil in
!!                                |watershed in the yield
!!    wsyf(:)     |(kg/ha)/(kg/ha)|Value of harvest index between 0 and HVSTI
!!                                |which represents the lowest value expected
!!                                |due to water stress
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    idorm_trc   |none          |dormancy status code:
!!                               |0 land cover growing (not dormant)
!!                               |1 land cover dormant
!!    laiday_trc  |m**2/m**2     |leaf area index
!!    thvstiadj   |(kg/ha)/(kg/ha)|optimal harvest index for current time during
!!                               |growing season
!!    trc_biohv(:,:)|kg/ha       |harvested biomass (dry weight)
!!    trc_bioms   |kg/ha         |land cover/crop biomass (dry weight)
!!    trc_bioyrms |metric tons/ha|annual biomass (dry weight) in the terrace segment
!!    trc_phuacc  |none          |fraction of plant heat units accumulated
!!    trc_pltn    |kg N/ha       |amount of nitrogen in plant biomass
!!    trc_pltp    |kg P/ha       |amount of phosphorus in plant biomass
!!    trc_tnyld(:,:)|kg N/kg yieldLocal|modifier for autofertilization target
!!                               |nitrogen content for plant
!!    trc_yldanu  |metric tons/ha|annual yieldLocal(dry weight) in the terrace segment
!!    trc_yldkg(:,:)|kg/ha       |yieldLocal(dry weight) by crop type in the terrace segment
!!    tsol_fon(:) |kg N/ha       |amount of nitrogen stored in the fresh
!!                               |organic (residue) pool
!!    tsol_fop(:) |kg P/ha       |amount of phosphorus stored in the fresh
!!                               |organic (residue) pool
!!    tsol_rsd(:) |kg/ha         |amount of organic matter in the soil
!!                               |classified as residue
!!    tstrsw      |none          |fraction of potential plant growth achieved
!!                               |on the day where the reduction is caused by
!!                               |water stress
!!    wshd_yldn   |kg N/ha       |amount of nitrogen removed from soil in
!!                               |watershed in the yield
!!    wshd_yldp   |kg P/ha       |amount of phosphorus removed from soil in
!!                               |watershed in the yield
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    hiad1       |
!!    j           |none          |HRU number
!!    k           |none          |counter
!!    resnew      |
!!    wur         |
!!    yieldLocal      |kg            |yieldLocal(dry weight)
!!    yieldn      |
!!    yieldp      |
!!    yldpst      |kg pst/ha     |pesticide removed in yield
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Exp, Max, Min
!!    SWAT: curno_trc

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      implicit none
      integer :: j, k
      real :: wur, hiad1, resnew, yieldLocal, yieldn, yieldp, yldpst, xx

      j = 0
      j = ihru


      hiad1 = 0.
      if (trc_hitarg(tnro,ticr) > 0.) then
        hiad1 = trc_hitarg(tnro,ticr)
      else
        if (trc_pltpet < 10.) then
          wur = 100.
        else
          wur = 0.
          wur = 100. * trc_pltet / trc_pltpet
        endif

        hiad1 = (thvstiadj - pldb(idplt_trc(tnro,ticr))%wsyf) *              &
     &      (wur / (wur + Exp(6.13 - .0883 * wur))) +                   &
     &      pldb(idplt_trc(tnro,ticr))%wsyf

        if (hiad1 > pldb(idplt_trc(tnro,ticr))%hvsti) then 
          hiad1 = pldb(idplt_trc(tnro,ticr))%hvsti
        end if
      end if


!! check if yieldLocalis from above or below ground
      yieldLocal= 0.
      resnew = 0.
      if (pldb(idplt_trc(tnro,ticr))%hvsti > 1.001) then
        yieldLocal= trc_bioms * (1. - 1. / (1. + hiad1))
      else
        yieldLocal= (1. - trc_rwt) * trc_bioms * hiad1
      endif
      if (yieldLocal< 0.) yieldLocal= 0.
      yieldLocal= yieldLocal* tharveff(tnro,tncut)


!! calculate nutrients removed with yield
      yieldn = 0.
      yieldp = 0.
      yieldn = yieldLocal* pldb(idplt_trc(tnro,ticr))%cnyld
      yieldp = yieldLocal* pldb(idplt_trc(tnro,ticr))%cpyld
      yieldn = Min(yieldn, 0.85 * trc_pltn)
      yieldp = Min(yieldp, 0.85 * trc_pltp)
      trc_pltn = trc_pltn - yieldn
      trc_pltn = amax1(0.,trc_pltn)
      trc_pltp = trc_pltp - yieldp
      trc_pltp = amax1(0.,trc_pltp)

!! calculate modifier for autofertilization target nitrogen content
      trc_tnyld(tnro,ticr) = 0.
      trc_tnyld(tnro,ticr) = (1. - trc_rwt) * trc_bioms * trc_pltfrn *  &
     &                                                       auto_eff(j)
      if (ticr > 1) then
        trc_tnyld(tnro,ticr-1) = trc_tnyld(tnro,ticr)
      else
        trc_tnyld(tnro,ticr+1) = trc_tnyld(tnro,ticr)
      end if

!! summary calculations
       xx = trc_bioms
       trc_bioms = trc_bioms - yield
       trc_rwt = trc_rwt * xx / trc_bioms
      if (time%yrs > pco%nyskip) then
!       wshd_yldn = wshd_yldn + yieldn * hru_dafr(j) * trc_ha / (hru(j)%km*100)
!       wshd_yldp = wshd_yldp + yieldp * hru_dafr(j) * trc_ha / (hru(j)%km*100)
       hru_yn = hru_yn + yieldn * trc_ha /(hru(j)%km*100)        !! for terraced hru balance test
       hru_yp = hru_yp + yieldp * trc_ha /(hru(j)%km*100)        !! for terraced hru balance test
       trc_yldkg(tnro,ticr) = trc_yldkg(tnro,ticr) + yield
       trc_biohv(tnro,ticr) = yieldLocal+ trc_biohv(tnro,ticr)
       trc_yldanu = trc_yldanu + yieldLocal/ 1000.
       trc_bioyrms = trc_bioyrms + trc_bioms / 1000.
!       ncrops(tnro,ticr) = ncrops(tnro,ticr) + 1
      endif

!! update curve number
      if (trc_cnop(tnro,ticnop) > 0.) then
        call curno_trc(trc_cnop(tnro,ticnop),j)
      end if

!! increment harvest sequence number
      tncut = tncut + 1
      ticnop = ticnop + 1

!     if (time%yrs > pco%nyskip) then
!        ncrops(tnro,ticr) = ncrops(tnro,ticr) + 1
!     end if 

      return
      end

      subroutine harvkillop_trc

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine performs the harvest and kill operation on terrace segment

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units          |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    auto_eff(:) |none           |fertilizer application efficiency calculated
!!                                |as the amount of N applied divided by the
!!                                |amount of N removed at harvest
!!    cnyld(:)    |kg N/kg yieldLocal |fraction of nitrogen in yield
!!    cpyld(:)    |kg P/kg yieldLocal |fraction of phosphorus in yield
!!    time%yrs       |none           |current year in simulation
!!    hru_dafr(:) |km2/km2        |fraction of watershed in HRU
!!    hvsti(:)    |(kg/ha)/(kg/ha)|harvest index: crop yield/aboveground
!!                                |biomass
!!    idplt_trc(:,:)|none         |land cover code from crop.dat
!!    ihru        |none           |HRU number
!!    pco%nyskip      |none           |number of years to not summarize/print output
!!    thvstiadj |(kg/ha)/(kg/ha)|optimal harvest index for current time during
!!                                |growing season
!!    ticr        |none           |sequence number of crop grown within the
!!                                |current year
!!    tnro        |none           |sequence number for year in rotation
!!    trc_biohv(:,:)|kg/ha        |harvested biomass (dry weight)
!!    trc_bioms   |kg/ha          |land cover/crop biomass (dry weight)
!!    trc_bioyrms |metric tons/ha |annual biomass (dry weight) in the terrace segment
!!    trc_cnop(:,:)|none          |SCS runoff curve number for moisture
!!                                |condition II
!!    trc_hitarg(:,:)|(kg/ha)/(kg/ha)|harvest index target of cover defined at
!!                                |planting
!!    trc_pltet   |mm H2O         |actual ET simulated during life of plant
!!    trc_pltfrn  |none           |fraction of plant biomass that is nitrogen
!!    trc_pltn    |kg N/ha        |amount of nitrogen in plant biomass
!!    trc_pltp    |kg P/ha        |amount of phosphorus in plant biomass
!!    trc_pltpet  |mm H2O         |potential ET simulated during life of plant
!!    trc_rwt     |none           |fraction of total plant biomass that is
!!                                |in roots
!!    trc_yldanu  |metric tons/ha |annual yieldLocal(dry weight) in the terrace segment
!!    trc_yldkg(:,:)|kg/ha        |yieldLocal(dry weight) by crop type in the terrace segment
!!    tsol_fon(:) |kg N/ha        |amount of nitrogen stored in the fresh
!!                                |organic (residue) pool
!!    tsol_fop(:) |kg P/ha        |amount of phosphorus stored in the fresh
!!    tsol_rsd(:) |kg/ha          |amount of organic matter in the soil
!!                                |classified as residue
!!    wshd_yldn   |kg N/ha        |amount of nitrogen removed from soil in
!!                                |watershed in the yield
!!    wshd_yldp   |kg P/ha        |amount of phosphorus removed from soil in
!!                                |watershed in the yield
!!    wsyf(:)     |(kg/ha)/(kg/ha)|Value of harvest index between 0 and HVSTI
!!                                |which represents the lowest value expected
!!                                |due to water stress
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    idorm_trc   |none          |dormancy status code:
!!                               |0 land cover growing (not dormant)
!!                               |1 land cover dormant
!!    laiday_trc  |m**2/m**2     |leaf area index
!!    thvstiadj   |(kg/ha)/(kg/ha)|optimal harvest index for current time during
!!                               |growing season
!!    trc_biohv(:,:)|kg/ha       |harvested biomass (dry weight)
!!    trc_bioms   |kg/ha         |land cover/crop biomass (dry weight)
!!    trc_bioyrms |metric tons/ha|annual biomass (dry weight) in the terrace segment
!!    trc_phuacc  |none          |fraction of plant heat units accumulated
!!    trc_pltn    |kg N/ha       |amount of nitrogen in plant biomass
!!    trc_pltp    |kg P/ha       |amount of phosphorus in plant biomass
!!    trc_tnyld(:,:)|kg N/kg yieldLocal|modifier for autofertilization target
!!                               |nitrogen content for plant
!!    trc_yldanu  |metric tons/ha|annual yieldLocal(dry weight) in the terrace segment
!!    trc_yldkg(:,:)|kg/ha       |yieldLocal(dry weight) by crop type in the terrace segment
!!    tsol_fon(:) |kg N/ha       |amount of nitrogen stored in the fresh
!!                               |organic (residue) pool
!!    tsol_fop(:) |kg P/ha       |amount of phosphorus stored in the fresh
!!                               |organic (residue) pool
!!    tsol_rsd(:) |kg/ha         |amount of organic matter in the soil
!!                               |classified as residue
!!    tstrsw      |none          |fraction of potential plant growth achieved
!!                               |on the day where the reduction is caused by
!!                               |water stress
!!    wshd_yldn   |kg N/ha       |amount of nitrogen removed from soil in
!!                               |watershed in the yield
!!    wshd_yldp   |kg P/ha       |amount of phosphorus removed from soil in
!!                               |watershed in the yield
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    hiad1       |
!!    j           |none          |HRU number
!!    k           |none          |counter
!!    resnew      |
!!    wur         |
!!    yieldLocal      |kg            |yieldLocal(dry weight)
!!    yieldn      |
!!    yieldp      |
!!    yldpst      |kg pst/ha     |pesticide removed in yield
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Exp, Max, Min
!!    SWAT: rootfr_trc, curno_trc

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      implicit none
      integer :: j, k, l
      real :: wur, hiad1, yieldLocal, yieldn, yieldp, yldpst
	real :: resnew, rtresnew 
      real :: xx, ff1, ff2
	

      j = 0
      j = ihru

	!! calculate modifier for autofertilization target nitrogen content
      trc_tnyld(tnro,ticr) = 0.
      trc_tnyld(tnro,ticr) = (1. - trc_rwt) * trc_bioms * trc_pltfrn *  &
     &                                                       auto_eff(j)
      if (ticr > 1) then
        trc_tnyld(tnro,ticr-1) = trc_tnyld(tnro,ticr)
      else
        trc_tnyld(tnro,ticr+1) = trc_tnyld(tnro,ticr)
      end if


      hiad1 = 0.
      if (trc_hitarg(tnro,ticr) > 0.) then
        hiad1 = trc_hitarg(tnro,ticr)
      else
        if (trc_pltpet < 10.) then
          wur = 100.
        else
          wur = 0.
          wur = 100. * trc_pltet / trc_pltpet
        endif

        hiad1 = (thvstiadj - pldb(idplt_trc(tnro,ticr))%wsyf) *              &
     &      (wur / (wur + Exp(6.13 - .0883 * wur))) +                   &
     &      pldb(idplt_trc(tnro,ticr))%wsyf

        if (hiad1 > pldb(idplt_trc(tnro,ticr))%hvsti) then 
          hiad1 = pldb(idplt_trc(tnro,ticr))%hvsti
        end if
      end if


!! check if yieldLocalis from above or below ground
      yieldLocal= 0.
      resnew = 0.
      rtresnew = 0.

!! stover fraction during harvkillop
      xx = trc_frharvk(tnro,ticr)
      if (xx .lt. 1.e-6) then
!       xx = hi_ovr(tnro,ticr)
        xx = trc_hiovr(tnro,tncut)
      endif
!! stover fraction during harvkillop

      if (pldb(idplt_trc(tnro,ticr))%hvsti > 1.001) then
        yieldLocal= trc_bioms * (1. - 1. / (1. + hiad1))
        resnew = trc_bioms / (1. + hiad1)
        resnew = resnew * (1. - xx)
      else
        yieldLocal= (1. - trc_rwt) * trc_bioms * hiad1
        resnew = (1. - trc_rwt) * (1. - hiad1) * trc_bioms
        !! remove stover during harvkillop
        resnew = resnew * (1. - xx)
        rtresnew = trc_rwt * trc_bioms	
      endif
      if (yieldLocal< 0.) yieldLocal= 0.
      if (resnew < 0.) resnew = 0.
	if (rtresnew < 0.) rtresnew = 0.	! Armen 19 May 2008
										! I would avoid this check, it is
										! safer to know if variable is negative

	!! calculate nutrients removed with yield
      yieldn = 0.
      yieldp = 0.
      yieldn = yieldLocal* pldb(idplt_trc(tnro,ticr))%cnyld
      yieldp = yieldLocal* pldb(idplt_trc(tnro,ticr))%cpyld
      yieldn = Min(yieldn, 0.80 * trc_pltn)
      yieldp = Min(yieldp, 0.80 * trc_pltp)

	!! Armen 19 May 2008 / 21 January 2008	
	!! fraction of roots in each layer
	call rootfr_trc

	!! fraction of N, P in residue (ff1) or roots (ff2)
	ff1 = (1 - hiad1) / (1 - hiad1 + trc_rwt)
	ff2 = 1 - ff1

	!! update residue, N, P on soil surface
      tsol_rsd(1) = resnew + tsol_rsd(1)
	tsol_fon(1) = tsol_fon(1) + FF1 * (trc_pltn - yieldn)
      tsol_fop(1) = tsol_fop(1) + FF1 * (trc_pltp - yieldp) 
      tsol_rsd(1) = Max(tsol_rsd(1),0.)
	tsol_fon(1) = Max(tsol_fon(1),0.)
	tsol_fop(1) = Max(tsol_fop(1),0.)

	!! allocate dead roots, N, P to soil layers
	do l=1, soil(j)%nly
	 tsol_rsd(l) = tsol_rsd(l) + soil(j)%ly(l)%rtfr *rtresnew
       tsol_fon(l) = tsol_fon(l) + soil(j)%ly(l)%rtfr *ff2 * (trc_pltn - yieldn)
       tsol_fop(l) = tsol_fop(l) + soil(j)%ly(l)%rtfr *ff2 * (trc_pltp - yieldp)
	end do
	!! summary calculations
      if (time%yrs > pco%nyskip) then
!       wshd_yldn = wshd_yldn + yieldn * hru_dafr(j) * trc_ha / (hru(j)%km*100)
!       wshd_yldp = wshd_yldp + yieldp * hru_dafr(j) * trc_ha / (hru(j)%km*100)
       hru_yn = hru_yn + yieldn * trc_ha /(hru(j)%km*100)        !! for terraced hru balance test
       hru_yp = hru_yp + yieldp * trc_ha /(hru(j)%km*100)        !! for terraced hru balance test
       trc_yldkg(tnro,ticr) = trc_yldkg(tnro,ticr) + yield
       trc_biohv(tnro,ticr) = trc_bioms + trc_biohv(tnro,ticr)
       trc_yldanu = trc_yldanu + yieldLocal/ 1000.
       trc_bioyrms = trc_bioyrms + trc_bioms / 1000.
       trc_ncrops(tnro,ticr) = trc_ncrops(tnro,ticr) + 1
      endif

	!! update curve number
      if (trc_cnop(tnro,ticnop) > 0.) then
        call curno_trc(trc_cnop(tnro,ticnop),j)
      end if


	!! reset variables
      igro_trc = 0
      idorm_trc = 0
      trc_bioms = 0.
	trc_rwt = 0.
      trc_pltn = 0.
      trc_pltp = 0.
      tstrsw = 1.
      laiday_trc = 0.
      thvstiadj = 0.
      trc_phuacc = 0.
!      phubase(j) = 0.
      ticnop = ticnop + 1
	!rtfr = 0. ! resetting root fraction per layer array to 0

      return
      end

      subroutine hrutrc
    
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine calculates the water, sediment and nutrients generated in 
!!    terraces and estimates the effects of terrace on these materials.
!!    The terrace simulation algorithm is developed and coded by Hui Shao from 
!!    Northwest A&F University. Please contact the author (Email: neo.shaohui@gmail.com) 
!!    if there is any problem about terrace simulation. 

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    hru_ha(:)   |ha            |area of HRU in hectares
!!    ihru        |none          |HRU number
!!    latno3(:)   |kg N/ha       |amount of NO3-N in lateral flow in HRU for
!!                               |the day
!!    nplnt(:)    |kg N/ha       |plant uptake of nitrogen in HRU for the day
!!    percn(:)    |kg N/ha       |amount of nitrate percolating past bottom
!!    pplnt(:)    |kg P/ha       |plant uptake of phosphorus in HRU for the 
!!                               |day
!!    qday        |mm H2O        |surface runoff loading to main channel for
!!                               |day in HRU
!!    sedminpa(:) |kg P/ha       |amount of active mineral phosphorus sorbed to
!!                               |sediment in surface runoff in HRU for day
!!    sedminps(:) |kg P/ha       |amount of stable mineral phosphorus sorbed to
!!                               |sediment in surface runoff in HRU for day
!!    sedorgn(:)  |kg N/ha       |amount of organic nitrogen in surface runoff
!!                               |in HRU for the day
!!    sedorgp(:)  |kg P/ha       |amount of organic phosphorus in surface runoff
!!                               |in HRU for the day
!!    sedyld(:)   |metric tons   |daily soil loss caused by water erosion in HRU
!!    sepbtm      |mm H2O        |percolation from bottom of soil profile for
!!                               |the day in HRU
!!    surqno3(:)  |kg N/ha       |amount of NO3-N in surface runoff in HRU for
!!                               |the day
!!    surqsolp(:) |kg P/ha       |amount of soluble phosphorus in surface runoff
!!                               |in HRU for the day
!!    tbsol_st(:,:)  |mm H2O     |amount of water stored in soil profile 
!!                               |on any given day for terrace bed area
!!    tbsol_sw(:)    |mm H2O     |amount of water stored in soil profile 
!!                               |of terrace bed area
!!    trc_fr(:)   |none          |fraction of terrace/HRU area
!!    trc_no3(:)  |kg N          |amount of nitrate originating from surface
!!                               |runoff in terrace at beginning of day
!!    trc_no3s(:) |kg N          |amount of nitrate originating from lateral
!!                               |flow in terrace at beginning of day
!!    trc_orgn(:) |kg N          |amount of organic N originating from
!!                               |surface runoff in terrace at beginning of day
!!    trc_orgp(:) |kg P          |amount of organic P originating from
!!    trc_pseda(:)|kg P          |amount of mineral P attached to sediment
!!                               |originating from surface runoff in terrace at
!!                               |beginning of day
!!                               |surface runoff in terrace at beginning of day
!!    trc_pseds(:)|kg P          |amount of mineral P attached to sediment
!!                               |originating from surface runoff in terrace at
!!                               |beginning of day
!!    trc_sed(:)  |metric tons   |amount of sediment in terrace
!!    trc_solp(:) |kg P          |amount of soluble P originating from surface
!!                               |runoff in terrace at beginning of day
!!    trc_stl(:)  |m^3           |accumulated settled sediment volume in terrace
!!    trc_sumev      |m^3 H2O    |daily evaporation on terrace
!!    trc_vmax    |m^3           |maximum volume of terrace storage at the beginning
!!                               |of the day
!!    trc_vol(:)  |m^3 H2O       |volume of water in terrace
!!    trc_volx(:) |m^3           |maximum storage volume for new terrace
!!    trcflwo_ov  |m^3 H2O       |volume of water flowing out of terrace in
!!                               |overland flow on day
!!    trci_fr(:)  |none          |fraction of HRU area without terrace area
!!                               |that drains into ponds
!!    trcno3o_ov  |kg N          |amount of nitrate in surface runoff leaving
!!                               |terrace in overland flow during day
!!    trcorgno_ov |kg N          |amount of organic N in surface runoff leaving
!!                               |terrace in overland flow during day
!!    trcorgpo_ov |kg P          |amount of organic P in surface runoff leaving
!!                               |terrace in overland flwo during day
!!    trcpsedao_ov|kg P          |amount of mineral phosphorus sorbed to sediment
!!                               |leaving terrace in overland flow during day
!!    trcpsedso_ov|kg P          |amount of mineral phosphorus sorbed to sediment
!!                               |leaving terrace in overland flow during day
!!    trcsedo_ov  |metric tons   |sediment leaving terrace in overland during day
!!    trcsolpo_ov |kg P          |amount of soluable phosphorus in surface runoff 
!!                               |leaving terrace in overland flow during day
!!    trsol_st(:,:)  |mm H2O     |amount of water stored in soil profile 
!!                               |on any given day for terrace raised area
!!    trsol_sw(:)    |mm H2O     |amount of water stored in soil profile 
!!                               |of terrace raised area
!!    tusol_st(:,:)  |mm H2O     |amount of water stored in soil profile 
!!                               |on any given day for terrace undisturbed area
!!    tusol_sw(:)    |mm H2O     |amount of water stored in soil profile 
!!                               |of terrace undisturbed area
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    latno3(:)   |kg N/ha       |amount of NO3-N in lateral flow in HRU for the
!!                               |day
!!    nplnt(:)    |kg N/ha       |plant uptake of nitrogen in HRU for the day
!!    percn(:)    |kg N/ha       |amount of nitrate percolating past bottom
!!    pplnt(:)    |kg P/ha       |plant uptake of phosphorus in HRU for the 
!!                               |day
!!    qday        |mm H2O        |surface runoff loading to main channel for
!!                               |day in HRU
!!    sedminpa(:) |kg P/ha       |amount of active mineral phosphorus sorbed to
!!                               |sediment in surface runoff in HRU for day
!!    sedminps(:) |kg P/ha       |amount of stable mineral phosphorus sorbed to
!!                               |sediment in surface runoff in HRU for day
!!    sedorgn(:)  |kg N/ha       |amount of organic nitrogen in surface runoff
!!                               |in HRU for the day
!!    sedorgp(:)  |kg P/ha       |amount of organic phosphorus in surface runoff
!!                               |in HRU for the day
!!    sedyld(:)   |metric tons   |daily soil loss caused by water erosion in HRU
!!    sepbtm      |mm H2O        |percolation from bottom of soil profile for
!!                               |the day in HRU
!!    surqno3(:)  |kg N/ha       |amount of NO3-N in surface runoff in HRU for
!!                               |the day
!!    surqsolp(:) |kg P/ha       |amount of soluble phosphorus in surface runoff
!!                               |in HRU for the day
!!    tbsol_st(:,:)  |mm H2O     |amount of water stored in soil profile 
!!                               |on any given day for terrace bed area
!!    tbsol_sw(:)    |mm H2O     |amount of water stored in soil profile 
!!                               |of terrace bed area
!!    trc_no3(:)  |kg N          |amount of nitrate originating from surface
!!                               |runoff in terrace at beginning of day
!!    trc_no3s(:) |kg N          |amount of nitrate originating from lateral
!!                               |flow in terrace at beginning of day
!!    trc_orgn(:) |kg N          |amount of organic N originating from
!!                               |surface runoff in terrace at beginning of day
!!    trc_orgp(:) |kg P          |amount of organic P originating from
!!                               |surface runoff in terrace at beginning of day
!!    trc_pseda(:)|kg P          |amount of mineral P attached to sediment
!!                               |originating from surface runoff in terrace at
!!                               |beginning of day
!!    trc_pseds(:)|kg P          |amount of mineral P attached to sediment
!!                               |originating from surface runoff in terrace at
!!                               |beginning of day
!!    trc_sed(:)  |metric tons   |amount of sediment in terrace
!!    trc_solp(:) |kg P          |amount of soluble P originating from surface
!!                               |runoff in terrace at beginning of day
!!    trc_sumev      |m^3 H2O    |daily evaporation on terrace
!!    trc_vmax    |m^3           |maximum volume of terrace storage at the beginning
!!                               |of the day
!!    trc_vol(:)  |m^3 H2O       |volume of water in terrace
!!    trcflwi     |m^3 H2O       |volume of water flowing into terrace on day
!!    trsol_st(:,:)  |mm H2O     |amount of water stored in soil profile 
!!                               |on any given day for terrace raised area
!!    trsol_sw(:)    |mm H2O     |amount of water stored in soil profile 
!!                               |of terrace raised area
!!    tusol_st(:,:)  |mm H2O     |amount of water stored in soil profile 
!!                               |on any given day for terrace undisturbed area
!!    tusol_sw(:)    |mm H2O     |amount of water stored in soil profile 
!!                               |of terrace undisturbed area
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    cnv         |none          |conversion factor (mm/ha => m^3)
!!    j           |none          |HRU number
!!    xx          |none          |calculation variable
!!    vol         |m^3 H2O       |volume of water in terrace at the beginning of day
!!    sed         |metric tons   |amount of sediment in terrace at the beginning of day


!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    SWAT: terrac

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none

      integer :: j
      real :: xx, vol, sed, san, sil, cla, sag, lag
      real :: etbkmm, cnv, qday_beforeTRC

      j = 0
      j = ihru
      
        !! conversion factors
        cnv = 0.
        cnv = (hru(j)%km*100) * 10.
        
        !! initialize variables and store the values of 
        !! the beginning of the day
        vol = 0.
        sed = 0.
        san = 0.
        sil = 0.
        cla = 0.
        sag = 0.
        lag = 0.
        vol = trc_vol(j)
        sed = trc_sed(j)
        san = trc_san(j)
        sil = trc_sil(j)
        cla = trc_cla(j)
        sag = trc_sag(j)
        lag = trc_lag(j)

        !! calculate the maximum terrace storage volume
        trc_vmax = 0.
        trc_vmax = trc_volx(j) - trc_stl(j)
        trc_vmax = max(0.,trc_vmax)
        
        !! Test only
        qday_beforeTRC = qday


        !! calculate water flowing into terrace for day
        xx = 0. 
        xx = 1. - trc_fr(j) 
        trcflwi = qday * cnv * xx * trci_fr(j)
        qday = qday * xx * (1-trci_fr(j))
        tloss = tloss * xx
        latq(j) = latq(j) * xx
        latno3(j) = latno3(j) * xx
        sepbtm(j) = sepbtm(j) * xx
        percn(j) = percn(j) * xx
        nplnt(j) = nplnt(j) * xx
        pplnt(j) = pplnt(j) * xx
 
        !! calculate sediment loading to terrace for day
        trc_sed(j) = trc_sed(j) + sedyld(j) * xx * trci_fr(j) 
        trc_san(j) = trc_san(j) + sanyld(j) * xx * trci_fr(j) 
        trc_sil(j) = trc_sil(j) + silyld(j) * xx * trci_fr(j) 
        trc_cla(j) = trc_cla(j) + clayld(j) * xx * trci_fr(j) 
        trc_sag(j) = trc_sag(j) + sagyld(j) * xx * trci_fr(j) 
        trc_lag(j) = trc_lag(j) + lagyld(j) * xx * trci_fr(j)

        sedyld(j) = sedyld(j) * xx * (1-trci_fr(j)) 
        sanyld(j) = sanyld(j) * xx * (1-trci_fr(j)) 
        silyld(j) = silyld(j) * xx * (1-trci_fr(j))
        clayld(j) = clayld(j) * xx * (1-trci_fr(j)) 
        sagyld(j) = sagyld(j) * xx * (1-trci_fr(j)) 
        lagyld(j) = lagyld(j) * xx * (1-trci_fr(j))

        !! compute nitrogen and phosphorus levels in terrace at beginning
        !! of day
        if (trc_solp(j) < 1.e-6) trc_solp(j) = 0.0
        if (trc_pseds(j) < 1.e-6) trc_pseds(j) = 0.0
        if (trc_pseda(j) < 1.e-6) trc_pseda(j) = 0.0
        if (trc_orgp(j) < 1.e-6) trc_orgp(j) = 0.0
        if (trc_orgn(j) < 1.e-6) trc_orgn(j) = 0.0
        if (trc_no3(j) < 1.e-6) trc_no3(j) = 0.0
        if (trc_no3s(j) < 1.e-6) trc_no3s(j) = 0.0

        xx = 0.
        xx = (1 - trc_fr(j)) * (hru(j)%km*100) * trci_fr(j)
        trc_solp(j) = trc_solp(j) + surqsolp(j) * xx
        trc_pseds(j) = trc_pseds(j) + sedminps(j) * xx
        trc_pseda(j) = trc_pseda(j) + sedminpa(j) * xx
        trc_orgp(j) = trc_orgp(j) + sedorgp(j) * xx
        trc_orgn(j) = trc_orgn(j) + sedorgn(j) * xx
        trc_no3(j) = trc_no3(j) + surqno3(j) * xx

        !! update the amount of HRU nutrients
        xx = 0.
        xx = (1. - trc_fr(j)) * (1 - trci_fr(j))
        sedorgn(j) = sedorgn(j) * xx
        surqno3(j) = surqno3(j) * xx
        sedorgp(j) = sedorgp(j) * xx
        sedminpa(j) = sedminpa(j) * xx
        sedminps(j) = sedminps(j) * xx
        surqsolp(j) = surqsolp(j) * xx

        call terrac(j)

        if (trcflwo_ov > 1.e-5) then
            
        write (*,*) " QdayBefore: ", qday_beforeTRC
        write (*,*) " QdayTRC: ", trcflwo_ov / cnv
        !! compute water leaving terrace
        qday = qday + trcflwo_ov / cnv

        !! compute sediment leaving terrace
        sedyld(j) = sedyld(j) + trcsedo_ov 
        sanyld(j) = sanyld(j) + trcsano_ov 
        silyld(j) = silyld(j) + trcsilo_ov 
        clayld(j) = clayld(j) + trcclao_ov 
        sagyld(j) = sagyld(j) + trcsago_ov 
        lagyld(j) = lagyld(j) + trclago_ov

        !! compute nutrients leaving terrace
        sedorgn(j) = sedorgn(j) + trcorgno_ov / (hru(j)%km*100) 
        surqno3(j) = surqno3(j) + trcno3o_ov / (hru(j)%km*100) 
        sedorgp(j) = sedorgp(j) + trcorgpo_ov / (hru(j)%km*100) 
        sedminps(j) = sedminps(j) + trcpsedso_ov / (hru(j)%km*100) 
        sedminpa(j) = sedminpa(j) + trcpsedao_ov / (hru(j)%km*100)
        surqsolp(j) = surqsolp(j) + trcsolpo_ov / (hru(j)%km*100)
        end if

        latno3(j) = latno3(j) + trc_no3s(j) / (hru(j)%km*100)
        trc_no3s(j) = 0.

        !! return over estimated ET to terrace soil
        xx = 0.
        xx = trc_sumev - pet_day * cnv * trc_fr(j)
        if (xx > 1.e-6) then
            etbkmm = 0.
            etbkmm = xx / (cnv * trc_fr(j))
            tusol_st(1,j) = tusol_st(1,j) + etbkmm
            trsol_st(1,j) = trsol_st(1,j) + etbkmm
            tbsol_st(1,j) = tbsol_st(1,j) + etbkmm
            tusol_sw(j) = tusol_sw(j) + etbkmm
            trsol_sw(j) = trsol_sw(j) + etbkmm
            tbsol_sw(j) = tbsol_sw(j) + etbkmm
            trc_sumev = trc_sumev - xx
        end if

        !! compute seepage depth for HRU water balance
        twltrc = trc_sumsep / ((hru(j)%km*100) * trc_fr(j))/10.

      if (qday < 0.) qday = 0.

      if (sedyld(j) < 0.) then
	    sedyld(j) = 0.0
        sanyld(j) = 0.0
        silyld(j) = 0.0
        clayld(j) = 0.0
        sagyld(j) = 0.0
        lagyld(j) = 0.0
      end if
      
      return
      end

      subroutine irrigate_trc(jj,volmm)
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine applies irrigation water to terrace segment

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    aairr(:)    |mm H2O        |average annual amount of irrigation water
!!                               |applied to HRU
!!    time%yrs       |none          |current year of simulation
!!    hru_ha(:)   |ha            |area of HRU in hectares
!!    hrumono(22,:)|mm H2O       |amount of irrigation water applied to HRU
!!                               |during month
!!    irn(:)      |none          |average annual number of irrigation 
!!                               |applications in HRU
!!    jj          |none          |HRU number
!!    pco%nyskip      |none          |number of years to skip output summarization
!!                               |and printing
!!    trc_ha      |ha            |terrace segment area
!!    tsq_rto     |none          |ratio of irrigated water infiltrate into soil
!!    volmm       |mm H2O        |amount of water irrigated
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    trc_aird    |mm H2O        |amount of water applied to terrace segment soil  
!!                               |on current day
!!    trc_qird    |mm H2O        |amount of water applied to terrace segment runoff  
!!                               |on current day
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer, intent (in) :: jj
      real, intent (in out) :: volmm

      trc_aird = volmm * (1. - tsq_rto)
      trc_qird = volmm * tsq_rto

      if (time%yrs > pco%nyskip) then 
!        irn(jj) = irn(jj) + 1
        aairr(jj) = aairr(jj) + trc_aird * trc_ha / (hru(jj)%km * 100)
!        hrumono(22,jj) = hrumono(22,jj) + trc_aird * trc_ha / (hru(jj)%km * 100)
      end if

      return
      end

      subroutine irrtnk_trc

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine performs the irrigation operation when the source is 
!!    the rainfall harvest tank inside the watershed

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name           |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tankirr(:)     |mm H2O        |amount of water removed from rainfall harvest tank
!!    hru_ha(:)      |ha            |area of HRU in hectares
!!    sol_sumfc(:)   |mm H2O        |amount of water held in the soil profile
!!                                  |at field capacity
!!    tirr_amt(:,:)  |mm H2O        |depth of irrigation water applied to 
!!                                  |terrace undisturbed area
!!    tirr_sq        |none          |surface runoff ratio (0-1) .1 is 10% 
!!                                  |surface runoff
!!    tnirr          |none          |sequence number of irrigation application
!!    tnro           |none          |sequence number of year in rotation
!!    trc_fr(:)      |none          |fraction of terrace/HRU area
!!    trc_tvol(:)    |m^3           |volume of rainfall harvest tank water storage 
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tankirr(:)  |mm H2O        |amount of water removed from rainfall harvest tank
!!    trc_tvol(:) |m^3           |volume of rainfall harvest tank water storage 
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    cnv         |none          |conversion factor (mm/ha => m^3)
!!    j           |none          |HRU number
!!    k           |none          |counter
!!    vmm         |mm H2O        |maximum amount of water to be applied
!!    vmma        |mm H2O        |amount of water in rainfall harvest tank
!!    vmxi        |mm H2O        |amount of water specified in irrigation
!!                               |operation
!!    vol         |m^3 H2O       |volume of water to be applied in irrigation
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Min
!!    SWAT: irrigate_trc

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j, k, flag
      real :: vmma, vmm, cnv, vmxi, vol

      j = 0
      j = ihru

      !! determine available amount of water in source
      !! ie upper limit on water removal on day
        vmma = 0.
        vmm = 0.

        cnv = 0.
        cnv = trc_ha * 10.
        vmma = trc_tvol(j) * tirr_efm(tnro,tnirr)
        vmm = vmma / cnv

!! if water available from source, proceed with irrigation
      if (vmm > 0.) then
        vmxi = 0.
        vmxi = tirr_amt(tnro,tnirr)
        if (vmxi < 1.e-6) vmxi = soil(j)%sumfc
        if (vmm > vmxi) vmm = vmxi

!! get correct SQ_RTO is this manual or auto
		 tsq_rto = tirr_sq(tnro,tnirr) 
         if (tauto_wstr(tnro,tnair) > 0.)  then
			tsq_rto = tirr_asq(tnro,tnair)
		 end if
         call irrigate_trc(j,vmm)
         
    !! subtract irrigation from terrace rainfall harvest tank
        vol = 0.
        vol = vmm * cnv
        trc_tvol(j) = trc_tvol(j) - vol
        if (trc_tvol(j) < 0.) then
            vol = vol + trc_tvol(j)
            trc_tvol(j) = 0.
        end if
        cnv = 0.
        cnv = (hru(j)%km*100) * trc_fr(j) * 10.
        tankirr(j) = tankirr(j) + vol / cnv
          
        !! advance irrigate operation number
        tnirr = tnirr + 1

      end if

      return
      end

      subroutine killop_trc

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine performs the kill operation on terrace segment

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name         |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    time%yrs        |none          |current year of simulation
!!    ihru         |none          |HRU number
!!    pco%nyskip       |none          |number of years to skip output printing/
!!                                |summarization
!!    ticr         |none          |sequence number of crop grown within the
!!                                |current year
!!    tnro         |none          |sequence number for year in rotation
!!    trc_bioms    |kg/ha         |land cover/crop biomass (dry weight)
!!    tsol_fon(:)  |kg N/ha       |amount of nitrogen stored in the fresh
!!                                |organic (residue) pool
!!    tsol_fop(:)  |kg P/ha       |amount of phosphorus stored in the fresh
!!                                |organic (residue) pool
!!    tsol_rsd(:)  |kg/ha         |amount of organic matter in the soil
!!                                |classified as residue
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name         |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    idorm_trc    |none          |dormancy status code:
!!                                |0 land cover growing (not dormant)
!!                                |1 land cover dormant
!!    laiday_trc   |m**2/m**2     |leaf area index
!!    ncrops(:,:,:)|
!!    trc_bioms    |kg/ha         |land cover/crop biomass (dry weight)
!!    trc_phuacc   |none          |fraction of plant heat units accumulated
!!    trc_pltn     |kg N/ha       |amount of nitrogen in plant biomass
!!    trc_pltp     |kg P/ha       |amount of phosphorus in plant biomass
!!    tsol_fon(:)  |kg N/ha       |amount of nitrogen stored in the fresh
!!                                |organic (residue) pool
!!    tsol_fop(:)  |kg P/ha       |amount of phosphorus stored in the fresh
!!                                |organic (residue) pool
!!    tsol_rsd(:)  |kg/ha         |amount of organic matter in the soil
!!                                |classified as residue
!!    tstrsw       |none          |fraction of potential plant growth achieved
!!                                |on the day where the reduction is caused by
!!                                |water stress
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    j           |none          |HRU number
!!    k           |none          |counter
!!    resnew      |
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Max

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j, k, l
      real :: resnew, rtresnew

      j = 0
      j = ihru

      if (time%yrs > pco%nyskip) then
       trc_ncrops(tnro,ticr) = trc_ncrops(tnro,ticr) + 1
      endif

	!! 22 January 2008	
	resnew = 0.
	rtresnew = 0.
      resnew = trc_bioms * (1. - trc_rwt)
	rtresnew = trc_bioms * trc_rwt
	call rootfr_trc

	!! update residue, N, P on soil surface
      tsol_rsd(1) = resnew + tsol_rsd(1)
      tsol_fon(1) = trc_pltn * (1. - trc_rwt) + tsol_fon(1)
      tsol_fop(1) = trc_pltp * (1. - trc_rwt) + tsol_fop(1)
      tsol_rsd(1) = Max(tsol_rsd(1),0.)
	tsol_fon(1) = Max(tsol_fon(1),0.)
	tsol_fop(1) = Max(tsol_fop(1),0.)

	!! allocate dead roots, N, P to soil layers
	do l=1, soil(j)%nly
	 tsol_rsd(l) = tsol_rsd(l) + soil(j)%ly(l)%rtfr * rtresnew
	 tsol_fon(l) = tsol_fon(l) + soil(j)%ly(l)%rtfr * trc_pltn * trc_rwt
	 tsol_fop(l) = tsol_fop(l) + soil(j)%ly(l)%rtfr * trc_pltp * trc_rwt
	end do

      trc_biohv(tnro,ticr) = trc_bioms + trc_biohv(tnro,ticr)
      trc_bioyrms = trc_bioyrms + trc_bioms / 1000.

	!! reset variables
      igro_trc = 0
      idorm_trc = 0
      trc_bioms = 0.
      trc_rwt = 0.
	trc_pltn = 0.
      trc_pltp = 0.
      tstrsw = 1.
      laiday_trc = 0.
      thvstiadj = 0.
      trc_phuacc = 0.
!      phubase(j) = 0.
!	rtfr = 0. ! Resetting roots fraction per layer array
	 
      return
      end

      subroutine ndenit_trc(k,j,cdg,wdn,void)
!!    this subroutine computes denitrification 

	use parm; use terrace_parm; use basin_module
      implicit none
	integer :: k,j
	real :: cdg, wdn, void, vof

      wdn = 0.
	vof = 1. / (1. + (void/0.04)**5)
	wdn = tsol_no3(k) * (1. - Exp(-bsn_prm%cdn * cdg * vof * tsol_cbn(k)))
	tsol_no3(k) = tsol_no3(k) - wdn

	return
	end

      subroutine newtillmix_trc(jj,bmix)

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine mixes residue and nutrients during tillage and 
!!    biological mixing on terrace segment
!!    New version developed by Armen R. Kemanian in collaboration with Stefan Julich and Cole Rossi
!!    Mixing was extended to all layers
!!    A subroutine to simulate stimulation of organic matter decomposition was added
!!    March 2009: testing has been minimal and further adjustments are expected
!!    use with caution and report anomalous results to akemanian@brc.tamus.edu and jeff.arnold@ars.usda.edu

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name          |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    time%yrs         |none          |current year of simulation
!!    deptil(:)     |mm            |depth of mixing caused by tillage
!!                                 |operation
!!    effmix(:)     |none          |mixing efficiency of tillage operation
!!    npmx          |none          |number of different pesticides used in
!!                                 |the simulation
!!    pco%nyskip        |none          |number of years to skip output printing/
!!                                 |summarization
!!    sol_nly(:)    |none          |maximum number of soil layers
!!    sol_nly(:)    |none          |number of soil layers
!!    sol_z(:,:)    |mm            |depth to bottom of soil layer
!!    tnro          |none          |sequence number of year in rotation
!!    tntil         |none          |sequence number of tillage operation within
!!                                 |current year
!!    trc_cnop(:,:) |none          |SCS runoff curve number for moisture
!!                                 |condition II
!!    tsol_actp(:)  |kg P/ha       |amount of phosphorus stored in the
!!                                 |active mineral phosphorus pool
!!    tsol_aorgn(:) |kg N/ha       |amount of nitrogen stored in the active
!!                                 |organic (humic) nitrogen pool
!!    tsol_fon(:)   |kg N/ha       |amount of nitrogen stored in the fresh
!!                                 |organic (residue) pool
!!    tsol_fop(:)   |kg P/ha       |amount of phosphorus stored in the fresh
!!                                 |organic (residue) pool
!!    tsol_nh3(:)   |kg N/ha       |amount of nitrogen stored in the ammonium
!!                                 |pool in soil layer
!!    tsol_no3(:)   |kg N/ha       |amount of nitrogen stored in the
!!                                 |nitrate pool.
!!    tsol_orgn(:)  |kg N/ha       |amount of nitrogen stored in the stable
!!                                 |organic N pool
!!    tsol_orgp(:)  |kg P/ha       |amount of phosphorus stored in the organic
!!                                 |P pool
!!    tsol_rsd(:)   |kg/ha         |amount of organic matter in the soil
!!                                 |classified as residue
!!    tsol_solp(:)  |kg P/ha       |amount of phosohorus stored in solution
!!    tsol_stap(:)  |kg P/ha       |amount of phosphorus in the soil layer
!!                                 |stored in the stable mineral phosphorus pool
!!    tsumix        |none          |sum of mixing efficiencies in terrace segment
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name          |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tntil         |none          |sequence number of tillage operation within
!!                                 |current year
!!    tsol_actp(:)  |kg P/ha       |amount of phosphorus stored in the
!!                                 |active mineral phosphorus pool
!!    tsol_aorgn(:) |kg N/ha       |amount of nitrogen stored in the active
!!                                 |organic (humic) nitrogen pool
!!    tsol_fon(:)   |kg N/ha       |amount of nitrogen stored in the fresh
!!                                 |organic (residue) pool
!!    tsol_fop(:)   |kg P/ha       |amount of phosphorus stored in the fresh
!!                                 |organic (residue) pool
!!    tsol_nh3(:)   |kg N/ha       |amount of nitrogen stored in the ammonium
!!                                 |pool in soil layer
!!    tsol_no3(:)   |kg N/ha       |amount of nitrogen stored in the
!!                                 |nitrate pool.
!!    tsol_orgn(:)  |kg N/ha       |amount of nitrogen stored in the stable
!!                                 |organic N pool
!!    tsol_orgp(:)  |kg P/ha       |amount of phosphorus stored in the organic
!!                                 |P pool
!!    tsol_rsd(:)   |kg/ha         |amount of organic matter in the soil
!!                                 |classified as residue
!!    tsol_solp(:)  |kg P/ha       |amount of phosohorus stored in solution
!!    tsol_stap(:)  |kg P/ha       |amount of phosphorus in the soil layer
!!                                 |stored in the stable mineral phosphorus pool
!!    tsumix        |none          |sum of mixing efficiencies in terrace segment
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    bmix        |none          |biological mixing efficiency: this 
!!                               |number is zero for tillage operations
!!    dg          |mm            |depth of soil layer
!!    dtil        |mm            |depth of mixing
!!    emix        |none          |mixing efficiency
!!    jj          |none          |HRU number
!!    k           |none          |counter
!!    l           |none          |counter
!!    nl          |none          |number of layers being mixed
!!    smix(:)     |varies        |amount of substance in soil profile
!!                               |that is being redistributed between 
!!                               |mixed layers
!!    thtill(:)   |none          |fraction of soil layer that is mixed
!!    sol_msm					 | sol_mass mixed
!!    sol_msn					 | sol_mass not mixed
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Min, Max
!!    SWAT: tillfactor_trc, curno_trc 

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      implicit none
      integer, intent (in) :: jj
      real, intent (in) :: bmix
!$$$$$$       integer :: l, k, nl, a
      integer :: l, k              !CB 12/2/09 nl and a are not used.
      real :: emix, dtil, XX, WW1, WW2, WW3, WW4
!$$$$$$       real :: thtill(soil(jj)%nly), smix(20+npmx)     
      real :: smix(20)        !CB 12/2/09 thtill is not used.
      real :: sol_mass(soil(jj)%nly)
      real :: sol_thick(soil(jj)%nly), sol_msm(soil(jj)%nly)
      real :: sol_msn(soil(jj)%nly)


      XX = 0.
      WW1 = 0.
      WW2 = 0.
      WW3 = 0.
      WW4 = 0.
      emix = 0.
      dtil = 0.
      if (bmix > 1.e-6) then
        !! biological mixing
        emix = bmix !bmix MJW (rev 412)
        dtil = Min(soil(jj)%phys(soil(jj)%nly)%d, 50.) ! it was 300.  MJW (rev 412)
      else 
        !! tillage operation
        emix = tilldb(tidtill(tnro,tntil))%effmix
        dtil = tilldb(tidtill(tnro,tntil))%deptil
      end if

      smix = 0.
      sol_mass = 0.
      sol_thick = 0.
      sol_msm = 0.
      sol_msn = 0.


      do l=1, soil(jj)%nly
        if ( l == 1) then
          sol_thick(l) = soil(jj)%phys(l)%d
        else	
          sol_thick(l) = soil(jj)%phys(l)%d - soil(jj)%phys(l-1)%d
        end if
	
      sol_mass(l) = (sol_thick(l) / 1000.) * 10000. *          &
        soil(jj)%phys(l)%bd * 1000. * (1.- soil(jj)%phys(l)%rock / 100.)

      end do

!       do l=1,20+npmx
!         smix(l)=0.
!       end do
      smix = 0.

      if (dtil > 0.) then
!!!  added by Armen 09/10/2010 next line only
        if (dtil < 10.0) dtil = 11.0
	 do l=1, soil(jj)%nly

          if (soil(jj)%phys(l)%d <= dtil) then
            !! msm = mass of soil mixed for the layer
            !! msn = mass of soil not mixed for the layer		
            sol_msm(l) = emix * sol_mass(l)	
            sol_msn(l) = sol_mass(l) - sol_msm(l)	
          else if (soil(jj)%phys(l)%d > dtil.AND.soil(jj)%phys(l-1)%d < dtil) then 
            sol_msm(l) = emix * sol_mass(l) *                           &
     &      (dtil - soil(jj)%phys(l-1)%d) / sol_thick(l)
            sol_msn(l) =  sol_mass(l) -  sol_msm(l)
          else
            sol_msm(l) = 0.
            sol_msn(l) = sol_mass(l)
          end if
			
          !! calculate the mass or concentration of each mixed element 
          !! mass based mixing
          WW1 = sol_msm(l)/(sol_msm(l) + sol_msn(l))
          smix(1) = smix(1) + tsol_no3(l) * WW1
          smix(2) = smix(2) + tsol_orgn(l) * WW1
          smix(3) = smix(3) + tsol_nh3(l) * WW1
          smix(4) = smix(4) + tsol_solp(l) * WW1
          smix(5) = smix(5) + tsol_orgp(l) * WW1
          smix(6) = smix(6) + tsol_aorgn(l) * WW1
          smix(7) = smix(7) + tsol_actp(l) * WW1
          smix(8) = smix(8) + tsol_fon(l) * WW1
          smix(9) = smix(9) + tsol_fop(l) * WW1
          smix(10) = smix(10) + tsol_stap(l) * WW1
          smix(11) = smix(11) + tsol_rsd(l) * WW1
          smix(12) = smix(12) + tsol_mc(l) * WW1
          smix(13) = smix(13) + tsol_mn(l) * WW1
          smix(14) = smix(14) + tsol_mp(l) * WW1

		!! concentration based mixing
          WW2 = XX + sol_msm(l)
          smix(15) = (XX * smix(15) + tsol_cbn(l) * sol_msm(l)) /WW2
          smix(16) = (XX * smix(16) + tsol_n(l) * sol_msm(l)) /WW2
          smix(17) = (XX * smix(17) + soil(jj)%phys(l)%clay * sol_msm(l)) /WW2
          smix(18) = (XX * smix(18) + soil(jj)%phys(l)%silt * sol_msm(l)) /WW2
          smix(19) = (XX * smix(19) + soil(jj)%phys(l)%sand * sol_msm(l)) /WW2
!          smix(20) = (XX * smix(20) + sol_rock(l) * sol_msm(l)) / WW2
			
          XX = XX + sol_msm(l)
        end do

          do l=1, soil(jj)%nly
			
            ! reconstitute each soil layer 
            WW3 = sol_msn(l) / sol_mass(l)
            WW4 = sol_msm(l) / XX

            tsol_no3(l) = tsol_no3(l) * WW3 + smix(1) * WW4
!            tsol_orgn(l) = tsol_orgn(l) * WW3 + smix(2) * WW4
            tsol_nh3(l) = tsol_nh3(l) * WW3 + smix(3) * WW4
            tsol_solp(l) = tsol_solp(l) * WW3 + smix(4) * WW4
            tsol_orgp(l) = tsol_orgp(l) * WW3 + smix(5) * WW4
            tsol_aorgn(l) = tsol_aorgn(l) * WW3 + smix(6) * WW4
            tsol_actp(l) = tsol_actp(l) * WW3 + smix(7) * WW4
            tsol_fon(l) = tsol_fon(l) * WW3 + smix(8) * WW4
            tsol_fop(l) = tsol_fop(l) * WW3 + smix(9) * WW4
            tsol_stap(l) = tsol_stap(l) * WW3 + smix(10) * WW4
            tsol_rsd(l) = tsol_rsd(l) * WW3 + smix(11) * WW4
            if (tsol_rsd(l) < 1.e-10) tsol_rsd(l) = 1.e-10
            tsol_mc(l) = tsol_mc(l) * WW3 + smix(12) * WW4
            tsol_mn(l) = tsol_mn(l) * WW3 + smix(13) * WW4
            tsol_mp(l) = tsol_mp(l) * WW3 + smix(14) * WW4

            tsol_cbn(l) = (tsol_cbn(l) * sol_msn(l) + smix(15)          &
     &           * sol_msm(l)) / sol_mass(l)
            tsol_n(l) = (tsol_n(l) * sol_msn(l) + smix(16)              &
     &           * sol_msm(l)) / sol_mass(l)
            soil(jj)%phys(l)%clay = (soil(jj)%phys(l)%clay * sol_msn(l) + smix(17)    &
     &           * sol_msm(l)) / sol_mass(l)
            soil(jj)%phys(l)%silt = (soil(jj)%phys(l)%silt * sol_msn(l) + smix(18)    &
     &           * sol_msm(l)) / sol_mass(l)
            soil(jj)%phys(l)%sand = (soil(jj)%phys(l)%sand * sol_msn(l) + smix(19)    &
     &           * sol_msm(l)) / sol_mass(l)
!		sol_rock(l,jj) = (sol_rock(l,jj) * sol_msn(l) + smix(20) * sol_msm(l)) / sol_mass(l)

	  end do
	
        if (bsn_cc%cswat == 1) then
            call tillfactor_trc(jj,bmix,emix,dtil,sol_thick)
        end if

		!! summary calculations
        if (time%yrs > pco%nyskip) then
            tsumix = tsumix + emix
        end if

      end if
	
      !! perform final calculations for tillage operation
      if (trc_cnop(tnro,ticnop) > 0.) then
        call curno_trc(trc_cnop(tnro,ticnop),jj)
      end if
      tntil = tntil + 1
      ticnop = ticnop + 1

      return
      end

      subroutine nfix_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine estimates nitrogen fixation by legumes on terrace segment

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    time%yrs       |none          |current year of simulation
!!    hru_dafr(:) |km**2/km**2   |fraction of watershed area in HRU
!!    ihru        |none          |HRU number
!!    pco%nyskip      |none          |number of years to skip output printing/
!!                               |summarization
!!    sol_nly(:)  |none          |number of layers in soil profile
!!    sol_sumfc(:)|mm H2O        |amount of water held in the soil profile
!!                               |at field capacity
!!    tnplnt      |kg N/ha       |plant uptake of nitrogen in terrace segment for the day
!!    trc_phuacc  |none          |fraction of plant heat units accumulated
!!    tsol_no3(:) |kg N/ha       |amount of nitrogen stored in the
!!                               |nitrate pool of the layer
!!    tsol_sw     |mm H2O        |amount of water stored in the soil profile
!!                               |on any given day
!!    tuno3d      |kg N/ha       |plant nitrogen deficiency for day in terrace segment
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    trc_fixn    |kg N/ha       |amount of nitrogen added to the plant via
!!                               |fixation on the day in terrace segment
!!    wshd_fixn   |kg N/ha       |average annual amount of nitrogen added to
!!                               |plant biomass via fixation
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    fixco       |none          |nitrogen fixation coefficient
!!    fxg         |
!!    fxn         |
!!    fxr         |
!!    fxw         |
!!    j           |none          |HRU number
!!    l           |none          |counter (soil layer)
!!    sumn        |kg N/ha       |total amount of nitrate stored in soil profile
!!    uno3l       |kg N/ha       |plant nitrogen demand
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Max, Min

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j, l
      real :: uno3l, fxw, sumn, fxn, fxg, fxr

      j = 0
      j = ihru
 
!! compute the difference between supply and demand
      if (tuno3d > tnplnt) then
        uno3l = 0.
        uno3l = tuno3d - tnplnt
      else
        !! if supply is being met, fixation=0 and return
        trc_fixn = 0.
        return
      endif


!! compute fixation as a function of no3, soil water, and growth stage

      !! compute soil water factor
      fxw = 0.
      fxw = tsol_sw / (.85 * soil(j)%sumfc)

      !! compute no3 factor
      sumn = 0.
      fxn = 0.
      do l = 1, soil(j)%nly
        sumn = sumn + tsol_no3(l)
      end do
      if (sumn > 300.) fxn = 0.
      if (sumn > 100. .and. sumn <= 300.) fxn = 1.5 - .0005 * sumn
      if (sumn <= 100.) fxn = 1.

      !! compute growth stage factor
      fxg = 0.
      if (trc_phuacc > .15 .and. trc_phuacc <= .30) then
         fxg = 6.67 * trc_phuacc - 1.
      endif
      if (trc_phuacc > .30 .and. trc_phuacc <= .55) fxg = 1.
      if (trc_phuacc > .55 .and. trc_phuacc <= .75) then
         fxg = 3.75 - 5. * trc_phuacc
      endif

      fxr = Min(1., fxw, fxn) * fxg
      fxr = Max(0., fxr)

      trc_fixn = Min(6., fxr * uno3l)
      trc_fixn = bsn_prm%fixco * trc_fixn + (1. - bsn_prm%fixco) * uno3l
             !! if fixco=0 then fix the entire demand
      trc_fixn = Min(trc_fixn, uno3l)
      trc_fixn = Min(bsn_prm%nfixmx, trc_fixn)

!! summary calculations
      if (time%yrs > pco%nyskip) then
!        wshd_fixn = wshd_fixn + trc_fixn * hru_dafr(j) *                &
!     &                                              trc_ha / (hru(j)%km*100)
        hru_fxn = hru_fxn + trc_fixn * trc_ha / (hru(j)%km*100)
      end if


      return
      end

      subroutine nitvol_trc

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine estimates daily mineralization (NH3 to NO3)
!!    and volatilization of NH3 on terrace segment

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name          |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    time%yrs         |none          |current year of simulation
!!    hru_dafr(:)   |km**2/km**2   |fraction of watershed area in HRU
!!    ihru          |none          |HRU number
!!    pco%nyskip        |none          |number of years to skip output
!!                                 |summarization and printing
!!    sol_fc(:,:)   |mm H2O        |amount of water available to plants in soil
!!                                 |layer at field capacity (fc - wp)
!!    sol_nly(:)    |none          |number of layers in soil profile
!!    sol_tmp(:,:)  |deg C         |daily average temperature of soil layer
!!    sol_wpmm(:,:) |mm H20        |water content of soil at -1.5 MPa (wilting
!!                                 |point)
!!    sol_z(:,:)    |mm            |depth to bottom of soil layer
!!    tsol_nh3(:)   |kg N/ha       |amount of nitrogen stored in the ammonium
!!                                 |pool in soil layer
!!    tsol_no3(:)   |kg N/ha       |amount of nitrogen stored in the
!!                                 |nitrate pool in soil layer
!!    tsol_st(:)    |mm H2O        |amount of water stored in the soil layer
!!                                 |on any given day (less wp water)
!!    wshd_nitn     |kg N/ha       |average annual amount of nitrogen moving
!!                                 |from the NH3 to the NO3 pool by
!!                                 |nitrification in the watershed
!!    wshd_voln     |kg N/ha       |average annual amount if nitrogen lost by
!!                                 |ammonia volatilization in watershed
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name          |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tsol_nh3(:)   |kg N/ha       |amount of nitrogen stored in the ammonium
!!                                 |pool in soil layer
!!    tsol_no3(:)   |kg N/ha       |amount of nitrogen stored in the
!!                                 |nitrate pool in soil layer
!!    wshd_nitn     |kg N/ha       |average annual amount of nitrogen moving
!!                                 |from the NH3 to the NO3 pool by
!!                                 |nitrification in the watershed
!!    wshd_voln     |kg N/ha       |average annual amount if nitrogen lost by
!!                                 |ammonia volatilization in watershed
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    akn         |
!!    akv         |
!!    cecf        |none          |volatilization CEC factor
!!    dmidl       |
!!    dpf         |
!!    j           |none          |HRU number
!!    k           |none          |counter (soil layer)
!!    rnit        |kg N/ha       |amount of nitrogen moving from the NH3 to the
!!                               |NO3 pool (nitrification) in the layer
!!    rnv         |
!!    rvol        |kg N/ha       |amount of nitrogen lost from the NH3 pool due
!!                               |to volatilization
!!    sw25        |
!!    swf         |
!!    swwp        |
!!    tf          |
!!    xx          |
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Exp, Max

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~
      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      implicit none
      integer :: j, k
      real :: sw25, swwp, swf, xx, dmidl, dpf, akn, akv, rnv, rnit, rvol
      real :: tf 
      real :: cecf = 0.15

      j = 0
      j = ihru 

      do k = 1, soil(j)%nly
        tf = 0.
        tf = .41 * (soil(j)%phys(k)%tmp - 5.) / 10.

        if (tsol_nh3(k) > 0. .and. tf >= 0.001) then
          sw25 = 0.
          swwp = 0.
          sw25 = soil(j)%phys(k)%wpmm + 0.25 * soil(j)%phys(k)%fc
          swwp = soil(j)%phys(k)%wpmm + tsol_st(k)
          if (swwp < sw25) then
            swf = 0.
            swf = (swwp - soil(j)%phys(k)%wpmm) /(sw25 - soil(j)%phys(k)%wpmm)
          else
            swf = 1.
          endif

          if (k == 1) then
            xx = 0.
          else
            xx = 0.
            xx = soil(j)%phys(k-1)%d
          endif

          dmidl = 0.
          dpf = 0.
          akn = 0.
          akv = 0.
          rnv = 0.
          rnit = 0.
          rvol = 0.
          dmidl = (soil(j)%phys(k)%d + xx) / 2.
          dpf = 1. - dmidl / (dmidl + Exp(4.706 - .0305 * dmidl))
          akn = tf * swf
          akv = tf * dpf * cecf
          rnv = tsol_nh3(k) * (1. - Exp(-akn - akv))
          rnit = 1. - Exp(-akn)
          rvol = 1. - Exp(-akv)

          !! calculate nitrification (NH3 => NO3)
	    !! apply septic algorithm only to active septic systems
          if(k/=i_sep(j).or.sep(isep)%opt/= 1) then  ! J.Jeong for septic, biozone layer
             if (rvol + rnit > 1.e-6) then
               rvol = rnv * rvol / (rvol + rnit)
               rnit = rnv - rvol
               if (rnit < 0.) rnit = 0.
               tsol_nh3(k) = Max(1.e-6, tsol_nh3(k) - rnit)
             endif
             if (tsol_nh3(k) < 0.) then
               rnit = rnit + tsol_nh3(k)
               tsol_nh3(k) = 0.
             endif
             tsol_no3(k) = tsol_no3(k) + rnit

             !! calculate ammonia volatilization
             tsol_nh3(k) = Max(1.e-6, tsol_nh3(k) - rvol)
             if (tsol_nh3(k) < 0.) then
               rvol = rvol + tsol_nh3(k)
               tsol_nh3(k) = 0.
             endif

             !! summary calculations
             if (time%yrs > pco%nyskip) then
!               wshd_voln = wshd_voln + rvol * hru_dafr(j) *             &
!     &                                              trc_ha / (hru(j)%km*100)
!               wshd_nitn = wshd_nitn + rnit * hru_dafr(j) *             &
!     &                                              trc_ha / (hru(j)%km*100)
               hru_vn = hru_vn + rvol * trc_ha / (hru(j)%km*100)
             end if
          end if
        end if

      end do

      return
      end


      subroutine nlch_trc

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine simulates the loss of nitrate via surface runoff, 
!!    lateral flow, and percolation out of the profile on terrace segment

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    anion_excl(:)|none         |fraction of porosity from which anions
!!                               |are excluded
!!    dis_stream(:)| m           |average distance to stream
!!    ihru        |none          |HRU number
!!    percn(:)    |kg N/ha       |amount of nitrate percolating past bottom
!!                               |of soil profile
!!    sol_nly(:)  |none          |number of layers in soil profile
!!    sol_ul(:,:) |mm H2O        |amount of water held in the soil layer at
!!                               |saturation
!!    trci_fr(:)  |none          |fraction of HRU area without terrace area
!!                               |that drains into ponds
!!    trcq        |mm H2O        |surface runoff generated on day in terrace segment
!!    tsol_flat(:)|mm H2O        |lateral flow in soil layer on current day
!!    tsol_no3(:) |kg N/ha       |amount of nitrogen stored in the nitrate pool
!!                               |in soil layer
!!    tsol_prk(:) |mm H2O        |percolation from soil layer on current day
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    percn(:)    |kg N/ha       |amount of nitrate percolating past bottom
!!                               |of soil profile
!!    tlatno3     |kg N/ha       |amount of nitrate transported with lateral
!!                               |flow
!!    trcqno3     |kg N/ha       |amount of nitrate transported with surface 
!!                               |runoff
!!    tsol_no3(:) |kg N/ha       |amount of nitrogen stored in the nitrate pool
!!                               |in soil layer
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    co          |kg N/mm       |concentration of nitrate in solution
!!    cosurf      |kg N/mm       |concentration of nitrate in surface runoff
!!    j           |none          |HRU number
!!    jj          |none          |counter (soil layers)
!!    percnlyr    |kg N/ha       |nitrate leached to next lower layer with
!!                               |percolation
!!    sro         |mm H2O        |surface runoff
!!    ssfnlyr     |kg N/ha       |nitrate transported in lateral flow from layer
!!    vno3        |
!!    vv          |mm H2O        |water mixing with nutrient in layer
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Exp, Max, Min

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j, jj
      real :: sro, ssfnlyr, percnlyr, vv, vno3, co
      real :: cosurf, nloss, ww, exp

      j = 0
      j = ihru

      percnlyr = 0.

      nloss = 0.
      nloss = (2.18 * (1. - trci_fr(j)) * hru(j)%topo%dis_stream - 8.63) / 100.
      nloss = Amax1(0.,nloss)
      nloss = Amin1(1.,nloss)

      do jj = 1, soil(j)%nly

        !! add nitrate leached from layer above
        tsol_no3(jj) = tsol_no3(jj) + percnlyr
	  if (tsol_no3(jj) < 1.e-6) tsol_no3(jj) = 0.0

        !! determine concentration of nitrate in mobile water
        sro = 0.
        vv = 0.
        vno3 = 0.
        co = 0.
        if (jj == 1) then
          sro = trcq
        else
          sro = 0.
        end if
        vv = tsol_prk(jj) + sro + tsol_flat(jj) + 1.e-10
        ww = -vv / ((1. - soil(j)%anion_excl) * soil(j)%phys(jj)%ul)
        vno3 = tsol_no3(jj) * (1. - Exp(ww))
        co = Max(vno3 / vv, 0.)

        !! calculate nitrate in surface runoff
        cosurf = 0.
        cosurf = bsn_prm%nperco * co
        if (jj == 1) then
          trcqno3 = trcq * cosurf
          trcqno3 = Min(trcqno3, tsol_no3(jj))
          tsol_no3(jj) = tsol_no3(jj) - trcqno3
        endif

        !! calculate nitrate in lateral flow
        ssfnlyr = 0.
        if (jj == 1) then
            ssfnlyr = cosurf * tsol_flat(jj) 
        else
            ssfnlyr = co * tsol_flat(jj) 
        end if
        ssfnlyr = ssfnlyr * (1 - nloss)
        ssfnlyr = Min(ssfnlyr, tsol_no3(jj))
        tlatno3 = tlatno3 + ssfnlyr
        tsol_no3(jj) = tsol_no3(jj) - ssfnlyr

        !! calculate nitrate in percolate
        percnlyr = 0.
        percnlyr = co * tsol_prk(jj)
        percnlyr = Min(percnlyr, tsol_no3(jj))
        tsol_no3(jj) = tsol_no3(jj) - percnlyr
      end do

      percn(j) = percn(j) + percnlyr * trc_ha / (hru(j)%km*100)

      return
      end

      subroutine nminrl_trc

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine estimates daily nitrogen and phosphorus
!!    mineralization and immobilization considering fresh organic
!!    material (plant residue) and active and stable humus material
!!    on terrace segment

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name          |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    cmn           |none          |rate factor for humus mineralization on
!!                                 |active organic N
!!    time%yrs         |none          |current year of simulation
!!    hru_dafr(:)   |km**2/km**2   |fraction of watershed area in HRU
!!    idplt_trc(:,:)|none          |land cover code from crop.dat
!!    ihru          |none          |HRU number
!!    nactfr        |none          |nitrogen active pool fraction. The fraction
!!                                 |of organic nitrogen in the active pool.
!!    pco%nyskip        |none          |number of years to skip output
!!                                 |summarization and printing
!!    rsdco_pl(:)   |none          |plant residue decomposition coefficient. The
!!                                 |fraction of residue which will decompose in
!!                                 |a day assuming optimal moisture,
!!                                 |temperature, C:N ratio, and C:P ratio
!!    sol_nly(:)    |none          |number of layers in soil profile
!!    sol_tmp(:,:)  |deg C         |daily average temperature of soil layer
!!    sol_ul(:,:)   |mm H2O        |amount of water held in the soil layer at
!!                                 |saturation
!!    ticr          |none          |sequence number of crop grown within the
!!                                 |current year
!!    tnro          |none          |sequence number of year in rotation
!!    tsol_aorgn(:) |kg N/ha       |amount of nitrogen stored in the active
!!                                 |organic (humic) nitrogen pool
!!    tsol_cbn(:)   |%             |percent organic carbon in soil layer
!!    tsol_fon(:)   |kg N/ha       |amount of nitrogen stored in the fresh
!!                                 |organic (residue) pool
!!    tsol_fop(:)   |kg P/ha       |amount of phosphorus stored in the fresh
!!                                 |organic (residue) pool
!!    tsol_no3(:)   |kg N/ha       |amount of nitrogen stored in the
!!                                 |nitrate pool in soil layer
!!    tsol_orgn(:)  |kg N/ha       |amount of nitrogen stored in the stable
!!                                 |organic N pool
!!    tsol_orgp(:)  |kg P/ha       |amount of phosphorus stored in the organic
!!                                 |P pool in soil layer
!!    tsol_rsd(:)   |kg/ha         |amount of organic matter in the soil
!!                                 |classified as residue
!!    tsol_solp(:)  |kg P/ha       |amount of phosohorus stored in solution
!!    tsol_st(:)    |mm H2O        |amount of water stored in the soil layer on
!!                                 |current day
!!    wshd_dnit     |kg N/ha       |average annual amount of nitrogen lost from
!!                                 |nitrate pool due to denitrification in
!!                                 |watershed
!!    wshd_hmn      |kg N/ha       |average annual amount of nitrogen moving
!!                                 |from active organic to nitrate pool in
!!                                 |watershed
!!    wshd_hmp      |kg P/ha       |average annual amount of phosphorus moving
!!                                 |from organic to labile pool in watershed
!!    wshd_rmn      |kg N/ha       |average annual amount of nitrogen moving
!!                                 |from fresh organic (residue) to nitrate
!!                                 |and active organic pools in watershed
!!    wshd_rmp      |kg P/ha       |average annual amount of phosphorus moving
!!                                 |from fresh organic (residue) to labile
!!                                 |and organic pools in watershed
!!    wshd_rwn      |kg N/ha       |average annual amount of nitrogen moving
!!                                 |from active organic to stable organic pool
!!                                 |in watershed
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name          |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    thmntl        |kg N/ha       |amount of nitrogen moving from active
!!                                 |organic to nitrate pool in soil profile
!!                                 |on current day in terrace segment
!!    thmptl        |kg P/ha       |amount of phosphorus moving from the
!!                                 |organic to labile pool in soil profile
!!                                 |on current day in terrace segment
!!    trmn2tl       |kg N/ha       |amount of nitrogen moving from the fresh
!!                                 |organic (residue) to the nitrate(80%) and
!!                                 |active organic(20%) pools in soil profile
!!                                 |on current day in terrace segment
!!    trmptl        |kg P/ha       |amount of phosphorus moving from the
!!                                 |fresh organic (residue) to the labile(80%)
!!                                 |and organic(20%) pools in soil profile
!!                                 |on current day in terrace segment
!!    trwntl        |kg N/ha       |amount of nitrogen moving from active
!!                                 |organic to stable organic pool in soil
!!                                 |profile on current day in terrace segment
!!    tsol_aorgn(:) |kg N/ha       |amount of nitrogen stored in the active
!!                                 |organic (humic) nitrogen pool
!!    tsol_fon(:)   |kg N/ha       |amount of nitrogen stored in the fresh
!!                                 |organic (residue) pool
!!    tsol_fop(:)   |kg P/ha       |amount of phosphorus stored in the fresh
!!                                 |organic (residue) pool
!!    tsol_no3(:)   |kg N/ha       |amount of nitrogen stored in the
!!                                 |nitrate pool in soil layer
!!    tsol_orgn(:)  |kg N/ha       |amount of nitrogen stored in the stable
!!                                 |organic N pool
!!    tsol_orgp(:)  |kg P/ha       |amount of phosphorus stored in the organic
!!                                 |P pool in soil layer
!!    tsol_rsd(:)   |kg/ha         |amount of organic matter in the soil
!!                                 |classified as residue
!!    tsol_solp(:)  |kg P/ha       |amount of phosohorus stored in solution
!!    twdntl        |kg N/ha       |amount of nitrogen lost from nitrate pool
!!                                 |by denitrification in soil profile on
!!                                 |current day in terrace segment
!!    wshd_dnit     |kg N/ha       |average annual amount of nitrogen lost from
!!                                 |nitrate pool due to denitrification in
!!                                 |watershed
!!    wshd_hmn      |kg N/ha       |average annual amount of nitrogen moving
!!                                 |from active organic to nitrate pool in
!!                                 |watershed
!!    wshd_hmp      |kg P/ha       |average annual amount of phosphorus moving
!!                                 |from organic to labile pool in watershed
!!    wshd_rmn      |kg N/ha       |average annual amount of nitrogen moving
!!                                 |from fresh organic (residue) to nitrate
!!                                 |and active organic pools in watershed
!!    wshd_rmp      |kg P/ha       |average annual amount of phosphorus moving
!!                                 |from fresh organic (residue) to labile
!!                                 |and organic pools in watershed
!!    wshd_rwn      |kg N/ha       |average annual amount of nitrogen moving
!!                                 |from active organic to stable organic pool
!!                                 |in watershed
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    ca          |
!!    cdg         |none          |soil temperature factor
!!    cdn         |
!!    cnr         |
!!    cnrf        |
!!    cpr         |
!!    cprf        |
!!    csf         |none          |combined temperature/soil water factor
!!    decr        |
!!    hmn         |kg N/ha       |amount of nitrogen moving from active organic
!!                               |nitrogen pool to nitrate pool in layer
!!    hmp         |kg P/ha       |amount of phosphorus moving from the organic
!!                               |pool to the labile pool in layer
!!    j           |none          |HRU number
!!    k           |none          |counter (soil layer)
!!    kk          |none          |soil layer used to compute soil water and
!!                               |soil temperature factors
!!    r4          |
!!    rdc         |
!!    rmn1        |kg N/ha       |amount of nitrogen moving from fresh organic
!!                               |to nitrate(80%) and active organic(20%)
!!                               |pools in layer
!!    rmp         |kg P/ha       |amount of phosphorus moving from fresh organic
!!                               |to labile(80%) and organic(20%) pools in layer
!!    rwn         |kg N/ha       |amount of nitrogen moving from active organic
!!                               |to stable organic pool in layer
!!    sdnco       |none          |denitrification threshold: fraction of field
!!                               | capacity
!!    sut         |none          |soil water factor
!!    wdn         |kg N/ha       |amount of nitrogen lost from nitrate pool in
!!                               |layer due to denitrification
!!    xx          |varies        |variable to hold intermediate calculation
!!                               |result
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Max, Exp, Sqrt, Min, Abs

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      implicit none
      integer :: j, k, kk
      real :: rmn1, rmp, xx, csf, rwn, hmn, hmp, r4, cnr, cnrf, cpr
      real :: cprf, ca, decr, rdc, wdn, cdg, sut

      j = 0
      j = ihru


      do k = 1, soil(j)%nly
 
        kk =0 
        if (k == 1) then
          kk = 2
        else
          kk = k
        end if

        !! mineralization can occur only if temp above 0 deg
        if (soil(j)%phys(kk)%tmp > 0.) then
          !! compute soil water factor
          sut = 0.
	!! change for domain error 1/29/09 gsm check with Jeff !!!
	    if (tsol_st(kk) < 0.) tsol_st(kk) = .0000001
          sut = .1 + .9 * Sqrt(tsol_st(kk) / soil(j)%phys(kk)%fc)
          sut = Min(1., sut)
          sut = Max(.05, sut)

          !!compute soil temperature factor
          xx = 0.
          cdg = 0.
          xx = soil(j)%phys(kk)%tmp
          cdg = .9 * xx / (xx + Exp(9.93 - .312 * xx)) + .1
          cdg = Max(.1, cdg)

          !! compute combined factor
          xx = 0.
          csf = 0.
          xx = cdg * sut
          if (xx < 0.) xx = 0.
          if (xx > 1.e6) xx = 1.e6
          csf = Sqrt(xx)

          !! compute flow from active to stable pools
          rwn = 0.
          rwn = .1e-4 * (tsol_aorgn(k) * (1. / nactfr - 1.) -           &
     &                                                    tsol_orgn(k))
          if (rwn > 0.) then
            rwn = Min(rwn, tsol_aorgn(k))
          else
            rwn = -(Min(Abs(rwn), tsol_orgn(k)))
          endif
          tsol_orgn(k) = Max(1.e-6, tsol_orgn(k) + rwn)
          tsol_aorgn(k) = Max(1.e-6, tsol_aorgn(k) - rwn)

          !! compute humus mineralization on active organic n
          hmn = 0.
          hmn = bsn_prm%cmn * csf * tsol_aorgn(k)
          hmn = Min(hmn, tsol_aorgn(k))
          !! compute humus mineralization on active organic p
          xx = 0.
          hmp = 0.
          xx = tsol_orgn(k) + tsol_aorgn(k)
          if (xx > 1.e-6) then
            hmp = 1.4 * hmn * tsol_orgp(k) / xx
          else
            hmp = 0.
          end if
          hmp = Min(hmp, tsol_orgp(k))
          !! move mineralized nutrients between pools
          tsol_aorgn(k) = Max(1.e-6, tsol_aorgn(k) - hmn)
          tsol_no3(k) = tsol_no3(k) + hmn
          tsol_orgp(k) = tsol_orgp(k) - hmp
          tsol_solp(k) = tsol_solp(k) + hmp

          !! compute residue decomp and mineralization of 
          !! fresh organic n and p (upper two layers only)
          rmn1 = 0.
          rmp = 0.
          if (k <= 2) then
            r4 = 0.
            r4 = .58 * tsol_rsd(k)

            if (tsol_fon(k) + tsol_no3(k) > 1.e-4) then
              cnr = 0.
              cnr = r4 / (tsol_fon(k) + tsol_no3(k))
              if (cnr > 500.) cnr = 500.
              cnrf = 0.
              cnrf = Exp(-.693 * (cnr - 25.) / 25.)
            else
              cnrf = 1.
            end if

            if (tsol_fop(k) + tsol_solp(k) > 1.e-4) then
              cpr = 0.
              cpr = r4 / (tsol_fop(k) + tsol_solp(k))
              if (cpr > 5000.) cpr = 5000.
              cprf = 0.
              cprf = Exp(-.693 * (cpr - 200.) / 200.)
            else
              cprf = 1.
            end if

            ca = 0.
            decr = 0.
            rdc = 0.
            ca = Min(cnrf, cprf, 1.)
        if (idplt_trc(tnro,ticr) > 0) then
            decr = pldb(idplt_trc(tnro,ticr))%rsdco_pl * ca * csf
        else
            decr = 0.05
        end if
            decr = Max(bsn_prm%decr_min, decr)
            decr = Min(decr, 1.)
            tsol_rsd(k) = amax1(1.e-6,tsol_rsd(k))
            rdc = decr * tsol_rsd(k)
            tsol_rsd(k) = tsol_rsd(k) - rdc
            if (tsol_rsd(k) < 0.) tsol_rsd(k) = 0.
            rmn1 = decr * tsol_fon(k)
            tsol_fop(k) = amax1(1.e-6,tsol_fop(k))
            rmp = decr * tsol_fop(k)

            tsol_fop(k) = tsol_fop(k) - rmp
            tsol_fon(k) = amax1(1.e-6,tsol_fon(k))
            tsol_fon(k) = tsol_fon(k) - rmn1
            tsol_no3(k) = tsol_no3(k) + .8 * rmn1
            tsol_aorgn(k) = tsol_aorgn(k) + .2 * rmn1
            tsol_solp(k) = tsol_solp(k) + .8 * rmp
            tsol_orgp(k) = tsol_orgp(k) + .2 * rmp
          end if
!! septic changes 1/28/09 gsm
!!  compute denitrification
        wdn = 0.   
	  if (i_sep(j) /= k .or. sep(isep)%opt /= 1) then
	    if (sut >= bsn_prm%sdnco) then
	      wdn = tsol_no3(k) * (1. - Exp(-bsn_prm%cdn * cdg * tsol_cbn(k)))
	    else
	      wdn = 0.
	    endif
	    tsol_no3(k) = tsol_no3(k) - wdn
	  end if
! septic changes 1/28/09 gsm

!			call ndenit(k,j,cdg,wdn,0.05)
	!!	end if

          !! summary calculations
          if (time%yrs > pco%nyskip) then
!            wshd_hmn = wshd_hmn + hmn * hru_dafr(j) * trc_ha / (hru(j)%km*100)
!            wshd_rwn = wshd_rwn + rwn * hru_dafr(j) * trc_ha / (hru(j)%km*100)
!            wshd_hmp = wshd_hmp + hmp * hru_dafr(j) * trc_ha / (hru(j)%km*100)
!            wshd_rmn = wshd_rmn + rmn1 * hru_dafr(j) *trc_ha / (hru(j)%km*100)
!            wshd_rmp = wshd_rmp + rmp * hru_dafr(j) *trc_ha / (hru(j)%km*100)
!            wshd_dnit = wshd_dnit + wdn * hru_dafr(j)*trc_ha / (hru(j)%km*100)
            thmntl = thmntl + hmn
            trwntl = trwntl + rwn
            thmptl = thmptl + hmp
            trmn2tl = trmn2tl + rmn1
            trmptl = trmptl + rmp
            twdntl = twdntl + wdn
          end if
        end if
      end do


      return
      end

      subroutine npup_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine calculates plant phosphorus uptake on terrace segment

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units          |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    bio_p1(:)   |none           |1st shape parameter for plant P uptake
!!                                |equation
!!    bio_p2(:)   |none           |2st shape parameter for plant P uptake
!!                                |equation
!!    time%yrs       |none           |current year of simulation
!!    hru_dafr(:) |km**2/km**2    |fraction of watershed area in HRU
!!    idplt_trc(:,:)|none         |land cover code from crop.dat
!!    ihru        |none           |HRU number
!!    pco%nyskip      |none           |number of years to skip output summarization/
!!                                |printing
!!    p_updis     |none           |phosphorus uptake distribution parameter
!!                                |This parameter controls the amount of
!!                                |phosphorus removed from the different soil
!!                                |layers by the plant. In particular, this
!!                                |parameter allows the amount of phosphorus
!!                                |removed from the surface layer via plant
!!                                |uptake to be controlled. While the relation-
!!                                |ship between UBP and P uptake from the
!!                                |surface layer is affected by the depth of the
!!                                |soil profile, in general, as UBP increases
!!                                |the amount of P removed from the surface
!!                                |layer relative to the amount removed from the
!!                                |entire profile increases
!!    pltpfr(1,:) |kg P/kg biomass|phosphorus uptake parameter #1: normal
!!                                |fraction of P in crop biomass at emergence
!!    pltpfr(3,:) |kg P/kg biomass|phosphorus uptake parameter #3: normal
!!                                |fraction of P in crop biomass at maturity
!!    pplnt       |kg P/ha        |plant uptake of phosphorus in HRU for the day
!!    sol_nly(:)  |none           |number of soil layers in profile
!!    sol_z(:,:)  |mm             |depth to bottom of soil layer
!!    ticr        |none           |sequence number of crop grown within the
!!                                |current year
!!    tnro        |none           |sequence number of year in rotation
!!    trc_bioday  |kg             |biomass generated on current day in terrace segment
!!    trc_bioms   |kg/ha          |land cover/crop biomass (dry weight)
!!    trc_phuacc  |none           |fraction of plant heat units accumulated
!!    trc_pltp    |kg P/ha        |amount of phosphorus stored in plant
!!    tsol_solp(:)|kg P/ha        |amount of phosohorus stored in solution
!!    uobp        |none           |phosphorus uptake normalization parameter
!!                                |This variable normalizes the phosphorus
!!                                |uptake so that the model can easily verify
!!                                |that uptake from the different soil layers
!!                                |sums to 1.0
!!    wshd_pup    |kg P/ha        |average annual amount of plant uptake of 
!!                                |phosphorus 
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    pplnt       |kg P/ha       |plant uptake of phosphorus in HRU for the day
!!    tpplnt      |kg P/ha       |plant uptake of phosphorus in terrace segment for the day
!!    trc_pltfrp  |none          |fraction of plant biomass that is phosphorus
!!    trc_pltp    |kg P/ha       |amount of phosphorus stored in plant
!!    tsol_solp(:)|kg P/ha       |amount of phosohorus stored in solution
!!    tstrsp      |none          |fraction of potential plant growth achieved on
!!                               |the day where the reduction is caused by
!!                               |phosphorus stress
!!    wshd_pup    |kg P/ha       |average annual amount of plant uptake of 
!!                               |phosphorus
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
     
!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    gx          |mm            |lowest depth in layer from which phosphorus
!!                               |may be removed
!!    icrop       |none          |land cover code
!!    ir          |none          |flag for bottom of root zone
!!    j           |none          |HRU number
!!    l           |none          |counter (soil layers)
!!    uapdlocal        |kg P/ha       |plant demand of phosphorus
!!    uapl        |kg P/ha       |amount of phosphorus removed from layer
!!    up2local         |kg P/ha       |optimal plant phosphorus content
!!    upmx        |kg P/ha       |maximum amount of phosphorus that can be
!!                               |removed from the soil layer
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Exp, Min
!!    SWAT: nuts

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      implicit none
      integer :: j, icrop, l, ir
      real :: up2local, uapdlocal, upmx, uapl, gx

      j = 0
      j = ihru

      icrop = 0
      icrop = idplt_trc(tnro,ticr)
      trc_pltfrp = (pldb(icrop)%pltpfr1 - pldb(icrop)%pltpfr3)*(1. - trc_phuacc &
     & /(trc_phuacc + Exp(plcp(icrop)%pup1 - plcp(icrop)%pup2 * trc_phuacc))) &
     &   + pldb(icrop)%pltpfr3

      up2local = 0.
      uapdlocal = 0.
      up2local = trc_pltfrp * trc_bioms
      if (up2local < trc_pltp) up2local = trc_pltp
      uapdlocal = up2local - trc_pltp
      uapdlocal = Min(4. * pldb(icrop)%pltpfr3 * trc_bioday, uapdlocal)
      uapdlocal = 1.5 * uapdlocal                         !! luxury p uptake

      tstrsp = 1.
      ir = 0
      if (uapdlocal < 1.e-6) return

      do l = 1, soil(j)%nly
        if (ir > 0) exit

        gx = 0.
        if (tsol_rd <= soil(j)%phys(l)%d) then
          gx = tsol_rd
          ir = 1
        else
          gx = soil(j)%phys(l)%d
        end if

        upmx = 0.
        uapl = 0.
        upmx = uapdlocal * (1. - Exp(-bsn_prm%p_updis * gx / tsol_rd)) / uobp
        uapl = Min(upmx - tpplnt, tsol_solp(l))
        tpplnt = tpplnt + uapl
        tsol_solp(l) = tsol_solp(l) - uapl
      end do
      if (tpplnt < 0.) tpplnt = 0.

      trc_pltp = trc_pltp + tpplnt

!! compute phosphorus stress
      call nuts(trc_pltp, up2local, tstrsp)

!! summary calculations
      if (time%yrs > pco%nyskip) then
!       wshd_pup = wshd_pup + tpplnt * hru_dafr(j) * trc_ha / (hru(j)%km*100)
      end if

      pplnt(j) = pplnt(j) + tpplnt * trc_ha / (hru(j)%km*100)

      return
      end

      subroutine nrain_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine adds nitrate from rainfall to the soil profile on 
!!    terrace segment

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    time%yrs       |none          |current year of simulation
!!    drydep_nh4  |kg/ha/yr      |atmospheric dry deposition of ammonia
!!    drydep_no3  |kg/ha/yr      |atmospheric dry deposition of nitrates
!!    hru_dafr(:) |none          |fraction of watershed in HRU
!!    ihru        |none          |HRU number
!!    rcn         |mg/L          |Concentration of nitrogen in the rainfall
!!    subp(:)     |mm H2O        |precipitation for the day in HRU
!!    tsol_nh3(:) |kg N/ha       |amount of nitrogen stored in the ammonium pool
!!                               |in soil layer
!!    tsol_no3(:) |kg N/ha       |amount of nitrogen stored in the nitrate pool
!!                               |in soil layer
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    hru_rn      |kg N/ha       |NO3 added to soil in terraced HRU
!!    tsol_nh3(:) |kg N/ha       |amount of nitrogen stored in the ammonium pool
!!                               |in soil layer
!!    tsol_no3(:) |kg N/ha       |amount of nitrogen stored in the nitrate pool
!!                               |in soil layer
!!    wshd_raino3 |kg N/ha       |average annual amount of NO3 added to soil
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    j           |none          |HRU number
!!    tno3pcp     |kg N/ha       |nitrate added to the soil in rainfall
!!    tnh3pcp     |kg N/ha       |ammonia added to the soil in rainfall
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j
      real :: tnh3pcp, tno3pcp

      j = 0
      j = ihru
      
!! calculate nitrate in precipitation
      tno3pcp = .01 * atmodep(iadep)%no3_rf * precipday
      tnh3pcp = .01 * atmodep(iadep)%nh4_rf * precipday
      tsol_no3(1)=tsol_no3(1)+tno3pcp+atmodep(iadep)%no3_dry/365.
      tsol_nh3(1)=tsol_nh3(1)+tnh3pcp+atmodep(iadep)%nh4_dry/365.

!! summary calculations
      if (time%yrs > pco%nyskip) then
!        wshd_raino3 = wshd_raino3 + tno3pcp * hru_dafr(j) *             &
!     &                                              trc_ha / (hru(j)%km*100)
        hru_rn = hru_rn + tno3pcp * trc_ha / (hru(j)%km*100)      !! for terraced hru balance test
      end if

      return
      end

      subroutine nup_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    This subroutine calculates plant nitrogen uptake

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units          |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    bio_n1(:)   |none           |1st shape parameter for plant N uptake
!!                                |equation
!!    bio_n2(:)   |none           |2nd shape parameter for plant N uptake
!!                                |equation
!!    idc(:)      |none           |crop/landcover category:
!!                                |1 warm season annual legume
!!                                |2 cold season annual legume
!!                                |3 perennial legume
!!                                |4 warm season annual
!!                                |5 cold season annual
!!                                |6 perennial
!!                                |7 trees
!!    idplt_trc(:,:)|none         |land cover code from crop.dat
!!    ihru        |none           |HRU number
!!    n_updis     |none           |nitrogen uptake distribution parameter
!!                                |This parameter controls the amount of
!!                                |nitrogen removed from the different soil 
!!                                |layers by the plant. In particular, this
!!                                |parameter allows the amount of nitrogen
!!                                |removed from the surface layer via plant
!!                                |uptake to be controlled. While the relation-
!!                                |ship between UBN and N removed from the
!!                                |surface layer is affected by the depth of the
!!                                |soil profile, in general, as UBN increases the
!!                                |amount of N removed from the surface layer
!!                                |relative to the amount removed from the entire
!!                                |profile increases
!!    nplnt(:)    |kg N/ha        |plant uptake of nitrogen in HRU for the day
!!    pltnfr(1,:) |kg N/kg biomass|nitrogen uptake parameter #1: normal fraction
!!                                |of N in crop biomass at emergence
!!    pltnfr(3,:) |kg N/kg biomass|nitrogen uptake parameter #3: normal fraction
!!                                |of N in crop biomass at maturity
!!    sol_nly(:)  |none           |number of soil layers in profile
!!    sol_z(:,:)  |mm             |depth to bottom of soil layer
!!    ticr        |none           |sequence number of crop grown within the
!!                                |current year
!!    tnro        |none           |sequence number of year in rotation
!!    trc_bioday  |kg             |biomass generated on current day in terrace segment
!!    trc_bioms   |kg/ha          |land cover/crop biomass (dry weight)
!!    trc_fixn    |kg N/ha        |amount of nitrogen added to soil via fixation
!!                                |on the day in terrace segment
!!    trc_phuacc  |none           |fraction of plant heat units accumulated
!!    trc_pltn    |kg N/ha        |amount of nitrogen in plant biomass
!!    tsol_no3(:) |kg N/ha        |amount of nitrogen stored in the
!!                                |nitrate pool.
!!    uobn        |none           |nitrogen uptake normalization parameter
!!                                |This variable normalizes the nitrogen uptake
!!                                |so that the model can easily verify that
!!                                |upake from the different soil layers sums to
!!                                |1.0
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    nplnt(:)    |kg N/ha       |plant uptake of nitrogen in HRU for the day
!!    pltfr_n(:)  |none          |fraction of plant biomass that is nitrogen
!!    tnplnt      |kg N/ha       |plant uptake of nitrogen in terrace segment for the day
!!    trc_pltn    |kg N/ha       |amount of nitrogen in plant biomass
!!    tsol_no3(:) |kg N/ha       |amount of nitrogen stored in the nitrate pool
!!                               |in the layer
!!    tstrsn      |none          |fraction of potential plant growth achieved on
!!                               |the day where the reduction is caused by
!!                               |nitrogen stress
!!    tuno3d      |kg N/ha       |plant nitrogen deficiency for day in terrace segment
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    gx          |mm            |lowest depth in layer from which nitrogen
!!                               |may be removed
!!    icrop       |none          |land cover code
!!    ir          |none          |flag to denote bottom of root zone reached
!!    j           |none          |HRU number
!!    l           |none          |counter (soil layer)
!!    un2local         |kg N/ha       |ideal plant nitrogen content
!!    unmx        |kg N/ha       |maximum amount of nitrogen that can be 
!!                               |removed from soil layer
!!    uno3l       |kg N/ha       |amount of nitrogen removed from soil layer
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Exp, Min
!!    SWAT: nfix_trc, nuts

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      implicit none
      integer :: j, icrop, l, ir
      real :: un2local, unmx, uno3l, gx, xx, amax1

      j = 0
      j = ihru

      icrop = 0
      icrop = idplt_trc(tnro,ticr)
      trc_pltfrn = (pldb(icrop)%pltnfr1 - pldb(icrop)%pltnfr3)*(1. - trc_phuacc &
     & /(trc_phuacc + Exp(plcp(icrop)%nup1 - plcp(icrop)%nup2 * trc_phuacc))) &
     &   + pldb(icrop)%pltnfr3

      un2local = 0.
      un2local = trc_pltfrn * trc_bioms
      if (un2local < trc_pltn) un2local = trc_pltn
      tuno3d = un2local - trc_pltn
      tuno3d = Min(4. * pldb(icrop)%pltnfr3 * trc_bioday, tuno3d)

      tstrsn = 1.
      ir = 0
      if (tuno3d < 1.e-6) return

      do l = 1, soil(j)%nly
        if (ir > 0) exit

        gx = 0.
        if (tsol_rd <= soil(j)%phys(l)%d) then
          gx = tsol_rd
          ir = 1
        else
          gx = soil(j)%phys(l)%d
        end if

        unmx = 0.
        uno3l = 0.
        unmx = tuno3d * (1. - Exp(-bsn_prm%n_updis * gx / tsol_rd)) / uobn
        uno3l = Min(unmx - tnplnt, tsol_no3(l))
        tnplnt = tnplnt + uno3l 
        tsol_no3(l) = tsol_no3(l) - uno3l
      end do
      if (tnplnt < 0.) tnplnt = 0.

!! if crop is a legume, call nitrogen fixation routine
      select case (pldb(idplt_trc(tnro,ticr))%idc)
        case (1,2,3)
          call nfix_trc
      end select

      tnplnt = tnplnt + trc_fixn
      trc_pltn = trc_pltn + tnplnt
 
!! compute nitrogen stress
      select case (pldb(idplt_trc(tnro,ticr))%idc)
        case (1,2,3)
          tstrsn = 1.
        case default
          call nuts(trc_pltn,un2local,tstrsn)
          if (tuno3d > 1.e-5) then
            xx = tnplnt / tuno3d
          else
            xx = 1.
          end if
          tstrsn = amax1(tstrsn, xx)
          tstrsn = amin1(tstrsn, 1.)
      end select

      nplnt(j) = nplnt(j) + tnplnt * trc_ha / (hru(j)%km*100)

      return
      end


      subroutine operatn_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine performs all management operations on terrace segment           

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    dayl(:)     |hours         |day length for current day
!!    daylmn(:)   |hours         |shortest daylength occurring during the
!!                               |year
!!    dormhr(:)   |hours         |time threshold used to define dormant
!!                               |period for plant (when daylength is within
!!                               |the time specified by dormhr from the minimum
!!                               |daylength for the area, the plant will go
!!                               |dormant)
!!    idc(:)      |none          |crop/landcover category:
!!                               |1 warm season annual legume
!!                               |2 cold season annual legume
!!                               |3 perennial legume
!!                               |4 warm season annual
!!                               |5 cold season annual
!!                               |6 perennial
!!                               |7 trees
!!    idplt_trc(:,:)|none        |land cover code from crop.dat
!!    igro_trc    |none          |land cover status code:
!!                               |0 no land cover currently growing
!!                               |1 land cover growing
!!    ihru        |none          |HRU number
!!    ihv_trc(:,:)  |julian date |date of harvest and kill operation
!!    ihvo_trc(:,:) |julian date |date of harvest operation
!!    iida        |julian date   |day being simulated (current julian date)
!!    ikill_trc(:,:)|julian date |date of kill operation
!!    iop_trc(:,:)  |julian date |date of tillage operation
!!    iplant_trc(:,:)|julian date|date of planting/beginning of growing
!!                               |season
!!    phubase(:)  |heat units    |base zero total heat units (used when no
!!                               |land cover is growing
!!    phuh_trc(:,:)|none         |fraction of plant heat units at which
!!                               |harvest and kill operation occurs
!!    phuho_trc(:,:)|none        |fraction of plant heat units at which 
!!                               |harvest operation occurs
!!    phuk_trc(:,:) |none        |fraction of plant heat units at which
!!                               |kill operation occurs
!!    phup_trc(:,:) |none        |fraction of base zero heat units at which
!!                               |planting occurs
!!    phut_trc(:,:) |none        |fraction of heat units (base zero or plant)
!!                               |at which tillage occurs
!!    ticr        |none          |sequence number of crop grown within a year
!!    tncut       |none          |sequence number of harvest operation within
!!                               |current year
!!    tnro        |none          |sequence number of year in rotation
!!    tntil       |none          |sequence number of tillage operation within
!!                               |current year
!!    trc_phuacc  |none          |fraction of plant heat units accumulated
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    aphu        |heat units    |fraction of total heat units accumulated
!!    j           |none          |HRU number
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    SWAT: plantop_trc, dormant_trc, harvkillop_trc, harvestop_trc
!!    SWAT: killop_trc, newtillmix_trc

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use climate_parms
      use basin_module
      use hydrograph_module
      use jrw_datalib_module
      
      integer :: j
      real :: aphu, tillphu

      j = 0
      j = ihru
      iob = hru(j)%obj_no
      iwst = ob(iob)%wst
      iwgn = wst(iwst)%wco%wgn


!! operations performed only when no land cover growing
      if (igro_trc == 0) then

        !! plant operation
        if (iplant_trc(tnro,ticr+1) > 0) then
          if (iida == iplant_trc(tnro,ticr+1)) then
            call plantop_trc
	        if (imgt_trc == 1) then
                write (145,1000) j, iyr, i_mo, iida, "     PLANT"
	        endif
          end if
        else if (phup_trc(tnro,ticr+1) > 0.) then
          if (phubase(j) > phup_trc(tnro,ticr+1))  then
            call plantop_trc
	        if (imgt_trc == 1) then
                write (145,1000) j, iyr, i_mo, iida, "     PLANT"
	        end if
          end if
        end if

      end if


!! operations performed only when land cover is growing
      if (igro_trc == 1) then

        !! check if plant going into or coming out of dormancy
        call dormant_trc

        !! check if end of annual growing season
        iwgn = wst(iwst)%wco%wgn
        if (wgn_pms(iwgn)%daylth-dormhr(j) < wgn_pms(iwgn)%daylmn .and.                &
     &                                         trc_phuacc > 0.75)  then
          select case (pldb(idplt_trc(tnro,ticr))%idc)
            case (1, 4, 5)
              call harvkillop_trc
	          if (imgt_trc == 1) then
                  write (145,1000) j, iyr, i_mo, iida, "  END GROW"
	          end if
          end select
        end if

        !! harvest and kill operation
        if (ihv_trc(tnro,tncut) > 0) then
          if (iida == ihv_trc(tnro,ticr)) then
            call harvkillop_trc
	        if (imgt_trc == 1) then
                write (145,1000) j, iyr, i_mo, iida, " HARV&KILL"
	        end if
          end if
        else
          if (trc_phuacc > phuh_trc(tnro,ticr)) then
            call harvkillop_trc
	        if (imgt_trc == 1) then
                write (145,1000) j, iyr, i_mo, iida, " HARV&KILL"
	        end if
          end if
        end if

        !! harvest operation (no kill)
        do 
        if (ihvo_trc(tnro,tncut) > 0) then
          if (iida == ihv_trc(tnro,tncut)) then
            if (ihv_gbm_trc(tnro,tncut) == 0) then
              call harvestop_trc
	          if (imgt_trc == 1) then
	            write (145,1000) j, iyr, i_mo, iida, "   HARVEST"
	          end if
            else 
              call harvgrainop_trc
            end if 
          else
            exit
          end if
          else
            if (trc_phuacc > phuho_trc(tnro,tncut)) then
              if (ihv_gbm_trc(tnro,tncut) == 0) then
                call harvestop_trc
	            if (imgt_trc == 1) then
		          write (145,1000) j, iyr, i_mo, iida, "   HARVEST"
	            end if
              else 
                call harvgrainop_trc
              end if 
            else
              exit
            end if
	    end if 
        end do 
	  

        !! kill operation
        if (ikill_trc(tnro,ticr) > 0) then
          if (iida == ikill_trc(tnro,ticr)) then
            call killop_trc
	        if (imgt_trc == 1) then
                write (145,1000) j, iyr, i_mo, iida, "      KILL"
	        end if
          end if
        else
          if (trc_phuacc > phuk_trc(tnro,ticr)) then
            call killop_trc
	        if (imgt_trc == 1) then
                write (145,1000) j, iyr, i_mo, iida, "      KILL"
	        end if
          end if 
        end if
      end if


!! operations performed at any time
      !! multiple tillage operation may be scheduled on same day

      !! scheduling by date
      if (iida == iop_trc(tnro,tntil)) then
        call newtillmix_trc(j,0.)
	    if (imgt_trc == 1) then
            write (145,1000) j, iyr, i_mo, iida, "   TILLAGE"
	    end if
      end if

      !! scheduling by heat unit
      aphu = 0.
      if (igro_trc == 0) then
        aphu = phubase(j)
        tillphu = phut_nocr_trc(tnro,tntil)
      else
        aphu = trc_phuacc
        tillphu = phut_trc(tnro,tntil)
      end if

      if (tillphu > 0. .and. aphu > tillphu) then
        call newtillmix_trc(j,0.)
	    if (imgt_trc == 1) then
            write (145,1000) j, iyr, i_mo, iida, "   TILLAGE"
	    end if
        if (igro_trc == 0) then
          aphu = phubase(j)
          tillphu = phut_nocr_trc(tnro,tntil)
        else
          aphu = trc_phuacc
          tillphu = phut_trc(tnro,tntil)
        end if
      end if
          
 1000 format(4i10,a10)
      return
      end

      subroutine orgncswat_trc

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine calculates the amount of organic nitrogen removed in
!!    surface runoff - when using CSWAT it excludes sol_aorgn, uses only sol_n = sol_orgn, 
!!    and includes sol_mn (nitrogen in manure)


!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name          |units        |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    enratio       |none         |enrichment ratio calculated for day in HRU
!!    ihru          |none         |HRU number
!!    sol_bd(:,:)   |Mg/m**3      |bulk density of the soil
!!    sol_z(:,:)    |mm           |depth to bottom of soil layer
!!    trcsed        |metric tons  |sediment generated on terrace segment during day
!!    tsol_fon(:)   |kg N/ha      |amount of nitrogen stored in the fresh
!!                                |nitrogen pool forterrace segment
!!    tsol_mn(:)    |kg N/ha      |amount of nitrogen stored in the active
!!                                |organic N pool
!!    tsol_n(:)     |kg N/ha      |amount of nitrogen stored in the stable
!!                                |organic N pool
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name          |units        |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tsedorgn      |kg N/ha      |amount of organic nitrogen in surface runoff
!!                                |on terrace segment for the day
!!    tsol_fon(:)   |kg N/ha      |amount of nitrogen stored in the fresh
!!                                |nitrogen pool forterrace segment
!!    tsol_mn(:)    |kg N/ha      |amount of nitrogen stored in the active
!!                                |organic N pool
!!    tsol_n(:)     |kg N/ha      |amount of nitrogen stored in the stable
!!                                |organic N pool
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    conc        |              |concentration of organic N in soil
!!    er          |none          |enrichment ratio
!!    j           |none          |HRU number
!!    wt1         |none          |conversion factor (mg/kg => kg/ha)
!!    xx          |kg N/ha       |amount of organic N in first soil layer
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j
      real :: xx, wt1, er, conc, xx1

      j = 0
      j = ihru

      xx = 0.
	wt1 = 0.  !! conversion factor
      er = 0.	!! enrichment ratio
     
      !! terrace calculations
      xx = tsol_n(1) + tsol_fon(1) + tsol_mn(1)
      wt1 = soil(j)%phys(1)%bd * soil(j)%phys(1)%d / 100.

      er = enratio

      conc = 0.
      conc = xx * er / wt1

      tsedorgn = .001 * conc * trcsed / trc_ha

	!! update soil nitrogen pools only for HRU calculations
      if (xx > 1.e-6) then
        xx1 = (1. - tsedorgn / xx)
		tsol_n(1) = tsol_n(1) * xx1
		tsol_fon(1) = tsol_fon(1) * xx1
		tsol_mn(1) = tsol_mn(1) * xx1
       if (tsol_n(1) < 0.) then
         tsedorgn = tsedorgn + tsol_n(1)
         tsol_n(1) = 0.
       end if

       if (tsol_mn(1) < 0.) then
         tsedorgn = tsedorgn + tsol_mn(1)
         tsol_mn(1) = 0.
       end if

       if (tsol_fon(1) < 0.) then
         tsedorgn = tsedorgn + tsol_fon(1)
         tsol_fon(1) = 0.
       end if
      end if

      return
      end

      subroutine orgn_trc

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine calculates the amount of organic nitrogen removed from 
!!    terrace segment in surface runoff

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name          |units        |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    da_ha         |ha           |area of watershed in hectares
!!    enratio       |none         |enrichment ratio calculated for day in HRU
!!    erorgn(:)     |none         |organic N enrichment ratio, if left blank
!!                                |the model will calculate for every event
!!    ihru          |none         |HRU number
!!    sol_bd(:,:)   |Mg/m**3      |bulk density of the soil
!!    sol_z(:,:)    |mm           |depth to bottom of soil layer
!!    trcsed        |metric tons  |daily soil loss caused by water erosion in
!!                                |terrace segment
!!    tsol_aorgn(:) |kg N/ha      |amount of nitrogen stored in the active
!!                                |organic (humic) nitrogen pool
!!    tsol_fon(:)   |kg N/ha      |amount of nitrogen stored in the fresh
!!                                |organic (residue) pool
!!    tsol_orgn(:)  |kg N/ha      |amount of nitrogen stored in the stable
!!                                |organic N pool
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name          |units        |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tsedorgn      |kg N/ha      |amount of organic nitrogen in surface runoff
!!                                |in terrace segment for the day
!!    tsol_aorgn(:) |kg N/ha      |amount of nitrogen stored in the active
!!                                |organic (humic) nitrogen pool
!!    tsol_fon(:)   |kg N/ha      |amount of nitrogen stored in the fresh
!!                                |organic (residue) pool
!!    tsol_orgn(:)  |kg N/ha      |amount of nitrogen stored in the stable
!!                                |organic N pool
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    conc        |              |concentration of organic N in soil
!!    er          |none          |enrichment ratio
!!    j           |none          |HRU number
!!    wt1         |none          |conversion factor (mg/kg => kg/ha)
!!    xx          |kg N/ha       |amount of organic N in first soil layer
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j
      real :: xx, wt1, er, conc

      j = 0
      j = ihru

      xx = 0.
	wt1 = 0.    !! conversion factor
      er = 0.		!! enrichment ratio

        !! HRU calculations
        xx = tsol_orgn(1) + tsol_aorgn(1) + tsol_fon(1)
        wt1 = soil(j)%phys(1)%bd * soil(j)%phys(1)%d / 100.

        if (hru(j)%hyd%erorgn > .001) then
          er = hru(j)%hyd%erorgn
        else
          er = enratio
        end if

      conc = 0.
      conc = xx * er / wt1

      tsedorgn = .001 * conc * trcsed / trc_ha

	!! update soil nitrogen pools only for HRU calculations
      if (xx > 1.e-6) then
       tsol_aorgn(1) = tsol_aorgn(1) - tsedorgn * (tsol_aorgn(1) / xx)
       tsol_orgn(1) = tsol_orgn(1) - tsedorgn * (tsol_orgn(1) / xx)
       tsol_fon(1) = tsol_fon(1) - tsedorgn * (tsol_fon(1) / xx)

       if (tsol_aorgn(1) < 0.) then
         tsedorgn = tsedorgn + tsol_aorgn(1)
         tsol_aorgn(1) = 0.
       end if

       if (tsol_orgn(1) < 0.) then
         tsedorgn = tsedorgn + tsol_orgn(1)
         tsol_orgn(1) = 0.
       end if

       if (tsol_fon(1) < 0.) then
         tsedorgn = tsedorgn + tsol_fon(1)
         tsol_fon(1) = 0.
       end if
      end if

      return
      end subroutine

      subroutine percmacro_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this surboutine computes percolation by crack flow

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    inflpcp     |mm H2O        |amount of precipitation that infiltrates
!!                               |into soil (enters soil)
!!    ihru        |none          |HRU number
!!    sol_fc(:,:) |mm H2O        |amount of water available to plants in soil
!!                               |layer at field capacity (fc-wp)
!!    sol_nly(:)  |none          |numer of layers in soil profile
!!    sol_st(:,:) |mm H2O        |amount of water stored in the soil layer on
!!                               |any given day (less wilting point water)
!!    sol_z(:,:)  |mm            |depth to bottom of soil layer
!!    volcr(:,:)  |mm            |crack volume for soil layer
!!    volcrmin    |mm            |minimum soil volume in profile
!!    tvoltot     |mm            |total volume of cracks expressed as depth
!!                               |per unit area
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tcrk         |mm H2O        |percolation due to crack flow
!!    sepbtm(:)   |mm H2O        |percolation from bottom of soil profile for
!!                               |the day in HRU
!!    sol_prk(:,:)|mm H2O        |percolation storage array
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    crklch      |none          |
!!    j           |none          |HRU number
!!    ly          |none          |counter (soil layer)
!!    tsepcrk      |mm H2O        |water entering cracks in soil
!!    xx          |mm H2O        |water deficiency in soil layer
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Min

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none

      integer :: j, ly
      real :: crklch = 0.5, xx

      j = 0
      j = ihru

      tsepcrk = 0.
      tsepcrk = Min(tvoltot, inftrc)
      tsepcrktot = tsepcrk
      if (tsepcrk > 1.e-4) then
        do ly = soil(j)%nly, 1, -1
          tcrk = 0.
          xx = 0.
          if (ly == soil(j)%nly) then
            tcrk = crklch*(soil(j)%ly(ly)%volcr/(soil(j)%phys(ly)%d -          &
                soil(j)%phys(ly-1)%d) * tvoltot - volcrmin)
            if (tcrk < tsepcrk) then
              tsepcrk = tsepcrk - tcrk
              sepbtm(j) = sepbtm(j) + tcrk * trc_ha / (hru(j)%km*100)
              tsol_prk(ly) = tsol_prk(ly) + tcrk
            else
              sepbtm(j) = sepbtm(j) + tsepcrk * trc_ha / (hru(j)%km*100)
              tsol_prk(ly) = tsol_prk(ly) + tsepcrk
              tsepcrk = 0.
            end if
          endif
          xx = soil(j)%phys(ly)%fc - tsol_st(ly)
          if (xx > 0.) then
            tcrk = Min(tsepcrk, xx)
            tsol_st(ly) = tsol_st(ly) + tcrk
            tsepcrk = tsepcrk - tcrk
            if (ly /= 1) tsol_prk(ly-1) = tsol_prk(ly-1) + tcrk
          end if
          if (tsepcrk < 1.e-6) exit
        end do

        !! if soil layers filled and there is still water attributed to
        !! crack flow, it is assumed to percolate out of bottom of profile
        if (tsepcrk > 1.e-4) then
          sepbtm(j) = sepbtm(j) + tsepcrk * trc_ha / (hru(j)%km*100)
          tsol_prk(soil(j)%nly) = tsol_prk(soil(j)%nly) + tsepcrk
        end if
      end if

      return
      end

      subroutine percmain_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine calculates the soil water movement in terrace segment 
!!    soil profile.

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    hru_sub(:)  |none          |subbasin in which HRU is located
!!    ihru        |none          |HRU number
!!    inftrc      |mm H2O        |amount of precipitation that infiltrates
!!                               |into soil (enters soil)
!!    slsoil(:)   |m             |slope length for lateral subsurface flow
!!    sol_fc(:,:) |mm H2O        |amount of water available to plants in soil
!!                               |layer at field capacity (fc - wp)
!!    sol_nly(:)  |none          |number of layers in soil profile
!!    sol_ul(:,:) |mm H2O        |amount of water held in the soil layer at
!!                               |saturation
!!    tsol_st(:)  |mm H2O        |amount of water stored in the soil layer on
!!                               |the current day (less wp water)
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tlatlyr     |mm H2O        |lateral flow in soil layer for the day
!!    trcbtm      |mm H2O        |percolation from bottom of soil profile for
!!                               |the day in terrace segment
!!    tsol_flat(:)|mm H2O        |lateral flow storage array for terrace segment
!!    tsol_prk(:) |mm H2O        |percolation storage array for terrace segment
!!    tsol_st(:)  |mm H2O        |amount of water stored in the soil layer on
!!                               |the current day (less wp water)
!!    tsol_sw     |mm H2O        |amount of water stored in the soil profile
!!                               |on current day
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    adjf         |none          |adjustment factor for lateral flow
!!    dg           |mm            |depth of soil layer
!!    ho           |none          |variable to hold intermediate calculation
!!                                |result
!!    j            |none          |HRU number
!!    j1           |none          |soil layer number
!!    ratio        |none          |ratio of seepage to (latq + sepday)
!!    yy           |mm            |depth to top of soil layer
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Max

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j, j1, ly, nn
      real :: trcsep, tsw_excess, tlatlyr, adjf, yy, dg, ho
      real :: ul_excess, ratio, xx

      j = 0
      j = ihru

      trcsep = 0.
      trcsep = inftrc + trc_aird
      trc_aird = 0.
      
      !! calculate crack flow 2014-12-28 20:40:17
      if (bsn_cc%crk == 1) then 
	    call percmacro_trc
	    trcsep = trcsep - tsepcrktot
	endif

      do j1 = 1, soil(j)%nly
        !! add water moving into soil layer from overlying layer
        tsol_st(j1) = tsol_st(j1) + trcsep
        
       !! determine gravity drained water in layer
        tsw_excess = 0.
        tsw_excess = tsol_st(j1) - soil(j)%phys(j1)%fc

        !! initialize variables for current layer
        trcsep = 0.
        tlatlyr = 0.

        if (tsw_excess > 1.e-5) then
             adjf = 1.

          !! if temperature of layer is 0 degrees C or below
          !! there is no water flow
          if (soil(j)%phys(j1)%tmp > 1.e-5) then
            if (j1 == 1) then
              yy = 0.
            else
              yy = 0.
              yy = soil(j)%phys(j1-1)%d
            end if

            dg = 0.
            ho = 0.
            tlatlyr = 0.
            dg = soil(j)%phys(j1)%d - yy
            if (soil(j)%phys(j1)%ul - soil(j)%phys(j1)%fc == 0.) then
              ho = 0.
            else
              ho = 2. * tsw_excess / ((soil(j)%phys(j1)%ul - soil(j)%phys(j1)%fc)     &
     &             / dg)
            end if
            tlatlyr = adjf * ho * soil(j)%phys(j1)%k * hru(j)%topo%slope / hru(j)%topo%lat_len*.024
            if (tlatlyr < 0.) tlatlyr = 0. 
            if (tlatlyr > tsw_excess) tlatlyr = tsw_excess

          !! compute seepage to the next layer
          trcsep = 0.
          trcsep = tsw_excess * (1. - Exp(-24. / soil(j)%phys(j1)%hk))
      
          !! restrict seepage if next layer is saturated
          if (j1 == soil(j)%nly) then
            xx = (hru(j)%hyd%dep_imp - soil(j)%phys(j1)%d) / 1000.
            if (xx < 1.e-4) then
              trcsep = 0.
            else
              trcsep = trcsep * xx / (xx + Exp(8.833 - 2.598 * xx))
            end if
          end if

          !! check mass balance
          if (trcsep + tlatlyr > tsw_excess) then
            ratio = 0.
            ratio = trcsep / (tlatlyr + trcsep)
            trcsep = 0.
            tlatlyr = 0.
            trcsep = tsw_excess * ratio
            tlatlyr = tsw_excess * (1. - ratio)
          end if
        else
          trcsep = 0.        
        end if
        tsol_st(j1) = tsol_st(j1) - trcsep - tlatlyr
        tsol_st(j1) = Max(1.e-6,tsol_st(j1))

      end if

      if (j1 < soil(j)%nly) then
        if (tsol_st(j1) - soil(j)%phys(j1)%ul > 1.e-4) then
          trcsep = trcsep + (tsol_st(j1) - soil(j)%phys(j1)%ul)
          tsol_st(j1) = soil(j)%phys(j1)%ul
        end if
      else
        if (tsol_st(j1) - soil(j)%phys(j1)%ul > 1.e-4) then
          ul_excess = tsol_st(j1) - soil(j)%phys(j1)%ul
          tsol_st(j1) = soil(j)%phys(j1)%ul
          nn = soil(j)%nly
          do ly = nn - 1, 1, -1
            tsol_st(ly) = tsol_st(ly) + ul_excess
            
            if (tsol_st(ly) > soil(j)%phys(ly)%ul) then
              ul_excess = tsol_st(ly) - soil(j)%phys(ly)%ul
              tsol_st(ly) = soil(j)%phys(ly)%ul
            else
              ul_excess = 0.
              exit
            end if
            
            if (ly == 1 .and. ul_excess > 0.) then
              trcq = trcq + ul_excess
              ul_excess = 0.
            end if

          end do
        end if
      end if

      if (j1 == soil(j)%nly) then
        sepbtm(j) = sepbtm(j) + trcsep * trc_ha / (hru(j)%km*100)
      end if

      trclatq = trclatq + tlatlyr
      tsol_flat(j1) = tlatlyr
      tsol_prk(j1) = trcsep

      if (trclatq < 1.e-5) trclatq = 0.
      end do

      return
      end

      subroutine pkq_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine computes the peak runoff rate for terrace segment

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name         |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    al5          |none          |fraction of daily rainfall that occurs
!!                                |during 0.5h highest intensity
!!    ihru         |none          |HRU number
!!    trc_ha       |ha            |area of terrace segment in ha
!!    trc_no       |none          |numbers of terrace unit in HRU
!!    trc_sll      |m/m           |slope length of terrace segment
!!    trc_slp      |m/m           |slope steepness of terrace segment
!!    trcq         |mm H2O        |surface runoff of terrace segment
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name         |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tpeakr        |m^3/s         |peak runoff rate on terrace segment
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name         |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    altc         |
!!    j            |none          |HRU number
!!    trct_ov      |hr            |concentration time of terrace segment flow
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Log, Expo, Ceiling, floor

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j
      real :: altc, trct_ov, expo

      j = 0
      j = ihru

      altc = 0.
      trct_ov = 0.
      
        !! calculate the concentration time of terrace segment flow
        trct_ov = .0556 * (trc_sll*trc_n) ** .6 / trc_slp ** .3

        !! terrace segment peak flow 
        altc = 1. - Expo(2. * trct_ov * Log(1. - al5))
        tpeakr = altc * trcq / trct_ov           !! mm/h
        tpeakr = tpeakr * trc_ha / 360 / trc_no(j)       !! m^3/s

      return
      end


      subroutine plantmod_trc

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine predicts terrace segment daily potential growth 
!!    of total plant biomass and roots and calculates leaf area index. 
!!    Incorporates residue for tillage functions and decays residue on 
!!    ground surface. Adjusts daily dry matter based on water stress.

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    igro_trc    |none          |land cover status code:
!!                               |0 no land cover currently growing
!!                               |1 land cover growing
!!    ihru        |none          |HRU number
!!    tmpav(:)    |deg C         |average temperature for the day in HRU
!!    trc_bioms   |kg/ha         |land cover/crop biomass (dry weight)
!!    tsol_rsd(:) |kg/ha         |amount of organic matter in the soil layer
!!                               |classified as residue
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tsol_cov    |kg/ha         |amount of residue on soil surface
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    j           |none          |HRU number
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Max
!!    SWAT: operatn_trc, swu_trc, grow_trc, curno_trc

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j
      
      j = 0
      j = ihru

!$$$$$$       !! update base zero total heat units
!$$$$$$       if (tmpav(j) > 0. .and. phutot(hru_sub(j)) > 0.01) then
!$$$$$$          phubase(j) = phubase(j) + tmpav(j) / phutot(hru_sub(j))
!$$$$$$       end if

      !! calculate residue on soil surface for current day
      tsol_cov = .8 * trc_bioms + tsol_rsd(1)
      tsol_cov = Max(tsol_cov,0.)

      !! perform management operations
      if (tnro > 0) call operatn_trc

      !! compute plant water use and water stress
      !! compute actual plant transpiration
      if (igro_trc == 1) call swu_trc
 
      if (igro_trc == 1) call grow_trc
!     write (99,9994) i, hru_ra(j), bio_ms(j), laiday(j),
!    * strsw(j), strstmp(j), strsn(j), strsp(j)
!9994  format (i4,7f10.2)

      return
      end

      subroutine plantop_trc

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine performs the plant operation on terrace segment

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name           |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    trc_inilai(:,:)|none          |initial leaf area index of transplants
!!    trc_inibio(:,:)|kg/ha         |initial biomass of transplants
!!    trc_cnop(:,:)  |none          |SCS runoff curve number for moisture 
!!                                  |condition II
!!    ticr           |none          |sequence number of crop grown within the
!!                                  |current year
!!    ihru           |none          |HRU number
!!    tnro           |none          |sequence number of year in rotation
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    trc_bioms   |kg/ha         |land cover/crop biomass (dry weight)
!!    thvstiadj   |(kg/ha)/(kg/ha)|optimal harvest index for current time during
!!                               |growing season
!!    ticr        |none          |sequence number of crop grown within the
!!                               |current year
!!    idorm_trc   |none          |dormancy status code:
!!                               |0 land cover growing (not dormant)
!!                               |1 land cover dormant
!!    igro_trc    |none          |land cover status code:
!!                               |0 no land cover currently growing
!!                               |1 land cover growing
!!    laiday_trc  |m**2/m**2     |leaf area index
!!    trc_laimxfr |
!!    trc_olai    |
!!    trc_phuacc  |none          |fraction of plant heat units accumulated
!!    trc_pltn    |kg N/ha       |amount of nitrogen in plant biomass
!!    trc_pltp    |kg P/ha       |amount of phosphorus in plant biomass
!!    trc_pltet   |mm H2O        |actual ET simulated during life of plant
!!    trc_pltpet  |mm H2O        |potential ET simulated during life of plant
!!    trc_rwt     |none          |fraction of total plant biomass that is
!!                               |in roots
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    j           |none          |HRU number
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    SWAT: curno

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j
      
      j = 0
      j = ihru

      igro_trc = 1
      idorm_trc = 0
      trc_phuacc = 0.
      trc_pltn = 0.
      trc_pltn = 0.
      trc_pltet = 0.
      trc_pltpet = 0.                                          
      trc_laimxfr = 0.
      thvstiadj = 0.
      trc_olai = 0.
      trc_rwt = 0.
      ticr = ticr + 1

      !! initialize transplant variables
      if (trc_inilai(tnro,ticr) > 0.) then
          laiday_trc = trc_inilai(tnro,ticr)
          trc_bioms = trc_inibio(tnro,ticr)
      endif

      !! reset curve number if given in .mgt file
      if (trc_cnop(tnro,ticnop) > 0.) then
        call curno_trc(trc_cnop(tnro,ticnop),j)
      end if
      ticnop = ticnop + 1


      return
      end

      subroutine pminrl_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine computes p flux between the labile, active mineral
!!    and stable mineral p pools on terrace segment.     

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name         |units         |definition  
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    time%yrs        |none          |current year of simulation
!!    hru_dafr(:)  |km**2/km**2   |fraction of watershed area in HRU
!!    ihru         |none          |HRU number
!!    pco%nyskip       |none          |number of years to skip output summarization
!!                                |and printing
!!    psp          |none          |Phosphorus availability index. The fraction
!!                                |of fertilizer P remaining in labile pool
!!                                |after initial rapid phase of P sorption
!!    sol_nly(:)   |none          |number of layers in soil profile
!!    tsol_actp(:) |kg P/ha       |amount of phosphorus stored in the
!!                                |active mineral phosphorus pool
!!    tsol_solp(:) |kg P/ha       |amount of phosohorus stored in solution
!!    tsol_stap(:) |kg P/ha       |amount of phosphorus in the soil layer
!!                                |stored in the stable mineral phosphorus pool
!!    wshd_pal     |kg P/ha       |average annual amount of phosphorus moving
!!                                |from labile mineral to active mineral pool
!!                                |in watershed
!!    wshd_pas     |kg P/ha       |average annual amount of phosphorus moving
!!                                |from active mineral to stable mineral pool
!!                                |in watershed
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name         |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    trmp1tl      |kg P/ha       |amount of phosphorus moving from the labile
!!                                |mineral pool to the active mineral pool in
!!                                |the soil profile on the current day in the
!!                                |terrace segment
!!    troctl       |kg P/ha       |amount of phosphorus moving from the active
!!                                |mineral pool to the stable mineral pool
!!                                |in the soil profile on the current day in
!!                                |the terrace segment
!!    tsol_actp(:) |kg P/ha       |amount of phosphorus stored in the
!!                                |active mineral phosphorus pool
!!    tsol_solp(:) |kg P/ha       |amount of phosohorus stored in solution
!!    tsol_stap(:) |kg P/ha       |amount of phosphorus in the soil layer
!!                                |stored in the stable mineral phosphorus pool
!!    wshd_pal     |kg P/ha       |average annual amount of phosphorus moving
!!                                |from labile mineral to active mineral pool
!!                                |in watershed
!!    wshd_pas     |kg P/ha       |average annual amount of phosphorus moving
!!                                |from active mineral to stable mineral pool
!!                                |in watershed
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    bk          |
!!    j           |none          |HRU number
!!    l           |none          |counter (soil layer)
!!    rmn1        |kg P/ha       |amount of phosphorus moving from the solution
!!                               |mineral to the active mineral pool in the
!!                               |soil layer
!!    roc         |kg P/ha       |amount of phosphorus moving from the active
!!                               |mineral to the stable mineral pool in the 
!!                               |soil layer
!!    rto         |
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Min

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      real, parameter :: bk = .0006
      integer :: j, l
      real :: rto, rmn1, roc

      j = 0
      j = ihru

      rto = 0.
      rto = bsn_prm%psp / (1.-bsn_prm%psp)

      do l = 1, soil(j)%nly
        rmn1 = 0.
        rmn1 = (tsol_solp(l) - tsol_actp(l) * rto)
!!  mike changed/added per isabelle beaudin's email from 01/21/09
        if (rmn1 > 0.) rmn1 = rmn1 * 0.1
        if (rmn1 < 0.) rmn1 = rmn1 * 0.6
!!  mike changed/added per isabelle beaudin's email from 01/21/09
        rmn1 = Min(rmn1, tsol_solp(l))

        roc = 0.
        roc = bk * (4. * tsol_actp(l) - tsol_stap(l))
        if (roc < 0.) roc = roc * .1
        roc = Min(roc, tsol_actp(l))

        tsol_stap(l) = tsol_stap(l) + roc
        if (tsol_stap(l) < 0.) tsol_stap(l) = 0.

        tsol_actp(l) = tsol_actp(l) - roc + rmn1
        if (tsol_actp(l) < 0.) tsol_actp(l) = 0.

        tsol_solp(l) = tsol_solp(l) - rmn1
        if (tsol_solp(l) < 0.) tsol_solp(l) = 0.

        if (time%yrs > pco%nyskip) then
!          wshd_pas = wshd_pas + roc * hru_dafr(j) * trc_ha / (hru(j)%km*100)
!          wshd_pal = wshd_pal + rmn1 * hru_dafr(j) * trc_ha / (hru(j)%km*100)
          troctl = troctl + roc
          trmp1tl = trmp1tl + rmn1
        end if 

      end do

      return
      end

      subroutine psed_trc
  
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine calculates the amount of organic and mineral phosphorus
!!    attached to sediment in surface runoff on terrace segment

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name          |units        |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    enratio       |none         |enrichment ratio calculated for day in HRU
!!    ihru          |none         |HRU number
!!    sol_bd(:,:)   |Mg/m**3      |bulk density of the soil
!!    sol_z(:,:)    |mm           |depth to bottom of soil layer
!!    trcsed        |metric tons  |daily soil loss caused by water erosion in
!!                                |HRU
!!    tsol_actp(:)  |kg P/ha      |amount of phosphorus stored in the
!!                                |active mineral phosphorus pool
!!    tsol_fop(:)   |kg P/ha      |amount of phosphorus stored in the fresh
!!                                |organic (residue) pool
!!    tsol_orgp(:)  |kg P/ha      |amount of phosphorus stored in the organic
!!                                |P pool
!!    tsol_stap(:)  |kg P/ha      |amount of phosphorus in the soil layer
!!                                |stored in the stable mineral phosphorus pool
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name         |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tsedminpa    |kg P/ha       |amount of active mineral phosphorus sorbed to
!!                                |sediment in surface runoff in terrace for day
!!    tsedminps    |kg P/ha       |amount of stable mineral phosphorus sorbed to
!!                                |sediment in surface runoff in terrace for day
!!    tsedorgp     |kg P/ha       |amount of organic phosphorus in surface
!!                                |runoff in terrace for the day
!!    tsol_actp(:) |kg P/ha       |amount of phosphorus stored in the
!!                                |active mineral phosphorus pool
!!    tsol_fop(:)  |kg P/ha       |amount of phosphorus stored in the fresh
!!                                |organic (residue) pool
!!    tsol_orgp(:) |kg P/ha       |amount of phosphorus stored in the organic
!!                                |P pool
!!    tsol_stap(:) |kg P/ha       |amount of phosphorus in the soil layer
!!                                |stored in the stable mineral phosphorus pool
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    conc        |              |concentration of P in soil
!!    er          |none          |enrichment ratio
!!    j           |none          |HRU number
!!    porgg       |kg P/ha       |total amount of P in organic pools prior to
!!                               |sediment removal
!!    psedd       |kg P/ha       |total amount of P in mineral sediment pools
!!                               |prior to sediment removal
!!    sedp        |kg P/ha       |total amount of P removed in sediment erosion
!!    sb          |none          |subbasin number
!!    wt1         |none          |conversion factor (mg/kg => kg/ha)
!!    xx          |kg P/ha       |amount of phosphorus attached to sediment 
!!                               |in soil
!!    xxa         |kg P/ha       |fraction of active mineral phosphorus in soil
!!    xxo         |kg P/ha       |fraction of organic phosphorus in soil
!!    xxs         |kg P/ha       |fraction of stable mineral phosphorus in soil
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j, sb
      real :: xx, wt1, er, conc, xxo, sedp, psedd, porgg, xxa, xxs

      j = 0
      j = ihru

      xx = 0.
      xxo = 0.
      xxa = 0.
      xxs = 0.

        !! HRU sediment calculations
        xx = tsol_orgp(1) + tsol_fop(1) + tsol_mp(1) +                  &
     &                                    tsol_actp(1) + tsol_stap(1)
        if (xx > 1.e-3) then
           xxo = (tsol_orgp(1) + tsol_fop(1)+ tsol_mp(1)) / xx
           xxa = tsol_actp(1) / xx
           xxs = tsol_stap(1) / xx
        end if

  

      wt1 = 0.
      wt1 = soil(j)%phys(1)%bd * soil(j)%phys(1)%d / 100.

      if (hru(j)%hyd%erorgp > .001) then
         er = hru(j)%hyd%erorgp
      else
         er = enratio
      end if

      conc = 0.
      conc = xx * er / wt1

      sedp = 0.

        sedp = .001 * conc * trcsed / trc_ha
        tsedorgp = sedp * xxo
        tsedminpa = sedp * xxa
        tsedminps = sedp * xxs


!! modify phosphorus pools only for HRU calculations

      psedd = 0.
      porgg = 0.
      psedd = tsol_actp(1) + tsol_stap(1)
      porgg = tsol_orgp(1) + tsol_fop(1)
      
      if (porgg > 1.e-3) then
      tsol_orgp(1) = tsol_orgp(1) - tsedorgp * (tsol_orgp(1) / porgg)
      tsol_fop(1) = tsol_fop(1) - tsedorgp * (tsol_fop(1) / porgg)
      tsol_mp(1) = tsol_mp(1) - tsedorgp * (tsol_mp(1) / porgg)
      end if

      tsol_actp(1) = tsol_actp(1) - tsedminpa
      tsol_stap(1) = tsol_stap(1) - tsedminps

        !! Not sure how can this happen but I reapeated 
        !! the check for sol_mp(1,j) - Armen March 2009
        if (tsol_orgp(1) < 0.) then
          tsedorgp = tsedorgp + tsol_orgp(1)
          tsol_orgp(1) = 0.
        end if

        if (tsol_fop(1) < 0.) then
          tsedorgp = tsedorgp + tsol_fop(1)
          tsol_fop(1) = 0.
        end if

        if (tsol_mp(1) < 0.) then
          tsedorgp = tsedorgp + tsol_mp(1)
          tsol_mp(1) = 0.
        end if

        if (tsol_actp(1) < 0.) then
          tsedminpa = tsedminpa + tsol_actp(1)
          tsol_actp(1) = 0.
        end if

        if (tsol_stap(1) < 0.) then
          tsedminps = tsedminps + tsol_stap(1)
          tsol_stap(1) = 0.
        end if

      return
      end

      subroutine readmtb
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine reads data from the terrace segment management input file
!!    (.mtb). This file contains data related to management practices used in
!!    the terrace bed area.

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name       |units            |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    bio_e(:)   |(kg/ha)/(MJ/m**2)|biomass-energy ratio
!!                                 |The potential (unstressed) growth rate per
!!                                 |unit of intercepted photosynthetically
!!                                 |active radiation.
!!    cnyld(:)   |kg N/kg yield    |fraction of nitrogen in yield
!!    hvsti(:)   |(kg/ha)/(kg/ha)  |harvest index: crop yield/aboveground 
!!                                 |biomass
!!    ihru       |none             |HRU number
!!    ndays(:)   |julian date      |julian date for last day of preceding
!!                                 |month (where the array location is the
!!                                 |number of the month) The dates are for
!!                                 |leap years
!!    tbirr_asq  |                 |surface runoff ratio
!!    tbirr_mx   |mm               |maximum irrigation amount per auto application
!!    tbirr_sq   |frac             |surface runoff ratio (0-1) .1 is 10% surface runoff
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name              |units       |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    canmx_trcb(:)     |mm          |Maximum canopy storage in bed area of terrace
!!    canstor_trcb(:)   |mm          |Initial canopy storage in bed area of terrace
!!    idplt_trcb(:,:,:) |none        |land cover code from crop.dat
!!    igro_trcb(:)      |none        |land cover status code. This code
!!                                   |informs the model whether or not a land
!!                                   |cover is growing at the beginning of 
!!                                   |the simulation
!!                                   |0 no land cover growing
!!                                   |1 land cover growing
!!    ikill_trcb(:,:,:) |julian date |date of kill operation
!!    iop_trcb(:,:,:)   |julian date |date of tillage operation
!!    iplant_trcb(:,:,:)|julian date |date of planting/beginning of growing 
!!                                   |season
!!    laiday_trcb(:)    |m**2/m**2   |leaf area index
!!    phuai_trcb(:,:,:) |none        |fraction of plant heat units at which
!!                                   |auto irrigation is initialized
!!    phucf_trcb(:,:,:) |none        |fraction of plant heat units at which
!!                                   |continuous fertilization is initialized
!!    phuh_trcb(:,:,:)  |none        |fraction of plant heat units at which
!!                                   |harvest and kill operation occurs
!!    phuho_trcb(:,:,:) |none        |fraction of plant heat units at which
!!                                   |harvest operation occurs
!!    phuirr_trcb(:,:,:)|none        |fraction of plant heat units at which
!!                                   |irrigation occurs
!!    phuk_trcb(:,:,:)  |none        |fraction of plant heat units at which
!!                                   |kill operation occurs
!!    phun_trcb(:,:,:)  |none        |fraction of plant heat units at which
!!                                   |fertilization occurs
!!    phup_trcb(:,:,:)  |none        |fraction of solar heat units at which 
!!                                   |planting occurs
!!    phut_trcb(:,:,:)  |none        |fraction of heat units  (base zero or
!!                                   |plant) at which tillage occurs
!!    tbafrt_surface(:) |none        |fraction of fertilizer which is applied
!!                                   |to top 10 mm of soil (the remaining 
!!                                   |fraction is applied to first soil 
!!                                   |layer)
!!    tbauto_napp(:)    |kg NO3-N/ha |maximum NO3-N content allowed in one
!!                                   |fertilizer application
!!    tbauto_nstrs(:)   |none        |nitrogen stress factor which triggers
!!                                   |auto fertilization
!!    tbauto_nyr(:)     |kg NO3-N/ha |maximum NO3-N content allowed to be
!!                                   |applied in one year
!!    tbauto_wstr(:,:,:)|none or mm  |water stress factor which triggers auto
!!                                   |irrigation
!!    tbcfrt_id(:,:,:)  |none        |fertilizer/manure id number from database
!!    tbcfrt_kg(:,:,:)  |kg/ha       |amount of fertilzier applied to terrace bed area 
!!                                   |on a given day
!!    tbfert_days(:,:,:)|none        |number of days continuous fertilization
!!                                   |will be simulated
!!    tbfrt_kg(:,:,:)   |kg/ha       |amount of fertilizer applied to terrace bed area
!!    tbfrt_surface(:,:,:)|none      |fraction of fertilizer which is applied
!!                                   |to the top 10 mm of soil (the remaining
!!                                   |fraction is applied to the first soil 
!!                                   |layer)
!!    tbharveff(:,:,:)  |none        |harvest efficiency: fraction of harvested
!!                                   |yield that is removed from terrace bed area; the 
!!                                   |remainder becomes residue on the soil
!!                                   |surface
!!    tbiairr(:,:,:)    |julian date |date of auto irrigation initialization
!!    tbifrt_freq(:,:,:)|days        |number of days between applications in
!!                                   |continuous fertilizer operation
!!    tbirr_amt(:,:,:)  |mm H2O      |depth of irrigation water applied to 
!!                                   |terrace bed area
!!    tbnrot(:)         |none        |number of years of rotation
!!    tbsol_cov(:)      |kg/ha       |Initial residue cover in bed area of terrace
!!    tbwstrs_id(:,:,:) |none        |water stress identifier
!!                                   |1: plant water demand
!!                                   |2: soil water deficit
!!    tbwtab(:)         |m           |Initial soil water table in bed area of terrace
!!    trcb_bioms(:)     |kg/ha       |cover/crop biomass
!!    trcb_biotarg(:,:,:)|kg/ha      |biomass target
!!    trcb_tankfr(:)    |ha/ha       |Area fraction of frontslope segment for rainfall harvest tank 
!!                                   |rainfall collection
!!    trcb_cn(:)        |none        |SCS runoff curve number for moisture
!!                                   |condition II
!!    trcb_cnop(:,:,:)  |none        |SCS runoff curve number for moisture
!!                                   |condition II
!!    trcb_hiovr(:,:,:) |(kg/ha)/(kg/ha)|harvest index target specified at 
!!                                   |harvest
!!    trcb_hitarg(:,:,:)|(kg/ha)/(kg/ha)|harvest index target of cover defined
!!                                   |at planting
!!    trcb_ht(:)        |m           |The hight of ridge for bench terrace
!!    trcb_icfert(:,:,:)|julian date |date of continuous fertilization 
!!                                   |initialization
!!    trcb_ifert(:,:,:) |julian date |date of fertilizer application
!!    trcb_ihv(:,:,:)   |julian date |date of harvest and kill operation
!!    trcb_ihvo(:,:,:)  |julian date |date of harvest operation
!!    trcb_iir(:,:,:)   |julian date |date of irrigation operation
!!    trcb_inibio(:,:,:)|kg/ha       |initial biomass of transplants
!!    trcb_inilai(:,:,:)|none        |initial leaf area index of transplants
!!    trcb_n(:)         |none        |Manning's "n" value for overland flow in bed 
!!                                   |area of terrace
!!    trcb_p(:)         |none        |USLE equation support practice (P) factor
!!    trcb_phuacc(:)    |none        |fraction of plant heat units 
!!                                   |accumulated
!!    trcb_phuplt(1,1,:)|heat units  |total number of heat units to bring
!!                                   |plant to maturity
!!    trcb_sll(:)       |m           |Average slope length in bed area of terrace
!!    trcb_slp(:)       |m/m         |Average slope stepness in bed area of terrace
!!    trcb_tnylda(:,:,:)|kg N/kg yield |estimated/target nitrogen content of
!!                                   |yield used in autofertilization
!!    trcb_uslek(:)     |none        |Soil erodibility factor in bed area of terrace
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    day         |none          |day operation occurs
!!    husc        |none          |heat unit scheduling for operation expressed
!!                               |as fraction of total heat units of crop
!!                               |at maturity
!!    icf         |none          |number of continuous fertilizer operation
!!                               |in year
!!    ifn         |none          |number of fertilizer application in year
!!    igr         |none          |number of grazing operation in year
!!    inop        |none          |number of tillage operation in year
!!    iro         |none          |counter for years of rotation
!!    j           |none          |counter
!!    lcr         |none          |crop id number
!!    mgt_op      |none          |operation code number
!!                               |0 end of rotation year
!!                               |1 plant/beginning of growing season
!!                               |2 irrigation operation
!!                               |3 fertilizer application
!!                               |5 harvest and kill operation
!!                               |6 tillage operation
!!                               |7 harvest only operation
!!                               |8 kill/end of growing season
!!                               |10 auto irrigation initialization
!!                               |11 auto fertilizer initialization
!!                               |14 continuous fertilization operation
!!    mgt1i       |none          |first management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt2i       |none          |second management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt3i       |none          |third management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt4        |none          |fourth management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt5        |none          |fifth management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt6        |none          |sixth management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt7        |none          |seventh management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt8        |none          |eighth management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt9        |none          |ninth management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mon         |none          |month operation occurs
!!    nafer       |none          |number of auto fertilization operation in
!!                               |year
!!    nairr       |none          |number of auto irrigation operation in year
!!    ncrp        |none          |land cover identification number 
!!                               |(from crop.dat). Need only if IGRO=1.
!!    newpest     |none          |pesticide flag
!!    nhv         |none          |number of harvest and kill operation in
!!                               |year
!!    nhvo        |none          |number of harvest operation in year
!!    nir         |none          |number of irrigation operation in year
!!    nkill       |none          |number of kill operation in year
!!    npllocal         |none          |number of planting operation in year
!!    npst        |none          |number of pesticide application in year
!!    nrel        |none          |number of release/impound operations in year
!!    nsw         |none          |number of street sweeping operation in year
!!    titldum     |NA            |title line from input dataset
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    SWAT: Jdt

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      implicit none
      character (len=80) :: titldum
      integer :: ncrp, iro, npllocal, mon, day, mgt_op, mgt2i, mgt1i, lcr
      integer :: nir, ifn, npst, j, nhv, inop, nhvo, nkill, newpest
      integer :: igr, nairr, nafer, nsw, nrel, icf, mgt3i
      integer :: icp, ibrn, icnp, igrow, jdtLocal, irotate
      real :: husc, mgt6, mgt9, mgt4, mgt5, mgt7, mgt8
      real :: disc

      lcr = 0
      ncrp = 0
      npllocal = 1 
      nir = 0
      ifn = 0
      npst = 0
      nhv = 1
      inop = 0
      nhvo = 0
      nkill = 1
      icf = 0
      icp = 0
      ibrn = 0
      igr = 0
      nairr = 1
      nafer = 0
      icnp = 1
      nsw = 0
      nrel = 0
      igrow = 0

!!    read general management parameters
      read (116,5000) titldum
      read (116,5000) titldum
      read (116,*) igro_trcb(ihru)
      read (116,*) ncrp
      read (116,*) laiday_trcb(ihru)
      read (116,*) trcb_bioms(ihru)
      read (116,*) trcb_phuplt(1,1,ihru)
!     read bed segment (frontslope) parameters
      read (116,5000) titldum
      read (116,*) trcb_ht(ihru)
      read (116,*) trcb_sll(ihru)
      read (116,*) trcb_slp(ihru)
      read (116,*) trcb_cn(ihru)
      read (116,*) trcb_p(ihru)
      read (116,*) trcb_n(ihru)
      read (116,*) trcb_uslek(ihru)
      read (116,*) tbsol_cov(ihru)
      read (116,*) canmx_trcb(ihru)
      read (116,*) canstor_trcb(ihru)
      read (116,*) tbwtab(ihru)
      read (116,*) trcb_tankfr(ihru)
      read (116,5000) titldum
      read (116,*) tbnrot(ihru)
      read (116,5000) titldum

!!    set values for cover/crop already growing
      if (igro_trcb(ihru) == 1) then
        igrow = 1
        idplt_trcb(1,1,ihru) = ncrp
        idplt_trcb(1,2,ihru) = ncrp
        lcr = ncrp
        trcb_phuacc(ihru) = .1
        npllocal = 1
        nhv = 0
        !! calculate tnylda for autofertilization 
        if (pldb(ncrp)%hvsti < 1.) then
          trcb_tnylda(1,1,ihru) = 350. * pldb(ncrp)%cnyld * pldb(ncrp)%bio_e
        else
          trcb_tnylda(1,1,ihru) = 1000. * pldb(ncrp)%cnyld * pldb(ncrp)%bio_e
        endif
      end if

!!    If years of rotation are set to zero, assume continuous fallow. For
!!    continuous fallow, no management practices allowed.
      if (tbnrot(ihru) > 0) then

!!      read scheduled management practices
        do iro = 1, tbnrot(ihru)                  !! rotation loop

          do                                    !! operation loop
          mon = 0
          day = 0
          husc = 0.
          mgt_op = 0
          mgt1i = 0
          mgt2i = 0
          mgt3i = 0
          mgt4 = 0.

          mgt5 = 0.
          mgt6 = 0
          mgt7 = 0.
          mgt8 = 0.
          mgt9 = 0.

          read (116,5200) mon, day, husc, mgt_op, mgt1i, mgt2i, mgt3i,  &
     &                   mgt4, mgt5, mgt6, mgt7, mgt8, mgt9
 

          select case (mgt_op)

          case (0)  !! end of rotation year

            !! this defines a crop for the model to recognize between harvest
            !! and the end of the year. The model does not simulate growth
            !! of the crop--it is needed to set parameters in erosion processes
            idplt_trcb(iro+1,1,ihru) = lcr

            !! the following equations set values for fallow years
           if (idplt_trcb(iro,1,ihru) == 0) idplt_trcb(iro,1,ihru) = lcr
           if (trcb_phuplt(iro,1,ihru) == 0.)                           &
     &                trcb_phuplt(iro,1,ihru) = trcb_phuplt(iro,2,ihru)

            !! re-initialize annual counters
            npllocal = 1
            if (igrow == 1) then
              nhv = 0
            else
              nhv = 1
            end if 
            nkill = 1
            nhvo = 0
            npst = 0
            nir = 0
            ifn = 0
            inop = 0
            igr = 0
            nairr = 1
            nafer = 0
	      nrel = 0
            icnp = 1
            nsw = 0
            icf = 0
            icp = 0
            exit

          case (1)  !! plant operation
            igrow = 1
            if (igro_trcb(ihru) == 1) idplt_trcb(1,1,ihru) = mgt1i
            npllocal = npllocal + 1
            idplt_trcb(iro,npllocal,ihru) = mgt1i
            idplt_trcb(iro,npllocal+1,ihru) = mgt1i
            lcr = mgt1i
            iplant_trcb(iro,npllocal,ihru) = Jdt(ndays,day,mon)
            if (mgt4 < 700.) mgt4 = 1700.
            if (mgt4 > 5000.) mgt4 = 5000.
            trcb_phuplt(iro,npllocal,ihru) = mgt4
            trcb_phuplt(iro,npllocal+1,ihru) = mgt4
!            if (isproj == 2) then
!              if (husc > .5) husc = .15      !!CEAP fix for winter crops
!            end if
            if (husc > 0.) then
              phup_trcb(iro,npllocal,ihru) = husc
              if (husc > .5) phup_trcb(iro,npllocal+1,ihru) = husc
            endif
            trcb_cnop(iro,icnp,ihru) = mgt9
            icnp = icnp + 1
            trcb_cyrmat(ihru) = mgt3i
            trcb_hitarg(iro,npllocal,ihru) = mgt7
            trcb_biotarg(iro,npllocal,ihru) = mgt8 * 1000.
            trcb_inilai(iro,npllocal,ihru) = mgt5
            trcb_inibio(iro,npllocal,ihru) = mgt6
          
            !! calculate tnylda for autofertilization 
            if (pldb(mgt1i)%hvsti < 1.) then
              trcb_tnylda(iro,npllocal,ihru) = 350. * pldb(mgt1i)%cnyld *         &
     &                                                      pldb(mgt1i)%bio_e
            else
              trcb_tnylda(iro,npllocal,ihru) = 1000. * pldb(mgt1i)%cnyld *        &
     &                                                      pldb(mgt1i)%bio_e
            endif

          case (2)  !! irrigation operation
            nir = nir + 1
            tbirr_amt(iro,nir,ihru) = mgt4
            trcb_iir(iro,nir,ihru) = Jdt(ndays,day,mon)
            if (husc > 0.) then
              if (igrow == 1) then 
                phuirr_trcb(iro,nir,ihru) = husc
              else
                phuirr_nocrop_trcb(iro,nir,ihru) = husc
              end if
            end if 
            tbirr_efm(iro,nir,ihru) = mgt6
            tbirr_sq(iro,nir,ihru) = mgt7
            if (tbirr_efm(iro,nir,ihru) < 1.e-6)&
     &                              tbirr_efm(iro,nir,ihru) =1.0

          case (3)  !! fertilizer operation
            if (mgt1i > 0) then           !! no fertilizer id #, ignore operation
              ifn = ifn + 1
              trcb_ifert(iro,ifn,ihru) = Jdt(ndays,day,mon)
              if (husc > 0.) then
                if (igrow == 1) then 
                  phun_trcb(iro,ifn,ihru) = husc
                else
                  phun_nocrop_trcb(iro,ifn,ihru) = husc
                end if
              end if
              tbfrt_surface(iro,ifn,ihru) = mgt5
              if (tbfrt_surface(iro,ifn,ihru) <= 1.e-6)                 &
     &                                  tbfrt_surface(iro,ifn,ihru) = .2
              trcb_ifrttyp(iro,ifn,ihru) = mgt1i
              tbfrt_kg(iro,ifn,ihru) = mgt4
            end if
            
          case (5)  !! harvest and kill operation
            nhv = nhv + 1
            igrow = 0
            ihv_trcb(iro,npllocal,ihru) = Jdt(ndays,day,mon)
            if (nhv > 1) then
              if (ihv_trcb(iro,nhv-1,ihru) <= 0) then
                ihv_trcb(iro,nhv-1,ihru) = ihv_trcb(iro,nhv,ihru)
              end if
            end if 
            if (husc > 0.) then
              phuh_trcb(iro,npllocal,ihru) = husc
              phuh_trcb(iro,npllocal+1,ihru) = husc
            endif
            trcb_cnop(iro,icnp,ihru) = mgt4
            icnp = icnp + 1
!!! add fraction stover removed for harvest and kill
            trcb_frharvk(iro,icnp,ihru) = mgt5

          case (6)  !! tillage operation
            inop = inop + 1
            iop_trcb(iro,inop,ihru) = Jdt(ndays,day,mon)
            if (husc > 0.) then
              if (igrow == 1) then
                phut_trcb(iro,inop,ihru) = husc
              else
                phut_nocr_trcb(iro,inop,ihru) = husc
              end if
            end if
            tbidtill(iro,inop,ihru) = mgt1i
            trcb_cnop(iro,icnp,ihru) = mgt4
            icnp = icnp + 1

          case (7)  !! harvest only operation
            nhvo = nhvo + 1
            ihvo_trcb(iro,nhvo,ihru) = Jdt(ndays,day,mon)
            if (husc > 0.) phuho_trcb(iro,nhvo,ihru) = husc
            ihv_gbm_trcb(iro,nhvo,ihru) = mgt2i
            trcb_hiovr(iro,nhvo,ihru) = mgt5
            tbharveff(iro,nhvo,ihru) = mgt4
            if (tbharveff(iro,nhvo,ihru) <= 0.) then
              tbharveff(iro,nhvo,ihru) = 1.
            endif

          case (8)  !! kill operation
            nkill = nkill + 1
            igrow = 0
            ikill_trcb(iro,npllocal,ihru) = Jdt(ndays,day,mon)
            if (husc > 0.) phuk_trcb(iro,npllocal,ihru) = husc
!           idplt(iro,npllocal+1,ihru) = lcr

          case (10)  !! auto irrigation operation
            nairr = nairr + 1
            tbiairr(iro,nairr-1,ihru) = Jdt(ndays,day,mon)
            if (husc > 0.) phuai_trcb(iro,nairr-1,ihru) = husc
            if (mgt1i <= 0) mgt1i = 1
            tbwstrs_id(iro,nairr,ihru) = mgt1i
            tbauto_wstr(iro,nairr,ihru) = mgt4
            tbirr_eff(iro,nairr,ihru) = mgt5
            tbirr_asq(iro,nairr,ihru) = mgt7
            tbirr_mx(iro,nairr,ihru) = mgt6
            if (tbirr_eff(iro,nairr,ihru) > 1.)  &
     &              tbirr_eff(iro,nairr,ihru) = 1.
            if (tbirr_eff(iro,nairr,ihru) == 0.)     &
     &              tbirr_eff(iro,nairr,ihru) = 1.
 !!    change per JGA for irrigation 4/2/2009
            if (tbirr_mx(iro,nairr,ihru) < 1.e-6)        &
     &		   tbirr_mx(iro,nairr,ihru) = 25.4

          case (11)  !! auto fertilizer operation
            if (mgt1i > 0) then  
              nafer = nafer + 1
              tbafrt_surface(ihru) = mgt8
              if (tbafrt_surface(ihru) <= 1.e-6)                    &
     &                                      tbafrt_surface(ihru) = .2
              tbauto_nstrs(ihru) = mgt4
              trcb_iafrttyp(ihru) = mgt1i
              tbauto_napp(ihru) = mgt5
              if (tbauto_napp(ihru) <= 0.) tbauto_napp(ihru) = 200.
              tbauto_nyr(ihru) = mgt6
              if (tbauto_nyr(ihru) <= 0.) tbauto_nyr(ihru) = 300.
            end if

          case (14)  !! continuous fertilization operation
            icf = icf + 1
            trcb_icfert(iro,icf,ihru) = Jdt(ndays,day,mon)
            if (husc > 0.) phucf_trcb(iro,icf,ihru) = husc
            tbfert_days(iro,icf,ihru) = mgt1i
            tbcfrt_kg(iro,icf,ihru) = mgt4
            tbcfrt_id(iro,icf,ihru) = mgt2i
            tbifrt_freq(iro,icf,ihru) = mgt3i
            if (tbifrt_freq(iro,icf,ihru) <= 0) then
              tbifrt_freq(iro,icf,ihru) = 1
            end if

            end select
          end do                                !! operation loop
 

        if (iro == tbnrot(ihru)) exit
        end do                                  !! rotation loop

        idplt_trcb(1,1,ihru) = lcr
        do irotate = 2, tbnrot(ihru)
         if (idplt_trcb(irotate,1,ihru) == 0)                           &
     &     idplt_trcb(irotate,1,ihru) = idplt_trcb(irotate-1,1,ihru)
         if (trcb_tnylda(irotate,2,ihru) == 0)                          &
     &     trcb_tnylda(irotate,2,ihru) = trcb_tnylda(irotate-1,2,ihru)
         if (trcb_tnylda(irotate,1,ihru) == 0)                          &
     &     trcb_tnylda(irotate,1,ihru) = trcb_tnylda(irotate,2,ihru)
        end do
      end if

      close (116)
     
      return
 5000 format (a)
 5200 format (1x,i2,1x,i2,1x,f8.3,1x,i2,1x,i4,1x,i3,1x,i2,1x,f12.5,1x,  &
     &        f6.2,1x,f11.5,1x,f4.2,1x,f6.2,1x,f5.2)
      end

      subroutine readmtr
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine reads data from the terrace segment management input file
!!    (.mtr). This file contains data related to management practices used in
!!    the terrace cutslope area.

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name       |units            |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    bio_e(:)   |(kg/ha)/(MJ/m**2)|biomass-energy ratio
!!                                 |The potential (unstressed) growth rate per
!!                                 |unit of intercepted photosynthetically
!!                                 |active radiation.
!!    cnyld(:)   |kg N/kg yield    |fraction of nitrogen in yield
!!    hvsti(:)   |(kg/ha)/(kg/ha)  |harvest index: crop yield/aboveground 
!!                                 |biomass
!!    ihru       |none             |HRU number
!!    ndays(:)   |julian date      |julian date for last day of preceding
!!                                 |month (where the array location is the
!!                                 |number of the month) The dates are for
!!                                 |leap years
!!    trirr_asq  |                 |surface runoff ratio
!!    trirr_mx   |mm               |maximum irrigation amount per auto application
!!    trirr_sq   |frac             |surface runoff ratio (0-1) .1 is 10% surface runoff
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name            |units          |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    canmx_trcr(:)     |mm          |Maximum canopy storage in raised area of terrace
!!    canstor_trcr(:)   |mm          |Initial canopy storage in raised area of terrace
!!    idplt_trcr(:,:,:) |none        |land cover code from crop.dat
!!    igro_trcr(:)      |none        |land cover status code. This code
!!                                   |informs the model whether or not a land
!!                                   |cover is growing at the beginning of 
!!                                   |the simulation
!!                                   |0 no land cover growing
!!                                   |1 land cover growing
!!    ikill_trcr(:,:,:) |julian date |date of kill operation
!!    iop_trcr(:,:,:)   |julian date |date of tillage operation
!!    iplant_trcr(:,:,:)|julian date |date of planting/beginning of growing 
!!                                   |season
!!    laiday_trcr(:)    |m**2/m**2   |leaf area index
!!    phuai_trcr(:,:,:) |none        |fraction of plant heat units at which
!!                                   |auto irrigation is initialized
!!    phucf_trcr(:,:,:) |none        |fraction of plant heat units at which
!!                                   |continuous fertilization is initialized
!!    phuh_trcr(:,:,:)  |none        |fraction of plant heat units at which
!!                                   |harvest and kill operation occurs
!!    phuho_trcr(:,:,:) |none        |fraction of plant heat units at which
!!                                   |harvest operation occurs
!!    phuirr_trcr(:,:,:)|none        |fraction of plant heat units at which
!!                                   |irrigation occurs
!!    phuk_trcr(:,:,:)  |none        |fraction of plant heat units at which
!!                                   |kill operation occurs
!!    phun_trcr(:,:,:)  |none        |fraction of plant heat units at which
!!                                   |fertilization occurs
!!    phup_trcr(:,:,:)  |none        |fraction of solar heat units at which 
!!                                   |planting occurs
!!    phut_trcr(:,:,:)  |none        |fraction of heat units  (base zero or
!!                                   |plant) at which tillage occurs
!!    trafrt_surface(:) |none        |fraction of fertilizer which is applied
!!                                   |to top 10 mm of soil (the remaining 
!!                                   |fraction is applied to first soil 
!!                                   |layer)
!!    trauto_napp(:)    |kg NO3-N/ha |maximum NO3-N content allowed in one
!!                                   |fertilizer application
!!    trauto_nstrs(:)   |none        |nitrogen stress factor which triggers
!!                                   |auto fertilization
!!    trauto_nyr(:)     |kg NO3-N/ha |maximum NO3-N content allowed to be
!!                                   |applied in one year
!!    trauto_wstr(:,:,:)|none or mm  |water stress factor which triggers auto
!!                                   |irrigation
!!    trcfrt_id(:,:,:)  |none        |fertilizer/manure id number from database
!!    trcfrt_kg(:,:,:)  |kg/ha       |amount of fertilzier applied to terrace raised area 
!!                                   |on a given day
!!    trcr_bioms(:)     |kg/ha       |cover/crop biomass
!!    trcr_biotarg(:,:,:)|kg/ha      |biomass target
!!    trcr_tankfr(:)    |ha/ha       |Area fraction of frontslope segment for rainfall harvest tank 
!!                                   |rainfall collection
!!    trcr_cn(:)        |none        |SCS runoff curve number for moisture
!!                                   |condition II
!!    trcr_cnop(:,:,:)  |none        |SCS runoff curve number for moisture
!!                                   |condition II
!!    trcr_hiovr(:,:,:) |(kg/ha)/(kg/ha)|harvest index target specified at 
!!                                   |harvest
!!    trcr_hitarg(:,:,:)|(kg/ha)/(kg/ha)|harvest index target of cover defined
!!                                   |at planting
!!    trcr_ht(:)        |m           |The hight of ridge for bench terrace
!!    trcr_icfert(:,:,:)|julian date |date of continuous fertilization 
!!                                   |initialization
!!    trcr_ifert(:,:,:) |julian date |date of fertilizer application
!!    trcr_ihv(:,:,:)   |julian date |date of harvest and kill operation
!!    trcr_ihvo(:,:,:)  |julian date |date of harvest operation
!!    trcr_iir(:,:,:)   |julian date |date of irrigation operation
!!    trcr_inibio(:,:,:)|kg/ha       |initial biomass of transplants
!!    trcr_inilai(:,:,:)|none        |initial leaf area index of transplants
!!    trcr_n(:)         |none        |Manning's "n" value for overland flow in raised 
!!                                   |area of terrace
!!    trcr_p(:)         |none        |USLE equation support practice (P) factor
!!    trcr_phuacc(:)    |none        |fraction of plant heat units 
!!                                   |accumulated
!!    trcr_phuplt(1,1,:)|heat units  |total number of heat units to bring
!!                                   |plant to maturity
!!    trcr_sll(:)       |m           |Average slope length in raised area of terrace
!!    trcr_slp(:)       |m/m         |Average slope stepness in raised area of terrace
!!    trcr_tnylda(:,:,:)|kg N/kg yield |estimated/target nitrogen content of
!!                                   |yield used in autofertilization
!!    trcr_uslek(:)     |none        |Soil erodibility factor in raised area of terrace
!!    trfert_days(:,:,:)|none        |number of days continuous fertilization
!!                                   |will be simulated
!!    trfrt_kg(:,:,:)   |kg/ha       |amount of fertilizer applied to terrace raised area
!!    trfrt_surface(:,:,:)|none      |fraction of fertilizer which is applied
!!                                   |to the top 10 mm of soil (the remaining
!!                                   |fraction is applied to the first soil 
!!                                   |layer)
!!    trharveff(:,:,:)  |none        |harvest efficiency: fraction of harvested
!!                                   |yield that is removed from terrace raised area; the 
!!                                   |remainder becomes residue on the soil
!!                                   |surface
!!    triairr(:,:,:)    |julian date |date of auto irrigation initialization
!!    trifrt_freq(:,:,:)|days        |number of days between applications in
!!                                   |continuous fertilizer operation
!!    trirr_amt(:,:,:)  |mm H2O      |depth of irrigation water applied to 
!!                                   |terrace raised area
!!    trnrot(:)         |none        |number of years of rotation
!!    trsol_cov(:)      |kg/ha       |Initial residue cover in raised area of terrace
!!    trwstrs_id(:,:,:) |none        |water stress identifier
!!                                   |1: plant water demand
!!                                   |2: soil water deficit
!!    trwtab(:)         |m           |Initial soil water table in raised area of terrace
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    day         |none          |day operation occurs
!!    husc        |none          |heat unit scheduling for operation expressed
!!                               |as fraction of total heat units of crop
!!                               |at maturity
!!    icf         |none          |number of continuous fertilizer operation
!!                               |in year
!!    ifn         |none          |number of fertilizer application in year
!!    igr         |none          |number of grazing operation in year
!!    inop        |none          |number of tillage operation in year
!!    iro         |none          |counter for years of rotation
!!    j           |none          |counter
!!    lcr         |none          |crop id number
!!    mgt_op      |none          |operation code number
!!                               |0 end of rotation year
!!                               |1 plant/beginning of growing season
!!                               |2 irrigation operation
!!                               |3 fertilizer application
!!                               |5 harvest and kill operation
!!                               |6 tillage operation
!!                               |7 harvest only operation
!!                               |8 kill/end of growing season
!!                               |10 auto irrigation initialization
!!                               |11 auto fertilizer initialization
!!                               |14 continuous fertilization operation
!!    mgt1i       |none          |first management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt2i       |none          |second management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt3i       |none          |third management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt4        |none          |fourth management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt5        |none          |fifth management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt6        |none          |sixth management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt7        |none          |seventh management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt8        |none          |eighth management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt9        |none          |ninth management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mon         |none          |month operation occurs
!!    nafer       |none          |number of auto fertilization operation in
!!                               |year
!!    nairr       |none          |number of auto irrigation operation in year
!!    ncrp        |none          |land cover identification number 
!!                               |(from crop.dat). Need only if IGRO=1.
!!    newpest     |none          |pesticide flag
!!    nhv         |none          |number of harvest and kill operation in
!!                               |year
!!    nhvo        |none          |number of harvest operation in year
!!    nir         |none          |number of irrigation operation in year
!!    nkill       |none          |number of kill operation in year
!!    npllocal         |none          |number of planting operation in year
!!    npst        |none          |number of pesticide application in year
!!    nrel        |none          |number of release/impound operations in year
!!    nsw         |none          |number of street sweeping operation in year
!!    titldum     |NA            |title line from input dataset
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    SWAT: Jdt

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      implicit none
      character (len=80) :: titldum
      integer :: ncrp, iro, npllocal, mon, day, mgt_op, mgt2i, mgt1i, lcr
      integer :: nir, ifn, npst, j, nhv, inop, nhvo, nkill, newpest
      integer :: igr, nairr, nafer, nsw, nrel, icf, mgt3i
      integer :: icp, ibrn, icnp, igrow, irotate
      real :: husc, mgt6, mgt9, mgt4, mgt5, mgt7, mgt8
      real :: disc

      lcr = 0
      ncrp = 0
      npllocal = 1 
      nir = 0
      ifn = 0
      npst = 0
      nhv = 1
      inop = 0
      nhvo = 0
      nkill = 1
      icf = 0
      icp = 0
      ibrn = 0
      igr = 0
      nairr = 1
      nafer = 0
      icnp = 1
      nsw = 0
      nrel = 0
      igrow = 0

!!    read general management parameters
      read (116,5000) titldum
      read (116,5000) titldum
      read (116,*) igro_trcr(ihru)
      read (116,*) ncrp
      read (116,*) laiday_trcr(ihru)
      read (116,*) trcr_bioms(ihru)
      read (116,*) trcr_phuplt(1,1,ihru)
!     read raiser segment (cutslope) parameters
      read (116,5000) titldum
      read (116,*) trcr_sll(ihru)
      read (116,*) trcr_slp(ihru)
      read (116,*) trcr_cn(ihru)
      read (116,*) trcr_p(ihru)
      read (116,*) trcr_n(ihru)
      read (116,*) trcr_uslek(ihru)
      read (116,*) trsol_cov(ihru)
      read (116,*) canmx_trcr(ihru)
      read (116,*) canstor_trcr(ihru)
      read (116,*) trwtab(ihru)
      read (116,*) trcr_tankfr(ihru)
      read (116,5000) titldum
      read (116,*) trnrot(ihru)
      read (116,5000) titldum

!!    set values for cover/crop already growing
      if (igro_trcr(ihru) == 1) then
        igrow = 1
        idplt_trcr(1,1,ihru) = ncrp
        idplt_trcr(1,2,ihru) = ncrp
        lcr = ncrp
        trcr_phuacc(ihru) = .1
        npllocal = 1
        nhv = 0
        !! calculate tnylda for autofertilization 
        if (pldb(ncrp)%hvsti < 1.) then
          trcr_tnylda(1,1,ihru) = 350. * pldb(ncrp)%cnyld * pldb(ncrp)%bio_e
        else
          trcr_tnylda(1,1,ihru) = 1000. * pldb(ncrp)%cnyld * pldb(ncrp)%bio_e
        endif
      end if

!!    If years of rotation are set to zero, assume continuous fallow. For
!!    continuous fallow, no management practices allowed.
      if (trnrot(ihru) > 0) then

!!      read scheduled management practices
        do iro = 1, trnrot(ihru)                  !! rotation loop

          do                                    !! operation loop
          mon = 0
          day = 0
          husc = 0.
          mgt_op = 0
          mgt1i = 0
          mgt2i = 0
          mgt3i = 0
          mgt4 = 0.

          mgt5 = 0.
          mgt6 = 0
          mgt7 = 0.
          mgt8 = 0.
          mgt9 = 0.

          read (116,5200) mon, day, husc, mgt_op, mgt1i, mgt2i, mgt3i,  &
     &                   mgt4, mgt5, mgt6, mgt7, mgt8, mgt9
 

          select case (mgt_op)

          case (0)  !! end of rotation year

            !! this defines a crop for the model to recognize between harvest
            !! and the end of the year. The model does not simulate growth
            !! of the crop--it is needed to set parameters in erosion processes
            idplt_trcr(iro+1,1,ihru) = lcr

            !! the following equations set values for fallow years
           if (idplt_trcr(iro,1,ihru) == 0) idplt_trcr(iro,1,ihru) = lcr
           if (trcr_phuplt(iro,1,ihru) == 0.)                           &
     &                trcr_phuplt(iro,1,ihru) = trcr_phuplt(iro,2,ihru)

            !! re-initialize annual counters
            npllocal = 1
            if (igrow == 1) then
              nhv = 0
            else
              nhv = 1
            end if 
            nkill = 1
            nhvo = 0
            npst = 0
            nir = 0
            ifn = 0
            inop = 0
            igr = 0
            nairr = 1
            nafer = 0
	      nrel = 0
            icnp = 1
            nsw = 0
            icf = 0
            icp = 0
            exit

          case (1)  !! plant operation
            igrow = 1
            if (igro_trcr(ihru) == 1) idplt_trcr(1,1,ihru) = mgt1i
            npllocal = npllocal + 1
            idplt_trcr(iro,npllocal,ihru) = mgt1i
            idplt_trcr(iro,npllocal+1,ihru) = mgt1i
            lcr = mgt1i
            iplant_trcr(iro,npllocal,ihru) = Jdt(ndays,day,mon)
            if (mgt4 < 700.) mgt4 = 1700.
            if (mgt4 > 5000.) mgt4 = 5000.
            trcr_phuplt(iro,npllocal,ihru) = mgt4
            trcr_phuplt(iro,npllocal+1,ihru) = mgt4
!            if (isproj == 2) then
!              if (husc > .5) husc = .15      !!CEAP fix for winter crops
!            end if
            if (husc > 0.) then
              phup_trcr(iro,npllocal,ihru) = husc
              if (husc > .5) phup_trcr(iro,npllocal+1,ihru) = husc
            endif
            trcr_cnop(iro,icnp,ihru) = mgt9
            icnp = icnp + 1
            trcr_cyrmat(ihru) = mgt3i
            trcr_hitarg(iro,npllocal,ihru) = mgt7
            trcr_biotarg(iro,npllocal,ihru) = mgt8 * 1000.
            trcr_inilai(iro,npllocal,ihru) = mgt5
            trcr_inibio(iro,npllocal,ihru) = mgt6
          
            !! calculate tnylda for autofertilization 
            if (pldb(mgt1i)%hvsti < 1.) then
              trcr_tnylda(iro,npllocal,ihru) = 350. * pldb(mgt1i)%cnyld * pldb(mgt1i)%bio_e
            else
              trcr_tnylda(iro,npllocal,ihru) = 1000. * pldb(mgt1i)%cnyld * pldb(mgt1i)%bio_e
            endif

          case (2)  !! irrigation operation
            nir = nir + 1
            trirr_amt(iro,nir,ihru) = mgt4
            trcr_iir(iro,nir,ihru) = Jdt(ndays,day,mon)
            if (husc > 0.) then
              if (igrow == 1) then 
                phuirr_trcr(iro,nir,ihru) = husc
              else
                phuirr_nocrop_trcr(iro,nir,ihru) = husc
              end if
            end if 
            trirr_efm(iro,nir,ihru) = mgt6
            trirr_sq(iro,nir,ihru) = mgt7
            if (trirr_efm(iro,nir,ihru) < 1.e-6)                   &
     &                              trirr_efm(iro,nir,ihru) =1.0

          case (3)  !! fertilizer operation
            if (mgt1i > 0) then           !! no fertilizer id #, ignore operation
              ifn = ifn + 1
              trcr_ifert(iro,ifn,ihru) = Jdt(ndays,day,mon)
              if (husc > 0.) then
                if (igrow == 1) then 
                  phun_trcr(iro,ifn,ihru) = husc
                else
                  phun_nocrop_trcr(iro,ifn,ihru) = husc
                end if
              end if
              trfrt_surface(iro,ifn,ihru) = mgt5
              if (trfrt_surface(iro,ifn,ihru) <= 1.e-6)                 &
     &                                  trfrt_surface(iro,ifn,ihru) = .2
              trcr_ifrttyp(iro,ifn,ihru) = mgt1i
              trfrt_kg(iro,ifn,ihru) = mgt4
            end if
            
          case (5)  !! harvest and kill operation
            nhv = nhv + 1
            igrow = 0
            ihv_trcr(iro,npllocal,ihru) = Jdt(ndays,day,mon)
            if (nhv > 1) then
              if (ihv_trcr(iro,nhv-1,ihru) <= 0) then
                ihv_trcr(iro,nhv-1,ihru) = ihv_trcr(iro,nhv,ihru)
              end if
            end if 
            if (husc > 0.) then
              phuh_trcr(iro,npllocal,ihru) = husc
              phuh_trcr(iro,npllocal+1,ihru) = husc
            endif
            trcr_cnop(iro,icnp,ihru) = mgt4
            icnp = icnp + 1
!!! add fraction stover removed for harvest and kill
            trcr_frharvk(iro,icnp,ihru) = mgt5

          case (6)  !! tillage operation
            inop = inop + 1
            iop_trcr(iro,inop,ihru) = Jdt(ndays,day,mon)
            if (husc > 0.) then
              if (igrow == 1) then
                phut_trcr(iro,inop,ihru) = husc
              else
                phut_nocr_trcr(iro,inop,ihru) = husc
              end if
            end if
            tridtill(iro,inop,ihru) = mgt1i
            trcr_cnop(iro,icnp,ihru) = mgt4
            icnp = icnp + 1

          case (7)  !! harvest only operation
            nhvo = nhvo + 1
            ihvo_trcr(iro,nhvo,ihru) = Jdt(ndays,day,mon)
            if (husc > 0.) phuho_trcr(iro,nhvo,ihru) = husc
            ihv_gbm_trcr(iro,nhvo,ihru) = mgt2i
            trcr_hiovr(iro,nhvo,ihru) = mgt5
            trharveff(iro,nhvo,ihru) = mgt4
            if (trharveff(iro,nhvo,ihru) <= 0.) then
              trharveff(iro,nhvo,ihru) = 1.
            endif

          case (8)  !! kill operation
            nkill = nkill + 1
            igrow = 0
            ikill_trcr(iro,npllocal,ihru) = Jdt(ndays,day,mon)
            if (husc > 0.) phuk_trcr(iro,npllocal,ihru) = husc
!           idplt(iro,npllocal+1,ihru) = lcr

          case (10)  !! auto irrigation operation
            nairr = nairr + 1
            triairr(iro,nairr-1,ihru) = Jdt(ndays,day,mon)
            if (husc > 0.) phuai_trcr(iro,nairr-1,ihru) = husc
            if (mgt1i <= 0) mgt1i = 1
            trwstrs_id(iro,nairr,ihru) = mgt1i
            trauto_wstr(iro,nairr,ihru) = mgt4
            trirr_eff(iro,nairr,ihru) = mgt5
            trirr_asq(iro,nairr,ihru) = mgt7
            trirr_mx(iro,nairr,ihru) = mgt6
            if (trirr_eff(iro,nairr,ihru) > 1.)          &
     &              trirr_eff(iro,nairr,ihru) = 1.
            if (trirr_eff(iro,nairr,ihru) == 0.)         &
     &              trirr_eff(iro,nairr,ihru) = 1.
 !!    change per JGA for irrigation 4/2/2009
            if (trirr_mx(iro,nairr,ihru) < 1.e-6)        &
     &		   trirr_mx(iro,nairr,ihru) = 25.4

          case (11)  !! auto fertilizer operation
            if (mgt1i > 0) then  
              nafer = nafer + 1
              trafrt_surface(ihru) = mgt8
              if (trafrt_surface(ihru) <= 1.e-6)            &
     &                                      trafrt_surface(ihru) = .2
              trauto_nstrs(ihru) = mgt4
              trcr_iafrttyp(ihru) = mgt1i
              trauto_napp(ihru) = mgt5
              if (trauto_napp(ihru) <= 0.) trauto_napp(ihru) = 200.
              trauto_nyr(ihru) = mgt6
              if (trauto_nyr(ihru) <= 0.) trauto_nyr(ihru) = 300.
            end if

          case (14)  !! continuous fertilization operation
            icf = icf + 1
            trcr_icfert(iro,icf,ihru) = Jdt(ndays,day,mon)
            if (husc > 0.) phucf_trcr(iro,icf,ihru) = husc
            trfert_days(iro,icf,ihru) = mgt1i
            trcfrt_kg(iro,icf,ihru) = mgt4
            trcfrt_id(iro,icf,ihru) = mgt2i
            trifrt_freq(iro,icf,ihru) = mgt3i
            if (trifrt_freq(iro,icf,ihru) <= 0) then
              trifrt_freq(iro,icf,ihru) = 1
            end if

            end select
          end do                                !! operation loop
 

        if (iro == trnrot(ihru)) exit
        end do                                  !! rotation loop

        idplt_trcr(1,1,ihru) = lcr
        do irotate = 2, trnrot(ihru)
         if (idplt_trcr(irotate,1,ihru) == 0)                           &
     &     idplt_trcr(irotate,1,ihru) = idplt_trcr(irotate-1,1,ihru)
         if (trcr_tnylda(irotate,2,ihru) == 0)                          &
     &     trcr_tnylda(irotate,2,ihru) = trcr_tnylda(irotate-1,2,ihru)
         if (trcr_tnylda(irotate,1,ihru) == 0)                          &
     &     trcr_tnylda(irotate,1,ihru) = trcr_tnylda(irotate,2,ihru)
        end do
      end if

      close (116)
     
      return
 5000 format (a)
 5200 format (1x,i2,1x,i2,1x,f8.3,1x,i2,1x,i4,1x,i3,1x,i2,1x,f12.5,1x,  &
     &        f6.2,1x,f11.5,1x,f4.2,1x,f6.2,1x,f5.2)
      end

      subroutine readmtu
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine reads data from the terrace segment management input file
!!    (.mtu). This file contains data related to management practices used in
!!    the terrace undisturbed area.

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name       |units            |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    bio_e(:)   |(kg/ha)/(MJ/m**2)|biomass-energy ratio
!!                                 |The potential (unstressed) growth rate per
!!                                 |unit of intercepted photosynthetically
!!                                 |active radiation.
!!    cnyld(:)   |kg N/kg yield    |fraction of nitrogen in yield
!!    hvsti(:)   |(kg/ha)/(kg/ha)  |harvest index: crop yield/aboveground 
!!                                 |biomass
!!    ihru       |none             |HRU number
!!    ndays(:)   |julian date      |julian date for last day of preceding
!!                                 |month (where the array location is the
!!                                 |number of the month) The dates are for
!!                                 |leap years
!!    tuirr_asq  |                 |surface runoff ratio
!!    tuirr_mx   |mm               |maximum irrigation amount per auto application
!!    tuirr_sq   |frac             |surface runoff ratio (0-1) .1 is 10% surface runoff
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name            |units          |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    canmx_trcu(:)     |mm          |Maximum canopy storage in undisturbed area 
!!                                   |of terrace
!!    canstor_trcu(:)   |mm          |Initial canopy storage in undisturbed area 
!!                                   |of terrace
!!    idplt_trcu(:,:,:) |none        |land cover code from crop.dat
!!    igro_trcu(:)      |none        |land cover status code. This code
!!                                   |informs the model whether or not a land
!!                                   |cover is growing at the beginning of 
!!                                   |the simulation
!!                                   |0 no land cover growing
!!                                   |1 land cover growing
!!    ikill_trcu(:,:,:) |julian date |date of kill operation
!!    iop_trcu(:,:,:)   |julian date |date of tillage operation
!!    iplant_trcu(:,:,:)|julian date |date of planting/beginning of growing 
!!                                   |season
!!    laiday_trcu(:)    |m**2/m**2   |leaf area index
!!    phuai_trcu(:,:,:) |none        |fraction of plant heat units at which
!!                                   |auto irrigation is initialized
!!    phucf_trcu(:,:,:) |none        |fraction of plant heat units at which
!!                                   |continuous fertilization is initialized
!!    phuh_trcu(:,:,:)  |none        |fraction of plant heat units at which
!!                                   |harvest and kill operation occurs
!!    phuho_trcu(:,:,:) |none        |fraction of plant heat units at which
!!                                   |harvest operation occurs
!!    phuirr_trcu(:,:,:)|none        |fraction of plant heat units at which
!!                                   |irrigation occurs
!!    phuk_trcu(:,:,:)  |none        |fraction of plant heat units at which
!!                                   |kill operation occurs
!!    phun_trcu(:,:,:)  |none        |fraction of plant heat units at which
!!                                   |fertilization occurs
!!    phup_trcu(:,:,:)  |none        |fraction of solar heat units at which 
!!                                   |planting occurs
!!    phut_trcu(:,:,:)  |none        |fraction of heat units  (base zero or
!!                                   |plant) at which tillage occurs
!!    trcu_bioms(:)     |kg/ha       |cover/crop biomass
!!    trcu_biotarg(:,:,:)|kg/ha      |biomass target
!!    trcu_tankfr(:)    |ha/ha       |Area fraction of frontslope segment for rainfall harvest tank 
!!                                   |rainfall collection
!!    trcu_cn(:)        |none        |SCS runoff curve number for moisture
!!                                   |condition II
!!    trcu_cnop(:,:,:)  |none        |SCS runoff curve number for moisture
!!                                   |condition II
!!    trcu_hiovr(:,:,:) |(kg/ha)/(kg/ha)|harvest index target specified at 
!!                                   |harvest
!!    trcu_hitarg(:,:,:)|(kg/ha)/(kg/ha)|harvest index target of cover defined
!!                                   |at planting
!!    trcu_ht(:)        |m           |The hight of ridge for bench terrace
!!    trcu_icfert(:,:,:)|julian date |date of continuous fertilization 
!!                                   |initialization
!!    trcu_ifert(:,:,:) |julian date |date of fertilizer application
!!    trcu_ihv(:,:,:)   |julian date |date of harvest and kill operation
!!    trcu_ihvo(:,:,:)  |julian date |date of harvest operation
!!    trcu_iir(:,:,:)   |julian date |date of irrigation operation
!!    trcu_inibio(:,:,:)|kg/ha       |initial biomass of transplants
!!    trcu_inilai(:,:,:)|none        |initial leaf area index of transplants
!!    trcu_n(:)         |none        |Manning's "n" value for overland flow in 
!!                                   |undisturbed area of terrace
!!    trcu_p(:)         |none        |USLE equation support practice (P) factor
!!    trcu_phuacc(:)    |none        |fraction of plant heat units 
!!                                   |accumulated
!!    trcu_phuplt(1,1,:)|heat units  |total number of heat units to bring
!!                                   |plant to maturity
!!    trcu_sll(:)       |m           |Average slope length in undisturbed area 
!!                                   |of terrace
!!    trcu_slp(:)       |m/m         |Average slope stepness in undisturbed area 
!!                                   |of terrace
!!    trcu_tnylda(:,:,:)|kg N/kg yield |estimated/target nitrogen content of
!!                                   |yield used in autofertilization
!!    trcu_uslek(:)     |none        |Soil erodibility factor in undisturbed area 
!!                                   |of terrace
!!    tuafrt_surface(:) |none        |fraction of fertilizer which is applied
!!                                   |to top 10 mm of soil (the remaining 
!!                                   |fraction is applied to first soil 
!!                                   |layer)
!!    tuauto_napp(:)    |kg NO3-N/ha |maximum NO3-N content allowed in one
!!                                   |fertilizer application
!!    tuauto_nstrs(:)   |none        |nitrogen stress factor which triggers
!!                                   |auto fertilization
!!    tuauto_nyr(:)     |kg NO3-N/ha |maximum NO3-N content allowed to be
!!                                   |applied in one year
!!    tuauto_wstr(:,:,:)|none or mm  |water stress factor which triggers auto
!!                                   |irrigation
!!    tucfrt_id(:,:,:)  |none        |fertilizer/manure id number from database
!!    tucfrt_kg(:,:,:)  |kg/ha       |amount of fertilzier applied to terrace 
!!                                   |undisturbed area on a given day
!!    tufert_days(:,:,:)|none        |number of days continuous fertilization
!!                                   |will be simulated
!!    tufrt_kg(:,:,:)   |kg/ha       |amount of fertilizer applied to terrace 
!!                                   |undisturbed area
!!    tufrt_surface(:,:,:)|none      |fraction of fertilizer which is applied
!!                                   |to the top 10 mm of soil (the remaining
!!                                   |fraction is applied to the first soil 
!!                                   |layer)
!!    tuharveff(:,:,:)  |none        |harvest efficiency: fraction of harvested
!!                                   |yield that is removed from terrace undisturbed
!!                                   |area; the remainder becomes residue on the soil
!!                                   |surface
!!    tuiairr(:,:,:)    |julian date |date of auto irrigation initialization
!!    tuifrt_freq(:,:,:)|days        |number of days between applications in
!!                                   |continuous fertilizer operation
!!    tuirr_amt(:,:,:)  |mm H2O      |depth of irrigation water applied to 
!!                                   |terrace undisturbed area
!!    tunrot(:)         |none        |number of years of rotation
!!    tusol_cov(:)      |kg/ha       |Initial residue cover in undisturbed area 
!!                                   |of terrace
!!    tuwstrs_id(:,:,:) |none        |water stress identifier
!!                                   |1: plant water demand
!!                                   |2: soil water deficit
!!    tuwtab(:)         |m           |Initial soil water table in undisturbed area 
!!                                   |of terrace
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    day         |none          |day operation occurs
!!    husc        |none          |heat unit scheduling for operation expressed
!!                               |as fraction of total heat units of crop
!!                               |at maturity
!!    icf         |none          |number of continuous fertilizer operation
!!                               |in year
!!    ifn         |none          |number of fertilizer application in year
!!    igr         |none          |number of grazing operation in year
!!    inop        |none          |number of tillage operation in year
!!    iro         |none          |counter for years of rotation
!!    j           |none          |counter
!!    lcr         |none          |crop id number
!!    mgt_op      |none          |operation code number
!!                               |0 end of rotation year
!!                               |1 plant/beginning of growing season
!!                               |2 irrigation operation
!!                               |3 fertilizer application
!!                               |5 harvest and kill operation
!!                               |6 tillage operation
!!                               |7 harvest only operation
!!                               |8 kill/end of growing season
!!                               |10 auto irrigation initialization
!!                               |11 auto fertilizer initialization
!!                               |14 continuous fertilization operation
!!    mgt1i       |none          |first management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt2i       |none          |second management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt3i       |none          |third management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt4        |none          |fourth management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt5        |none          |fifth management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt6        |none          |sixth management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt7        |none          |seventh management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt8        |none          |eighth management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mgt9        |none          |ninth management parameter out of .mgt
!!                               |file (definition changes depending on
!!                               |mgt_op)
!!    mon         |none          |month operation occurs
!!    nafer       |none          |number of auto fertilization operation in
!!                               |year
!!    nairr       |none          |number of auto irrigation operation in year
!!    ncrp        |none          |land cover identification number 
!!                               |(from crop.dat). Need only if IGRO=1.
!!    newpest     |none          |pesticide flag
!!    nhv         |none          |number of harvest and kill operation in
!!                               |year
!!    nhvo        |none          |number of harvest operation in year
!!    nir         |none          |number of irrigation operation in year
!!    nkill       |none          |number of kill operation in year
!!    npllocal         |none          |number of planting operation in year
!!    npst        |none          |number of pesticide application in year
!!    nrel        |none          |number of release/impound operations in year
!!    nsw         |none          |number of street sweeping operation in year
!!    titldum     |NA            |title line from input dataset
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    SWAT: Jdt

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      implicit none
      character (len=80) :: titldum
      integer :: ncrp, iro, npllocal, mon, day, mgt_op, mgt2i, mgt1i, lcr
      integer :: nir, ifn, npst, j, nhv, inop, nhvo, nkill, newpest
      integer :: igr, nairr, nafer, nsw, nrel, icf, mgt3i
      integer :: icp, ibrn, icnp, igrow, irotate
      real :: husc, mgt6, mgt9, mgt4, mgt5, mgt7, mgt8
      real :: disc

      lcr = 0
      ncrp = 0
      npllocal = 1 
      nir = 0
      ifn = 0
      npst = 0
      nhv = 1
      inop = 0
      nhvo = 0
      nkill = 1
      icf = 0
      icp = 0
      ibrn = 0
      igr = 0
      nairr = 1
      nafer = 0
      icnp = 1
      nsw = 0
      nrel = 0
      igrow = 0

!!    read general management parameters
      read (116,5000) titldum
      read (116,5000) titldum
      read (116,*) igro_trcu(ihru)
      read (116,*) ncrp
      read (116,*) laiday_trcu(ihru)
      read (116,*) trcu_bioms(ihru)
      read (116,*) trcu_phuplt(1,1,ihru)
!     read undisturbed segment (between terrace) parameters
      read (116,5000) titldum
      read (116,*) trcu_sll(ihru)
      read (116,*) trcu_cn(ihru)
      read (116,*) trcu_p(ihru)
      read (116,*) trcu_n(ihru)
      read (116,*) trcu_uslek(ihru)
      read (116,*) tusol_cov(ihru)
      read (116,*) canmx_trcu(ihru)
      read (116,*) canstor_trcu(ihru)
      read (116,*) tuwtab(ihru)
      read (116,*) trcu_tankfr(ihru)
      read (116,5000) titldum
      read (116,*) tunrot(ihru)
      read (116,5000) titldum

!!    set values for cover/crop already growing
      if (igro_trcu(ihru) == 1) then
        igrow = 1
        idplt_trcu(1,1,ihru) = ncrp
        idplt_trcu(1,2,ihru) = ncrp
        lcr = ncrp
        trcu_phuacc(ihru) = .1
        npllocal = 1
        nhv = 0
        !! calculate tnylda for autofertilization 
        if (pldb(ncrp)%hvsti < 1.) then
          trcu_tnylda(1,1,ihru) = 350. * pldb(ncrp)%cnyld * pldb(ncrp)%bio_e
        else
          trcu_tnylda(1,1,ihru) = 1000. * pldb(ncrp)%cnyld * pldb(ncrp)%bio_e
        endif
      end if

!!    If years of rotation are set to zero, assume continuous fallow. For
!!    continuous fallow, no management practices allowed.
      if (tunrot(ihru) > 0) then

!!      read scheduled management practices
        do iro = 1, tunrot(ihru)                  !! rotation loop

          do                                    !! operation loop
          mon = 0
          day = 0
          husc = 0.
          mgt_op = 0
          mgt1i = 0
          mgt2i = 0
          mgt3i = 0
          mgt4 = 0.

          mgt5 = 0.
          mgt6 = 0
          mgt7 = 0.
          mgt8 = 0.
          mgt9 = 0.

          read (116,5200) mon, day, husc, mgt_op, mgt1i, mgt2i, mgt3i,  &
     &                   mgt4, mgt5, mgt6, mgt7, mgt8, mgt9
 

          select case (mgt_op)

          case (0)  !! end of rotation year

            !! this defines a crop for the model to recognize between harvest
            !! and the end of the year. The model does not simulate growth
            !! of the crop--it is needed to set parameters in erosion processes
            idplt_trcu(iro+1,1,ihru) = lcr

            !! the following equations set values for fallow years
           if (idplt_trcu(iro,1,ihru) == 0) idplt_trcu(iro,1,ihru) = lcr
           if (trcu_phuplt(iro,1,ihru) == 0.)                           &
     &                trcu_phuplt(iro,1,ihru) = trcu_phuplt(iro,2,ihru)

            !! re-initialize annual counters
            npllocal = 1
            if (igrow == 1) then
              nhv = 0
            else
              nhv = 1
            end if 
            nkill = 1
            nhvo = 0
            npst = 0
            nir = 0
            ifn = 0
            inop = 0
            igr = 0
            nairr = 1
            nafer = 0
	      nrel = 0
            icnp = 1
            nsw = 0
            icf = 0
            icp = 0
            exit

          case (1)  !! plant operation
            igrow = 1
            if (igro_trcu(ihru) == 1) idplt_trcu(1,1,ihru) = mgt1i
            npllocal = npllocal + 1
            idplt_trcu(iro,npllocal,ihru) = mgt1i
            idplt_trcu(iro,npllocal+1,ihru) = mgt1i
            lcr = mgt1i
            iplant_trcu(iro,npllocal,ihru) = Jdt(ndays,day,mon)
            if (mgt4 < 700.) mgt4 = 1700.
            if (mgt4 > 5000.) mgt4 = 5000.
            trcu_phuplt(iro,npllocal,ihru) = mgt4
            trcu_phuplt(iro,npllocal+1,ihru) = mgt4
!            if (isproj == 2) then
!              if (husc > .5) husc = .15      !!CEAP fix for winter crops
!            end if
            if (husc > 0.) then
              phup_trcu(iro,npllocal,ihru) = husc
              if (husc > .5) phup_trcu(iro,npllocal+1,ihru) = husc
            endif
            trcu_cnop(iro,icnp,ihru) = mgt9
            icnp = icnp + 1
            trcu_cyrmat(ihru) = mgt3i
            trcu_hitarg(iro,npllocal,ihru) = mgt7
            trcu_biotarg(iro,npllocal,ihru) = mgt8 * 1000.
            trcu_inilai(iro,npllocal,ihru) = mgt5
            trcu_inibio(iro,npllocal,ihru) = mgt6
          
            !! calculate tnylda for autofertilization 
            if (pldb(mgt1i)%hvsti < 1.) then
              trcu_tnylda(iro,npllocal,ihru) = 350. * pldb(mgt1i)%cnyld * pldb(mgt1i)%bio_e
            else
              trcu_tnylda(iro,npllocal,ihru) = 1000. * pldb(mgt1i)%cnyld * pldb(mgt1i)%bio_e
            endif

          case (2)  !! irrigation operation
            nir = nir + 1
            tuirr_amt(iro,nir,ihru) = mgt4
            trcu_iir(iro,nir,ihru) = Jdt(ndays,day,mon)
            if (husc > 0.) then
              if (igrow == 1) then 
                phuirr_trcu(iro,nir,ihru) = husc
              else
                phuirr_nocrop_trcu(iro,nir,ihru) = husc
              end if
            end if 
            tuirr_efm(iro,nir,ihru) = mgt6
            tuirr_sq(iro,nir,ihru) = mgt7
            if (tuirr_efm(iro,nir,ihru) < 1.e-6)                &
     &                              tuirr_efm(iro,nir,ihru) =1.0

          case (3)  !! fertilizer operation
            if (mgt1i > 0) then           !! no fertilizer id #, ignore operation
              ifn = ifn + 1
              trcu_ifert(iro,ifn,ihru) = Jdt(ndays,day,mon)
              if (husc > 0.) then
                if (igrow == 1) then 
                  phun_trcu(iro,ifn,ihru) = husc
                else
                  phun_nocrop_trcu(iro,ifn,ihru) = husc
                end if
              end if
              tufrt_surface(iro,ifn,ihru) = mgt5
              if (tufrt_surface(iro,ifn,ihru) <= 1.e-6)                 &
     &                                  tufrt_surface(iro,ifn,ihru) = .2
              trcu_ifrttyp(iro,ifn,ihru) = mgt1i
              tufrt_kg(iro,ifn,ihru) = mgt4
            end if
            
          case (5)  !! harvest and kill operation
            nhv = nhv + 1
            igrow = 0
            ihv_trcu(iro,npllocal,ihru) = Jdt(ndays,day,mon)
            if (nhv > 1) then
              if (ihv_trcu(iro,nhv-1,ihru) <= 0) then
                ihv_trcu(iro,nhv-1,ihru) = ihv_trcu(iro,nhv,ihru)
              end if
            end if 
            if (husc > 0.) then
              phuh_trcu(iro,npllocal,ihru) = husc
              phuh_trcu(iro,npllocal+1,ihru) = husc
            endif
            trcu_cnop(iro,icnp,ihru) = mgt4
            icnp = icnp + 1
!!! add fraction stover removed for harvest and kill
            trcu_frharvk(iro,icnp,ihru) = mgt5

          case (6)  !! tillage operation
            inop = inop + 1
            iop_trcu(iro,inop,ihru) = Jdt(ndays,day,mon)
            if (husc > 0.) then
              if (igrow == 1) then
                phut_trcu(iro,inop,ihru) = husc
              else
                phut_nocr_trcu(iro,inop,ihru) = husc
              end if
            end if
            tuidtill(iro,inop,ihru) = mgt1i
            trcu_cnop(iro,icnp,ihru) = mgt4
            icnp = icnp + 1

          case (7)  !! harvest only operation
            nhvo = nhvo + 1
            ihvo_trcu(iro,nhvo,ihru) = Jdt(ndays,day,mon)
            if (husc > 0.) phuho_trcu(iro,nhvo,ihru) = husc
            ihv_gbm_trcu(iro,nhvo,ihru) = mgt2i
            trcu_hiovr(iro,nhvo,ihru) = mgt5
            tuharveff(iro,nhvo,ihru) = mgt4
            if (tuharveff(iro,nhvo,ihru) <= 0.) then
              tuharveff(iro,nhvo,ihru) = 1.
            endif

          case (8)  !! kill operation
            nkill = nkill + 1
            igrow = 0
            ikill_trcu(iro,npllocal,ihru) = Jdt(ndays,day,mon)
            if (husc > 0.) phuk_trcu(iro,npllocal,ihru) = husc
!           idplt(iro,npllocal+1,ihru) = lcr

          case (10)  !! auto irrigation operation
            nairr = nairr + 1
            tuiairr(iro,nairr-1,ihru) = Jdt(ndays,day,mon)
            if (husc > 0.) phuai_trcu(iro,nairr-1,ihru) = husc
            if (mgt1i <= 0) mgt1i = 1
            tuwstrs_id(iro,nairr,ihru) = mgt1i
            tuauto_wstr(iro,nairr,ihru) = mgt4
            tuirr_eff(iro,nairr,ihru) = mgt5
            tuirr_asq(iro,nairr,ihru) = mgt7
            tuirr_mx(iro,nairr,ihru) = mgt6
            if (tuirr_eff(iro,nairr,ihru) > 1.)                    &
     &              tuirr_eff(iro,nairr,ihru) = 1.
            if (tuirr_eff(iro,nairr,ihru) == 0.)                   &
     &              tuirr_eff(iro,nairr,ihru) = 1.
 !!    change per JGA for irrigation 4/2/2009
            if (tuirr_mx(iro,nairr,ihru) < 1.e-6)                 &
     &		   tuirr_mx(iro,nairr,ihru) = 25.4

          case (11)  !! auto fertilizer operation
            if (mgt1i > 0) then  
              nafer = nafer + 1
              tuafrt_surface(ihru) = mgt8
              if (tuafrt_surface(ihru) <= 1.e-6)                     &
     &                                      tuafrt_surface(ihru) = .2
              tuauto_nstrs(ihru) = mgt4
              trcu_iafrttyp(ihru) = mgt1i
              tuauto_napp(ihru) = mgt5
              if (tuauto_napp(ihru) <= 0.) tuauto_napp(ihru) = 200.
              tuauto_nyr(ihru) = mgt6
              if (tuauto_nyr(ihru) <= 0.) tuauto_nyr(ihru) = 300.
            end if

          case (14)  !! continuous fertilization operation
            icf = icf + 1
            trcu_icfert(iro,icf,ihru) = Jdt(ndays,day,mon)
            if (husc > 0.) phucf_trcu(iro,icf,ihru) = husc
            tufert_days(iro,icf,ihru) = mgt1i
            tucfrt_kg(iro,icf,ihru) = mgt4
            tucfrt_id(iro,icf,ihru) = mgt2i
            tuifrt_freq(iro,icf,ihru) = mgt3i
            if (tuifrt_freq(iro,icf,ihru) <= 0) then
              tuifrt_freq(iro,icf,ihru) = 1
            end if

            end select
          end do                                !! operation loop
 

        if (iro == tunrot(ihru)) exit
        end do                                  !! rotation loop

        idplt_trcu(1,1,ihru) = lcr
        do irotate = 2, tunrot(ihru)
         if (idplt_trcu(irotate,1,ihru) == 0)                           &
     &     idplt_trcu(irotate,1,ihru) = idplt_trcu(irotate-1,1,ihru)
         if (trcu_tnylda(irotate,2,ihru) == 0)                          &
     &     trcu_tnylda(irotate,2,ihru) = trcu_tnylda(irotate-1,2,ihru)
         if (trcu_tnylda(irotate,1,ihru) == 0)                          &
     &     trcu_tnylda(irotate,1,ihru) = trcu_tnylda(irotate,2,ihru)
        end do
      end if

      close (116)
     
      return
 5000 format (a)
 5200 format (1x,i2,1x,i2,1x,f8.3,1x,i2,1x,i4,1x,i3,1x,i2,1x,f12.5,1x,  &
     &        f6.2,1x,f11.5,1x,f4.2,1x,f6.2,1x,f5.2)
      end

      subroutine readtrc
 
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    This subroutine reads data from the HRU terrace input file (.trc).
!!    This file contains data related to terrace in the HRU.

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name           |units      |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
!!    hru_ha(:)      |ha         |area of HRU in hectares
!!    hru_slp(:)     |m/m        |average slope steepness
!!    slsubbsn(:)    |m          |average slope length for subbasin
!!    trc_fr(:)      |none       |fraction of terrace/HRU area
!!    trcb_ht(:)     |m          |The hight of ridge for bench terrace
!!    trcb_sll(:)    |m          |Average slope length in bed area of terrace
!!    trcr_sll(:)    |m          |Average slope length in cutslope area of terrace
!!    trcu_sll(:)    |m          |Average slope length in undisturbed area of terrace
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
 
!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name           |units      |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
!!    itrc1(:)       |none       |beginning month of nutrient settling 
!!                               |season
!!    itrc2(:)       |none       |ending month of nutrient settling season
!!    tdr_flag(:)    |none       |Code of drainage output. 
!!                               |1: terrace drainage output goes to secondary tributary; 
!!                               |2: terrace drainage output goes to subbasin main channel
!!    trc_tnkmm(:)   |mm         |minimum depth of rainfall for rainfall harvest tank to get water stored
!!    trc_chl(:)     |m          |Inside terrace segment channel length
!!    trc_chs(:)     |m/m        |Inside terrace segment channel slope
!!    trc_chtype(:)  |none       |Inside terrace segment channel type
!!                               |0: soil channel or no channel; 1: stone channel
!!    trc_tvolx(:)   |m^3         |maximum volume of rainfall harvest tank water storage 
!!    trc_drmm(:)    |mm H2O     |Drainage trigger depth of stored water 
!!                               |in terrace
!!    trc_no3(:)     |kg N       |amount of nitrate originating from 
!!                               |surface runoff in terrace at beginning of day
!!    trc_nsed(:)    |mg/L       |normal sediment concentration in terrace 
!!                               |water
!!    trc_orgn(:)    |kg N       |amount of organic N originating from 
!!                               |surface runoff in terrace at beginning of day
!!    trc_orgp(:)    |kg P       |amount of organic P originating from 
!!                               |surface runoff in terrace at beginning of day
!!    trc_pseda(:)   |kg P       |amount of mineral P attached to sediment 
!!                               |originating from surface runoff in terrace at 
!!                               |beginning of day
!!    trc_pseds(:)   |kg P       |amount of mineral P attached to sediment 
!!                               |originating from surface runoff in terrace at 
!!                               |beginning of day
!!    trc_release(:) |none       |Drainage trigger code. (0: No Drainage; 
!!                               |1: with drainage.)
!!    trc_sax(:)     |ha         |maximum free water surface area of terrace
!!    trc_solp(:)    |kg P       |amount of soluble P originating from 
!!                               |surface runoff in terrace at beginning of day
!!    trc_stlr_co(:) |none       |terrace sediment settling coefficient
!!    trc_type(:)    |none       |Code of terrace type. (1 : normal terrace; 
!!                               |2 : bench terrace)
!!    trc_vol(:)     |m^3 H2O    |volume of water in terrace
!!    trc_volx(:)    |m^3        |maximum storage volume for new terrace
!!    trc_wth(:)     |m          |Average terrace width
!!    trcb_fr(:)     |none       |fraction of terrace bed (frontslope)/terrace 
!!                               |area
!!    trcb_sll(:)    |m          |Average slope length in bed area of terrace
!!    trcb_slp(:)    |m/m        |terrace bed area slope
!!    trci_fr(:)     |none       |fraction of HRU area without terrace 
!!                               |area that drains into terrace
!!    trcr_fr(:)     |none       |fraction of terrace riser (cutslope)/terrace 
!!                               |area
!!    trcr_sll(:)    |m          |Average slope length in cutslope area of terrace
!!    trcr_slp(:)    |m/m        |terrace cutslope area slope
!!    trcu_fr(:)     |none       |fraction of terrace undisturbed area/terrace 
!!                               |area
!!    trcu_sll(:)    |m          |Average slope length in undisturbed area of terrace
!!    trcu_slp(:)    |m/m        |terrace undisturbed area slope
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name           |units       |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
!!    tnkm3ha        |m3/ha       |Maximum rainfall harvest tank water storage volume per area
!!    radu           |none        |radian of terrace undisturbed area
!!    radr           |none        |radian of terrace riser segment(cutslope)
!!    radr           |none        |radian of terrace bed segment(frontslope)
!!    sumsll         |m           |sum of slope length of one undisturbed area
!!                                |one cutslope and one frontslope
!!    wth            |m           |total width of terrace in HRU
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    SWAT: readmtu, readmtr, readmtb

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~


      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      use reservoir_module
      implicit none
      character (len=80) :: titldum
      character (len=13) :: mtufile, mtrfile, mtbfile
      integer :: eof
      real :: radu, radr, radb, wth, sumsll, tnkm3ha, xx

      radu = 0.
      radr = 0.
      radb = 0.
      wth = 0.
      sumsll = 0.
      tnkm3ha = 0.

      do
      read (115,5100,iostat=eof) titldum
      read (115,5100,iostat=eof) titldum
      read (115,*,iostat=eof) trc_fr(ihru) !! for SWAT+
      if (trc_fr(ihru) < 1.e-6) return !! for SWAT+
      read (115,*,iostat=eof) trc_type(ihru)
      read (115,*,iostat=eof) trci_fr(ihru)
      read (115,*,iostat=eof) trc_vol(ihru)
      read (115,*,iostat=eof) trc_sed(ihru)
      read (115,*,iostat=eof) trc_nsed(ihru)
      read (115,*,iostat=eof) trc_no3(ihru)
      read (115,*,iostat=eof) trc_solp(ihru)
      read (115,*,iostat=eof) trc_orgn(ihru)
      read (115,*,iostat=eof) trc_orgp(ihru)
      read (115,*,iostat=eof) itrc1(ihru)
      read (115,*,iostat=eof) itrc2(ihru)
      read (115,*,iostat=eof) trc_psetlp1(ihru)
      read (115,*,iostat=eof) trc_psetlp2(ihru)
      read (115,*,iostat=eof) trc_nsetlp1(ihru)
      read (115,*,iostat=eof) trc_nsetlp2(ihru)
      read (115,*,iostat=eof) trc_release(ihru)
      read (115,*,iostat=eof) trc_drmm(ihru)
      read (115,*,iostat=eof) trc_drx(ihru)
      read (115,*,iostat=eof) tdr_flag(ihru)
      read (115,*,iostat=eof) trc_tnkmm(ihru)
      read (115,*,iostat=eof) tnkm3ha
      read (115,*,iostat=eof) trc_chtype(ihru)
      read (115,*,iostat=eof) trc_chl(ihru)
      read (115,*,iostat=eof) trc_chs(ihru)
      read (115,*,iostat=eof) trc_wth(ihru)
      read (115,*,iostat=eof) trc_stlr_co(ihru)
      if (eof < 0) exit

      mtufile = ""
      mtrfile = ""
      mtbfile = ""
!     read undisturbed segment (between terrace) parameters
      read (115,5100,iostat=eof) titldum
      if (eof < 0) exit
      read (115,5300,iostat=eof) mtufile
      if (mtufile /= '             ') then
        call caps(mtufile)
        open (116,file=mtufile)
        call readmtu
        close (116)
      end if
      if (eof < 0) exit
!     read raiser segment (cutslope) parameters
      read (115,5100,iostat=eof) titldum
      if (eof < 0) exit
      read (115,5300,iostat=eof) mtrfile
      if (mtrfile /= '             ') then
        call caps(mtrfile)
        open (116,file=mtrfile)
        call readmtr
        close (116)
      end if
      if (eof < 0) exit
!     read bed segment (frontslope) parameters
      read (115,5100,iostat=eof) titldum
      if (eof < 0) exit
      read (115,5300,iostat=eof) mtbfile
      if (mtbfile /= '             ') then
        call caps(mtbfile)
        open (116,file=mtbfile)
        call readmtb
        close (116)
      end if
      if (eof < 0) exit
      exit
      end do

!     set general parameter default values
      if (trci_fr(ihru) < 1.e-6) trci_fr(ihru) = 0.
      if (trc_vol(ihru) < 1.e-6) trc_vol(ihru) = 0.
      if (trc_sed(ihru) < 1.e-6) trc_sed(ihru) = 0.
      if (trc_nsed(ihru) < 1.e-6) trc_nsed(ihru) = 0.
      if (trc_no3(ihru) < 1.e-6) trc_no3(ihru) = 0.
      if (trc_solp(ihru) < 1.e-6) trc_solp(ihru) = 0.
      if (trc_orgn(ihru) < 1.e-6) trc_orgn(ihru) = 0.
      if (trc_orgp(ihru) < 1.e-6) trc_orgp(ihru) = 0.
      if (itrc1(ihru) < 1 .or. itrc1(ihru) > 12) itrc1(ihru) = 0
      if (itrc2(ihru) < 1 .or. itrc2(ihru) > 12) itrc2(ihru) = 0
      if (trc_release(ihru) < 1.e-6) trc_release(ihru) = 0
      if (trc_drmm(ihru) < 1.e-6) trc_drmm(ihru) = 0.
      if (trc_drx(ihru) < 1.e-6) trc_drx(ihru) = 0.
      if (trc_tnkmm(ihru) < 1.e-6) trc_tnkmm(ihru) = 5.0
      trc_tvolx(ihru) = 0.
      if (tnkm3ha > 0.1) then
        trc_tvolx(ihru) = tnkm3ha * (hru(ihru)%km * 100) * trc_fr(ihru)
      end if
      if (trc_chtype(ihru) /= 1) then
        trc_chtype(ihru) = 0
      end if
      if (trc_wth(ihru) < 1.e-6) trc_wth(ihru) = 50.
      if (trc_chl(ihru) < 1.e-6) trc_chl(ihru) = 0.71 * trc_wth(ihru)
      if (trc_chs(ihru) < 1.e-6) trc_chs(ihru) = 0.

!     set segment parameter default values
      if (trc_type(ihru) /= 2) then
        trc_type(ihru) = 1
      end if

      if (trcu_sll(ihru) < 1.e-6) trcu_sll(ihru) = 0.
      if (trcu_slp(ihru) < 1.e-6) trcu_slp(ihru) = hru(ihru)%topo%slope
      if (trcb_sll(ihru) < 1.e-6) then
        trcb_sll(ihru) = min(10.,hru(ihru)%topo%slope_len)  ! default frontslope length 10m
      end if
      if (trcr_slp(ihru) < hru(ihru)%topo%slope) then
        trcr_slp(ihru) = max(1.,hru(ihru)%topo%slope)  ! default cutslope gradient 1m/1m
      end if


      if (trc_type(ihru) == 1) then  ! normal terrace
        if (trcb_slp(ihru) < 1.e-6) trcb_slp(ihru) = 0.01  ! default frontslope gradient 1%
        radu = atan(hru(ihru)%topo%slope)
        radr = atan(trcr_slp(ihru))
        radb = atan(trcb_slp(ihru))
        if (trcr_sll(ihru) < 1.e-6) then
            trcr_sll (ihru) = 0.5 * trcb_sll(ihru) * cos(radr)          &
     &                 / cos(radb) * sin(radb + radu) / sin(radr - radu)
        end if
        xx = 0.
        sumsll = trcu_sll(ihru) + 2 * trcr_sll(ihru) + trcb_sll(ihru)
        if (sumsll > hru(ihru)%topo%slope_len) then
            xx = hru(ihru)%topo%slope_len / sumsll
            trcu_sll(ihru) = trcu_sll(ihru) * xx
            trcr_sll(ihru) = trcr_sll(ihru) * xx
            trcb_sll(ihru) = trcb_sll(ihru) * xx
        end if
        wth = (hru(ihru)%km * 100) * 10000. * trc_fr(ihru) /                    &
     &             (trcu_sll(ihru) + trcr_sll(ihru) + trcb_sll(ihru))
        trc_volx(ihru) = 0.5 * (trcb_sll(ihru) * trcb_slp(ihru))**2.    &
     &                  * (1 / trcb_slp(ihru) + 1/trcr_slp(ihru)) * wth
        trc_sax(ihru) = trcb_sll(ihru) * trcb_slp(ihru)                 &
     &                  * (1 / trcb_slp(ihru) + 1/trcr_slp(ihru)) * wth
        trc_sax(ihru) = trc_sax(ihru) / 10000.      ! m^2 => ha
        trc_htx(ihru) = trcb_sll(ihru) * trcb_slp(ihru)
      else                          ! bench terrace
        trcb_slp(ihru) = 0.  ! default frontslope gradient 0%
        if (trcb_ht(ihru) < 1.e-6) trcb_ht(ihru) = 0.3  ! default frontslope ridge hight 30cm
        radu = atan(hru(ihru)%topo%slope)
        radr = atan(trcr_slp(ihru))
        if (trcr_sll(ihru) < 1.e-6) then
            trcr_sll (ihru) = 0.5 * trcb_sll(ihru) * cos(radr)          &
     &                                  * sin(radu) / sin(radr - radu)
        end if
        xx = 0.
        sumsll = trcu_sll(ihru) + 2 * trcr_sll(ihru) + trcb_sll(ihru)
        if (sumsll > hru(ihru)%topo%slope_len) then
            xx = hru(ihru)%topo%slope_len / sumsll
            trcu_sll(ihru) = trcu_sll(ihru) * xx
            trcr_sll(ihru) = trcr_sll(ihru) * xx
            trcb_sll(ihru) = trcb_sll(ihru) * xx
        end if
        wth = (hru(ihru)%km * 100) * 10000. * trc_fr(ihru) / (trcu_sll(ihru)    &
     &                          + 2. * trcr_sll(ihru) + trcb_sll(ihru))
        trc_volx(ihru) = trcb_ht(ihru) * trcb_sll(ihru) * wth
        trc_sax(ihru) = trcb_sll(ihru) * wth
        trc_sax(ihru) = trc_sax(ihru) / 10000.      ! m^2 => ha
      end if

      sumsll = trcu_sll(ihru) + 2 * trcr_sll(ihru) + trcb_sll(ihru)
      trcu_fr(ihru) = trcu_sll(ihru) / sumsll
      trcr_fr(ihru) = 2 * trcr_sll(ihru) / sumsll
      trcb_fr(ihru) = trcb_sll(ihru) / sumsll

      if (trc_vol(ihru) < 1.e-6) then
           trc_vol(ihru) = 0.
           trc_sed(ihru) = 0.
           trc_no3(ihru) = 0.
           trc_solp(ihru) = 0.
           trc_orgn(ihru) = 0.
           trc_orgp(ihru) = 0.
      end if

      ! drainage default goes to secondary tributary
      if (tdr_flag(ihru) /= 2)  tdr_flag(ihru) = 1

      close (115)

      return
 5100 format (a)
 5300 format (a13)
    end
    
    subroutine hru_trc_read
        use jrw_datalib_module
        use input_file_module
        use parm 
        integer :: eof, ii, imax
        eof = 0
        imax = 0
        mhru_db = 0
      do
        open (113,file=in_hru%hru_data)
        read (113,*,iostat=eof) titldum
        if (eof < 0) exit
        read (113,*,iostat=eof) header
        if (eof < 0) exit
         do while (eof >= 0)
            read (113,*,iostat=eof) ii
            if (eof < 0) exit
            imax = amax1(imax,ii)
            mhru_db = mhru_db + 1
         end do
         end do
         close (113)
    do ihru = 1, mhru_db        
    !! H.Shao >>
        i = ihru
        inquire (file=hru_db(i)%dbsc%trc_data, exist=i_exist)
        if (i_exist /= 0) then
            open (115,file=hru_db(i)%dbsc%trc_data)
            call readtrc
            call trc_init
        end if
        !! H.Shao <<
    end do
    close (115)
    end subroutine

	subroutine rootfr_trc
	!! This subroutine distributes dead root mass through the soil profile
	!! code developed by Armen R. Kemanian in 2008 
	!! March, 2009 further adjustments expected
		
	use parm; use terrace_parm; use basin_module
      implicit none
	real :: sol_thick(soil(ihru)%nly)
	real :: cum_rd, cum_d, cum_rf, x1, x2
      real :: a, b, c, d, xx1, xx2, exp, xx
	integer :: k, l, jj
	
	jj = ihru	

	! Normalized Root Density = 1.15*exp[-11.7*NRD] + 0.022, where NRD = normalized rooting depth
	! Parameters of Normalized Root Density Function from Dwyer et al 19xx
	a = 1.15
	b = 11.7
	c = 0.022
	d = 0.12029 ! Integral of Normalized Root Distribution Function 
				! from 0 to 1 (normalized depth) = 0.12029

	l = 0
	k = 0
	cum_d = 0.
	cum_rf = 0.
         sol_thick(:) = 0.
 !        rtfr = 0.

	do l=1, soil(jj)%nly
	  if (l == 1) then
	    sol_thick(l) = soil(jj)%phys(l)%d
	  else	
	    sol_thick(l) = soil(jj)%phys(l)%d - soil(jj)%phys(l-1)%d
	  end if
		
	  cum_d = cum_d + sol_thick(l)
	  if (cum_d >= trc_rd) cum_rd = trc_rd
	  if (cum_d < trc_rd) cum_rd = cum_d
	  x1 = (cum_rd - sol_thick(l)) / trc_rd
	  x2 = cum_rd / trc_rd
           xx1 = -b * x1
	  if (xx1 > 20.) xx1 = 20.
           xx2 = -b * x2
           if (xx2 > 20.) xx2 = 20.
	  soil(jj)%ly(l)%rtfr=(a/b*(Exp(xx1) - Exp(xx2)) + c *(x2 - x1)) / d
           xx = cum_rf
	  cum_rf = cum_rf + soil(jj)%ly(l)%rtfr
           if (cum_rf > 1.) then
	   soil(jj)%ly(l)%rtfr = 1. - xx
             cum_rf = 1.0
           end if
	  k = l
	  if (cum_rd >= trc_rd) Exit
		 
	end do

	!!	 ensures that cumulative fractional root distribution = 1
	do l=1, soil(jj)%nly
		soil(jj)%ly(l)%rtfr = soil(jj)%ly(l)%rtfr / cum_rf
		If (l == k) Exit ! exits loop on the same layer as the previous loop
	end do
   
	end subroutine



      subroutine solp_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine calculates the amount of phosphorus lost from the soil
!!    profile in runoff and the movement of soluble phosphorus from the first
!!    to the second layer via percolation

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name          |units        |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    conv_wt(:,:)  |none         |factor which converts kg/kg soil to kg/ha
!!    time%yrs         |none         |current year of simulation
!!    ihru          |none         |HRU number
!!    pco%nyskip        |none         |number of years to skip output
!!    phoskd        |none         |Phosphorus soil partitioning coefficient
!!                                |Ratio of phosphorus attached to sediment to
!!                                |phosphorus dissolved in soil water
!!    pperco        |none         |phosphorus percolation coefficient (0-1)
!!                                |0:concentration of soluble P in surface
!!                                |  runoff is zero
!!                                |1:percolate has same concentration of soluble
!!                                |  P as surface runoff
!!    sol_bd(:,:)   |Mg/m**3      |bulk density of the soil
!!    sol_nly(:)    |none         |number of layers in soil profile
!!    sol_z(:,:)    |mm           |depth to bottom of soil layer
!!    trc_ha        |ha           |terrace segment area in hectare
!!    trcq          |mm H2O       |surface runoff of terrace segment
!!    tsol_prk(:)   |mm H2O       |percolation from soil layer on current day
!!    tsol_solp(:)  |kg P/ha      |amount of phosohorus stored in solution
!!    wshd_plch     |kg P/ha      |average annual amount of phosphorus leached
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name          |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    trcqsolp      |kg P/ha       |amount of soluble phosphorus in surface
!!                                 |runoff in terrace segment for the day
!!    tsol_solp(:)  |kg P/ha       |amount of phosohorus stored in solution
!!    wshd_plch     |kg P/ha       |average annual amount of phosphorus leached
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    j           |none          |HRU number
!!    vap         |kg P/ha       |amount of P leached from soil layer
!!    xx          |none          |variable to hold intermediate calculation
!!                               |result
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Min, Max

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j, ii
      real :: xx, vap

      j = 0
      j = ihru

!! compute soluble P lost in surface runoff
      xx = 0.
      xx = soil(j)%phys(1)%bd * soil(j)%phys(1)%d * bsn_prm%phoskd
      trcqsolp = tsol_solp(1) * trcq / xx 

      trcqsolp = Min(trcqsolp, tsol_solp(1))
      trcqsolp = Max(trcqsolp, 0.)
      tsol_solp(1) = tsol_solp(1) - trcqsolp


!! compute soluble P leaching
      vap = 0.
      vap = tsol_solp(1) * tsol_prk(1) / ((soil(j)%phys(1)%conv_wt/ 1000.) * bsn_prm%pperco + .1)
      vap = Min(vap, .5 * tsol_solp(1))
      tsol_solp(1) = tsol_solp(1) - vap
      if (soil(j)%nly >= 2) then
        tsol_solp(2) = tsol_solp(2) + vap
      end if
   
      do ii=2,soil(j)%nly-1
        vap = 0.
        vap = tsol_solp(ii) * tsol_prk(ii) / ((soil(j)%phys(ii)%conv_wt / 1000.) * bsn_prm%pperco + .1)
	   vap = Min(vap, .2 * tsol_solp(ii))
	   tsol_solp(ii) = tsol_solp(ii) - vap
	   tsol_solp(ii+1) = tsol_solp(ii+1) + vap
  
	end do
	
      percp(j) = vap * trc_ha / (hru(j)%km*100)

    !! summary calculation
 !     if (time%yrs > pco%nyskip) then
!        wshd_plch = wshd_plch + vap * hru_dafr(j) * trc_ha / (hru(j)%km*100)
 !     end if


      return
      end

      subroutine swu_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine distributes terrace segment potential plant evaporation 
!!    through the root zone and calculates actual plant water use based on soil
!!    water availability. Also estimates water stress factor.     

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    epco(:)     |none          |plant water uptake compensation factor (0-1)
!!    idc(:)      |none          |crop/landcover category:
!!                               |1 warm season annual legume
!!                               |2 cold season annual legume
!!                               |3 perennial legume
!!                               |4 warm season annual
!!                               |5 cold season annual
!!                               |6 perennial
!!                               |7 trees
!!    idplt_trc   |none          |land cover code from crop.dat
!!    ihru        |none          |HRU number
!!    iwatable    |none          |high water table code:
!!                               |0 no high water table
!!                               |1 high water table
!!    phuacc(:)   |none          |fraction of plant heat units accumulated
!!    sol_fc(:,:) |mm H2O        |amount of water available to plants in soil
!!                               |layer at field capacity (fc - wp water)
!!    sol_nly(:)  |none          |number of soil layers in profile
!!    sol_ul(:,:) |mm H2O        |amount of water held in the soil layer at
!!                               |saturation
!!    sol_z(:,:)  |mm            |depth to bottom of soil layer
!!    sol_zmx(:)  |mm            |maximum rooting depth
!!    tep_max     |mm H2O        |maximum amount of transpiration (plant et)
!!                               |that can occur on current day in HRU
!!    tsol_st(:)  |mm H2O        |amount of water stored in the soil layer on
!!                               |current day
!!    ubw         |none          |water uptake distribution parameter
!!                               |This parameter controls the amount of
!!                               |water removed from the different soil layers
!!                               |by the plant. In particular, this parameter
!!                               |allows the amount of water removed from
!!                               |the surface layer via plant uptake to be
!!                               |controlled. While the relationship between
!!                               |UBW and H2O removed from the surface layer is
!!                               |affected by the depth of the soil profile, in
!!                               |general, as UBW increases the amount of water
!!                               |removed from the surface layer relative to the
!!                               |amount removed from the entire profile
!!                               |increases
!!    uobw        |none          |water uptake normalization parameter
!!                               |This variable normalizes the water uptake so
!!                               |that the model can easily verify that uptake
!!                               |from the different soil layers sums to 1.0
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tep_day     |mm H2O        |actual amount of transpiration that occurs
!!                               |on day in HRU
!!    tsol_rd     |mm            |current rooting depth
!!    tsol_st(:)  |mm H2O        |amount of water stored in the soil layer on
!!                               |current day
!!    tsol_sw     |mm H2O        |amount of water stored in soil profile on
!!                               |current day
!!    tstrsw      |none          |fraction of potential plant growth achieved
!!                               |on the day where the reduction is caused by
!!                               |water stress
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    gx          |
!!    ir          |
!!    j           |none          |HRU number
!!    k           |none          |counter (soil layer)
!!    reduc       |none          |fraction of water uptake by plants achieved
!!                               |where the reduction is caused by low water
!!                               |content
!!    sum         |
!!    sump        |
!!    ul4         |
!!    wuse(:)     |mm H2O        |water uptake by plants in each soil layer
!!    xx          |mm H2O        |water uptake by plants from all layers
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    
!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Exp, Max

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      implicit none
      integer :: j, k, ir
      real, dimension(mlyr + 1) :: wuse
      real :: sum, xx, gx, reduc, ul4, sump, satco, yy

      j = 0
      j = ihru

      select case (pldb(idplt_trc(tnro,ticr))%idc)
        case (1, 2, 4, 5)
          tsol_rd = 2.5 * trc_phuacc * soil(j)%zmx
          if (tsol_rd > soil(j)%zmx) tsol_rd = soil(j)%zmx
          if (tsol_rd < 10.) tsol_rd = 10.
        case default
          tsol_rd = soil(j)%zmx
      end select

	  trc_rd = tsol_rd

      if (tep_max <= 0.01) then
        tstrsw = 1.
      else
        !! initialize variables
        gx = 0.
        ir = 0
        sump = 0.
        wuse = 0.
        xx = 0.
 
!!  compute aeration stress
        if (tsol_sw > soil(j)%sumfc) then
          satco = (tsol_sw - soil(j)%sumfc) / (soil(j)%sumul -              &
     &                                                 soil(j)%sumfc)
          tstrsa = 1. - (satco / (satco + Exp(.176 - 4.544 *                 &
     &                                                      satco)))
        else
          tstrsa = 1.
        end if

        do k = 1, soil(j)%nly
          if (ir > 0) exit

          if (tsol_rd <= soil(j)%phys(k)%d) then
            gx = tsol_rd
            ir = k
          else
            gx = soil(j)%phys(k)%d
          end if

          sum = 0.
          if (tsol_rd <= 0.01) then
            sum = tep_max / uobw
          else
            sum = tep_max * (1. - Exp(-ubw * gx / tsol_rd)) / uobw
          end if

          !! don't allow compensation for aeration stress
          if (tstrsa > .99) then
            yy = 0.
          else
            yy= sump - xx
          end if
          wuse(k) = sum - sump + yy * hru(j)%hyd%epco
          wuse(k) = sum - sump + (sump - xx) * hru(j)%hyd%epco
          sump = sum

          !! adjust uptake if sw is less than 25% of plant available water
          reduc = 0.
          if (tsol_st(k) < soil(j)%phys(k)%fc/4.) then
            reduc = Exp(5. * (4. * tsol_st(k) / soil(j)%phys(k)%fc - 1.))
          else
            reduc = 1.
          endif
!         adjust the water uptake
          reduc = 1.
          wuse(k) = wuse(k) * reduc

          if (tsol_st(k) < wuse(k)) then
            wuse(k) = tsol_st(k)
          end if

          tsol_st(k) = Max(1.e-6, tsol_st(k) - wuse(k))
          xx = xx + wuse(k)
          end do

        !! update total soil water in profile
        tsol_sw = 0.
        do k = 1, soil(j)%nly
          tsol_sw = tsol_sw + tsol_st(k)
        end do

        tstrsw = xx / tep_max
        tep_day = xx
      end if

      return
      end

      subroutine tankstor_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine collects the rainfall for rainfall harvest tank storage.

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name         |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tnk_eff(:)   |none          |rainfall harvest tank water collecting efficiency
!!    prec_trc     |mm H2O        |precipitation for the day in terrace segment
!!    trc_tankfr   |ha/ha         |fraction of terrace segment area for rainfall
!!                                |collection
!!    trc_tnkmm(:) |mm            |minimum depth of rainfall for rainfall harvest tank to get
!!                                |water stored
!!    trc_tvol(:)  |m^3           |volume of rainfall harvest tank water storage 
!!    trc_tvolx(:) |m^3           |maximum volume of rainfall harvest tank water storage 
!!    trc_sumev    |m^3 H2O       |daily evaporation on terrace
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name         |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    trc_tvol(:)  |m^3 H2O       |volume of rainfall harvest tank water storage 
!!    trc_sumev    |m^3 H2O       |daily evaporation on terrace
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    cnv         |none          |conversion factor
!!    vol         |m^3 H2O       |volume of rainfall for rainfall harvest tank to collect
!!    volbkmm     |mm H2O        |amount of water exceeds top of the rainfall harvest tank
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Max

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j
      real :: vol, volbkmm, cnv, sump

      j = 0
      j = ihru

      cnv = 0.
      cnv = trc_ha * 10.

      sump = 0.
      vol = 0.
      volbkmm = 0.
      sump = prec_trc
      vol = prec_trc * cnv * trc_tankfr
      prec_trc = prec_trc - vol / cnv
      if (sump > trc_tnkmm(j)) then
          trc_tvol(j) = trc_tvol(j) + vol * tnk_eff(j)
          trc_sumev = trc_sumev + vol * (1. - tnk_eff(j))

          !! check water not excess rainfall harvest tank maximum volume
          if (trc_tvol(j) > trc_tvolx(j)) then
            volbkmm = (trc_tvol(j) - trc_tvolx(j)) / cnv
            trc_tvol(j) = trc_tvolx(j)
            prec_trc = prec_trc + volbkmm
          end if
      else
          trc_sumev = trc_sumev + vol
      end if
      
      return
      end

      subroutine terrac(k)
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine controls the simulation of terrace segment and storage 
!!    effects.

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name           |units      |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    blai_trc       |none       |maximum (potential) leaf area index
!!    canmx_trc      |mm H2O     |maximum canopy storage for terrace segment
!!    cncoef_sub     |           |soil water depletion coefficient used
!!                               | in the new (modified curve number method)
!!                               | same as soil index coeff used in APEX
!!                               | range: 0.5 - 2.0
!!    hru_sub(:)     |none       |subbasin in which HRU is located
!!    idplt_trc(:,:) |none       |land cover code from crop.dat for terrace 
!!                               |segment
!!    itrc1(:)       |none       |beginning month of nutrient settling 
!!                               |season
!!    itrc2(:)       |none       |ending month of nutrient settling season
!!    laiday_trc     |m**2/m**2  |leaf area index for terrace segment
!!    ovflwi         |m^3 H2O    |volume of water flow in terrace
!!    pet_day        |mm H2O     |potential evapotranspiration on current day in
!!                               |HRU
!!    phuirr_trc(:,:)|none       |fraction of plant heat units at which
!!                               |irrigation occurs
!!    prec_trc       |mm H2O     |daily precipitation on terrace
!!    snofall        |mm H2O     |amount of precipitation falling as freezing 
!!    snomlt         |mm H2O     |amount of snow melt in HRU on current day
!!    tbsol_actp(:,:)|kg P/ha    |amount of phosphorus stored in the 
!!                               |active mineral phosphorus pool for bed (frontslope)
!!    tbsol_fon(:,:) |kg N/ha    |amount of nitrogen stored in the fresh 
!!                               |nitrogen pool for bed (frontslope)
!!    tbsol_fop(:,:) |kg P/ha    |amount of phosphorus stored in the 
!!                               |fresh phosphorus pool for bed (frontslope)
!!    tbsol_mn(:,:)  |kg N/ha    |amount of nitrogen stored in the active 
!!                               |organic N pool
!!    tbsol_mp(:,:)  |kg P/ha    |amount of phosphorus stored in the 
!!                               |active organic P pool
!!    tbsol_n(:,:)   |kg N/ha    |amount of nitrogen stored in the stable 
!!                               |organic N pool
!!    tbsol_nh3(:,:) |kg N/ha    |amount of nitrogen stored in the ammonium 
!!                               |pool for bed (frontslope)
!!    tbsol_no3(:,:) |kg N/ha    |amount of nitrogen stored in the nitrate 
!!                               |pool for bed (frontslope)
!!    tbsol_orgp(:,:)|kg P/ha    |amount of phosphorus stored in organic 
!!                               |for bed (frontslope)
!!    tbsol_solp(:,:)|kg P/ha    |amount of phosohorus stored in solution 
!!                               |for bed (frontslope)
!!    tbsol_st(:,:)  |mm H2O     |amount of water stored in soil profile 
!!                               |on any given day for terrace bed (frontslope)
!!    tbsol_stap(:,:)|kg P/ha    |amount of phosphorus stored in the 
!!                               |stable mineral phosphorus pool for bed (frontslope)
!!    tbsol_sw(:)    |mm H2O     |amount of water stored in soil profile 
!!                               |of terrace bed (frontslope)
!!    tdr_flag(:)    |none       |Code of drainage output. 
!!                               |1: terrace drainage output goes to HRU outlet; 
!!                               |2: terrace drainage output goes to subbasin main channel
!!    tes_day        |mm H2O     |actual amount of evaporation (soil 
!!                               |et) that occurs on day in terrace segment
!!    tflag          |none       |Code for terrace calculation. 1: undisturb; 
!!                               |2: riser(cutslope); 3: bed (frontslope)
!!    tlatno3        |kg N/ha    |amount of NO3-N in lateral flow in 
!!                               |terrace segment
!!    tpeakr         |m^3/s H2O  |peak runoff rate for the day
!!    trc_chs(:)     |m/m        |Inside terrace segment channel slope
!!    trc_chtype(:)  |none       |Inside terrace segment channel type
!!    trc_cla(:)     |metric tons|amount of clay sediment in terrace
!!    trc_cn         |none       |terrace segment curve number for moisture 
!!                               |condition II
!!    trc_drmm(:)    |mm H2O     |Drainage trigger depth of stored water 
!!                               |in terrace
!!    trc_fr(:)      |none       |fraction of terrace/HRU area
!!    trc_ha         |ha         |terrace area
!!    trc_iir(:,:,:) |julian date|date of irrigation operation on terrace segment
!!    trc_lag(:)     |metric tons|amount of large aggregate sediment 
!!                               |in terrace
!!    trc_n          |none       |Manning's "n" value for overland flow
!!    trc_no3(:)     |kg N       |amount of nitrate originating from 
!!                               |surface runoff in terrace at beginning of day
!!    trc_no3s(:)    |kg N       |amount of nitrate originating from 
!!                               |lateral flow in terrace at beginning of day
!!    trc_nsed(:)    |mg/L       |normal sediment concentration in terrace 
!!                               |water
!!    trc_orgn(:)    |kg N       |amount of organic N originating from 
!!                               |surface runoff in terrace at beginning of day
!!    trc_orgp(:)    |kg P       |amount of organic P originating from 
!!                               |surface runoff in terrace at beginning of day
!!    trc_pseda(:)   |kg P       |amount of mineral P attached to sediment 
!!                               |originating from surface runoff in terrace at 
!!                               |beginning of day
!!    trc_pseds(:)   |kg P       |amount of mineral P attached to sediment 
!!                               |originating from surface runoff in terrace at 
!!                               |beginning of day
!!    trc_release(:) |none       |Drainage trigger code. (0: No Drainage; 
!!                               |1: with drainage.)
!!    trc_sag(:)     |metric tons|amount of small aggregate sediment 
!!                               |in terrace
!!    trc_san(:)     |metric tons|amount of sand sediment in terrace
!!    trc_sax(:)     |ha         |maximum free water surface area of terrace
!!    trc_sed(:)     |metric tons|amount of sediment in terrace
!!    trc_sil(:)     |metric tons|amount of silt sediment in terrace
!!    trc_sll        |m          |terrace segment slope length
!!    trc_slp        |m/m        |terrace segment slope
!!    trc_solp(:)    |kg P       |amount of soluble P originating from 
!!                               |surface runoff in terrace at beginning of day
!!    trc_tankfr     |ha/ha      |fraction of terrace segment area for rainfall
!!                               |collection
!!    trc_tvol(:)    |m^3        |volume of rainfall harvest tank water storage 
!!    trc_tvolx(:)   |m^3        |maximum volume of rainfall harvest tank water storage 
!!    trc_type(:)    |none       |Code of terrace type. (1 : normal terrace; 
!!                               |2 : bench terrace)
!!    trc_uslek      |none       |USLE equation soil erodibility (K) 
!!                               |factor for terrace segment
!!    trc_vmax       |m^3        |maximum volume of terrace storage at 
!!                               |the beginning of the day
!!    trc_vol(:)     |m^3 H2O    |volume of water in terrace
!!    trcb_fr(:)     |none       |fraction of terrace bed (frontslope)/terrace 
!!                               |area
!!    trcbtm         |mm H2O     |amount of infiltrated water into the 
!!                               |shallow aquifer
!!    trccla         |metric tons|amount of clay sediment generated on 
!!                               |terrace segment
!!    trcflwi        |m^3 H2O    |volume of water flowing into terrace 
!!                               |on day
!!    trci_fr(:)     |none       |fraction of HRU area without terrace 
!!                               |area that drains into terrace
!!    trclag         |metric tons|amount of large aggregate sediment 
!!                               |generated on terrace segment
!!    trclatq        |mm H2O     |total lateral flow in soil profile
!!    trcq           |mm H2O     |surface runoff generated on terrace 
!!                               |segment
!!    trcqno3        |kg N/ha    |amount of nitrate transported with 
!!                               |surface runoff for terrace segment
!!    trcqsolp       |kg P/ha    |amount of soluble phosphorus in surface 
!!                               |runoff for terrace segment
!!    trcr_fr(:)     |none       |fraction of terrace riser (cutslope)/terrace 
!!                               |area
!!    trcsag         |metric tons|amount of small aggregate sediment 
!!                               |generated in terrace segment
!!    trcsan         |metric tons|amount of sand sediment generated in 
!!                               |terrace segment
!!    trcsed         |metric tons|amount of sediment generated in terrace 
!!                               |segment
!!    trcsil         |metric tons|amount of silt sediment generated in 
!!                               |terrace segment
!!    trcu_fr(:)     |none       |fraction of terrace undisturbed area/terrace 
!!                               |area
!!    tsedminpa      |kg P/ha    |amount of active mineral phosphorus 
!!                               |sorbed to sediment in surface runoff in terrace
!!                               |for day
!!    tsedminps      |kg P/ha    |amount of stable mineral phosphorus 
!!                               |sorbed to sediment in surface runoff in terrace
!!                               |for day
!!    tsedorgn       |kg N/ha    |amount of organic nitrogen in surface 
!!                               |runoff in terrace for the day
!!    tsedorgp       |kg P/ha    |amount of organic phosphorus in surface 
!!                               |runoff in terrace for the day
!!    tsol_cov       |kg/ha      |amount of residue on soil surface of 
!!                               |terrace segment
!!    twltrc         |mm H2O     |water lost through seepage from terrace 
!!                               |on day in HRU
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name           |units      |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    inftrc         |mm H2O     |amount of water infiltrated into soil 
!!                               |for terrace segment
!!    tankirr(:)     |mm H2O     |amount of water removed from rainfall harvest tank
!!    tbsol_actp(:,:)|kg P/ha    |amount of phosphorus stored in the 
!!                               |active mineral phosphorus pool for bed (frontslope)
!!    tbsol_fon(:,:) |kg N/ha    |amount of nitrogen stored in the fresh 
!!                               |nitrogen pool for bed (frontslope)
!!    tbsol_fop(:,:) |kg P/ha    |amount of phosphorus stored in the 
!!                               |fresh phosphorus pool for bed (frontslope)
!!    tbsol_mn(:,:)  |kg N/ha    |amount of nitrogen stored in the active 
!!                               |organic N pool
!!    tbsol_mp(:,:)  |kg P/ha    |amount of phosphorus stored in the 
!!                               |active organic P pool
!!    tbsol_n(:,:)   |kg N/ha    |amount of nitrogen stored in the stable 
!!                               |organic N pool
!!    tbsol_nh3(:,:) |kg N/ha    |amount of nitrogen stored in the ammonium 
!!                               |pool for bed (frontslope)
!!    tbsol_no3(:,:) |kg N/ha    |amount of nitrogen stored in the nitrate 
!!                               |pool for bed (frontslope)
!!    tbsol_orgp(:,:)|kg P/ha    |amount of phosphorus stored in organic 
!!                               |for bed (frontslope)
!!    tbsol_solp(:,:)|kg P/ha    |amount of phosohorus stored in solution 
!!                               |for bed (frontslope)
!!    tbsol_st(:,:)  |mm H2O     |amount of water stored in soil profile 
!!                               |on any given day for terrace bed (frontslope)
!!    tbsol_stap(:,:)|kg P/ha    |amount of phosphorus stored in the 
!!                               |stable mineral phosphorus pool for bed (frontslope)
!!    tbsol_sw(:)    |mm H2O     |amount of water stored in soil profile 
!!                               |of terrace bed (frontslope)
!!    trc_stl(:)     |m^3        |accumulated settled sediment volume 
!!                               |in terrace
!!    trc_sumev      |m^3 H2O    |daily evaporation on terrace
!!    trc_sumsep     |m^3 H2O    |daily seepage on terrace
!!    trcclao_dr     |metric tons|amount of terrace clay sediment output 
!!                               |through drainage
!!    trcclao_ov     |metric tons|amount of terrace clay sediment output 
!!                               |through overland flow
!!    trcflwo_dr     |m^3 H2O    |volume of water flowing out of terrace 
!!                               |in drainage flow on day
!!    trcflwo_ov     |m^3 H2O    |volume of water flowing out of terrace 
!!                               |in overland flow on day
!!    trcht_stl(:)   |mm         |hight of the settled sediment in terrace
!!    trclago_dr     |metric tons|amount of large aggregate sediment 
!!                               |out of terrace in drainage flow on day
!!    trclago_ov     |metric tons|amount of large aggregate sediment 
!!                               |out of terrace in overland flow on day
!!    trcno3o_dr     |kg N       |amount of nitrate out of terrace by 
!!                               |drainage
!!    trcno3o_ov     |kg N       |amount of nitrate out of terrace by 
!!                               |overland flow
!!    trcorgno_dr    |kg N       |amount of organic nitrogen out of terrace 
!!                               |by drainage
!!    trcorgno_ov    |kg N       |amount of organic nitrogen out of terrace 
!!                               |by overland flow
!!    trcorgpo_dr    |kg P       |amount of organic phosphorus out of 
!!                               |terrace by drainage
!!    trcorgpo_ov    |kg P       |amount of organic phosphorus out of 
!!                               |terrace by overland flow
!!    trcpsedao_dr   |kg P       |amount of sediment phosphorus out of 
!!                               |terrace by drainage
!!    trcpsedao_ov   |kg P       |amount of sediment phosphorus out of 
!!                               |terrace by overland flow
!!    trcpsedso_dr   |kg P       |amount of sediment phosphorus out of 
!!                               |terrace by drainage
!!    trcpsedso_ov   |kg P       |amount of sediment phosphorus out of 
!!                               |terrace by overland flow
!!    trcsa_stl(:)   |ha         |surface area of the settled sediment in terrace
!!    trcsago_dr     |metric tons|amount of small aggregate sediment 
!!                               |out of terrace by drainage
!!    trcsago_ov     |metric tons|amount of small aggregate sediment 
!!                               |out of terrace by overland flow
!!    trcsano_dr     |metric tons|amount of sand sediment out of terrace 
!!                               |by drainage
!!    trcsano_ov     |metric tons|amount of sand sediment out of terrace 
!!                               |by overland flow
!!    trcsedo_dr     |metric tons|amount of sediment out of terrace by 
!!                               |drainage
!!    trcsedo_ov     |metric tons|amount of sediment out of terrace by 
!!                               |overland flow
!!    trcsilo_dr     |metric tons|amount of silt sediment out of terrace 
!!                               |by drainage
!!    trcsilo_ov     |metric tons|amount of silt sediment out of terrace 
!!                               |by overland flow
!!    trcsolpo_dr    |kg P       |amount of soluble P out of terrace 
!!                               |by drainage
!!    trcsolpo_ov    |kg P       |amount of soluble P out of terrace 
!!                               |by overland flow
!!    tsci           |none       |retention coefficient for cn method based on
!!                               |plant ET in terrace segment
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    cla         |metric tons   |amount of clay sediment in terrace at 
!!                               |the beginning of terrace simulation
!!    cnv         |none          |conversion factor (mm/ha => m^3)
!!    dg          |mm            |depth of soil layer
!!    excess      |mm H2O        |amount of water moving into soil that exceeds
!!                               |storage of layer
!!    i30         |none          |30 day counter
!!    iseas       |none          |nutrient settling rate season
!!    jj          |none          |soil layer number
!!    k           |none          |HRU number
!!    lag         |metric tons   |amount of large aggregate sediment in terrace
!!                               |at the beginning of terrace simulation
!!    nitrok      |none          |fraction of nitrogen in pond removed by
!!                               |settling
!!    phosk       |none          |fraction of phosphorus in pond removed by
!!                               |settling
!!    sag         |metric tons   |amount of small aggregate sediment in terrace
!!                               |at the beginning of terrace simulation
!!    san         |metric tons   |amount of sand sediment in terrace at 
!!                               |the beginning of terrace simulation
!!    sed         |metric tons   |amount of sediment in terrace at the beginning
!!                               |of terrace simulation
!!    sedsetl     |metric tons   |amount of settled sediment in terrace
!!    sil         |metric tons   |amount of silt sediment in terrace at 
!!                               |the beginning of terrace simulation
!!    stmax       |mm H2O        |maximum water storage in soil layer       
!!    tfactor     |none          |sub-daily adjustment factor
!!    trc_dr      |m^3 H2O       |volume of water needs to be drained out of 
!!                               |terrace in one day
!!    trc_drsa    |ha            |surface area when stored water reaches the 
!!                               |drainage trigger depth in terrace
!!    trc_exsep   |m^3 H2O       |seepage due to water storage in terrace
!!    trcev       |mm H2O        |evaperation due to water storage in terrace
!!    trcsa       |ha            |free wate surface area of terrace in day
!!    vol         |m^3 H2O       |volume of water in terrace at the beginning 
!!                               |of terrace simulation
!!    xx          |none          |calculation variable
!!    yy          |none          |calculation variable
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Min, Max
!!    SWAT: canopyint_trc, tankstor_trc, curno_trc, dailycn_trc, trcq_daycn
!!    SWAT: alph_trc, pkq_trc, tran_trc, ysed_trc, percmain_trc, etact_trc
!!    SWAT: wattable_trc, trcplant, orgn_trc, orgncswat_trc, psed_trc
!!    SWAT: nrain_trc, nlch_trc, solp_trc, irrtnk_trc, autoirr_trc
!!    SWAT: vartran_trc, varinit_trc, trc_chsed

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      !implicit none

      integer, intent (in) :: k
      real :: vol, sed, san, sil, cla, sag, lag
      integer :: ly, jj, iseas, kk, ttflag
	  real :: xx, yy, cnv, dg, stmax, excess, trc_dr
      real :: trcsa, trc_exsep, aphu, phuop, trcurb_fr, trc_drsa
      real :: sedsetl, remsetl, phosk, nitrok, sumvol, trcev

       !! store initial values
       vol = 0.
       sed = 0.
       san = 0.
       sil = 0.
       cla = 0.
       sag = 0.
       lag = 0.
       prec_trc = 0.
       ovflwi = 0.
       trc_sumsep = 0.
       trc_sumev = 0.
       vol = trc_vol(k)
       sed = trc_sed(k)
       san = trc_san(k)
       sil = trc_sil(k)
       cla = trc_cla(k)
       sag = trc_sag(k)
       lag = trc_lag(k)
       if (trcflwi > 1.e-5) then
        ovflwi = trcflwi
       end if
       ttflag = 1
       tflag = 0
       trcev = 0.
       tankirr(k) = 0.
       tfactor = 1.
       trc_drsa = 0.
       if (trc_drmm(k) >1.e-6) then
        trc_drsa = 0.001 * trc_sax(k) * trc_drmm(k) / trc_htx(k)
       end if
      !if (iida == 155 .and. iyr == 1959 .and. k == 150) pause   !For testing the code
      !! simulate the slopeland process of each part
      !! on terrace area
      do ttflag = 1, 3
       tflag = ttflag
       trcurb_fr = 0.
       select case(tflag)
       case(1)
        trcurb_fr = trcu_fr(k)
       case(2)
        trcurb_fr = trcr_fr(k)
       case(3)
        trcurb_fr = trcb_fr(k)
       end select

       !! terrace segment routine
       if (trcurb_fr > 1.e-5) then
        
        !! transfer segment variables to compute variables
        call vartran_trc(0,k)
        cnv = 0. 
        cnv = trc_ha * 10.            !!conversion factor m^3 => mm

        !! compute canopy interception            
        if (blai_trc > 0.001 .and. idplt_trc(tnro,ticr) > 0)then
            call canopyint_trc
        end if

        !! compute snow melt
        !! it is assumed that terrace has the same snow
        !! melt as HRU
        prec_trc = prec_trc + snomlt - snofall
        if (prec_trc < 0.)  prec_trc = 0.

        !! calculate rainfall harvest tank storage volume
        if (prec_trc > 0.1 .and. trc_tankfr > 1.e-6 .and.               &
     &                      trc_tvolx(k) > 1.e-6) call tankstor_trc
        
        !! compute crack volume
        if (bsn_cc%crk == 1) call crackvol_trc

        !! calculate CN for runoff simulation
        call dailycn_trc

        !! calculate runoff(SCS method)
        call trcq_daycn
        
        !! adjust runoff for loss into crack volume
        if (trcq > 0. .and. bsn_cc%crk == 1) call crackflow_trc
           
        !! calculate infiltration
        inftrc = Max(0.,prec_trc - trcq)
        
        !! add overland flow from other part of HRU 
        if (trcu_fr(k) > 1.e-6) then
            if (tflag == 1) trcq = trcq + ovflwi / cnv
        else
            if (tflag == 2) trc_vol(k) = trc_vol(k) + ovflwi
        end if

        !! add runoff caused by irrigation
        trcq = trcq + trc_qird
        trc_qird = 0.
            
        !! calculate half-hour rainfall
        if (prec_trc > 0.001) call alph_trc

        !! compute peak rate - tpeakr in m3/s
        if (trcq > 0.001) call pkq_trc

        !! compute sediment yield - trcsed in matric tons
        if (trcq > 1.e-6 .and. tpeakr > 1.e-6) then
            !call tran_trc        !! In-activate the subroutine of calculating transmission losses
                                  !! in terrace based on the talk with C.Baffaut. 8/30/2012
            call ysed_trc
        end if

        !! compute soil water movement
        call percmain_trc

        !! update soil profile water
        tsol_sw = 0.
        do ly = 1, soil(k)%nly
          tsol_sw = tsol_sw + tsol_st(ly)
        end do

        !! compute evapotranspiration
        call etact_trc

        !! compute water table for this part of terrace
        call wattable_trc

        !! new CN method
        if (icn == 1) then 
        tsci = tsci + pet_day * exp(-hru(k)%hyd%cncoef*tsci / tsmx)&
     &                                               - prec_trc + trcq
        else if (icn == 2) then 
        tsci = tsci + pet_day*exp(-hru(k)%hyd%cncoef*tsci/tsmx)    &
     &                - prec_trc + trcq + trclatq + tsol_prk(soil(k)%nly)
        tsci = amin1(tsci,bsn_prm%smxco * tsmx)
        end if 

        !! compute plant routine
        call trcplant          

        !! terrace segment soil nutrient routines
        if (bsn_cc%cswat == 0) then
            call nminrl_trc
	    else
		    call carbon_trc
	    end if

        call nitvol_trc
        call pminrl_trc

        !! calculate organic N in surface runoff
		if (bsn_cc%cswat == 0) then
			call orgn_trc
	    else
		    call orgncswat_trc
		end if

        !! calculate sediment attached P
        call psed_trc

        !! add nitrate in rainfall to soil profile
        call nrain_trc

        !! compute nitrate movement leaching
        call nlch_trc

        !! compute phosphorus movement
        call solp_trc

        if (trc_tvol(k) > 0.1) then
            !! perform irrigation operations from terrace rainfall harvest tank
            if (trc_iir(tnro,tnirr) > 0) then
              if (iida == trc_iir(tnro,tnirr)) then
                call irrtnk_trc
	            !if (imgt == 1) then
                    !write (143,1000) j, iyr, i_mo, iida, "  IRRIGATE"
	            !end if
              end if
            else
              if (igro_trc == 0) then
                aphu = phubase(k)
                phuop = phuirr_nocrop_trc(tnro,tnirr)
              else
                aphu = trc_phuacc 
                phuop = phuirr_trc(tnro,tnirr)
              end if
              if (phuop > 0. .and. aphu > phuop) then
                call irrtnk_trc
	              !if (imgt == 1) then
                !write (143,1000) j, iyr, i_mo, iida, "  IRRIGATE"
	              !end if
              end if
            endif

            !! auto-irrigation operation
            call autoirr_trc
        end if

        !! add runoff to terrace storage
        if (trcq < 0.0001) trcq = 0.
        trc_vol(k) = trc_vol(k) + trcq * cnv
        
        !! add lateral flow to HRU
        if (trclatq > 0.001) then
            latq(k) = latq(k) + trclatq * trc_ha / (hru(k)%km * 100)
        end if

        !! update terrace sediment and nutrient storage
        trc_sed(k) = trc_sed(k) + trcsed 
        trc_san(k) = trc_san(k) + trcsan 
        trc_sil(k) = trc_sil(k) + trcsil 
        trc_cla(k) = trc_cla(k) + trccla 
        trc_sag(k) = trc_sag(k) + trcsag 
        trc_lag(k) = trc_lag(k) + trclag

        trc_solp(k) = trc_solp(k) + trcqsolp * trc_ha 
        trc_pseds(k) = trc_pseds(k) + tsedminps * trc_ha 
        trc_pseda(k) = trc_pseda(k) + tsedminpa * trc_ha 
        trc_orgp(k) = trc_orgp(k) + tsedorgp * trc_ha 
        trc_orgn(k) = trc_orgn(k) + tsedorgn * trc_ha 
        trc_no3(k) = trc_no3(k) + trcqno3 * trc_ha 
        trc_no3s(k) = trc_no3s(k) + tlatno3 * trc_ha

        trc_sumsep = trc_sumsep + inftrc * cnv
        trc_sumev = trc_sumev + (tep_day + tes_day + tcanev) * cnv

        !! transfer compute variables to segment variables
        call vartran_trc(1,k)

        !! set compute variables to daily initial value
        call varinit_trc
       end if
      end do

       !! calculate materials flow out of the terrace
       !! for day

       !! initial output
            trcflwo_ov = 0. 
            trcno3o_ov = 0. 
            trcorgno_ov = 0. 
            trcsolpo_ov = 0. 
            trcpsedso_ov = 0. 
            trcpsedao_ov = 0. 
            trcorgpo_ov = 0.

            trcsedo_ov = 0. 
            trcsano_ov = 0. 
            trcsilo_ov = 0. 
            trcclao_ov = 0. 
            trcsago_ov = 0. 
            trclago_ov = 0.

            trcflwo_dr = 0. 
            trcno3o_dr = 0. 
            trcorgno_dr = 0. 
            trcsolpo_dr = 0. 
            trcpsedso_dr = 0. 
            trcpsedao_dr = 0.
            trcorgpo_dr = 0.

            trcsedo_dr = 0. 
            trcsano_dr = 0. 
            trcsilo_dr = 0. 
            trcclao_dr = 0. 
            trcsago_dr = 0. 
            trclago_dr = 0.
       if (trc_vol(k) < 0.001) then
          !! no output of terrace
            trc_vol(k) = 0.

            trc_solp(k) = 0. 
            trc_pseds(k) = 0. 
            trc_pseda(k) = 0.
            trc_orgp(k) = 0. 
            trc_orgn(k) = 0. 
            trc_no3(k) = 0

            trc_sed(k) = 0. 
            trc_san(k) = 0. 
            trc_sil(k) = 0. 
            trc_cla(k) = 0. 
            trc_sag(k) = 0. 
            trc_lag(k) = 0.
            return
       end if
        
        if (trc_vmax < 0.001) then
            !! no terrace storage effect
            !! all of the storage volume is filled by sediment
            !! overland output of terrace
            trcflwo_ov = trc_vol(k)
            trc_vol(k) = 0.

            trcno3o_ov = trc_no3(k)
            trcorgno_ov = trc_orgn(k)
            trcsolpo_ov = trc_solp(k)
            trcpsedso_ov = trc_pseds(k)
            trcpsedao_ov = trc_pseda(k)
            trcorgpo_ov = trc_orgp(k)

            trc_solp(k) = 0. 
            trc_pseds(k) = 0.
            trc_pseda(k) = 0. 
            trc_orgp(k) = 0. 
            trc_orgn(k) = 0. 
            trc_no3(k) = 0.

            trcsedo_ov = trc_sed(k) 
            trcsano_ov = trc_san(k) 
            trcsilo_ov = trc_sil(k) 
            trcclao_ov = trc_cla(k) 
            trcsago_ov = trc_sag(k) 
            trclago_ov = trc_lag(k)

            trc_sed(k) = 0. 
            trc_san(k) = 0. 
            trc_sil(k) = 0. 
            trc_cla(k) = 0. 
            trc_sag(k) = 0. 
            trc_lag(k) = 0.
            return
        end if
        !! simulate terrace storage effects
            trc_ha = 0.
            trc_ha = (hru(k)%km * 100) * trc_fr(k) * trcb_fr(k)
            cnv = 0. 
            cnv = trc_ha * 10.

            !! calculate terrace free water surface
            trcsa = 0. 
            if (trc_type(k) == 1) then   ! normal terrace
                trcsa = trc_sax(k) * ((trc_vol(k) + trc_stl(k)) /       &
     &                                              trc_volx(k)) ** .5
                trcsa = min(trc_sax(k),trcsa)
            else                         ! bench terrace
                trcsa = trc_sax(k)
            end if

            !! calculate the fraction of day (tfactor) that   
            !! sediment, N/P settlement, extra infiltration and 
            !! evaporation happen. It is assumed that 99% of the 
            !! drainage capacity can be drained out from terrace 
            !! in 24 hours
            if (trc_drx(k) > 1.e-6) then
                trc_dr = 0.
                xx = 0.
                xx = trc_drx(k)*(hru(k)%km * 100)*trc_fr(k)
                yy = 0.
                yy = max(0.,trc_drmm(k) - trcht_stl(k))
                if(trc_type(k) == 1) then
                    trc_dr = trc_vol(k) - 5. * yy * (trcsa_stl(k)       &
     &                                                      + trc_drsa) ! normal terrace 
                else
                    trc_dr = trc_vol(k) - yy * trcsa * 10.              ! bench terrace
                end if
                trc_dr = max(trc_dr,0.)
                if (xx > 1.e-6 .and. trc_dr > 1.e-6) then
                    tfactor = 0.01 * xx / trc_dr
                    if (tfactor > 1.) then
                        tfactor = 1.
                    else
                        if(trc_type(k) == 1) then                       ! normal terrace 
                            trc_dr = trc_dr ** (-1/3)
                            xx = xx ** (-1/3)
                            tfactor = 4.64 * xx - trc_dr
                            tfactor = tfactor / xx / 3.64
                        else                                            ! bench terrace 
                            trc_dr = trc_dr ** (-2/3)
                            xx = xx ** (-2/3)
                            tfactor = 21.54 * xx - trc_dr
                            tfactor = tfactor / xx / 20.54
                        end if
                        tfactor = min(1.,tfactor)
                    end if
                end if
      !          if (xx > 1.e-6 .and. trc_dr > 1.e-6) then
      !            tfactor = 0.01 * xx / trc_dr
      !            if (tfactor > 1.) then
      !              tfactor = 1.
      !            else
      !              tfactor = -0.217 * log(tfactor) !! -0.217=1/ln(0.01)
      !              tfactor = min(1.,tfactor)
      !            end if
      !          end if
            end if

            !! calculate sediment settlement
            sedsetl = 0. 
            remsetl = 0. 
            xx = 0.
            xx = 1 - tsed_stl(k) ** tfactor
            sedsetl = trc_sed(k) - trc_nsed(k) * trc_vol(k) / 1.e6
            sedsetl = sedsetl * xx
            sedsetl = Max(sedsetl,0.)
            sedsetl = Min(sedsetl,trc_sed(k)) 
            trc_stl(k) = trc_stl(k) + sedsetl / soil(k)%phys(1)%bd   !accumulate settlement m^3
            if (trc_stl(k) > 1.e-6) then
                if (trc_type(k) == 1) then   ! normal terrace
                    trcsa_stl(k) = trc_sax(k) * (trc_stl(k)             &
     &                                              / trc_volx(k)) ** .5
                    trcsa_stl(k) = min(trc_sax(k),trcsa_stl(k))
                    trcht_stl(k) = 0.2 * trc_stl(k) / trcsa_stl(k)
                else                         ! bench terrace
                    trcsa_stl(k) = trc_sax(k)
                    trcht_stl(k) = 0.1 * trc_stl(k) / trcsa_stl(k)
                end if
            end if
                
            trc_sed(k) = trc_sed(k) - sedsetl
            trc_sed(k) = Max(0.,trc_sed(k))

            if (sedsetl <= trc_lag(k)) then
                trc_lag(k) = trc_lag(k) - sedsetl
            else
                remsetl = sedsetl-trc_lag(k) 
                trc_lag(k) = 0.
                if (remsetl <= trc_san(k)) then
                    trc_san(k) = trc_san(k) - remsetl
                else
                    remsetl = remsetl-trc_san(k) 
                    trc_san(k) = 0.
                    if (remsetl <= trc_sag(k)) then
                        trc_sag(k) = trc_sag(k) - remsetl
                    else
                        remsetl = remsetl-trc_sag(k) 
                        trc_san(k) = 0.
                        if (remsetl <= trc_sil(k)) then
                            trc_sil(k) = trc_sil(k) - remsetl
                        else
                            remsetl = remsetl-trc_sil(k) 
                            trc_sil(k) = 0.
                            if (remsetl <= trc_cla(k)) then
                                trc_cla(k) = trc_cla(k) - remsetl
                            else
                                remsetl = remsetl-trc_cla(k) 
                                trc_cla(k) = 0.
                            end if
                        end if
                    end if
                end if
            end if

            trc_san(k) = Max(0.,trc_san(k)) 
            trc_sil(k) = Max(0.,trc_sil(k)) 
            trc_cla(k) = Max(0.,trc_cla(k)) 
            trc_sag(k) = Max(0.,trc_sag(k)) 
            trc_lag(k) = Max(0.,trc_lag(k))
                
            !! calculate N/P settlement
            
            phosk = 0.
            nitrok = 0.
            if (i_mo >= itrc1(k) .and. i_mo <= itrc2(k)) then
                phosk = trc_psetlp1(k) * trcsa * 10000. / trc_vol(k)
                nitrok = trc_nsetlp1(k) * trcsa * 10000. / trc_vol(k)
            else
                phosk = trc_psetlp2(k) * trcsa * 10000. / trc_vol(k)
                nitrok = trc_nsetlp2(k) * trcsa * 10000. / trc_vol(k)
            end if            
            phosk = phosk * tfactor
            phosk = Min(phosk,1.)            
            nitrok = nitrok * tfactor
            nitrok = Min(nitrok,1.)

            tbsol_no3(1,k) = tbsol_no3(1,k) + trc_no3(k) * nitrok       &
     &                                                          / trc_ha
            tbsol_solp(1,k) = tbsol_solp(1,k) + trc_solp(k) * phosk     &
     &                                                          / trc_ha

            if (bsn_cc%cswat == 0) then
            xx = 0.
            xx = trc_orgn(k) * nitrok / ((tbsol_aorgn(1,k)              &
     &                   + tbsol_fon(1,k) + tbsol_orgn(1,k)) * trc_ha) 
            tbsol_orgn(1,k) = tbsol_orgn(1,k) * (1. + xx)
            tbsol_fon(1,k) = tbsol_fon(1,k) * (1. + xx)
            tbsol_aorgn(1,k) = tbsol_aorgn(1,k) * (1. + xx)

            tbsol_stap(1,k) = tbsol_stap(1,k) + trc_pseds(k) * phosk     &
     &                                                          / trc_ha
            tbsol_actp(1,k) = tbsol_actp(1,k) + trc_pseda(k) * phosk     &
     &                                                          / trc_ha

            xx = 0.
            xx = trc_orgp(k) * phosk / ((tbsol_orgp(1,k)                &
     &                                      + tbsol_fop(1,k)) * trc_ha) 
            tbsol_orgp(1,k) = tbsol_orgp(1,k) * (1. + xx)
            tbsol_fop(1,k) = tbsol_fop(1,k) * (1. + xx)
            else
            xx = 0.
            xx = trc_orgn(k) * nitrok / ((tbsol_mn(1,k) + tbsol_fon(1,k)&
     &                                      + tbsol_orgn(1,k)) * trc_ha)
            tbsol_orgn(1,k) = tbsol_orgn(1,k) * (1. + xx)
            tbsol_fon(1,k) = tbsol_fon(1,k) * (1. + xx)
            tbsol_mn(1,k) = tbsol_mn(1,k) * (1. + xx)

            tbsol_stap(1,k) = tbsol_stap(1,k) + trc_pseds(k) * phosk     &
     &                                                          / trc_ha
            tbsol_actp(1,k) = tbsol_actp(1,k) + trc_pseda(k) * phosk     &
     &                                                          / trc_ha

            xx = 0.
            xx = trc_orgp(k) * phosk / ((tbsol_orgp(1,k)                &
     &                      + tbsol_fop(1,k) + tbsol_mp(1,k)) * trc_ha)
            tbsol_orgp(1,k) = tbsol_orgp(1,k) * (1. + xx)
            tbsol_fop(1,k) = tbsol_fop(1,k) * (1. + xx)
            tbsol_mp(1,k) = tbsol_mp(1,k) * (1. + xx)
            end if

            trc_solp(k) = trc_solp(k) * (1. - phosk)
            trc_pseds(k) = trc_pseds(k) * (1. - phosk)
            trc_pseda(k) = trc_pseda(k) * (1. - phosk)
            trc_orgp(k) = trc_orgp(k) * (1. - phosk)
            trc_orgn(k) = trc_orgn(k) * (1. - nitrok)
            trc_no3(k) = trc_no3(k) * (1. - nitrok)

            !! limit seepage into soil if profile if near field capacity
            yy = 0.

            if (tbsol_sw(k) / soil(k)%sumfc < .5) then
                yy = 1.
            else
                if (tbsol_sw(k) / soil(k)%sumfc < 1.) then
                    yy = 1. - tbsol_sw(k) / soil(k)%sumfc
                end if
            end if

                !! calculate the extra infiltration 
                !! storedof terrace water
                trc_exsep = 0.
                trc_exsep = yy * soil(k)%phys(1)%k * trcsa * 240. * tfactor
                trc_exsep = Min(trc_exsep,trc_vol(k))

                tbsol_st(1,k) = tbsol_st(1,k) + trc_exsep/cnv
                trc_vol(k) = trc_vol(k) - trc_exsep
                
            !! redistribute terrace soil water so that no layer 
            !! exceeds maximum storage
            do ly = 1, soil(k)%nly
                
                dg = 0. 
                stmax = 0. 
                excess = 0.
                
                if (ly == 1) then
                    dg = soil(k)%phys(ly)%d
                else
                    dg = soil(k)%phys(ly)%d - soil(k)%phys(ly-1)%d
                end if

                stmax = soil(k)%phys(ly)%por * dg

                if (tbsol_st(ly,k) <= stmax) exit

                excess = tbsol_st(ly,k) - stmax 
                tbsol_st(ly,k) = stmax
                
                if (ly + 1 <= soil(k)%nly) then
                    tbsol_st(ly+1,k) = tbsol_st(ly+1,k) + excess
                end if
                
                if (ly == soil(k)%nly .and. excess > 0.001) then
                    sepbtm(k) = sepbtm(k) + excess * trc_ha / (hru(k)%km * 100)
                end if
            end do

            !! re-calculate soil water
            tbsol_sw(k) = 0.
            do ly = 1, soil(k)%nly
                tbsol_sw(k) = tbsol_sw(k) + tbsol_st(ly,k)
            end do

            !! recalculate terrace free water surface due to extra-infiltration
            trcsa = 0. 
            if (trc_type(k) == 1) then   ! normal terrace
                trcsa = trc_sax(k) * ((trc_vol(k) + trc_stl(k))         &
     &                                              / trc_volx(k)) ** .5
                trcsa = min(trc_sax(k),trcsa)
            else                         ! bench terrace
                trcsa = trc_sax(k)
            end if

            !! calculate free water surface evaporation
            if (laiday_trcb(k) < bsn_prm%evlai) then
                trcev = (1. - laiday_trcb(k) / bsn_prm%evlai) * pet_day         &
     &                                                      * tfactor 
                trcev = 10. * trcev * trcsa            !! units mm => m^3 
                trcev = max(trcev,0.)
                trcev = Min(trcev,trc_vol(k))
                trc_vol(k) = trc_vol(k) - trcev
            end if

            if (trc_vol(k) > 1.e-5) then
            !! check the volume of stored water
                trc_sumsep = trc_sumsep + trc_exsep
                trc_sumev = trc_sumev + trcev
            else
                trc_exsep = trc_exsep + trc_vol(k) 
                trc_vol(k) = 0.
                if (trc_exsep < 0.) then
                    trcev = trcev + trc_exsep
                    trc_exsep = 0.
                end if

                trc_sumsep = trc_sumsep + trc_exsep
                trc_sumev = trc_sumev + trcev

               !! calculate sediment settlement
                trc_stl(k) = trc_stl(k) + trc_sed(k)
                
                !! calculate N/P settlement
                tbsol_no3(1,k) = tbsol_no3(1,k) + trc_no3(k) / trc_ha
                tbsol_solp(1,k) = tbsol_solp(1,k) + trc_solp(k) / trc_ha

                if (bsn_cc%cswat == 0) then
                xx = 0.
                xx = trc_orgn(k) / ((tbsol_aorgn(1,k) + tbsol_fon(1,k)  &
     &                                      + tbsol_orgn(1,k)) * trc_ha)
                tbsol_orgn(1,k) = tbsol_orgn(1,k) * (1. + xx)
                tbsol_fon(1,k) = tbsol_fon(1,k) * (1. + xx)
                tbsol_aorgn(1,k) = tbsol_aorgn(1,k) * (1. + xx)

                tbsol_stap(1,k) = tbsol_stap(1,k) + trc_pseds(k)/trc_ha
                tbsol_actp(1,k) = tbsol_actp(1,k) + trc_pseda(k)/trc_ha

                xx = 0.
                xx = trc_orgp(k) / ((tbsol_orgp(1,k) + tbsol_fop(1,k))  &
     &                                                         * trc_ha)
                tbsol_orgp(1,k) = tbsol_orgp(1,k) * (1. + xx)
                tbsol_fop(1,k) = tbsol_fop(1,k) * (1. + xx)
                else
                xx = 0.
                xx = trc_orgn(k) / ((tbsol_mn(1,k) + tbsol_fon(1,k)     &
     &                                      + tbsol_orgn(1,k)) * trc_ha)
                tbsol_orgn(1,k) = tbsol_orgn(1,k) * (1. + xx)
                tbsol_fon(1,k) = tbsol_fon(1,k) * (1. + xx)
                tbsol_mn(1,k) = tbsol_mn(1,k) * (1. + xx)

                tbsol_stap(1,k) = tbsol_stap(1,k) + trc_pseds(k)/trc_ha
                tbsol_actp(1,k) = tbsol_actp(1,k) + trc_pseda(k)/trc_ha

                xx = 0.
                xx = trc_orgp(k) / ((tbsol_orgp(1,k) + tbsol_fop(1,k)   &
     &                                        + tbsol_mp(1,k)) * trc_ha)
                tbsol_orgp(1,k) = tbsol_orgp(1,k) * (1. + xx)
                tbsol_fop(1,k) = tbsol_fop(1,k) * (1. + xx)
                tbsol_mp(1,k) = tbsol_mp(1,k) * (1. + xx)
                end if

                trc_sed(k) = 0. 
                trc_san(k) = 0. 
                trc_sil(k) = 0. 
                trc_cla(k) = 0. 
                trc_sag(k) = 0. 
                trc_lag(k) = 0. 
                trc_solp(k) = 0. 
                trc_pseds(k) = 0. 
                trc_pseda(k) = 0. 
                trc_orgp(k) = 0. 
                trc_orgn(k) = 0. 
                trc_no3(k) = 0.
            return
            end if
            
            !! calculate terrace output through drainage
            if (trc_release(k) > 0) then
                trc_dr = 0.
                yy = 0.
                yy = max(0.,trc_drmm(k) - trcht_stl(k))
                if(trc_type(k) == 1) then
                    trc_dr = trc_vol(k) - 5. * yy * (trcsa_stl(k)       &
     &                                                      + trc_drsa)      ! normal terrace 
                else
                    trc_dr = trc_vol(k) - yy * trcsa * 10.     ! bench terrace
                end if
                trc_dr = max(trc_dr,0.)
                if (trc_dr > 1.e-6) then
                    !!water ouput
                    sumvol = 0.
                    sumvol = trc_vol(k)
                    xx = 0.
                    xx = trc_drx(k)*(hru(k)%km * 100)*trc_fr(k)
                    trcflwo_dr = min(trc_dr,xx)
                    !! compute channel erosion
                    if (trc_type(k) == 1 .and. trc_chtype(k) == 0       &
     &                         .and. trc_chs(k) > 1.e-6) call trc_chsed
                    trc_vol(k) = trc_vol(k) - trcflwo_dr
                    xx = 0.
                    xx = trcflwo_dr / sumvol
                    xx = min(1.,xx)

                    !!sediment ouput
                    trcsedo_dr = trc_sed(k) * xx + trcsedo_dr
                    trcsano_dr = trc_san(k) * xx + trcsano_dr
                    trcsilo_dr = trc_sil(k) * xx + trcsilo_dr
                    trcclao_dr = trc_cla(k) * xx + trcclao_dr
                    trcsago_dr = trc_sag(k) * xx + trcsago_dr
                    trclago_dr = trc_lag(k) * xx + trclago_dr

                    trc_sed(k) = trc_sed(k) * (1. - xx)
                    trc_san(k) = trc_san(k) * (1. - xx)
                    trc_sil(k) = trc_sil(k) * (1. - xx)
                    trc_cla(k) = trc_cla(k) * (1. - xx)
                    trc_sag(k) = trc_sag(k) * (1. - xx)
                    trc_lag(k) = trc_lag(k) * (1. - xx)

                    !!nutrient ouput
                    trcno3o_dr = trc_no3(k) * xx + trcno3o_dr
                    trcorgno_dr = trc_orgn(k) * xx + trcorgno_dr
                    trcsolpo_dr = trc_solp(k) * xx + trcsolpo_dr
                    trcpsedso_dr = trc_pseds(k) * xx + trcpsedso_dr
                    trcpsedao_dr = trc_pseda(k) * xx + trcpsedao_dr
                    trcorgpo_dr = trc_orgp(k) * xx + trcorgpo_dr

                    trc_solp(k) = trc_solp(k) * (1. - xx)
                    trc_pseds(k) = trc_pseds(k) * (1. - xx)
                    trc_pseda(k) = trc_pseda(k) * (1. - xx)
                    trc_orgp(k) = trc_orgp(k) * (1. - xx)
                    trc_orgn(k) = trc_orgn(k) * (1. - xx)
                    trc_no3(k) = trc_no3(k) * (1. - xx)
                end if
            end if
            
        !! calculate terrace output through overland flow
        if (trc_vol(k) > trc_vmax) then
            trcflwo_ov = 0. 
            trcno3o_ov = 0. 
            trcorgno_ov = 0. 
            trcsolpo_ov = 0. 
            trcpsedao_ov = 0. 
            trcpsedso_ov = 0.
            trcorgpo_ov = 0.

            trcsedo_ov = 0. 
            trcsano_ov = 0. 
            trcsilo_ov = 0. 
            trcclao_ov = 0. 
            trcsago_ov = 0. 
            trclago_ov = 0.

            !!water ouput
            sumvol = 0.
            sumvol = trc_vol(k) 
            trcflwo_ov = trc_vol(k) - trc_vmax 
            trc_vol(k) = trc_vmax 
            xx = 0. 
            xx = trcflwo_ov / sumvol
            xx = min(1.,xx)

            !!sediment ouput
            trcsedo_ov = trc_sed(k) * xx
            trcsano_ov = trc_san(k) * xx
            trcsilo_ov = trc_sil(k) * xx
            trcclao_ov = trc_cla(k) * xx
            trcsago_ov = trc_sag(k) * xx
            trclago_ov = trc_lag(k) * xx

            trc_sed(k) = trc_sed(k) * (1. - xx)  
            trc_san(k) = trc_san(k) * (1. - xx) 
            trc_sil(k) = trc_sil(k) * (1. - xx) 
            trc_cla(k) = trc_cla(k) * (1. - xx) 
            trc_sag(k) = trc_sag(k) * (1. - xx) 
            trc_lag(k) = trc_lag(k) * (1. - xx)

            !!nutrient ouput
            trcno3o_ov = trc_no3(k) * xx 
            trcorgno_ov = trc_orgn(k) * xx 
            trcsolpo_ov = trc_solp(k) * xx 
            trcpsedso_ov = trc_pseds(k) * xx 
            trcpsedao_ov = trc_pseda(k) * xx 
            trcorgpo_ov = trc_orgp(k) * xx

            trc_solp(k) = trc_solp(k) * (1. - xx) 
            trc_pseds(k) = trc_pseds(k) * (1. - xx) 
            trc_pseda(k) = trc_pseda(k) * (1. - xx)
            trc_orgp(k) = trc_orgp(k) * (1. - xx) 
            trc_orgn(k) = trc_orgn(k) * (1. - xx) 
            trc_no3(k) = trc_no3(k) * (1. - xx)
        end if    
      return
      end

       subroutine tillfactor_trc(jj,bmix,emix,dtil,sol_thick)
	!!!!!!!!!!!!!!!!!!!!!!!
	! Armen 16 January 2008
	! This procedure increases tillage factor (tillagef(l,jj) per layer for each operation
	! The tillage factor settling will depend of soil moisture (tentatively) and must be called every day
	! For simplicity the settling is calculated now at the soil carbon sub because soil water content is available

	! The tillage factor depends on the cumulative soil disturbance rating = csdr
	! For simplicity, csdr is a function of emix
	! First step is to calculate "current" csdr by inverting tillage factor function
	! The effect of texture on tillage factor (ZZ) is removed first (and recovered at the end of the procedure)
	! YY = tillagef(l,jj) / ZZ
	! Since the tillage factor function is non linear, iterations are needed 
	! XX = 0.5 is the initial value that works OK for the range of values observed
	! If a layer is only partially tilled then emix is corrected accordingly
      use parm; use terrace_parm; use basin_module
	implicit none
	integer, intent (in) :: jj
      real, intent (in) :: bmix
      integer :: l, m1, m2
      real :: emix, dtil, csdr, xx, yy, zz, xx1, xx2
	real :: sol_thick(soil(jj)%nly)
	
	emix = emix - bmix ! this is to avoid affecting tillage factor with biological mixing
	
	if (emix > 0.) then

	  do l=1, soil(jj)%nly
			
	    if (soil(jj)%phys(l)%d <= dtil) then
		emix = emix
	    else if (soil(jj)%phys(l)%d > dtil .AND. soil(jj)%phys(l-1)%d < dtil) then 
		emix = emix * (dtil - soil(jj)%phys(l-1)%d) / sol_thick(l)
	    else
		emix = 0.
	    end if
			
	    ! to save computation time if emix = 0 here then the other layers can be avoided
	    ! tillage always proceeds from top to bottom
	    if (emix == 0.) exit

	    xx = 0.
	    zz = 3. + (8. - 3.) * exp(-5.5 * soil(jj)%phys(l)%clay/100.)
	    yy = trc_tillagef(l) / zz
	    m1 = 1
	    m2 = 2

	    ! empirical solution for x when y is known and y=x/(x+exp(m1-m2*x)) 
	    if (yy > 0.01) then
		 xx1 = yy ** exp(-0.13 + 1.06 * yy)
		 xx2 = exp(0.64 + 0.64 * yy ** 100.)
		 xx = xx1 * xx2
	    end if

	    csdr = xx + emix
	    trc_tillagef(l) = zz * (csdr / (csdr + exp(m1 - m2 * csdr)))

	  end do		
		
	end if
		
	return
	end subroutine

      subroutine tran_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine computes tributary channel transmission losses on terrace
!!    segment.

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    hru_km(:)   |km**2         |area of HRU in square kilometers
!!    ihru        |none          |HRU number
!!    sol_k(:,:)  |mm/hr         |saturated hydraulic conductivity of soil 
!!                               |layer. 
!!    sol_sumfc(:)|mm H2O        |amount of water held in soil profile at
!!                               |field capacity
!!    tpeakr      |m^3/s         |peak runoff rate
!!    trc_ha      |ha            |terrace segment area in hectare
!!    trc_no(:)   |none          |numbers of terrace unit in HRU
!!    trcq        |mm H2O        |surface runoff loading to main channel
!!                               |from HRU on current day
!!    tsol_sw     |mm H2O        |amount of water stored in soil profile on
!!                               |any given day
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tpeakr      |m^3/s         |peak runoff rate
!!    tloss       |mm H2O        |amount of water removed from surface runoff
!!                               |via transmission losses on day
!!    trcq        |mm H2O        |amount of surface runoff loading to main 
!!                               |channel from HRU on current day
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    a           |m^3           |regression intercept for unit channel
!!    axw         |m^3           |regression intercept for unit channel with
!!                               |a flow of duration DUR
!!    b           |none          |regression slope for unit channel
!!    bxw         |none          |regression slope for a channel of length L
!!                               |and width W
!!    dur         |hr            |length of time runoff occurs if traveling
!!                               |at peak runoff rate
!!    j           |none          |HRU number
!!    k           |1/(m-km)      |decay factor for unit channel with a flow
!!                               |duration DUR and volume VOL
!!    pr1         |m^3/s         |peak runoff rate prior to accounting for
!!                               |transmission losses
!!    pxw         |m^3           |threshold volume for a channel reach of 
!!                               |length L and width W
!!    qinit       |mm H2O        |amount of water in surface runoff loading
!!                               |prior to accounting for transmission losses
!!    vo          |m**3 H2O      |volume of water in surface runoff loading
!!                               |to main channel on day
!!    xx          |none          |variable to hold intermediate calculation
!!    zz          |none          |variable to hold intermediate calculation

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Log, Exp

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j
      real :: qinit, vo, dur, k, b, zz, bxw, pr1, a, xx, axw, pxw

      !! initialize variables
      j = 0
      j = ihru

!      if (trc_hafrtl(j) < 1.e-6) return

!! save runoff amount prior to transmission losses
      qinit = 0.
      pr1 = 0.
      qinit = trcq
      pr1 = tpeakr

!! calculate incoming volume of water
      vo = 0.
      vo = trcq * trc_ha * 10. / trc_no(j)   !!volume incoming: m^3

!! calculate flow duration
      dur = 0.
      dur = vo / (tpeakr * 3600.)      !!duration: hr
      if (dur > 24.) dur = 24.

      xx = 0.
!      xx = 2.6466 * soil(j)%phys(1)%k * dur * trc_hafrtl(j) / trcq

!! zero surface runoff/peak rate
      trcq = 0.
      tpeakr = 0.

      if (xx < 1.) then
        k = 0.
        k = -2.22 * Log(1. - xx)
        b = 0.
        b = Exp(-0.4905 * k)
        if ((1. - b) > 1.e-20) then
          zz = 0.
!          zz = - 10. * k * trc_ha * trc_hafrtl(j) / trc_no(j) 
                                              !10 is the conversion factor to 
                                              !convert ha to km*m
          if (zz >= -30.) then
            bxw = 0.
            bxw = Exp(zz)
            a = 0.
            a = -.2258 * soil(j)%phys(1)%k * dur
            if (1. - b > 0.01) then
              axw = (a / (1. - b)) * (1. - bxw)
            else
              axw = 0.
            end if
            pxw = -axw / bxw
            if (vo > pxw) then
              trcq = axw + bxw * vo              !!surface runoff: m^3
              trcq = trcq / (10. * trc_ha)       !!surface runoff: mm
              if (trcq < 0.) trcq = 0.
              if (trcq > 0.) then
                tpeakr = (1. / (dur * 3600.)) * (axw - (1. - bxw) * vo) &
     &                  + bxw * pr1              !!peak rate: m^3/s
                if (tpeakr < 0.) tpeakr = 0.
              end if
            end if
          end if
        end if
      end if

      if(qinit > trcq) then
        tloss = tloss + (qinit - trcq) * trc_ha / (hru(j)%km*100)
      else
        trcq = qinit        
      end if

      return
      end

      subroutine trcaa(years)

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine writes simulation period terrace output to the 
!!    output.trc file

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name          |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    hru_sub(:)   |none        |subbasin in which HRU is located
!!    nhru         |none        |number of HRUs in watershed
!!    trcaao(1,:)  |mm H2O      |evaporation on terrace for simulation
!!    trcaao(2,:)  |mm H2O      |seepage on terrace for simulation
!!    trcaao(3,:)  |mm H2O      |amount of water flow into terrace for 
!!                              |simulation
!!    trcaao(4,:)  |mm H2O      |amount of water flow out of terrace by 
!!                              |overland flow for simulation
!!    trcaao(5,:)  |t/ha        |amount of sediment flow out of terrace 
!!                              |by overland flow for simulation
!!    trcaao(6,:)  |mm H2O      |amount of water flow out of terrace by 
!!                              |drainage flow for simulation
!!    trcaao(7,:)  |t/ha        |amount of sediment flow out of terrace 
!!                              |by drainage flow for simulation
!!    trcaao(8,:)  |mm H2O      |amount of precipitation on terrace for 
!!                              |simulation
!!    trcaao(9,:)  |ppm N       |concentration of organic N stored in 
!!                              |terrace for simulation
!!    trcaao(10,:) |ppm P       |concentration of organic P stored in 
!!                              |terrace for simulation
!!    trcaao(11,:) |ppm N       |concentration of nitrate stored in 
!!                              |terrace for simulation
!!    trcaao(12,:) |ppm P       |concentration of mineral P stored in 
!!                              |terrace for simulation
!!    trcaao(13,:) |m^3 H2O     |amount of water stored in terrace for 
!!                              |simulation
!!    trcaao(14,:) |metric tons |amount of sediment stored in terrace for 
!!                              |simulation
!!    trcaao(15,:) |mm H2O      |water in terrace soil profile for simulation
!!    trcaao(16,:) |mm H2O      |water in undisturbed area soil profile 
!!                              |for simulation
!!    trcaao(17,:) |kg N/ha     |mineral N in undisturbed area soil profile 
!!                              |for simulation
!!    trcaao(18,:) |kg N/ha     |organic N in undisturbed area soil profile 
!!                              |for simulation
!!    trcaao(19,:) |kg P/ha     |mineral P in undisturbed area soil profile 
!!                              |for simulation
!!    trcaao(20,:) |kg P/ha     |organic P in undisturbed area soil profile 
!!                              |for simulation
!!    trcaao(21,:) |m           |water table depth from soil surface in
!!                              |undisturbed area for simulation
!!    trcaao(22,:) |mm H2O      |water in riser (cutslope) soil profile 
!!                              |for simulation
!!    trcaao(23,:) |kg N/ha     |mineral N in riser (cutslope) soil profile 
!!                              |for simulation
!!    trcaao(24,:) |kg N/ha     |organic N in riser (cutslope) soil profile 
!!                              |for simulation
!!    trcaao(25,:) |kg P/ha     |mineral P in riser (cutslope) soil profile 
!!                              |for simulation
!!    trcaao(26,:) |kg P/ha     |organic P in riser (cutslope) soil profile 
!!                              |for simulation
!!    trcaao(27,:) |m           |water table depth from soil surface in
!!                              |riser (cutslope) for simulation
!!    trcaao(28,:) |mm H2O      |water in bed (frontslope) soil profile for 
!!                              |simulation
!!    trcaao(29,:) |kg N/ha     |mineral N in bed (frontslope) soil profile 
!!                              |for simulation
!!    trcaao(30,:) |kg N/ha     |organic N in bed (frontslope) soil profile 
!!                              |for simulation
!!    trcaao(31,:) |kg P/ha     |mineral P in bed (frontslope) soil profile 
!!                              |for simulation
!!    trcaao(32,:) |kg P/ha     |organic P in bed (frontslope) soil profile 
!!                              |for simulation
!!    trcaao(33,:) |m           |water table depth from soil surface in
!!                              |bed (frontslope) for simulation    
!!    trcaao(34,:) |stress days |water stress days in undisturbed area 
!!                              |during simulation
!!    trcaao(35,:) |stress days |temperature stress days in undisturbed area 
!!                              |during simulation
!!    trcaao(36,:) |stress days |nitrogen stress days in undisturbed area 
!!                              |during simulation
!!    trcaao(37,:) |stress days |phosphorus stress days in undisturbed area 
!!                              |during simulation
!!    trcaao(38,:) |stress days |air stress days in undisturbed area during 
!!                              |simulation
!!    trcaao(39,:) |stress days |water stress days in raised area during 
!!                              |simulation
!!    trcaao(40,:) |stress days |temperature stress days in raised area during 
!!                              |simulation
!!    trcaao(41,:) |stress days |nitrogen stress days in raised area during 
!!                              |simulation
!!    trcaao(42,:) |stress days |phosphorus stress days in raised area during 
!!                              |simulation
!!    trcaao(43,:) |stress days |air stress days in raised area during 
!!                              |simulation
!!    trcaao(44,:) |stress days |water stress days in bed area during 
!!                              |simulation
!!    trcaao(45,:) |stress days |temperature stress days in bed area during 
!!                              |simulation
!!    trcaao(46,:) |stress days |nitrogen stress days in bed area during 
!!                              |simulation
!!    trcaao(47,:) |stress days |phosphorus stress days in bed area during 
!!                              |simulation
!!    trcaao(48,:) |stress days |air stress days in bed area during simulation
!!    trcaao(49,:) |mm H2O      |amount of water removed from rainfall harvest tank
!!    trcaao(50,:) |mm H2O      |amount of rainfall harvest tank water storage
!!    trcb_bioaams(:)|metric tons/ha|simulation biomass (dry weight) in the 
!!                              |bed segment
!!    trcb_laiaamx(:)|none      |maximum leaf area index for the simulation in the
!!                              |bed segment
!!    trcb_yldaa(:)|metric tons/ha|simulation yield (dry weight) in the 
!!                              |bed segment
!!    trcr_bioaams(:)|metric tons/ha|simulation biomass (dry weight) in the 
!!                              |raised segment
!!    trcr_laiaamx(:)|none      |maximum leaf area index for the simulation in the
!!                              |raised segment
!!    trcr_yldaa(:)|metric tons/ha|simulation yield (dry weight) in the 
!!                              |raised segment
!!    trcu_bioaams(:)|metric tons/ha|simulation biomass (dry weight) in the 
!!                              |undisturbe segment
!!    trcu_laiaamx(:)|none      |maximum leaf area index for the simulation in the
!!                              |undisturbe segment
!!    trcu_yldaa(:)|metric tons/ha|simulation yield (dry weight) in the 
!!                              |undisturbe segment
!!    years       |years        |length of simulation
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    iflag       |none          |flag to denote presence of terrace in
!!                               |HRU
!!    j           |none          |HRU number
!!    sb          |none          |subbasin number  
!!    trcvas(:)   |varies        |array to hold terrace output values
!!    t_km        |km^2          |HRU terrace area in km**2
!!    tb_km       |km^2          |terrace undisturbed area in km**2
!!    tr_km       |km^2          |terrace riser (cutslope) area in km**2
!!    tu_km       |km^2          |terrace bed (frontslope) area in km**2
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      real, intent (in) :: years
      integer :: j, sb, iflag, ii
      real :: t_km, tu_km, tr_km, tb_km
      real, dimension (59) :: trcvas
      character*4 :: crop_trcu, crop_trcr, crop_trcb

      sb = 0

      do j = 1, nhru
      sb = 0
      sb = hru_sub(j)
      iflag = 0
      if (trc_fr(j) >= 1.e-6) iflag = 1
      if (iflag == 1) then

      trcvas = 0.

      trcvas(1) = trcaao(8,j)
      trcvas(2) = trcaao(3,j)
      trcvas(3) = trcaao(1,j)
      trcvas(4) = trcaao(2,j)
      trcvas(5) = trcaao(4,j)
      trcvas(6) = trcaao(5,j)
      trcvas(7) = trcaao(6,j)
      trcvas(8) = trcaao(7,j)
      trcvas(9) = trcaao(13,j)
      trcvas(10) = trcaao(14,j)
      trcvas(11) = trcaao(9,j) / 365.4
      trcvas(12) = trcaao(10,j) / 365.4
      trcvas(13) = trcaao(11,j) / 365.4
      trcvas(14) = trcaao(12,j) / 365.4
      trcvas(15) = trcaao(15,j) / 365.4
      trcvas(16) = trcaao(16,j) / 365.4
      trcvas(17) = trcaao(17,j) / 365.4
      trcvas(18) = trcaao(18,j) / 365.4
      trcvas(19) = trcaao(19,j) / 365.4
      trcvas(20) = trcaao(20,j) / 365.4
      trcvas(21) = trcaao(21,j) / 365.4
      trcvas(22) = trcaao(22,j) / 365.4
      trcvas(23) = trcaao(23,j) / 365.4
      trcvas(24) = trcaao(24,j) / 365.4
      trcvas(25) = trcaao(25,j) / 365.4
      trcvas(26) = trcaao(26,j) / 365.4
      trcvas(27) = trcaao(27,j) / 365.4
      trcvas(28) = trcaao(28,j) / 365.4
      trcvas(29) = trcaao(29,j) / 365.4
      trcvas(30) = trcaao(30,j) / 365.4
      trcvas(31) = trcaao(31,j) / 365.4
      trcvas(32) = trcaao(32,j) / 365.4
      trcvas(33) = trcaao(33,j) / 365.4

      trcvas(34) = trcu_bioaams(j)
      trcvas(35) = trcu_laiaamx(j)
      trcvas(36) = trcu_yldaa(j)
      trcvas(37) = trcr_bioaams(j)
      trcvas(38) = trcr_laiaamx(j)
      trcvas(39) = trcr_yldaa(j)
      trcvas(40) = trcb_bioaams(j)
      trcvas(41) = trcb_laiaamx(j)
      trcvas(42) = trcb_yldaa(j)

      trcvas(43) = trcaao(34,j)
      trcvas(44) = trcaao(35,j)
      trcvas(45) = trcaao(36,j)
      trcvas(46) = trcaao(37,j)
      trcvas(47) = trcaao(38,j)

      trcvas(48) = trcaao(39,j)
      trcvas(49) = trcaao(40,j)
      trcvas(50) = trcaao(41,j)
      trcvas(51) = trcaao(42,j)
      trcvas(52) = trcaao(43,j)

      trcvas(53) = trcaao(44,j)
      trcvas(54) = trcaao(45,j)
      trcvas(55) = trcaao(46,j)
      trcvas(56) = trcaao(47,j)
      trcvas(57) = trcaao(48,j)
      trcvas(58) = trcaao(49,j)
      trcvas(59) = trcaao(50,j) / 365.4

      if (idplt_trcu(tunro(j),tuicr(j),j) > 0) then
        crop_trcu = pcom(j)%plg(idplt_trcu(tunro(j),tuicr(j),j))%cpnm        
      else
        crop_trcu = 'BARR'
      end if

      if (idplt_trcr(trnro(j),tricr(j),j) > 0) then
        crop_trcr = pcom(j)%plg(idplt_trcr(trnro(j),tricr(j),j))%cpnm
      else
        crop_trcr = 'BARR'
      end if

      if (idplt_trcb(tbnro(j),tbicr(j),j) > 0) then
        crop_trcb = pcom(j)%plg(idplt_trcb(tbnro(j),tbicr(j),j))%cpnm
      else
        crop_trcb = 'BARR'
      end if

      t_km = 0.
      tu_km = 0.
      tr_km = 0.
      tb_km = 0.
      t_km = hru(j)%km * trc_fr(j)
      tu_km = t_km * trcu_fr(j)
      tr_km = t_km * trcr_fr(j)
      tb_km = t_km * trcb_fr(j)

      write (32,1000) 'terrace', j, 0, sb,                      &
     &  years, t_km,(trcvas(ii), ii = 1, 2), trcvas(58),                &
     &  (trcvas(ii), ii = 3, 10), trcvas(59), (trcvas(ii), ii = 11, 15),&
     & 'undisturbed', crop_trcu, tu_km, (trcvas(ii), ii = 16, 21),      &
     & (trcvas(ii), ii = 34, 36), (trcvas(ii), ii = 43, 47),            &
     & 'cutslope', crop_trcr, tr_km, (trcvas(ii), ii = 22, 27),         &
     & (trcvas(ii), ii = 37, 39), (trcvas(ii), ii = 48, 52),            &
     & 'frontslope', crop_trcb, tb_km, (trcvas(ii), ii = 28, 33),       &
     & (trcvas(ii), ii = 40, 42), (trcvas(ii), ii = 53, 57)
      end if
      end do

      return
 1000 format (a7,1x,i5,1x,i8,1x,i4,1x,f4.0,13f11.3,4e11.3,f11.3         &
     & a12,1x,a4,2f11.3,13e11.3,a12,1x,a4,2f11.3,13e11.3,               &
     & a12,1x,a4,2f11.3,13e11.3)

      end

      subroutine trcday

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine writes daily terrace output to the output.trc file

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name           |units      |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tankirr(:)     |mm H2O     |amount of water removed from rainfall harvest tank
!!    laiday_trcb(:) |m**2/m**2  |leaf area index for bed area
!!    laiday_trcr(:) |m**2/m**2  |leaf area index for raised area
!!    laiday_trcu(:) |m**2/m**2  |leaf area index for undisturbed area
!!    tbsol_fon(:,:) |kg N/ha    |amount of nitrogen stored in the fresh 
!!                               |nitrogen pool for bed (frontslope)
!!    tbsol_fop(:,:) |kg P/ha    |amount of phosphorus stored in the 
!!                               |fresh phosphorus pool for bed (frontslope)
!!    tbsol_mn(:,:)  |kg N/ha    |amount of nitrogen stored in the active 
!!                               |organic N pool
!!    tbsol_mp(:,:)  |kg P/ha    |amount of phosphorus stored in the 
!!                               |active organic P pool
!!    tbsol_n(:,:)   |kg N/ha    |amount of nitrogen stored in the stable 
!!                               |organic N pool
!!    tbsol_nh3(:,:) |kg N/ha    |amount of nitrogen stored in the ammonium 
!!                               |pool for bed (frontslope)
!!    tbsol_no3(:,:) |kg N/ha    |amount of nitrogen stored in the nitrate 
!!                               |pool for bed (frontslope)
!!    tbsol_orgp(:,:)|kg P/ha    |amount of phosphorus stored in organic 
!!                               |for bed (frontslope)
!!    tbsol_prk(:,:) |mm H2O     |percolation storage array for bed 
!!                               |(frontslope)
!!    tbsol_solp(:,:)|kg P/ha    |amount of phosohorus stored in solution 
!!                               |for bed (frontslope)
!!    tbsol_sw(:)    |mm H2O     |amount of water stored in soil profile 
!!                               |of terrace bed (frontslope)
!!    tbstrsn(:)     |none       |fraction of potential plant growth achieved on
!!                               |the day where the reduction is caused by
!!                               |nitrogen stress
!!    tbstrsp(:)     |none       |fraction of potential plant growth achieved on
!!                               |the day where the reduction is caused by
!!                               |phosphorus stress
!!    tbstrstmp(:)   |none       |fraction of potential plant growth achieved
!!                               |on the day in terrace segment where the reduction is
!!                               |caused by temperature stress
!!    tbstrsw(:)     |none       |fraction of potential plant growth achieved
!!                               |on the day where the reduction is caused by
!!                               |water stress
!!    tbwtab(:)      |m          |water table depth from soil surface 
!!                               |of terrace bed (frontslope)
!!    trc_tvol(:)    |m^3        |volume of rainfall harvest tank water storage 
!!    trc_sed(:)     |metric tons|amount of sediment in terrace
!!    trc_sumev      |m^3 H2O    |daily evaporation on terrace
!!    trc_sumsep     |m^3 H2O    |daily seepage on terrace
!!    trc_vol(:)     |m^3 H2O    |volume of water in terrace
!!    trcb_bioms(:)  |kg/ha      |cover/crop biomass
!!    trcb_fr(:)     |none       |fraction of terrace bed (frontslope)/terrace 
!!                               |area
!!    trcflwi        |m^3 H2O    |volume of water flowing into terrace 
!!                               |on day
!!    trcflwo_dr     |m^3 H2O    |volume of water flowing out of terrace 
!!                               |in drainage flow on day
!!    trcflwo_ov     |m^3 H2O    |volume of water flowing out of terrace 
!!                               |in overland flow on day
!!    trcno3o_dr     |kg N       |amount of nitrate out of terrace by 
!!                               |drainage
!!    trcno3o_ov     |kg N       |amount of nitrate out of terrace by 
!!                               |overland flow
!!    trcorgno_dr    |kg N       |amount of organic nitrogen out of terrace 
!!                               |by drainage
!!    trcorgno_ov    |kg N       |amount of organic nitrogen out of terrace 
!!                               |by overland flow
!!    trcorgpo_dr    |kg P       |amount of organic phosphorus out of 
!!                               |terrace by drainage
!!    trcorgpo_ov    |kg P       |amount of organic phosphorus out of 
!!                               |terrace by overland flow
!!    trcpsedao_dr   |kg P       |amount of sediment phosphorus out of 
!!                               |terrace by drainage
!!    trcpsedao_ov   |kg P       |amount of sediment phosphorus out of 
!!                               |terrace by overland flow
!!    trcpsedso_dr   |kg P       |amount of sediment phosphorus out of 
!!                               |terrace by drainage
!!    trcpsedso_ov   |kg P       |amount of sediment phosphorus out of 
!!                               |terrace by overland flow
!!    trcr_bioms(:)  |kg/ha      |cover/crop biomass
!!    trcr_fr(:)     |none       |fraction of terrace riser (cutslope)/terrace 
!!                               |area
!!    trcsolpo_dr    |kg P       |amount of soluble P out of terrace 
!!                               |by drainage
!!    trcsolpo_ov    |kg P       |amount of soluble P out of terrace 
!!                               |by overland flow
!!    trcu_bioms(:)  |kg/ha      |cover/crop biomass
!!    trcu_fr(:)     |none       |fraction of terrace undisturbed area/terrace 
!!                               |area
!!    trsol_fon(:,:) |kg N/ha    |amount of nitrogen stored in the fresh 
!!                               |nitrogen pool for riser (cutslope)
!!    trsol_fop(:,:) |kg P/ha    |amount of phosphorus stored in the 
!!                               |fresh phosphorus pool for riser (cutslope)
!!    trsol_mn(:,:)  |kg N/ha    |amount of nitrogen stored in the active 
!!                               |organic N pool
!!    trsol_mp(:,:)  |kg P/ha    |amount of phosphorus stored in the 
!!                               |active organic P pool
!!    trsol_n(:,:)   |kg N/ha    |amount of nitrogen stored in the stable 
!!                               |organic N pool
!!    trsol_nh3(:,:) |kg N/ha    |amount of nitrogen stored in the ammonium 
!!                               |pool for riser (cutslope)
!!    trsol_no3(:,:) |kg N/ha    |amount of nitrogen stored in the nitrate 
!!                               |pool for riser (cutslope)
!!    trsol_orgp(:,:)|kg P/ha    |amount of phosphorus stored in organic 
!!                               |for riser (cutslope)
!!    trsol_prk(:,:) |mm H2O     |percolation storage array for riser 
!!                               |(cutslope)
!!    trsol_solp(:,:)|kg P/ha    |amount of phosohorus stored in solution 
!!                               |for riser (cutslope)
!!    trsol_sw(:)    |mm H2O     |amount of water stored in soil profile 
!!                               |of terrace riser (cutslope)
!!    trstrsn(:)     |none       |fraction of potential plant growth achieved on
!!                               |the day where the reduction is caused by
!!                               |nitrogen stress
!!    trstrsp(:)     |none       |fraction of potential plant growth achieved on
!!                               |the day where the reduction is caused by
!!                               |phosphorus stress
!!    trstrstmp(:)   |none       |fraction of potential plant growth achieved
!!                               |on the day in terrace segment where the reduction is
!!                               |caused by temperature stress
!!    trstrsw(:)     |none       |fraction of potential plant growth achieved
!!                               |on the day where the reduction is caused by
!!                               |water stress
!!    trwtab(:)      |m          |water table depth from soil surface 
!!                               |of terrace riser (cutslope)
!!    tusol_fon(:,:) |kg N/ha    |amount of nitrogen stored in the fresh 
!!                               |nitrogen pool for undisturbed area
!!    tusol_fop(:,:) |kg P/ha    |amount of phosphorus stored in the 
!!                               |fresh phosphorus pool for undisturbed area
!!    tusol_mn(:,:)  |kg N/ha    |amount of nitrogen stored in the active 
!!                               |organic N pool
!!    tusol_mp(:,:)  |kg P/ha    |amount of phosphorus stored in the 
!!                               |active organic P pool
!!    tusol_n(:,:)   |kg N/ha    |amount of nitrogen stored in the stable 
!!                               |organic N pool
!!    tusol_nh3(:,:) |kg N/ha    |amount of nitrogen stored in the ammonium 
!!                               |pool for undisturbed area
!!    tusol_no3(:,:) |kg N/ha    |amount of nitrogen stored in the nitrate 
!!                               |pool for undisturbed area
!!    tusol_orgp(:,:)|kg P/ha    |amount of phosphorus stored in organic 
!!                               |for undisturbed area
!!    tusol_sw(:)    |mm H2O     |amount of water stored in soil profile 
!!                               |of terrace undisturbed area
!!    tustrsn(:)     |none       |fraction of potential plant growth achieved on
!!                               |the day where the reduction is caused by
!!                               |nitrogen stress
!!    tustrsp(:)     |none       |fraction of potential plant growth achieved on
!!                               |the day where the reduction is caused by
!!                               |phosphorus stress
!!    tustrstmp(:)   |none       |fraction of potential plant growth achieved
!!                               |on the day in terrace segment where the reduction is
!!                               |caused by temperature stress
!!    tustrsw(:)     |none       |fraction of potential plant growth achieved
!!                               |on the day where the reduction is caused by
!!                               |water stress
!!    tuwtab(:)      |m          |water table depth from soil surface 
!!                               |of terrace undisturbed area
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    iflag       |none          |flag to denote presence of terrace in
!!                               |HRU
!!    ii          |none          |counter
!!    j           |none          |HRU number
!!    minp_ppm    |mg P/L        |mineral P concentration in terrace
!!    no3_ppm     |mg N/L        |nitrate concentration in terrace
!!    orgn_ppm    |mg N/L        |organic N concentration in terrace
!!    orgp_ppm    |mg P/L        |organic P concentration in terrace
!!    sb          |none          |subbasin number  
!!    tcnv        |none          |convert mm H2O to m^3 H2O
!!    trcvas(:)   |varies        |array to hold terrace output values
!!    t_ha        |ha            |HRU terrace area in ha
!!    t_km        |km^2          |HRU terrace area in km**2
!!    tb_km       |km^2          |terrace undisturbed area in km**2
!!    tr_km       |km^2          |terrace riser (cutslope) area in km**2
!!    tu_km       |km^2          |terrace bed (frontslope) area in km**2
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use hydrograph_module
      use climate_parms
      use jrw_datalib_module
      implicit none
      integer :: j, sb, ii, iflag, jj
      real :: orgn_ppm, orgp_ppm, no3_ppm, minp_ppm
      real :: tcnv, t_ha, t_km, tu_km, tr_km, tb_km
      real, dimension (59) :: trcvas
      character*4 :: crop_trcu, crop_trcr, crop_trcb

      j = 0
      sb = 0
      j = ihru
      sb = inum1

      iflag = 0
      if (trc_fr(j) >= 1.e-6) iflag = 1
      if (iflag == 1) then

      t_ha = 0.
      t_ha = (hru(j)%km*100) * trc_fr(j)
      tcnv = 0.
      tcnv = 10. * t_ha
!! calculate nutrient concentrations
      orgn_ppm = 0.
      orgp_ppm = 0.
      no3_ppm = 0.
      minp_ppm = 0.

      if (trc_vol(j) > 1.) then
        orgn_ppm = 1000. * trc_orgn(j) / trc_vol(j)
        orgp_ppm = 1000. * trc_orgp(j) / trc_vol(j)
        no3_ppm = 1000. * trc_no3(j) / trc_vol(j)
        minp_ppm = 1000. * (trc_solp(j) + trc_pseda(j) + trc_pseds(j))  &
     &                                                  / trc_vol(j)
      endif

      trcvas = 0.

      trcvas(1) = wst(ob(hru(j)%obj_no)%wst)%weat%precip
      trcvas(2) = trcflwi / tcnv
      trcvas(3) = trc_sumev / tcnv
      trcvas(4) = trc_sumsep / tcnv
      trcvas(5) = trcflwo_ov / tcnv
      trcvas(6) = trcsedo_ov / t_ha
      trcvas(7) = trcflwo_dr / tcnv
      trcvas(8) = trcsedo_dr / t_ha
      trcvas(9) = trc_vol(j) / tcnv
      trcvas(10) = trc_sed(j) / t_ha
      trcvas(11) = orgn_ppm
      trcvas(12) = no3_ppm
      trcvas(13) = orgp_ppm
      trcvas(14) = minp_ppm
      trcvas(15) = tusol_sw(j) * trcu_fr(j) + trsol_sw(j) * trcr_fr(j)  &
     &             + tbsol_sw(j) * trcb_fr(j)

      trcvas(16) = tusol_sw(j)
      do jj = 1, soil(j)%nly      
          trcvas(17) = trcvas(17) + tusol_no3(jj,j) + tusol_nh3(jj,j)
          trcvas(19) = trcvas(19) + tusol_solp(jj,j) + tusol_stap(jj,j) &
     &                 + tusol_actp(jj,j)
        if (bsn_cc%cswat == 0) then
          trcvas(18) = trcvas(18) + tusol_aorgn(jj,j) +                 &
     &                              tusol_orgn(jj,j) + tusol_fon(jj,j)
          trcvas(20) = trcvas(20) + tusol_orgp(jj,j) + tusol_fop(jj,j)
        else
          trcvas(18) = trcvas(18) + tusol_mn(jj,j) +                    &
     &                              tusol_orgn(jj,j) + tusol_fon(jj,j)
          trcvas(20) = trcvas(20) + tusol_orgp(jj,j) + tusol_fop(jj,j)  &
     &                 + tusol_mp(jj,j)
        end if
      end do
      trcvas(21) = tuwtab(j)
      
      trcvas(22) = trsol_sw(j)
      do jj = 1, soil(j)%nly      
          trcvas(23) = trcvas(23) + trsol_no3(jj,j) + trsol_nh3(jj,j)
          trcvas(25) = trcvas(25) + trsol_solp(jj,j) + trsol_stap(jj,j) &
     &                 + trsol_actp(jj,j)
        if (bsn_cc%cswat == 0) then
          trcvas(24) = trcvas(24) + trsol_aorgn(jj,j) +                 &
     &                              trsol_orgn(jj,j) + trsol_fon(jj,j)
          trcvas(26) = trcvas(26) + trsol_orgp(jj,j) + trsol_fop(jj,j)
        else
          trcvas(24) = trcvas(24) + trsol_mn(jj,j) +                    &
     &                              trsol_orgn(jj,j) + trsol_fon(jj,j)
          trcvas(26) = trcvas(26) + trsol_orgp(jj,j) + trsol_fop(jj,j)  &
     &                 + trsol_mp(jj,j)
        end if
      end do
      trcvas(27) = trwtab(j)

      trcvas(28) = tbsol_sw(j)
      do jj = 1, soil(j)%nly      
          trcvas(29) = trcvas(29) + tbsol_no3(jj,j) + tbsol_nh3(jj,j)
          trcvas(31) = trcvas(31) + tbsol_solp(jj,j) + tbsol_stap(jj,j) &
     &                 + tbsol_actp(jj,j)
        if (bsn_cc%cswat == 0) then
          trcvas(30) = trcvas(30) + tbsol_aorgn(jj,j) +                 &
     &                              tbsol_orgn(jj,j) + tbsol_fon(jj,j)
          trcvas(32) = trcvas(32) + tbsol_orgp(jj,j) + tbsol_fop(jj,j)
        else
          trcvas(30) = trcvas(30) + tbsol_mn(jj,j) +                    &
     &                              tbsol_orgn(jj,j) + tbsol_fon(jj,j)
          trcvas(32) = trcvas(32) + tbsol_orgp(jj,j) + tbsol_fop(jj,j)  &
     &                 + tbsol_mp(jj,j)
        end if
      end do
      trcvas(33) = tbwtab(j)

      trcvas(34) = trcu_bioms(j)
      trcvas(35) = laiday_trcu(j)
      trcvas(36) = 0.
      trcvas(37) = trcr_bioms(j)
      trcvas(38) = laiday_trcr(j)
      trcvas(39) = 0.
      trcvas(40) = trcb_bioms(j)
      trcvas(41) = laiday_trcb(j)
      trcvas(42) = 0.

      trcvas(43) = 1.-tustrsw(j)
      trcvas(44) = 1.-tustrstmp(j)
      trcvas(45) = 1.-tustrsn(j)
      trcvas(46) = 1.-tustrsp(j)
      trcvas(47) = 1.-tustrsa(j)

      trcvas(48) = 1.-trstrsw(j)
      trcvas(49) = 1.-trstrstmp(j)
      trcvas(50) = 1.-trstrsn(j)
      trcvas(51) = 1.-trstrsp(j)
      trcvas(52) = 1.-trstrsa(j)

      trcvas(53) = 1.-tbstrsw(j)
      trcvas(54) = 1.-tbstrstmp(j)
      trcvas(55) = 1.-tbstrsn(j)
      trcvas(56) = 1.-tbstrsp(j)
      trcvas(57) = 1.-tbstrsa(j)
      trcvas(58) = tankirr(j)
      trcvas(59) = trc_tvol(j) / tcnv

      if (idplt_trcu(tunro(j),tuicr(j),j) > 0) then
        crop_trcu = pcom(j)%plg(idplt_trcu(tunro(j),tuicr(j),j))%cpnm
      else
        crop_trcu = 'BARR'
      end if

      if (idplt_trcr(trnro(j),tricr(j),j) > 0) then
        crop_trcr = pcom(j)%plg(idplt_trcr(trnro(j),tricr(j),j))%cpnm
      else
        crop_trcr = 'BARR'
      end if

      if (idplt_trcb(tbnro(j),tbicr(j),j) > 0) then
        crop_trcb = pcom(j)%plg(idplt_trcb(tbnro(j),tbicr(j),j))%cpnm
      else
        crop_trcb = 'BARR'
      end if

      t_km = 0.
      tu_km = 0.
      tr_km = 0.
      tb_km = 0.
      t_km = t_ha / 100.
      tu_km = t_km * trcu_fr(j)
      tr_km = t_km * trcr_fr(j)
      tb_km = t_km * trcb_fr(j)

      write (32,1000) 'terrace', j, 0, sb,                      &
     &  iida, t_km,(trcvas(ii), ii = 1, 2), trcvas(58),                 &
     &  (trcvas(ii), ii = 3, 10), trcvas(59), (trcvas(ii), ii = 11, 15),&
     & 'undisturbed', crop_trcu, tu_km, (trcvas(ii), ii = 16, 21),      &
     & (trcvas(ii), ii = 34, 36), (trcvas(ii), ii = 43, 47),            &
     & 'cutslope', crop_trcr, tr_km, (trcvas(ii), ii = 22, 27),         &
     & (trcvas(ii), ii = 37, 39), (trcvas(ii), ii = 48, 52),            &
     & 'frontslope', crop_trcb, tb_km, (trcvas(ii), ii = 28, 33),       &
     & (trcvas(ii), ii = 40, 42), (trcvas(ii), ii = 53, 57)
      end if

      return
 1000 format (a7,1x,i5,1x,i8,1x,i4,1x,i4,13f11.3,4e11.3,f11.3           &
     & a12,1x,a4,2f11.3,13e11.3,a12,1x,a4,2f11.3,13e11.3,               &
     & a12,1x,a4,2f11.3,13e11.3)

      end

      subroutine trcmon

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine writes monthly terrace output to the output.trc file

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name          |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    hru_sub(:)   |none        |subbasin in which HRU is located
!!    iyr          |year        |current year of simulation (actual year)
!!    laiday_trcb(:)|m**2/m**2  |leaf area index for bed area
!!    laiday_trcr(:)|m**2/m**2  |leaf area index for raised area
!!    laiday_trcu(:)|m**2/m**2  |leaf area index for undisturbed area
!!    mo_chk       |none        |current month of simulation
!!    trcmono(1,:) |mm H2O      |evaporation on terrace for month
!!    trcmono(2,:) |mm H2O      |seepage on terrace for month
!!    trcmono(3,:) |mm H2O      |amount of water flow into terrace for 
!!                              |month
!!    trcmono(4,:) |mm H2O      |amount of water flow out of terrace by 
!!                              |overland flow for month
!!    trcmono(5,:) |t/ha        |amount of sediment flow out of terrace 
!!                              |by overland flow for month
!!    trcmono(6,:) |mm H2O      |amount of water flow out of terrace by 
!!                              |drainage flow for month
!!    trcmono(7,:) |t/ha        |amount of sediment flow out of terrace 
!!                              |by drainage flow for month
!!    trcmono(8,:) |mm H2O      |amount of precipitation on terrace for 
!!                              |month
!!    trcmono(9,:) |ppm N       |concentration of organic N stored in 
!!                              |terrace for month
!!    trcmono(10,:)|ppm P       |concentration of organic P stored in 
!!                              |terrace for month
!!    trcmono(11,:)|ppm N       |concentration of nitrate stored in 
!!                              |terrace for month
!!    trcmono(12,:)|ppm P       |concentration of mineral P stored in 
!!                              |terrace for month
!!    trcmono(13,:)|m^3 H2O     |amount of water stored in terrace for 
!!                              |month
!!    trcmono(14,:)|metric tons |amount of sediment stored in terrace for 
!!                              |month
!!    trcmono(15,:)|mm H2O      |water in terrace soil profile for month
!!    trcmono(16,:)|mm H2O      |water in undisturbed area soil profile 
!!                              |for month
!!    trcmono(17,:)|kg N/ha     |mineral N in undisturbed area soil profile 
!!                              |for month
!!    trcmono(18,:)|kg N/ha     |organic N in undisturbed area soil profile 
!!                              |for month
!!    trcmono(19,:)|kg P/ha     |mineral P in undisturbed area soil profile 
!!                              |for month
!!    trcmono(20,:)|kg P/ha     |organic P in undisturbed area soil profile 
!!                              |for month
!!    trcmono(21,:)|m           |water table depth from soil surface in
!!                              |undisturbed area for month
!!    trcmono(22,:)|mm H2O      |water in riser (cutslope) soil profile 
!!                              |for month
!!    trcmono(23,:)|kg N/ha     |mineral N in riser (cutslope) soil profile 
!!                              |for month
!!    trcmono(24,:)|kg N/ha     |organic N in riser (cutslope) soil profile 
!!                              |for month
!!    trcmono(25,:)|kg P/ha     |mineral P in riser (cutslope) soil profile 
!!                              |for month
!!    trcmono(26,:)|kg P/ha     |organic P in riser (cutslope) soil profile 
!!                              |for month
!!    trcmono(27,:)|m           |water table depth from soil surface in
!!                              |riser (cutslope) for month
!!    trcmono(28,:)|mm H2O      |water in bed (frontslope) soil profile for 
!!                              |month
!!    trcmono(29,:)|kg N/ha     |mineral N in bed (frontslope) soil profile 
!!                              |for month
!!    trcmono(30,:)|kg N/ha     |organic N in bed (frontslope) soil profile 
!!                              |for month
!!    trcmono(31,:)|kg P/ha     |mineral P in bed (frontslope) soil profile 
!!                              |for month
!!    trcmono(32,:)|kg P/ha     |organic P in bed (frontslope) soil profile 
!!                              |for month
!!    trcmono(33,:)|m           |water table depth from soil surface in
!!                              |bed (frontslope) for month    
!!    trcmono(34,:)|stress days |water stress days in undisturbed area 
!!                              |during month
!!    trcmono(35,:)|stress days |temperature stress days in undisturbed area 
!!                              |during month
!!    trcmono(36,:)|stress days |nitrogen stress days in undisturbed area 
!!                              |during month
!!    trcmono(37,:)|stress days |phosphorus stress days in undisturbed area 
!!                              |during month
!!    trcmono(38,:)|stress days |air stress days in undisturbed area during 
!!                              |month
!!    trcmono(39,:)|stress days |water stress days in raised area during 
!!                              |month
!!    trcmono(40,:)|stress days |temperature stress days in raised area during 
!!                              |month
!!    trcmono(41,:)|stress days |nitrogen stress days in raised area during 
!!                              |month
!!    trcmono(42,:)|stress days |phosphorus stress days in raised area during 
!!                              |month
!!    trcmono(43,:)|stress days |air stress days in raised area during 
!!                              |month
!!    trcmono(44,:)|stress days |water stress days in bed area during 
!!                              |month
!!    trcmono(45,:)|stress days |temperature stress days in bed area during 
!!                              |month
!!    trcmono(46,:)|stress days |nitrogen stress days in bed area during 
!!                              |month
!!    trcmono(47,:)|stress days |phosphorus stress days in bed area during 
!!                              |month
!!    trcmono(48,:)|stress days |air stress days in bed area during month
!!    trcmono(49,:)|mm H2O      |amount of water removed from rainfall harvest tank
!!    trcmono(50,:)|mm H2O      |amount of rainfall harvest tank water storage
!!    tbhvstiadj(:) |(kg/ha)/(kg/ha)|optimal harvest index for current time during
!!                              |growing season
!!    trcb_bioms(:)|kg/ha       |cover/crop biomass
!!    trcb_rwt(:)   |none       |fraction of total plant biomass that is
!!                              |in roots
!!    trcr_bioms(:)|kg/ha       |cover/crop biomass
!!    trcr_rwt(:)   |none       |fraction of total plant biomass that is
!!                              |in roots
!!    trcu_bioms(:)|kg/ha       |cover/crop biomass
!!    trcu_rwt(:)   |none       |fraction of total plant biomass that is
!!                              |in roots
!!    trhvstiadj(:) |(kg/ha)/(kg/ha)|optimal harvest index for current time during
!!                              |growing season
!!    tuhvstiadj(:)|(kg/ha)/(kg/ha)|optimal harvest index for current time during
!!                              |growing season
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    iflag       |none          |flag to denote presence of terrace in
!!                               |HRU
!!    j           |none          |HRU number
!!    sb          |none          |subbasin number  
!!    trcvas(:)   |varies        |array to hold terrace output values
!!    t_km        |km^2          |HRU terrace area in km**2
!!    tb_km       |km^2          |terrace undisturbed area in km**2
!!    tr_km       |km^2          |terrace riser (cutslope) area in km**2
!!    tu_km       |km^2          |terrace bed (frontslope) area in km**2    
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j, sb, iflag, ii, days
      real :: t_km, tu_km, tr_km, tb_km
      real, dimension (59) :: trcvas
      character*4 :: crop_trcu, crop_trcr, crop_trcb
      integer leapyr
      
      if (Mod(time%yrc,4) == 0) then 
          leapyr = 0
        else 
          leapyr = 1
        end if

      days = 0

      select case(time%mo)
        case (9, 4, 6, 11)
          days = 30
        case (2)
          days = 29 - leapyr
        case default
          days = 31
      end select

      do j = 1, nhru
      sb = 0
      sb = hru_sub(j)
      iflag = 0
      if (trc_fr(j) >= 1.e-6) iflag = 1
      if (iflag == 1) then

      trcvas = 0.

      trcvas(1) = trcmono(8,j)
      trcvas(2) = trcmono(3,j)
      trcvas(3) = trcmono(1,j)
      trcvas(4) = trcmono(2,j)
      trcvas(5) = trcmono(4,j)
      trcvas(6) = trcmono(5,j)
      trcvas(7) = trcmono(6,j)
      trcvas(8) = trcmono(7,j)
      trcvas(9) = trcmono(13,j) / Real(days)
      trcvas(10) = trcmono(14,j)
      trcvas(11) = trcmono(9,j) / Real(days)
      trcvas(12) = trcmono(10,j) / Real(days)
      trcvas(13) = trcmono(11,j) / Real(days)
      trcvas(14) = trcmono(12,j) / Real(days)
      trcvas(15) = trcmono(15,j) / Real(days)
      trcvas(16) = trcmono(16,j) / Real(days)
      trcvas(17) = trcmono(17,j) / Real(days)
      trcvas(18) = trcmono(18,j) / Real(days)
      trcvas(19) = trcmono(19,j) / Real(days)
      trcvas(20) = trcmono(20,j) / Real(days)
      trcvas(21) = trcmono(21,j) / Real(days)
      trcvas(22) = trcmono(22,j) / Real(days)
      trcvas(23) = trcmono(23,j) / Real(days)
      trcvas(24) = trcmono(24,j) / Real(days)
      trcvas(25) = trcmono(25,j) / Real(days)
      trcvas(26) = trcmono(26,j) / Real(days)
      trcvas(27) = trcmono(27,j) / Real(days)
      trcvas(28) = trcmono(28,j) / Real(days)
      trcvas(29) = trcmono(29,j) / Real(days)
      trcvas(30) = trcmono(30,j) / Real(days)
      trcvas(31) = trcmono(31,j) / Real(days)
      trcvas(32) = trcmono(32,j) / Real(days)
      trcvas(33) = trcmono(33,j) / Real(days)

      trcvas(34) = trcu_bioms(j) / 1000.
      trcvas(35) = laiday_trcu(j)
      trcvas(36) = trcvas(34) * (1 - trcu_rwt(j)) * tuhvstiadj(j)
      trcvas(37) = trcr_bioms(j) / 1000.
      trcvas(38) = laiday_trcr(j)
      trcvas(39) = trcvas(37) * (1 - trcr_rwt(j)) * trhvstiadj(j)
      trcvas(40) = trcb_bioms(j) / 1000.
      trcvas(41) = laiday_trcb(j)
      trcvas(42) = trcvas(40) * (1 - trcb_rwt(j)) * tbhvstiadj(j)

      trcvas(43) = trcmono(34,j)
      trcvas(44) = trcmono(35,j)
      trcvas(45) = trcmono(36,j)
      trcvas(46) = trcmono(37,j)
      trcvas(47) = trcmono(38,j)

      trcvas(48) = trcmono(39,j)
      trcvas(49) = trcmono(40,j)
      trcvas(50) = trcmono(41,j)
      trcvas(51) = trcmono(42,j)
      trcvas(52) = trcmono(43,j)

      trcvas(53) = trcmono(44,j)
      trcvas(54) = trcmono(45,j)
      trcvas(55) = trcmono(46,j)
      trcvas(56) = trcmono(47,j)
      trcvas(57) = trcmono(48,j)
      trcvas(58) = trcmono(49,j)
      trcvas(59) = trcmono(50,j) / Real(days)

      if (idplt_trcu(tunro(j),tuicr(j),j) > 0) then
        crop_trcu = pcom(j)%plg(idplt_trcu(tunro(j),tuicr(j),j))%cpnm
      else
        crop_trcu = 'BARR'
      end if

      if (idplt_trcr(trnro(j),tricr(j),j) > 0) then
        crop_trcr = pcom(j)%plg(idplt_trcr(trnro(j),tricr(j),j))%cpnm
      else
        crop_trcr = 'BARR'
      end if

      if (idplt_trcb(tbnro(j),tbicr(j),j) > 0) then
        crop_trcb = pcom(j)%plg(idplt_trcb(tbnro(j),tbicr(j),j))%cpnm
      else
        crop_trcb = 'BARR'
      end if
      
      t_km = 0.
      tu_km = 0.
      tr_km = 0.
      tb_km = 0.
      t_km = hru(j)%km * trc_fr(j)
      tu_km = t_km * trcu_fr(j)
      tr_km = t_km * trcr_fr(j)
      tb_km = t_km * trcb_fr(j)

      write (32,1000) 'terrace', j, 0, sb,                      &
     &  time%mo, t_km,(trcvas(ii), ii = 1, 2), trcvas(58),               &
     &  (trcvas(ii), ii = 3, 10), trcvas(59), (trcvas(ii), ii = 11, 15),&
     & 'undisturbed', crop_trcu, tu_km, (trcvas(ii), ii = 16, 21),      &
     & (trcvas(ii), ii = 34, 36), (trcvas(ii), ii = 43, 47),            &
     & 'cutslope', crop_trcr, tr_km, (trcvas(ii), ii = 22, 27),         &
     & (trcvas(ii), ii = 37, 39), (trcvas(ii), ii = 48, 52),            &
     & 'frontslope', crop_trcb, tb_km, (trcvas(ii), ii = 28, 33),       &
     & (trcvas(ii), ii = 40, 42), (trcvas(ii), ii = 53, 57)
      end if
      end do
      return
 1000 format (a7,1x,i5,1x,i8,1x,i4,1x,i4,13f11.3,4e11.3,f11.3           &
     & a12,1x,a4,2f11.3,13e11.3,a12,1x,a4,2f11.3,13e11.3,               &
     & a12,1x,a4,2f11.3,13e11.3)

      end

      subroutine trcplant

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    This subroutine carries out the fertilization operation and the 
!!    plant growth simulation

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name          |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    igro_trc      |none          |land cover status code:
!!                                 |0 no land cover currently growing
!!                                 |1 land cover growing
!!    iida          |day           |day being simulated
!!    phubase(:)    |heat units    |base zero total heat units (used when no
!!                                 |land cover is growing
!!    phun_trc(:,:) |none          |fraction of plant heat units at which
!!                                 |fertilization occurs
!!    tnfert        |none          |sequence number of fertilizer application
!!    tnro          |none          |sequence number of year in rotation
!!    trc_ifert(:,:)|julian date   |date of fertilizer application
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    aphu        |heat units    |fraction of total heat units accumulated
!!    j           |none          |HRU number
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    SWAT: fert_trc, confert_trc, plantmod_trc, nminrl_trc
!!          carbon_trc, nitvol_trc, pminrl_trc

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      use reservoir_module
      implicit none
      integer :: j
      real :: aphu, phuop
      
      j = 0
      j = ihru

        !! apply fertilizer-check day and heat units
        do while (iida == trc_ifert(tnro,tnfert))
          call fert_trc
!	      if (imgt == 1) then
!              write (143,1000) j, iyr, i_mo, iida, "  FERT APP"
!	      end if
        end do

        if (igro_trc == 0) then
          aphu = phubase(j)
          phuop = phun_nocrop_trc(tnro,tnfert)
        else
          aphu = pcom(j)%plcur(pcom(j)%npl)%phuacc 
          phuop = phun_trc(tnro,tnfert)
        endif

        do while (phuop>0. .and. aphu > phuop)
          call fert_trc
!	      if (imgt == 1) then
!              write (143,1000) j, iyr, i_mo, iida, "  FERT APP"
!	      end if
          if (pcom(j)%plcur(pcom(j)%npl)%gro == 0) then
            aphu = phubase(j)
            phuop = phun_nocrop_trc(tnro,tnfert)
          else
            aphu = pcom(j)%plcur(pcom(j)%npl)%phuacc 
            phuop = phun_trc(tnro,tnfert)
          endif
        end do

      !! apply fertilizer/manure in continuous fert operation
      call confert_trc

      !! simulate the plant growth routine
      call plantmod_trc

      return
      end

      subroutine trcq_daycn

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    Predicts daily runoff given daily precipitation and snow melt
!!    using a modified SCS curve number approach on terrace segment

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    cnday_trc   |none          |curve number for current day, terrace segment and at 
!!                               |current soil moisture
!!    prec_trc    |mm H2O        |precipitation for the day in HRU
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    trcq        |mm H2O        |surface runoff for the day in terrace segment
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    bb          |none          |variable used to store intermediate 
!!                               |calculation result
!!    j           |none          |HRU number
!!    pb          |none          |variable used to store intermediate
!!                               |calculation result
!!    r2          |none          |retention parameter in CN equation
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~


      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j
      real :: r2, bb, pb

      j = 0
      j = ihru

      r2 = 0.
      bb = 0.
      pb = 0.
      r2 = 25400. / cnday_trc - 254.
      bb = .2 * r2
      pb = prec_trc - bb

      if (pb > 0.) then
        trcq = pb * pb / (prec_trc + .8 * r2)
      end if
    
      return
      end

      subroutine trcyr

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine writes yearly terrace output to the output.trc file

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name          |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    hru_sub(:)   |none        |subbasin in which HRU is located
!!    iyr          |year        |current year of simulation (actual year)
!!    nhru         |none        |number of HRUs in watershed
!!    trcyro(1,:)  |mm H2O      |evaporation on terrace for year
!!    trcyro(2,:)  |mm H2O      |seepage on terrace for year
!!    trcyro(3,:)  |mm H2O      |amount of water flow into terrace for 
!!                              |year
!!    trcyro(4,:)  |mm H2O      |amount of water flow out of terrace by 
!!                              |overland flow for year
!!    trcyro(5,:)  |t/ha        |amount of sediment flow out of terrace 
!!                              |by overland flow for year
!!    trcyro(6,:)  |mm H2O      |amount of water flow out of terrace by 
!!                              |drainage flow for year
!!    trcyro(7,:)  |t/ha        |amount of sediment flow out of terrace 
!!                              |by drainage flow for year
!!    trcyro(8,:)  |mm H2O      |amount of precipitation on terrace for 
!!                              |year
!!    trcyro(9,:)  |ppm N       |concentration of organic N stored in 
!!                              |terrace for year
!!    trcyro(10,:) |ppm P       |concentration of organic P stored in 
!!                              |terrace for year
!!    trcyro(11,:) |ppm N       |concentration of nitrate stored in 
!!                              |terrace for year
!!    trcyro(12,:) |ppm P       |concentration of mineral P stored in 
!!                              |terrace for year
!!    trcyro(13,:) |m^3 H2O     |amount of water stored in terrace for 
!!                              |year
!!    trcyro(14,:) |metric tons |amount of sediment stored in terrace for 
!!                              |year
!!    trcyro(15,:) |mm H2O      |water in terrace soil profile for year
!!    trcyro(16,:) |mm H2O      |water in undisturbed area soil profile 
!!                              |for year
!!    trcyro(17,:) |kg N/ha     |mineral N in undisturbed area soil profile 
!!                              |for year
!!    trcyro(18,:) |kg N/ha     |organic N in undisturbed area soil profile 
!!                              |for year
!!    trcyro(19,:) |kg P/ha     |mineral P in undisturbed area soil profile 
!!                              |for year
!!    trcyro(20,:) |kg P/ha     |organic P in undisturbed area soil profile 
!!                              |for year
!!    trcyro(21,:) |m           |water table depth from soil surface in
!!                              |undisturbed area for year
!!    trcyro(22,:) |mm H2O      |water in riser (cutslope) soil profile 
!!                              |for year
!!    trcyro(23,:) |kg N/ha     |mineral N in riser (cutslope) soil profile 
!!                              |for year
!!    trcyro(24,:) |kg N/ha     |organic N in riser (cutslope) soil profile 
!!                              |for year
!!    trcyro(25,:) |kg P/ha     |mineral P in riser (cutslope) soil profile 
!!                              |for year
!!    trcyro(26,:) |kg P/ha     |organic P in riser (cutslope) soil profile 
!!                              |for year
!!    trcyro(27,:) |m           |water table depth from soil surface in
!!                              |riser (cutslope) for year
!!    trcyro(28,:) |mm H2O      |water in bed (frontslope) soil profile for 
!!                              |year
!!    trcyro(29,:) |kg N/ha     |mineral N in bed (frontslope) soil profile 
!!                              |for year
!!    trcyro(30,:) |kg N/ha     |organic N in bed (frontslope) soil profile 
!!                              |for year
!!    trcyro(31,:) |kg P/ha     |mineral P in bed (frontslope) soil profile 
!!                              |for year
!!    trcyro(32,:) |kg P/ha     |organic P in bed (frontslope) soil profile 
!!                              |for year
!!    trcyro(33,:) |m           |water table depth from soil surface in
!!                              |bed (frontslope) for year    
!!    trcyro(34,:) |stress days |water stress days in undisturbed area 
!!                              |during year
!!    trcyro(35,:) |stress days |temperature stress days in undisturbed area 
!!                              |during year
!!    trcyro(36,:) |stress days |nitrogen stress days in undisturbed area 
!!                              |during year
!!    trcyro(37,:) |stress days |phosphorus stress days in undisturbed area 
!!                              |during year
!!    trcyro(38,:) |stress days |air stress days in undisturbed area during 
!!                              |year
!!    trcyro(39,:) |stress days |water stress days in raised area during 
!!                              |year
!!    trcyro(40,:) |stress days |temperature stress days in raised area during 
!!                              |year
!!    trcyro(41,:) |stress days |nitrogen stress days in raised area during 
!!                              |year
!!    trcyro(42,:) |stress days |phosphorus stress days in raised area during 
!!                              |year
!!    trcyro(43,:) |stress days |air stress days in raised area during 
!!                              |year
!!    trcyro(44,:) |stress days |water stress days in bed area during 
!!                              |year
!!    trcyro(45,:) |stress days |temperature stress days in bed area during 
!!                              |year
!!    trcyro(46,:) |stress days |nitrogen stress days in bed area during 
!!                              |year
!!    trcyro(47,:) |stress days |phosphorus stress days in bed area during 
!!                              |year
!!    trcyro(48,:) |stress days |air stress days in bed area during year
!!    trcyro(49,:) |mm H2O      |amount of water removed from rainfall harvest tank
!!    trcyro(50,:) |mm H2O      |amount of rainfall harvest tank water storage
!!    trcb_bioyrms(:)|metric tons/ha|annual biomass (dry weight) in the 
!!                              |bed segment
!!    trcb_laiyrmx(:)|none      |maximum leaf area index for the year in the
!!                              |bed segment
!!    trcb_yldanu(:) |metric tons/ha|annual yield (dry weight) in the 
!!                              |bed segment
!!    trcr_bioyrms(:)|metric tons/ha|annual biomass (dry weight) in the 
!!                              |raised segment
!!    trcr_laiyrmx(:)|none      |maximum leaf area index for the year in the
!!                              |raised segment
!!    trcr_yldanu(:) |metric tons/ha|annual yield (dry weight) in the 
!!                              |raised segment
!!    trcu_bioyrms(:)|metric tons/ha|annual biomass (dry weight) in the 
!!                              |undisturbe segment
!!    trcu_laiyrmx(:)|none      |maximum leaf area index for the year in the
!!                              |undisturbe segment
!!    trcu_yldanu(:) |metric tons/ha|annual yield (dry weight) in the 
!!                              |undisturbe segment
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    iflag       |none          |flag to denote presence of terrace in
!!                               |HRU
!!    j           |none          |HRU number
!!    sb          |none          |subbasin number  
!!    trcvas(:)   |varies        |array to hold terrace output values
!!    t_km        |km^2          |HRU terrace area in km**2
!!    tb_km       |km^2          |terrace undisturbed area in km**2
!!    tr_km       |km^2          |terrace riser (cutslope) area in km**2
!!    tu_km       |km^2          |terrace bed (frontslope) area in km**2    
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none
      integer :: j, sb, iflag, ii
      real :: t_km, tu_km, tr_km, tb_km
      real, dimension (59) :: trcvas
      character*4 :: crop_trcu, crop_trcr, crop_trcb
      integer leapyr
      
      if (Mod(time%yrc,4) == 0) then 
          leapyr = 0
        else 
          leapyr = 1
        end if

      j = 0

      do j = 1, nhru
      sb = 0
      sb = hru_sub(j)
      iflag = 0
      if (trc_fr(j) >= 1.e-6) iflag = 1
      if (iflag == 1) then

      trcvas = 0.

      trcvas(1) = trcyro(8,j)
      trcvas(2) = trcyro(3,j)
      trcvas(3) = trcyro(1,j)
      trcvas(4) = trcyro(2,j)
      trcvas(5) = trcyro(4,j)
      trcvas(6) = trcyro(5,j)
      trcvas(7) = trcyro(6,j)
      trcvas(8) = trcyro(7,j)
      trcvas(9) = trcyro(13,j) / Real(366 - leapyr)
      trcvas(10) = trcyro(14,j)
      trcvas(11) = trcyro(9,j) / Real(366 - leapyr)
      trcvas(12) = trcyro(10,j) / Real(366 - leapyr)
      trcvas(13) = trcyro(11,j) / Real(366 - leapyr)
      trcvas(14) = trcyro(12,j) / Real(366 - leapyr)
      trcvas(15) = trcyro(15,j) / Real(366 - leapyr)
      trcvas(16) = trcyro(16,j) / Real(366 - leapyr)
      trcvas(17) = trcyro(17,j) / Real(366 - leapyr)
      trcvas(18) = trcyro(18,j) / Real(366 - leapyr)
      trcvas(19) = trcyro(19,j) / Real(366 - leapyr)
      trcvas(20) = trcyro(20,j) / Real(366 - leapyr)
      trcvas(21) = trcyro(21,j) / Real(366 - leapyr)
      trcvas(22) = trcyro(22,j) / Real(366 - leapyr)
      trcvas(23) = trcyro(23,j) / Real(366 - leapyr)
      trcvas(24) = trcyro(24,j) / Real(366 - leapyr)
      trcvas(25) = trcyro(25,j) / Real(366 - leapyr)
      trcvas(26) = trcyro(26,j) / Real(366 - leapyr)
      trcvas(27) = trcyro(27,j) / Real(366 - leapyr)
      trcvas(28) = trcyro(28,j) / Real(366 - leapyr)
      trcvas(29) = trcyro(29,j) / Real(366 - leapyr)
      trcvas(30) = trcyro(30,j) / Real(366 - leapyr)
      trcvas(31) = trcyro(31,j) / Real(366 - leapyr)
      trcvas(32) = trcyro(32,j) / Real(366 - leapyr)
      trcvas(33) = trcyro(33,j) / Real(366 - leapyr)

      trcvas(34) = trcu_bioyrms(j)
      trcvas(35) = trcu_laiyrmx(j)
      trcvas(36) = trcu_yldanu(j)
      trcvas(37) = trcr_bioyrms(j)
      trcvas(38) = trcr_laiyrmx(j)
      trcvas(39) = trcr_yldanu(j)
      trcvas(40) = trcb_bioyrms(j)
      trcvas(41) = trcb_laiyrmx(j)
      trcvas(42) = trcb_yldanu(j)

      trcvas(43) = trcyro(34,j)
      trcvas(44) = trcyro(35,j)
      trcvas(45) = trcyro(36,j)
      trcvas(46) = trcyro(37,j)
      trcvas(47) = trcyro(38,j)

      trcvas(48) = trcyro(39,j)
      trcvas(49) = trcyro(40,j)
      trcvas(50) = trcyro(41,j)
      trcvas(51) = trcyro(42,j)
      trcvas(52) = trcyro(43,j)

      trcvas(53) = trcyro(44,j)
      trcvas(54) = trcyro(45,j)
      trcvas(55) = trcyro(46,j)
      trcvas(56) = trcyro(47,j)
      trcvas(57) = trcyro(48,j)
      trcvas(58) = trcyro(49,j)
      trcvas(59) = trcyro(50,j) / Real(366 - leapyr)

      if (idplt_trcu(tunro(j),tuicr(j),j) > 0) then
        crop_trcu = pcom(j)%plg(idplt_trcu(tunro(j),tuicr(j),j))%cpnm
      else
        crop_trcu = 'BARR'
      end if

      if (idplt_trcr(trnro(j),tricr(j),j) > 0) then
        crop_trcr = pcom(j)%plg(idplt_trcr(trnro(j),tricr(j),j))%cpnm
      else
        crop_trcr = 'BARR'
      end if

      if (idplt_trcb(tbnro(j),tbicr(j),j) > 0) then
        crop_trcb = pcom(j)%plg(idplt_trcb(tbnro(j),tbicr(j),j))%cpnm
      else
        crop_trcb = 'BARR'
      end if
            
      t_km = 0.
      tu_km = 0.
      tr_km = 0.
      tb_km = 0.
      t_km = hru(j)%km * trc_fr(j)
      tu_km = t_km * trcu_fr(j)
      tr_km = t_km * trcr_fr(j)
      tb_km = t_km * trcb_fr(j)

      write (32,1000) 'terrace', j, 0, sb,                      &
     &  time%yrs, t_km,(trcvas(ii), ii = 1, 2), trcvas(58),                  &
     &  (trcvas(ii), ii = 3, 10), trcvas(59), (trcvas(ii), ii = 11, 15),&
     & 'undisturbed', crop_trcu, tu_km, (trcvas(ii), ii = 16, 21),      &
     & (trcvas(ii), ii = 34, 36), (trcvas(ii), ii = 43, 47),            &
     & 'cutslope', crop_trcr, tr_km, (trcvas(ii), ii = 22, 27),         &
     & (trcvas(ii), ii = 37, 39), (trcvas(ii), ii = 48, 52),            &
     & 'frontslope', crop_trcb, tb_km, (trcvas(ii), ii = 28, 33),       &
     & (trcvas(ii), ii = 40, 42), (trcvas(ii), ii = 53, 57)
      end if
      end do

      return
 1000 format (a7,1x,i5,1x,i8,1x,i4,1x,i4,13f11.3,4e11.3,f11.3           &
     & a12,1x,a4,2f11.3,13e11.3,a12,1x,a4,2f11.3,13e11.3,               &
     & a12,1x,a4,2f11.3,13e11.3)

      end


      subroutine trc_chsed
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    This subroutine predicts the inside terrace soil channel 
!!    erosion and nutrient loss

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name           |units      |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    enratio        |none       |enrichment ratio calculated for day in HRU
!!    erorgn(:)      |none       |organic N enrichment ratio, if left blank
!!                               |the model will calculate for every event
!!    sol_bd(:,:)    |Mg/m**3    |bulk density of the soil
!!    sol_z(:,:)     |mm         |depth to bottom of soil layer
!!    spcon          |none       |linear parameter for calculating sediment
!!                               |reentrained in channel sediment routing
!!    spexp          |none       |exponent parameter for calculating sediment
!!                               |reentrained in channel sediment routing
!!    tbsol_actp(:,:)|kg P/ha    |amount of phosphorus stored in the 
!!                               |active mineral phosphorus pool for bed (frontslope)
!!    tbsol_fon(:,:) |kg N/ha    |amount of nitrogen stored in the fresh 
!!                               |nitrogen pool for bed (frontslope)
!!    tbsol_fop(:,:) |kg P/ha    |amount of phosphorus stored in the 
!!                               |fresh phosphorus pool for bed (frontslope)
!!    tbsol_mn(:,:)  |kg N/ha    |amount of nitrogen stored in the active 
!!                               |organic N pool
!!    tbsol_mp(:,:)  |kg P/ha    |amount of phosphorus stored in the 
!!                               |active organic P pool
!!    tbsol_n(:,:)   |kg N/ha    |amount of nitrogen stored in the stable 
!!                               |organic N pool
!!    tbsol_orgp(:,:)|kg P/ha    |amount of phosphorus stored in organic 
!!                               |for bed (frontslope)
!!    tbsol_solp(:,:)|kg P/ha    |amount of phosohorus stored in solution 
!!                               |for bed (frontslope)
!!    tbsol_stap(:,:)|kg P/ha    |amount of phosphorus stored in the 
!!                               |stable mineral phosphorus pool for bed (frontslope)
!!    trc_chl(:)     |m          |Inside terrace segment channel length
!!    trc_chs(:)     |m/m        |Inside terrace segment channel slope
!!    trc_ha         |ha         |terrace bed area in hectare
!!    trc_no(:)      |none       |numbers of terrace unit in HRU
!!    trc_sed(:)     |metric tons|amount of sediment in terrace
!!    trc_stl(:)     |m^3        |accumulated settled sediment volume in terrace
!!    trc_vol(:)     |m^3 H2O    |volume of water in terrace
!!    trcb_n(:)      |none       |Manning's "n" value for overland flow in bed 
!!                               |area of terrace
!!    trcb_slp(:)    |m/m        |Average slope stepness in bed area of terrace
!!    trcflwo_dr     |m^3 H2O    |volume of water flowing out of terrace 
!!    trcr_slp(:)    |m/m        |Average slope stepness in cutslope area of terrace
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name           |units      |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tbsol_actp(:,:)|kg P/ha    |amount of phosphorus stored in the 
!!                               |active mineral phosphorus pool for bed (frontslope)
!!    tbsol_fon(:,:) |kg N/ha    |amount of nitrogen stored in the fresh 
!!                               |nitrogen pool for bed (frontslope)
!!    tbsol_fop(:,:) |kg P/ha    |amount of phosphorus stored in the 
!!                               |fresh phosphorus pool for bed (frontslope)
!!    tbsol_mn(:,:)  |kg N/ha    |amount of nitrogen stored in the active 
!!                               |organic N pool
!!    tbsol_mp(:,:)  |kg P/ha    |amount of phosphorus stored in the 
!!                               |active organic P pool
!!    tbsol_n(:,:)   |kg N/ha    |amount of nitrogen stored in the stable 
!!                               |organic N pool
!!    tbsol_orgp(:,:)|kg P/ha    |amount of phosphorus stored in organic 
!!                               |for bed (frontslope)
!!    tbsol_solp(:,:)|kg P/ha    |amount of phosohorus stored in solution 
!!                               |for bed (frontslope)
!!    tbsol_stap(:,:)|kg P/ha    |amount of phosphorus stored in the 
!!                               |stable mineral phosphorus pool for bed (frontslope)
!!    trc_stl(:)     |m^3        |accumulated settled sediment volume in terrace
!!    trcorgno_dr    |kg N       |amount of organic nitrogen out of terrace 
!!                               |by drainage
!!    trcorgpo_dr    |kg P       |amount of organic phosphorus out of 
!!                               |terrace by drainage
!!    trcpsedao_dr   |kg P       |amount of sediment phosphorus out of 
!!                               |terrace by drainage
!!    trcpsedso_dr   |kg P       |amount of sediment phosphorus out of 
!!                               |terrace by drainage
!!    trcsedo_dr     |metric tons|amount of sediment out of terrace by 
!!                               |drainage
!!    trcsilo_dr     |metric tons|amount of silt sediment out of terrace 
!!                               |by drainage
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    c           |none          |inverse of channel side slope
!!    deg         |metric tons   |sediment reentrained in water by channel
!!                               |degradation
!!    dep         |metric tons   |sediment deposited on river bottom
!!    p           |m             |wetted perimeter
!!    porgg       |kg P/ha       |total amount of P in organic pools prior to
!!                               |sediment removal
!!    qdin        |m^3 H2O       |water in terrace during time step
!!    rh          |m             |hydraulic radius
!!    tchminpa    |kg P/ha       |amount of active mineral phosphorus sorbed to
!!                               |sediment in terrace channel for day
!!    tchminps    |kg P/ha       |amount of stable mineral phosphorus sorbed to
!!                               |sediment in terrace channel for day
!!    tchorgp     |kg P/ha       |amount of organic phosphorus in terrace
!!                               |channel for the day
!!    tchsedp     |kg P/ha       |total amount of P removed in sediment erosion
!!    tsdti       |m^3/s         |average flow on day in terrace channel
!!    vc          |m/s           |flow velocity in terrace channel
!!    vol         |m^3 H2O       |volume of water by terrace drainage
!!    wt1         |none          |conversion factor (mg/kg => kg/ha)
!!    xx          |kg P/ha       |amount of N or P attached to sediment 
!!                               |in soil
!!    xxa         |kg P/ha       |fraction of active mineral phosphorus in soil
!!    xxo         |kg P/ha       |fraction of organic phosphorus in soil
!!    xxs         |kg P/ha       |fraction of stable mineral phosphorus in soil
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: 
!!    SWAT: 

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none

      integer :: j, idpLocal
      real :: vol, volrt, tsdti, dep, p, rh, vc, cr, cb
      real :: sedin, qdin, tcharea, tbase, cyin, cych
      real :: degnet, deg, xx, er, wt1, xx1, conc
      real :: xxo, xxa, xxs, tchsedp, tchorgp, tchminpa, tchminps
      real :: porgg, qman, c

      j = 0
      j = ihru

      !! Find average flowrate in a day
      vol = 0.
      vol = trcflwo_dr
      if (tfactor > 1.e-6) then
        volrt = vol / 86400 / trc_no(j) / tfactor
      else
        volrt = vol / 86400 / trc_no(j)
      end if

      tsdti = 0.
	dep = 0.
	p = 0.
	rh = 0.
	vc = 0.
      idpLocal = 0
      c = 0.
      cr = 1. / trcr_slp(j)
      cb = 1. / trcb_slp(j)
      sedin = 0.
      qdin = 0.
      sedin = trc_sed(j)
      qdin = trc_vol(j)
      idpLocal = idplt_trcb(tbnro(j),tbicr(j),j)

	!! find the crossectional area and depth for volrt
	!! by iteration method at 1cm interval depth
	!! find the depth until the discharge rate is equal to volrt
	  Do While (tsdti < volrt)
	    dep = dep + 0.01
	    tcharea = 0.5 * dep **2. * (cr + cb)
	    p = dep * (Sqrt(1. + cr * cr) + Sqrt(1. + cb * cb))
	    rh = tcharea / p
          tsdti = Qman(tcharea, rh, trcb_n(j), trc_chs(j))
	  end do
	  tsdti = volrt

      if (tsdti > 0.) then
        !! calculate velocity
  	    vc = tsdti / tcharea  
      end if

      tbase = 0.
      tbase = trc_chl(j) / (3600. * vc)
      if (tbase > 1.) tbase = 1.

        !! JIMMY'S NEW IMPROVED METHOD for sediment transport
        cyin = 0.
        cych = 0.
        degnet = 0.
        deg = 0.
        cyin = sedin / qdin
        cych = bsn_prm%spcon * vc ** bsn_prm%spexp
        degnet = qdin * (cych - cyin)
	    if(abs(degnet) < 1.e-6) degnet = 0.

!!  tbase is multiplied so that erosion is proportional to the traveltime, 
!!  which is directly related to the length of the channel
!!  Otherwise for the same discharge rate and sediment deficit
!!  the model will erode more sediment per unit length of channel 
!!  from a small channel than a larger channel.
        if (degnet > 1.e-6) then
            !!  compute the cover factor on terrace bed segment
            if (idpLocal > 0) then
              c = Exp((-.2231 - cvm_com(idpLocal)) *                             &
     &            Exp(-.00115 * tbsol_cov(j)) + cvm_com(idpLocal)) 
            else
              if (tbsol_cov(j) > 1.e-4) then
                c = Exp(-.2231 * Exp(-.00115 * tbsol_cov(j)))
              else
                c = .8
              end if
	      end if
            deg = degnet * tbase * c * trcb_uslek(j)
        end if

        if (deg > 1.e-6) then
!! add channel erosion to terrace storage and update soil nutrient pool.
        trc_stl(j) = trc_stl(j) - deg / soil(j)%phys(1)%bd        
        if (trc_stl(j) < 1.e-6) trc_stl(j) = 0.
        trc_vmax = trc_volx(j) - trc_stl(j)
        trcsedo_dr = trcsedo_dr + deg
        trcsilo_dr = trcsilo_dr + deg        !!All are assumed to silt type particles

!!  add organic N to terrace storage
        xx = 0.
        er = 0.
        wt1 = 0.
        xx1 = 0.
        conc = 0.
        if (bsn_cc%cswat == 0) then
            xx = tbsol_orgn(1,j) + tbsol_aorgn(1,j) + tbsol_fon(1,j)
        else
            xx = tbsol_n(1,j) + tbsol_fon(1,j) + tbsol_mn(1,j)
        endif
        wt1 = soil(j)%phys(1)%bd * soil(j)%phys(1)%d / 100.
        if (hru(j)%hyd%erorgn > .001) then
          er = hru(j)%hyd%erorgn
        else
          er = enratio
        end if
          conc = 0.
          conc = xx * er / wt1
          trcorgno_dr = trcorgno_dr + 0.001 * deg * conc

	!! update soil nitrogen pools only for terrace bed segment
      if (bsn_cc%cswat == 0) then
          if (xx > 1.e-6) then
           xx1 = (1. - 0.001 * deg * conc / (xx * trc_ha))
           tbsol_aorgn(1,j) = tbsol_aorgn(1,j) * xx1
           tbsol_orgn(1,j) = tbsol_orgn(1,j) * xx1
           tbsol_fon(1,j) = tbsol_fon(1,j) * xx1
          end if
           if (tbsol_aorgn(1,j) < 0.) then
             trcorgno_dr = trcorgno_dr + tbsol_aorgn(1,j) * trc_ha
             tbsol_aorgn(1,j) = 0.
           end if

           if (tbsol_orgn(1,j) < 0.) then
             trcorgno_dr = trcorgno_dr + tbsol_orgn(1,j) * trc_ha
             tbsol_orgn(1,j) = 0.
           end if

           if (tbsol_fon(1,j) < 0.) then
             trcorgno_dr = trcorgno_dr + tbsol_fon(1,j) * trc_ha
             tbsol_fon(1,j) = 0.
           end if
       else
          if (xx > 1.e-6) then
            xx1 = (1. - 0.001 * deg * conc / xx / trc_ha)
		    tbsol_n(1,j) = tbsol_n(1,j) * xx1
		    tbsol_fon(1,j) = tbsol_fon(1,j) * xx1
		    tbsol_mn(1,j) = tbsol_mn(1,j) * xx1
          end if

           if (tbsol_n(1,j) < 0.) then
             trcorgno_dr = trcorgno_dr + tbsol_n(1,j) * trc_ha
             tbsol_n(1,j) = 0.
           end if

           if (tbsol_mn(1,j) < 0.) then
             trcorgno_dr = trcorgno_dr + tbsol_mn(1,j) * trc_ha
             tbsol_mn(1,j) = 0.
           end if

           if (tbsol_fon(1,j) < 0.) then
             trcorgno_dr = trcorgno_dr + tbsol_fon(1,j) * trc_ha
             tbsol_fon(1,j) = 0.
           end if
      end if

!!  add phosphorous to terrace storage
        xx = 0.
        xxo = 0.
        xxa = 0.
        xxs = 0.

        xx = tbsol_orgp(1,j) + tbsol_fop(1,j) + tbsol_mp(1,j) +         &
     &                                tbsol_actp(1,j) + tbsol_stap(1,j)
        if (xx > 1.e-3) then
           xxo = (tbsol_orgp(1,j) + tbsol_fop(1,j)+ tbsol_mp(1,j)) / xx
           xxa = tbsol_actp(1,j) / xx
           xxs = tbsol_stap(1,j) / xx
        end if

          wt1 = 0.
          wt1 = soil(j)%phys(1)%bd * soil(j)%phys(1)%d / 100.

          er = enratio

          conc = 0.
          conc = xx * er / wt1

        tchsedp = 0.
        tchorgp = 0.
        tchminpa = 0.
        tchminps = 0.
        tchsedp = .001 * conc * deg / trc_ha
        tchorgp = tchsedp * xxo
        tchminpa = tchsedp * xxa
        tchminps = tchsedp * xxs

        trcorgpo_dr = trcorgpo_dr + tchorgp * trc_ha
        trcpsedso_dr = trcpsedso_dr + tchminps * trc_ha
        trcpsedao_dr = trcpsedao_dr + tchminpa * trc_ha

!! update soil phosphorous pools only for terrace bed segment
      porgg = 0.
      porgg = tbsol_orgp(1,j) + tbsol_fop(1,j)
      
      if (porgg > 1.e-3) then
      tbsol_orgp(1,j) = tbsol_orgp(1,j) - tchorgp * (tbsol_orgp(1,j)    &
     &                                                          / porgg)
      tbsol_fop(1,j) = tbsol_fop(1,j) - tchorgp * (tbsol_fop(1,j)       &
     &                                                          / porgg)
      tbsol_mp(1,j) = tbsol_mp(1,j) - tchorgp * (tbsol_mp(1,j) / porgg)
      end if

      tbsol_actp(1,j) = tbsol_actp(1,j) - tchminpa
      tbsol_stap(1,j) = tbsol_stap(1,j) - tchminps

        if (tbsol_orgp(1,j) < 0.) then
          trcorgpo_dr = trcorgpo_dr + tbsol_orgp(1,j) * trc_ha
          tbsol_orgp(1,j) = 0.
        end if

        if (tbsol_fop(1,j) < 0.) then
          trcorgpo_dr = trcorgpo_dr + tbsol_fop(1,j) * trc_ha
          tbsol_fop(1,j) = 0.
        end if

        if (tbsol_mp(1,j) < 0.) then
          trcorgpo_dr = trcorgpo_dr + tbsol_mp(1,j) * trc_ha
          tbsol_mp(1,j) = 0.
        end if

        if (tbsol_actp(1,j) < 0.) then
          trcpsedao_dr = trcpsedao_dr + tbsol_actp(1,j) * trc_ha
          tbsol_actp(1,j) = 0.
        end if

        if (tbsol_stap(1,j) < 0.) then
          trcpsedso_dr = trcpsedso_dr + tbsol_stap(1,j) * trc_ha
          tbsol_stap(1,j) = 0.
        end if
        
      end if

      return
      end


      subroutine trc_init


!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine initializes variables related to terrace


!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name           |units      |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    canmx(:)       |mm H2O     |maximum canopy storage
!!    cn2(:)         |none       |SCS runoff curve number for moisture
!!                               |condition II
!!    idplt(:,:,:)   |none       |land cover code from crop.dat
!!    lai_init(:,:,:)|none       |initial leaf area index of transplants
!!    ov_n(:)        |none       |Manning's "n" value for overland flow
!!    pnd_nsed(:)    |mg/kg      |normal sediment concentration in pond water
!!    sol_actp(:,:)  |kg P/ha    |amount of phosphorus stored in the 
!!                               |active mineral phosphorus pool for HRU
!!    sol_cov(:)     |kg/ha      |amount of residue on soil surface
!!    sol_fon(:,:)   |kg N/ha    |amount of nitrogen stored in the fresh 
!!                               |nitrogen pool for HRU
!!    sol_fop(:,:)   |kg P/ha    |amount of phosphorus stored in the 
!!                               |fresh phosphorus pool for HRU
!!    sol_mn(:,:)    |kg N/ha    |amount of nitrogen stored in the active 
!!                               |organic N pool
!!    sol_mp(:,:)    |kg P/ha    |amount of phosphorus stored in the 
!!                               |active organic P pool
!!    sol_n(:,:)     |kg N/ha    |amount of nitrogen stored in the stable 
!!                               |organic N pool
!!    sol_nh3(:,:)   |kg N/ha    |amount of nitrogen stored in the ammonium 
!!                               |pool for HRU
!!    sol_no3(:,:)   |kg N/ha    |amount of nitrogen stored in the nitrate 
!!                               |pool for HRU
!!    sol_orgp(:,:)  |kg P/ha    |amount of phosphorus stored in organic 
!!                               |for HRU
!!    sol_solp(:,:)  |kg P/ha    |amount of phosohorus stored in solution 
!!                               |for HRU
!!    sol_st(:,:)    |mm H2O     |amount of water stored in soil profile 
!!                               |on any given day for HRU
!!    sol_stap(:,:)  |kg P/ha    |amount of phosphorus stored in the 
!!                               |stable mineral phosphorus pool for HRU
!!    sol_sw(:)      |mm H2O     |amount of water stored in soil profile 
!!                               |of HRU
!!    trc_no(:)      |none       |numbers of terrace unit in HRU
!!    usle_k(:)      |none       |USLE equation soil erodibility (K) factor
!!    usle_p(:)      |none       |USLE equation support practice (P) factor
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name           |units      |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    canmx_trcb(:)  |mm H2O     |maximum canopy storage for terrace 
!!                               |bed (frontslope)
!!    canmx_trcr(:)  |mm H2O     |maximum canopy storage for terrace 
!!                               |riser (cutslope)
!!    canmx_trcb(:)  |mm H2O     |maximum canopy storage for terrace 
!!                               |undisturbed area
!!    canstor_trcb(:)|mm H2O     |amount of water held in canopy storage 
!!                               |for terrace bed (frontslope)
!!    canstor_trcr(:)|mm H2O     |amount of water held in canopy storage 
!!                               |for terrace riser (cutslope)
!!    canstor_trcb(:)|mm H2O     |amount of water held in canopy storage 
!!                               |for terrace undisturbed area
!!    idplt_trcb(:)  |none       |land cover code from crop.dat for terrace 
!!                               |bed (frontslope)
!!    idplt_trcr(:)  |none       |land cover code from crop.dat for terrace 
!!                               |riser (cutslope)
!!    idplt_trcb(:)  |none       |land cover code from crop.dat for terrace 
!!                               |undisturbed area
!!    laiday_trc     |m**2/m**2  |leaf area index for terrace segment
!!    laiday_trcb(:) |m**2/m**2  |leaf area index for terrace bed (frontslope)
!!    laiday_trcr(:) |m**2/m**2  |leaf area index for terrace riser (cutslope)
!!    laiday_trcb(:) |m**2/m**2  |leaf area index for terrace undisturbed 
!!                               |area
!!    tbsol_actp(:,:)|kg P/ha    |amount of phosphorus stored in the 
!!                               |active mineral phosphorus pool for bed (frontslope)
!!    tbsol_cov(:)    |kg/ha      |amount of residue on soil surface of 
!!                               |terrace bed (frontslope)
!!    tbsol_fon(:,:) |kg N/ha    |amount of nitrogen stored in the fresh 
!!                               |nitrogen pool for bed (frontslope)
!!    tbsol_fop(:,:) |kg P/ha    |amount of phosphorus stored in the 
!!                               |fresh phosphorus pool for bed (frontslope)
!!    tbsol_mn(:,:)  |kg N/ha    |amount of nitrogen stored in the active 
!!                               |organic N pool
!!    tbsol_mp(:,:)  |kg P/ha    |amount of phosphorus stored in the 
!!                               |active organic P pool
!!    tbsol_n(:,:)   |kg N/ha    |amount of nitrogen stored in the stable 
!!                               |organic N pool
!!    tbsol_nh3(:,:) |kg N/ha    |amount of nitrogen stored in the ammonium 
!!                               |pool for bed (frontslope)
!!    tbsol_no3(:,:) |kg N/ha    |amount of nitrogen stored in the nitrate 
!!                               |pool for bed (frontslope)
!!    tbsol_orgp(:,:)|kg P/ha    |amount of phosphorus stored in organic 
!!                               |for bed (frontslope)
!!    tbsol_solp(:,:)|kg P/ha    |amount of phosohorus stored in solution 
!!                               |for bed (frontslope)
!!    tbsol_st(:,:)  |mm H2O     |amount of water stored in soil profile 
!!                               |on any given day for terrace bed (frontslope)
!!    tbsol_stap(:,:)|kg P/ha    |amount of phosphorus stored in the 
!!                               |stable mineral phosphorus pool for bed (frontslope)
!!    tbsol_sw(:)    |mm H2O     |amount of water stored in soil profile 
!!                               |of terrace bed (frontslope)
!!    tbwtab(:)      |m          |water table depth from soil surface 
!!                               |of terrace bed (frontslope)
!!    trc_cla(:)     |metric tons|amount of clay sediment in terrace
!!    trc_lag(:)     |metric tons|amount of large aggregate sediment 
!!                               |in terrace
!!    trc_nsed(:)    |mg/L       |normal sediment concentration in terrace 
!!                               |water
!!    trc_sag(:)     |metric tons|amount of small aggregate sediment 
!!                               |in terrace
!!    trc_san(:)     |metric tons|amount of sand sediment in terrace
!!    trc_sed(:)     |metric tons|amount of sediment in terrace
!!    trc_sil(:)     |metric tons|amount of silt sediment in terrace
!!    trcb_cn(:)     |none       |terrace bed (frontslope) curve number 
!!                               |for moisture condition II
!!    trcb_n(:)      |none       |Manning's "n" value for bed (frontslope)
!!    trcb_p(:)      |none       |USLE support practice factor terrace 
!!                               |sement x
!!    trcb_slp(:)    |m/m        |terrace bed (frontslope) slope
!!    trcb_uslek(:)  |none       |USLE equation soil erodibility (K) 
!!                               |factor for terrace bed (frontslope)
!!    trcr_cn(:)     |none       |terrace riser (cutslope) curve number 
!!                               |for moisture condition II
!!    trcr_n(:)      |none       |Manning's "n" value for riser (cutslope)
!!    trcr_p(:)      |none       |USLE support practice factor terrace 
!!                               |sement x
!!    trcr_slp(:)    |m/m        |terrace riser (cutslope) slope
!!    trcr_uslek(:)  |none       |USLE equation soil erodibility (K) 
!!                               |factor for terrace riser (cutslope)
!!    trcb_cn(:)     |none       |terrace undisturbed area curve number 
!!                               |for moisture condition II
!!    trcb_n(:)      |none       |Manning's "n" value for undisturbed area
!!    trcb_p(:)      |none       |USLE support practice factor terrace 
!!                               |sement x
!!    trcb_slp(:)    |m/m        |terrace undisturbed area slope
!!    trcb_uslek(:)  |none       |USLE equation soil erodibility (K) 
!!    trsol_actp(:,:)|kg P/ha    |amount of phosphorus stored in the 
!!                               |active mineral phosphorus pool for riser (cutslope)
!!    trsol_cov(:)    |kg/ha      |amount of residue on soil surface of 
!!                               |terrace riser (cutslope)
!!    trsol_fon(:,:) |kg N/ha    |amount of nitrogen stored in the fresh 
!!                               |nitrogen pool for riser (cutslope)
!!    trsol_fop(:,:) |kg P/ha    |amount of phosphorus stored in the 
!!                               |fresh phosphorus pool for riser (cutslope)
!!    trsol_mn(:,:)  |kg N/ha    |amount of nitrogen stored in the active 
!!                               |organic N pool
!!    trsol_mp(:,:)  |kg P/ha    |amount of phosphorus stored in the 
!!                               |active organic P pool
!!    trsol_n(:,:)   |kg N/ha    |amount of nitrogen stored in the stable 
!!                               |organic N pool
!!    trsol_nh3(:,:) |kg N/ha    |amount of nitrogen stored in the ammonium 
!!                               |pool for riser (cutslope)
!!    trsol_no3(:,:) |kg N/ha    |amount of nitrogen stored in the nitrate 
!!                               |pool for riser (cutslope)
!!    trsol_orgp(:,:)|kg P/ha    |amount of phosphorus stored in organic 
!!                               |for riser (cutslope)
!!    trsol_solp(:,:)|kg P/ha    |amount of phosohorus stored in solution 
!!                               |for riser (cutslope)
!!    trsol_st(:,:)  |mm H2O     |amount of water stored in soil profile 
!!                               |on any given day for terrace riser (cutslope)
!!    trsol_stap(:,:)|kg P/ha    |amount of phosphorus stored in the 
!!                               |stable mineral phosphorus pool for riser (cutslope)
!!    trsol_sw(:)    |mm H2O     |amount of water stored in soil profile 
!!                               |of terrace riser (cutslope)
!!    trwtab(:)      |m          |water table depth from soil surface 
!!                               |of terrace riser (cutslope)
!!    tusol_actp(:,:)|kg P/ha    |amount of phosphorus stored in the active
!!                               |mineral phosphorus pool for undisturbed area
!!    tusol_cov(:)    |kg/ha      |amount of residue on soil surface of 
!!                               |terrace undisturbed area
!!    tusol_fon(:,:) |kg N/ha    |amount of nitrogen stored in the fresh 
!!                               |nitrogen pool for undisturbed area
!!    tusol_fop(:,:) |kg P/ha    |amount of phosphorus stored in the 
!!                               |fresh phosphorus pool for undisturbed area
!!    tusol_mn(:,:)  |kg N/ha    |amount of nitrogen stored in the active 
!!                               |organic N pool
!!    tusol_mp(:,:)  |kg P/ha    |amount of phosphorus stored in the 
!!                               |active organic P pool
!!    tusol_n(:,:)   |kg N/ha    |amount of nitrogen stored in the stable 
!!                               |organic N pool
!!    tusol_nh3(:,:) |kg N/ha    |amount of nitrogen stored in the ammonium 
!!                               |pool for undisturbed area
!!    tusol_no3(:,:) |kg N/ha    |amount of nitrogen stored in the nitrate 
!!                               |pool for undisturbed area
!!    tusol_orgp(:,:)|kg P/ha    |amount of phosphorus stored in organic 
!!                               |for undisturbed area
!!    tusol_solp(:,:)|kg P/ha    |amount of phosohorus stored in solution 
!!                               |for undisturbed area
!!    tusol_st(:,:)  |mm H2O     |amount of water stored in soil profile 
!!                               |on any given day for terrace undisturbed area
!!    tusol_stap(:,:)|kg P/ha    |amount of phosphorus stored in the stable
!!                               |mineral phosphorus pool for undisturbed area
!!    tusol_sw(:)    |mm H2O     |amount of water stored in soil profile 
!!                               |of terrace undisturbed area
!!    tuwtab(:)      |m          |water table depth from soil surface 
!!                               |of terrace undisturbed area
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
!!    cl          |none          |fraction of clay in soil material
!!    mnpsz       |none          |mean particle size
!!    sa          |none          |fraction of sand in soil material
!!    si          |none          |fraction of silt in soil material    
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: 
!!    SWAT: curno_trc 
!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      use reservoir_module
      implicit none
      real :: cl, si, sa, mnpsz
      integer :: nly

!     sediment parameter
      if (trc_sed(i) > 1.e-6) then
	  trc_san(i) = trc_sed(i) * soil(i)%det_san    !! Sand yield
	  trc_sil(i) = trc_sed(i) * soil(i)%det_sil    !! Silt yield
	  trc_cla(i) = trc_sed(i) * soil(i)%det_cla    !! Clay yield
	  trc_sag(i) = trc_sed(i) * soil(i)%det_sag    !! Small Aggregate yield
	  trc_lag(i) = trc_sed(i) * soil(i)%det_lag    !! Large Aggregate yield
      else
        trc_san(i) = 0.    !! Sand yield
	  trc_sil(i) = 0.    !! Silt yield
	  trc_cla(i) = 0.    !! Clay yield
	  trc_sag(i) = 0.    !! Small Aggregate yield
	  trc_lag(i) = 0.    !! Large Aggregate yield
      end if

      !if (trc_nsed(i) < 1.e-6) then
      !    trc_nsed(i) = res_sed(res_dat(res_ob(hru(i)%res)%props)%sed)%nsed
      !end if

!     segment parameter
      if (trcb_p(i) < 1.e-6) trcb_p(i) = hru(i)%luse%usle_p
      if (trcb_n(i) < 1.e-6) trcb_n(i) = hru(i)%luse%ovn
      if (tusol_cov(i) < 1.e-6) tusol_cov(i) = sol_cov(i)
      if (trcb_cn(i) < 1.e-6) trcb_cn(i) = cn2(i)
      if (laiday_trcb(i) < 1.e-6) then
        laiday_trcb(i) = lai_init
      end if
      if (canmx_trcb(i) < 1.e-6) canmx_trcb(i) = hru(i)%hyd%canmx
      if (canstor_trcb(i) < 1.e-6) canstor_trcb(i) = 0.
      if (trcb_uslek(i) < 1.e-6) trcb_uslek(i) = soil(i)%usle_k
      if (idplt_trcb(1,1,i) < 1.e-6) then
          do ipl = 1, pcom(i)%npl
            idplt_trcb(1,ipl,i) = pcom(i)%plcur(ipl)%idplt
          end do
      end if
      if (tuwtab(i) < 1.e-6) tuwtab(i) = 0.8

      if (trcr_p(i) < 1.e-6) trcr_p(i) = hru(i)%luse%usle_p
      if (trcr_n(i) < 1.e-6) trcr_n(i) = hru(i)%luse%ovn
      if (trsol_cov(i) < 1.e-6) trsol_cov(i) = sol_cov(i)
      if (trcr_cn(i) < 1.e-6) trcr_cn(i) = cn2(i)
      if (laiday_trcr(i) < 1.e-6) then
        laiday_trcr(i) = lai_init
      end if
      if (canmx_trcr(i) < 1.e-6) canmx_trcr(i) = hru(i)%hyd%canmx
      if (canstor_trcr(i) < 1.e-6) canstor_trcr(i) = 0.
      if (trcr_uslek(i) < 1.e-6) trcr_uslek(i) = soil(i)%usle_k
      if (idplt_trcr(1,1,i) < 1.e-6) then
          do ipl = 1, pcom(i)%npl
            idplt_trcr(1,ipl,i) = pcom(i)%plcur(ipl)%idplt
          end do
      end if
      if (trwtab(i) < 1.e-6) trwtab(i) = 0.8

      if (trcb_p(i) < 1.e-6) trcb_p(i) = hru(i)%luse%usle_p
      if (trcb_n(i) < 1.e-6) trcb_n(i) = hru(i)%luse%ovn
      if (tbsol_cov(i) < 1.e-6) tbsol_cov(i) = sol_cov(i)
      if (trcb_cn(i) < 1.e-6) trcb_cn(i) = cn2(i)
      if (laiday_trcb(i) < 1.e-6) then
        laiday_trcb(i) = lai_init
      end if
      if (canmx_trcb(i) < 1.e-6) canmx_trcb(i) = hru(i)%hyd%canmx
      if (canstor_trcb(i) < 1.e-6) canstor_trcb(i) = 0.
      if (trcb_uslek(i) < 1.e-6) trcb_uslek(i) = soil(i)%usle_k
      if (idplt_trcb(1,1,i) < 1.e-6) then
          do ipl = 1, pcom(i)%npl
            idplt_trcb(1,ipl,i) = pcom(i)%plcur(ipl)%idplt
          end do
      end if
      if (tbwtab(i) < 1.e-6) tbwtab(i) = 0.8

!     soil parameter
      tusol_sw(i) = soil(i)%sw
      trsol_sw(i) = soil(i)%sw
      tbsol_sw(i) = soil(i)%sw
      do nly = 1, soil(i)%nly
          tusol_st(nly,i) = soil(i)%phys(nly)%st
        tusol_solp(nly,i) = soil(i)%nut(nly)%solp
        tusol_orgp(nly,i) = soil(i)%nut(nly)%orgp
        tusol_mp(nly,i) = soil(i)%nut(nly)%mp
        tusol_stap(nly,i) = soil(i)%nut(nly)%stap
        tusol_actp(nly,i) = soil(i)%nut(nly)%actp
        tusol_fop(nly,i) = soil(i)%nut(nly)%fop
        tusol_fon(nly,i) = soil(i)%nut(nly)%fon
        tusol_mn(nly,i) = soil(i)%nut(nly)%mn
        tusol_no3(nly,i) = soil(i)%nut(nly)%no3
        tusol_nh3(nly,i) = soil(i)%nut(nly)%nh3
        tusol_n(nly,i) = soil(i)%ly(nly)%n
        tusol_mc(nly,i) = soil(i)%cbn(nly)%mc
        tusol_rsd(nly,i) = soil(i)%ly(nly)%rsd
        tusol_orgn(nly,i) = soil(i)%nut(nly)%orgn
        tusol_aorgn(nly,i) = soil(i)%nut(nly)%aorgn
        tusol_cbn(nly,i) = soil(i)%cbn(nly)%cbn
        tusol_volcr(nly,i) = soil(i)%ly(nly)%volcr
        
        trsol_st(nly,i) = soil(i)%phys(nly)%st
        trsol_solp(nly,i) = soil(i)%nut(nly)%solp
        trsol_orgp(nly,i) = soil(i)%nut(nly)%orgp
        trsol_mp(nly,i) = soil(i)%nut(nly)%mp
        trsol_stap(nly,i) = soil(i)%nut(nly)%stap
        trsol_actp(nly,i) = soil(i)%nut(nly)%actp
        trsol_fop(nly,i) = soil(i)%nut(nly)%fop
        trsol_fon(nly,i) = soil(i)%nut(nly)%fon
        trsol_mn(nly,i) = soil(i)%nut(nly)%mn
        trsol_no3(nly,i) = soil(i)%nut(nly)%no3
        trsol_nh3(nly,i) = soil(i)%nut(nly)%nh3
        trsol_n(nly,i) = soil(i)%ly(nly)%n
        trsol_mc(nly,i) = soil(i)%cbn(nly)%mc
        trsol_rsd(nly,i) = soil(i)%ly(nly)%rsd
        trsol_orgn(nly,i) = soil(i)%nut(nly)%orgn
        trsol_aorgn(nly,i) = soil(i)%nut(nly)%aorgn
        trsol_cbn(nly,i) = soil(i)%cbn(nly)%cbn
        trsol_volcr(nly,i) = soil(i)%ly(nly)%volcr

        tbsol_st(nly,i) = soil(i)%phys(nly)%st
        tbsol_solp(nly,i) = soil(i)%nut(nly)%solp
        tbsol_orgp(nly,i) = soil(i)%nut(nly)%orgp
        tbsol_mp(nly,i) = soil(i)%nut(nly)%mp
        tbsol_stap(nly,i) = soil(i)%nut(nly)%stap
        tbsol_actp(nly,i) = soil(i)%nut(nly)%actp
        tbsol_fop(nly,i) = soil(i)%nut(nly)%fop
        tbsol_fon(nly,i) = soil(i)%nut(nly)%fon
        tbsol_mn(nly,i) = soil(i)%nut(nly)%mn
        tbsol_no3(nly,i) = soil(i)%nut(nly)%no3
        tbsol_nh3(nly,i) = soil(i)%nut(nly)%nh3
        tbsol_n(nly,i) = soil(i)%ly(nly)%n
        tbsol_mc(nly,i) = soil(i)%cbn(nly)%mc
        tbsol_rsd(nly,i) = soil(i)%ly(nly)%rsd
        tbsol_orgn(nly,i) = soil(i)%nut(nly)%orgn
        tbsol_aorgn(nly,i) = soil(i)%nut(nly)%aorgn
        tbsol_cbn(nly,i) = soil(i)%cbn(nly)%cbn
        tbsol_volcr(nly,i) = soil(i)%ly(nly)%volcr
      end do
      

        


      ! calculate runoff variables in SCS method
      if(trcu_cn(i) > 0.) then
        trc_cn1 = 35.
        trc_cn2 = 0.
        trc_cn3 = 0.
        tsci = 0.
        tsmx = 0.
        twrt(1) = 0.
        twrt(2) = 0.
        call curno_trc(trcu_cn(i),i)
        trcu_cn1(i) = trc_cn1
        trcu_cn2(i) = trc_cn2
        trcu_cn3(i) = trc_cn3
        trcu_sci(i) = tsci
        trcu_smx(i) = tsmx
        trcu_wrt(1,i) = twrt(1)
        trcu_wrt(2,i) = twrt(2)
      end if
      if(trcr_cn(i) > 0.) then
        trc_cn1 = 35.
        trc_cn2 = 0.
        trc_cn3 = 0.
        tsci = 0.
        tsmx = 0.
        twrt(1) = 0.
        twrt(2) = 0.
        call curno_trc(trcr_cn(i),i)
        trcr_cn1(i) = trc_cn1
        trcr_cn2(i) = trc_cn2
        trcr_cn3(i) = trc_cn3
        trcr_sci(i) = tsci
        trcr_smx(i) = tsmx
        trcr_wrt(1,i) = twrt(1)
        trcr_wrt(2,i) = twrt(2)
      end if
      if(trcb_cn(i) > 0.) then
        trc_cn1 = 35.
        trc_cn2 = 0.
        trc_cn3 = 0.
        tsci = 0.
        tsmx = 0.
        twrt(1) = 0.
        twrt(2) = 0.
        call curno_trc(trcb_cn(i),i)
        trcb_cn1(i) = trc_cn1
        trcb_cn2(i) = trc_cn2
        trcb_cn3(i) = trc_cn3
        trcb_sci(i) = tsci
        trcb_smx(i) = tsmx
        trcb_wrt(1,i) = twrt(1)
        trcb_wrt(2,i) = twrt(2)
      end if

      ! calculate the number of terrace unit in HRU
      trc_no(i) = 0.
      trc_no(i) = 100 * hru(i)%km * trc_fr(i) / (trcb_sll(i) +        &
     &          trcr_sll(i) * 2. + trcb_sll(i)) / trc_wth(i)
      trc_no(i) = max(1,ceiling(floor(trc_no(i) * 2.) / 2.))

        !! calculate terrace sediment settling rate
        !if (trc_stlr_co(i) < 1.e-6) trc_stlr_co(i) = res_sed(res_dat(res_ob(hru(i)%res)%props)%sed)%sed_stlr
        cl = 0.
        si = 0.
        sa = 0.
        mnpsz = 0.
        cl = 0.4100 * sol(i)%phys(1)%clay / 100.
        si = 2.7100 * sol(i)%phys(1)%silt / 100.
        sa = 5.7000 * sol(i)%phys(1)%sand / 100.
        mnpsz = Exp(cl + si + sa)
        tsed_stl(i) = Exp(-trc_stlr_co(i) * mnpsz)
      return
      end

      subroutine trc_test
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine is used for testing water and nutrient balance for terraced HRU
!!    the balance output file is trctest.dat

      use parm; use terrace_parm; use basin_module
      use hydrograph_module
      use climate_parms
      use jrw_datalib_module
      implicit none
      integer :: j
      real :: wbal_l, wbal_r, nbal_l, nbal_r, pbal_l, pbal_r
      real :: cnv, t_ha, sumsw, sumsoln, sumsolp, tsumsoln, tsumsolp
      real :: sumwbal, sumnbal, sumpbal, sumpltn, sumpltp, testvalue
      real :: uthruirr
      real :: wl1, wl2, wl3, wr1, wr2, wr3, wr4, wr5, wr6, wr7, wr8, wr9
      real :: nl1, nl2, nl3, nr1, nr2, nr3, nr4, nr5, nr6, nr7, nr8, nr9
      real :: pl1, pl2, pl3, pr1, pr2, pr3, pr4, pr5, pr6, pr7, pr8, pr9

      j = 0
      j = ihru

      cnv = 0.
      t_ha = 0.
      sumsw = 0.
      sumsoln = 0.
      sumsolp = 0.
      tsumsoln = 0.
      tsumsolp = 0.
      sumpltn = 0.
      sumpltp = 0.
      wbal_l = 0.
      wbal_r = 0.
      nbal_l = 0.
      nbal_r = 0.
      pbal_l = 0.
      pbal_r = 0.
      sumwbal = 0.
      sumnbal = 0.
      sumpbal = 0.
      uthruirr = 0.

      cnv = (hru(j)%km*100) * 10.
      t_ha = (hru(j)%km*100) * trc_fr(j)
      
      !! water balance equations
      sumsw = soil(j)%sw * (1. - trc_fr(j)) + (tusol_sw(j) * trcu_fr(j) +  &
     &  trsol_sw(j) * trcr_fr(j) + tbsol_sw(j) * trcb_fr(j)) * trc_fr(j)

      sumwbal = sumsw + (trc_vol(j) + trc_tvol(j)) / cnv
      wbal_l = sumwbal - pre_wbal(j)
      uthruirr = aird(j) * (1. - trc_fr(j))
      if (tdr_flag(j) == 1) then
      wbal_r = wst(ob(hru(j)%obj_no)%wst)%weat%precip - snofall + snomlt - (qday + latq(j) + sepbtm(j) &
     &    + etday + bss(1,j) - bssprev + surf_bs(1,j) - bsprev + tloss) &
     &    + uthruirr
      else
      wbal_r = wst(ob(hru(j)%obj_no)%wst)%weat%precip - snofall + snomlt - (qday + latq(j) + sepbtm(j) &
     &       + etday + trcflwo_dr / cnv + bss(1,j) - bssprev            &
     &       + surf_bs(1,j) - bsprev+ tloss) + uthruirr
      end if      

      !! nitrogen balance equations
      sumsoln = sum(soil(j)%nut(:)%no3) + sum(soil(j)%nut(:)%nh3) +                 &
     &          sum(soil(j)%ly(:)%n) + sum(soil(j)%nut(:)%orgn) +                  &
     &          sum(soil(j)%nut(:)%aorgn) + sum(soil(j)%nut(:)%fon) +               &
     &          sum(soil(j)%nut(:)%mn)
      tsumsoln = (sum(tusol_no3(:,j)) + sum(tusol_nh3(:,j)) +           &
     &          sum(tusol_n(:,j)) + sum(tusol_orgn(:,j)) +              &
     &          sum(tusol_aorgn(:,j)) + sum(tusol_fon(:,j)) +           &
     &          sum(tusol_mn(:,j))) * trcu_fr(j)
      tsumsoln = (sum(trsol_no3(:,j)) + sum(trsol_nh3(:,j)) +           &
     &          sum(trsol_n(:,j)) + sum(trsol_orgn(:,j)) +              &
     &          sum(trsol_aorgn(:,j)) + sum(trsol_fon(:,j)) +           &
     &          sum(trsol_mn(:,j))) * trcr_fr(j) + tsumsoln
      tsumsoln = (sum(tbsol_no3(:,j)) + sum(tbsol_nh3(:,j)) +            &
     &          sum(tbsol_n(:,j)) + sum(tbsol_orgn(:,j)) +               &
     &          sum(tbsol_aorgn(:,j)) + sum(tbsol_fon(:,j)) +            &
     &          sum(tbsol_mn(:,j))) * trcb_fr(j) + tsumsoln

      sumsoln = sumsoln * (1. - trc_fr(j)) + tsumsoln * trc_fr(j)       

      sumpltn = pcom(j)%plm(ipl)%nmass * (1. - trc_fr(j)) + (trcu_pltn(j)*trcu_fr(j)+  &
     & trcr_pltn(j)*trcr_fr(j)+trcb_pltn(j)*trcb_fr(j)) * trc_fr(j)

      sumnbal = sumsoln + sumpltn + (trc_no3(j)+trc_orgn(j))/(hru(j)%km*100)
      nbal_l = sumnbal - pre_nbal(j)
      
      nbal_r = hru_fn + hru_rn + hru_fxn -                                &
     &         (sedorgn(j) + surqno3(j) + latno3(j) + percn(j) +          &
     &          hru_yn + hru_vn + hru_dnt + bss(2,j) - bssnprev +         &
     &          surf_bs(3,j) - bsorgnpre + surf_bs(5,j) - bsno3pre)
           
      !! phosphorous balance equations
      sumsolp = sum(soil(j)%nut(:)%actp) + sum(soil(j)%nut(:)%fop) +                  &
     &          sum(soil(j)%nut(:)%mp) + sum(soil(j)%nut(:)%orgp) +                   &
     &          sum(soil(j)%nut(:)%solp) + sum(soil(j)%nut(:)%stap)
      tsumsolp = (sum(tusol_actp(:,j)) + sum(tusol_fop(:,j)) +            &
     &          sum(tusol_mp(:,j)) + sum(tusol_orgp(:,j)) +               &
     &          sum(tusol_solp(:,j)) + sum(tusol_stap(:,j)))*trcu_fr(j)
      tsumsolp = (sum(trsol_actp(:,j)) + sum(trsol_fop(:,j)) +            &
     &          sum(trsol_mp(:,j)) + sum(trsol_orgp(:,j)) +               &
     &          sum(trsol_solp(:,j)) + sum(trsol_stap(:,j)))*trcr_fr(j)   &
     &               + tsumsolp
      tsumsolp = (sum(tbsol_actp(:,j)) + sum(tbsol_fop(:,j)) +            &
     &          sum(tbsol_mp(:,j)) + sum(tbsol_orgp(:,j)) +               &
     &          sum(tbsol_solp(:,j)) + sum(tbsol_stap(:,j)))*trcb_fr(j)   &
     &               + tsumsolp
      
      sumsolp = sumsolp * (1. - trc_fr(j)) + tsumsolp * trc_fr(j)

      sumpltp = pcom(j)%plm(ipl)%pmass * (1. - trc_fr(j)) + (trcu_pltp(j)*trcu_fr(j)+  &
     & trcr_pltp(j)*trcr_fr(j)+trcb_pltp(j)*trcb_fr(j)) * trc_fr(j)

      sumpbal = sumsolp + sumpltp + (trc_solp(j) + trc_orgp(j) +          &
     &                          trc_pseda(j) + trc_pseds(j)) / (hru(j)%km*100)
      pbal_l = sumpbal - pre_pbal(j)
      
      pbal_r = hru_fp - (surqsolp(j) + sedminpa(j) +                      &
     &          sedminps(j) + sedorgp(j) + hru_yp + surf_bs(4,j) -        &
     &          bsorgppre + surf_bs(6,j) - bssolppre + surf_bs(7,j) -     &
     &          bsminpapre + surf_bs(8,j) - bsminpspre)

      !! store values for next days' balance check
      pre_wbal(j) = sumwbal
      pre_nbal(j) = sumnbal
      pre_pbal(j) = sumpbal

      !! print specific test value in trctest.dat
      testvalue = tusol_cov(j) * trcu_fr(j) +                           &
     &   trsol_cov(j) * trcr_fr(j) + tbsol_cov(j)                       &
     &   * trcb_fr(j)

      !nl1 = hru_fn
      !nl2 = hru_rn
      !nl3 = hru_fxn
      !nr1 = sedorgn(j)
      !nr2 = surqno3(j)
      !nr3 = latno3(j)
      !nr4 = percn(j)
      !nr5 = hru_yn
      !nr6 = bss(2,j) - bssnprev
      !nr7 = surf_bs(3,j) - bsorgnpre
      !nr8 = surf_bs(5,j) - bsno3pre
      !nr9 = hru_dnt

    !  if (time%yrs == 1 .and. iida == idaf) then    !!$$!!
    !    return   !!$$!!
    !  else   !!$$!!
    !    write (66,1000) iida, j, wbal_l, wbal_r, nbal_l,     !!$$!!
    ! &                 nbal_r, pbal_l, pbal_r  !, nl1, nl2, nl3, nr1, nr2, !!$$!!
    !                  !nr3, nr4, nr5, nr6, nr7, nr8, nr9 !!$$!!
    !  end if   !!$$!!

      return
 1000 format (i4,1x,i5,1x,18f10.3)

      end


      subroutine tstr_trc
      
!!     ~ ~ ~ PURPOSE ~ ~ ~
!!     computes terrace segment temperature stress for crop growth - tstrstmp

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    idplt_trc(:,:)|none        |land cover code from crop.dat
!!    ihru        |none          |HRU number
!!    t_base(:)   |deg C         |minimum temperature for plant growth
!!    t_opt(:)    |deg C         |optimal temperature for plant growth
!!    ticr        |none          |sequence number of crop grown within the
!!                               |current year
!!    tmn(:)      |deg C         |minimum temperature for the day in HRU
!!    tmp_an(:)   |deg C         |average annual air temperature
!!    tmpav(:)    |deg C         |average air temperature on current day in HRU
!!    tnro        |none          |sequence number of year in rotation
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    tstrstmp    |none          |fraction of potential plant growth achieved on
!!                               |the day in terrace segment where the reduction 
!!                               |is caused by temperature stress
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    j           |none          |HRU number
!!    rto         |
!!    tgx         |
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Exp

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use climate_parms
      use jrw_datalib_module
      implicit none
      integer :: j
      real :: tgx, rto

      j = 0
      j = ihru

      tgx = 0.
      tgx = tmpav(j) - pldb(idplt_trc(tnro,ticr))%t_base

      if (tgx <= 0.) then
        tstrstmp = 0.
      else
        if (tmpav(j) > pldb(idplt_trc(tnro,ticr))%t_opt) then
         tgx = 2. * pldb(idplt_trc(tnro,ticr))%t_opt -                       &
     &              pldb(idplt_trc(tnro,ticr))%t_base - tmpav(j)
        end if

        rto = 0.
        rto = ((pldb(idplt_trc(tnro,ticr))%t_opt - tmpav(j)) /               &
     &                                              (tgx + 1.e-6)) ** 2

        if (rto <= 200. .and. tgx > 0.) then
          tstrstmp = Exp(-0.1054 * rto)
        else
          tstrstmp = 0.
        end if

        if (tmn(j) <= wgn_pms(iwgen)%tmp_an - 15.) tstrstmp = 0.

      end if

      return
      end

      subroutine varinit_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine initializes variables for the daily simulation of the 
!!    terrace segment of the hydrologic cycle.

      use parm; use terrace_parm; use basin_module
      implicit none
      !!initialize variables
        tnro = 1
        tncut = 1
        ticr = 1
        ticnop = 1
        tntil = 1
        tsol_prk = 0.
        tsol_flat = 0.
        trcbtm = 0. 
        trclatq = 0.
        tpeakr = 0.
        trcq = 0.
        inftrc = 0.
        tcanev = 0.
        tes_day = 0.
        trcsed = 0.
        trcsan = 0.
        trcsil = 0.
        trccla = 0.
        trcsag = 0.
        trclag = 0.
        trcqsolp = 0.
        tsedminpa = 0.
        tsedminps = 0.
        tsedorgp = 0.
        tsedorgn = 0.
        trcqno3 = 0.
        tlatno3 = 0.
        cnday_trc = 0.

      idorm_trc = 0
      trc_phuacc = 0.
      trc_bioday = 0.
      trc_bioms = 0.
      trc_cyrmat = 0
      trc_rwt = 0.
      trc_laimxfr = 0.
      trc_olai = 0.
      thvstiadj = 0.
      trc_pltfrn = 0.
      trc_pltfrp = 0.
      trc_pltn = 0.
      trc_pltp = 0.
      tpplnt = 0.
      tnplnt = 0.
      trc_fixn = 0.
      imgt_trc = 0.
      trc_bioyrms = 0.
      trc_yldanu = 0.
      tsumix = 0.
      trc_pltet = 0.
      trc_pltpet = 0.
      trc_rd = 0.

      trc_tillagef = 0.

      trc_biotarg = 0.
      phup_trc = 0.
      phuho_trc = 0.
      phuk_trc = 0.
      phut_nocr_trc = 0.
      phut_trc = 0.
      trc_inilai = 0.
      trc_inibio = 0.
      trc_cnop = 0.
      trc_biohv = 0.
      trc_hitarg = 0.
      trc_phuplt = 0.
      trc_tnyld = 0.
      trc_frharvk = 0.
      trc_hiovr = 0.
      trc_yldkg = 0.
      tidtill = 0.
      tharveff = 0.
      phuh_trc = 0.
      iplant_trc = 0
      ihv_trc = 0
      ihvo_trc = 0
      ihv_gbm_trc = 0
      ikill_trc = 0
      trc_ncrops = 0
      iop_trc = 0
      trc_tnylda = 0.
      tsol_rd = 0.
            
      tstrsw = 1.
      tstrsa = 1.
      tstrsn = 1.
      tstrsp = 1.
      tstrstmp = 1.

      thmntl = 0.
      trwntl = 0.
      thmptl = 0.
      trmn2tl = 0.
      trmptl = 0.
      twdntl = 0.
      troctl = 0.
      trmp1tl = 0.

      trc_cfertn = 0.
      trc_cfertp = 0.
      trc_auton = 0.
      trc_autop = 0.
      trc_fertn = 0.
      trc_fertp = 0.

      tep_day = 0.
      tuno3d = 0.
      tsci = 0.
      tsmx = 0.
      twrt = 0.

      !! calculate crack flow 2014-12-28 20:46:05
      tsepcrk = 0.
      tcrk = 0.
       return
       end

      subroutine vartran_trc(twave,j)

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine transfers variable values between the
!!    terrace segment variables and the compute variables
!!    twave =0 transfer terrace segment variable value to 
!!    compute variables
!!    twave = 1 transfer terrace compute variable value to 
!!    terrace segment variables

      use parm; use terrace_parm; use basin_module
      use hydrograph_module
      use climate_parms
      use jrw_datalib_module
      implicit none
      integer, intent (in) :: twave
      integer, intent (in) :: j
      real :: aphu, phuop

      if (twave <= 0) then
      !! assign plant related variables
      select case(tflag)
      case(1)
         tnro = tunro(j)
         tnrot = tunrot(j)
         tncut = tuncut(j)
         ticr = tuicr(j)
         ticnop = tuicnop(j)
         tntil = tuntil(j)
         !! initial the local variables
           tsol_st = tusol_st(:,j) 

           tsol_solp = tusol_solp(:,j) 
           tsol_orgp = tusol_orgp(:,j) 
           tsol_mp = tusol_mp(:,j) 
           tsol_actp = tusol_actp(:,j) 
           tsol_stap = tusol_stap(:,j) 
           tsol_fop = tusol_fop(:,j)

           tsol_n = tusol_n(:,j) 
           tsol_fon = tusol_fon(:,j) 
           tsol_mn = tusol_mn(:,j) 
           tsol_no3 = tusol_no3(:,j) 
           tsol_nh3 = tusol_nh3(:,j)

           tsol_rsd = tusol_rsd(:,j)
           tsol_mc = tusol_mc(:,j)
           tsol_aorgn = tusol_aorgn(:,j)
           tsol_orgn = tusol_orgn(:,j)
           tsol_cbn = tusol_cbn(:,j)
           
           tsol_volcr = tusol_volcr(:,j)

           tsol_sw = tusol_sw(j)

           trc_ha = (hru(j)%km*100)*trc_fr(j)*trcu_fr(j) 
           trc_slp = trcu_slp(j) 
           trc_sll = trcu_sll(j) 
           trc_n = trcu_n(j) 
           trc_cn = trcu_cn(j)
           trc_cn1 = trcu_cn1(j)
           trc_cn2 = trcu_cn2(j)
           trc_cn3 = trcu_cn3(j)
           tsci = trcu_sci(j)
           tsmx = trcu_smx(j)
           twrt(1) = trcu_wrt(1,j)
           twrt(2) = trcu_wrt(2,j)

           laiday_trc = laiday_trcu(j)
           canmx_trc = canmx_trcu(j) 
           blai_trc = pldb(idplt_trcu(tnro,ticr,j))%blai 
           canstor_trc = canstor_trcu(j)

           trc_uslek = trcu_uslek(j)
           trc_p = trcu_p(j)
           tsol_cov = tusol_cov(j)
           idplt_trc(:,:) = idplt_trcu(:,:,j)
           igro_trc = igro_trcu(j)
           trc_cht = trcu_cht(j)

           prec_trc = wst(ob(hru(j)%obj_no)%wst)%weat%precip 
           twtab = tuwtab(j)

          trc_aird = trcu_aird(j)
          trc_qird = trcu_qird(j)
          trc_tankfr = trcu_tankfr(j)
          tnair = tunair(j)
          tnirr = tunirr(j)

          tauto_wstr(:,:) = tuauto_wstr(:,:,j)
          tirr_mx(:,:) = tuirr_mx(:,:,j)
          tirr_asq(:,:) = tuirr_asq(:,:,j)
          tirr_sq(:,:) = tuirr_sq(:,:,j)
          tirr_efm(:,:) = tuirr_efm(:,:,j)
          tirr_amt(:,:) = tuirr_amt(:,:,j)
          phuirr_nocrop_trc(:,:) = phuirr_nocrop_trcu(:,:,j)
          phuirr_trc(:,:) = phuirr_trcu(:,:,j)
          tirr_eff(:,:) = tuirr_eff(:,:,j)

          twstrs_id(:,:) = tuwstrs_id(:,:,j)
          trc_iir(:,:) = trcu_iir(:,:,j)
          trfqeo_30d = turfqeo_30d(:,j)


      idorm_trc = idorm_trcu(j)
      trc_phuacc = trcu_phuacc(j)
      trc_bioms = trcu_bioms(j)
      trc_cyrmat = trcu_cyrmat(j)
      trc_rwt = trcu_rwt(j)
      trc_laimxfr = trcu_laimxfr(j)
      trc_olai = trcu_olai(j)
      thvstiadj = tuhvstiadj(j)
      trc_pltfrn = trcu_pltfrn(j)
      trc_pltfrp = trcu_pltfrp(j)
      trc_pltn = trcu_pltn(j)
      trc_pltp = trcu_pltp(j)
      imgt_trc = imgt_trcu(j)
      trc_bioyrms = trcu_bioyrms(j)
      trc_yldanu = trcu_yldanu(j)
      tsumix = tusumix(j)
      trc_pltet = trcu_pltet(j)
      trc_pltpet = trcu_pltpet(j)
      trc_rd = trcu_rd(j)
      trc_laiyrmx = trcu_laiyrmx(j)
      
      trc_tillagef(:) = trcu_tillagef(:,j)

      trc_biotarg(:,:) = trcu_biotarg(:,:,j)
      phup_trc(:,:) = phup_trcu(:,:,j)
      phuho_trc(:,:) = phuho_trcu(:,:,j)
      phuk_trc(:,:) = phuk_trcu(:,:,j)
      phut_nocr_trc(:,:) = phut_nocr_trcu(:,:,j)
      phut_trc(:,:) = phut_trcu(:,:,j)
      trc_inilai(:,:) = trcu_inilai(:,:,j)
      trc_inibio(:,:) = trcu_inibio(:,:,j)
      trc_cnop(:,:) = trcu_cnop(:,:,j)
      trc_biohv(:,:) = trcu_biohv(:,:,j)
      trc_hitarg(:,:) = trcu_hitarg(:,:,j)
      trc_phuplt(:,:) = trcu_phuplt(:,:,j)
      trc_tnyld(:,:) = trcu_tnyld(:,:,j)
      trc_frharvk(:,:) = trcu_frharvk(:,:,j)
      trc_hiovr(:,:) = trcu_hiovr(:,:,j)
      trc_yldkg(:,:) = trcu_yldkg(:,:,j)
      tidtill(:,:) = tuidtill(:,:,j)
      tharveff(:,:) = tuharveff(:,:,j)
      phuh_trc(:,:) = phuh_trcu(:,:,j)
      iplant_trc(:,:) = iplant_trcu(:,:,j)
      ihv_trc(:,:) = ihv_trcu(:,:,j)
      ihvo_trc(:,:) = ihvo_trcu(:,:,j)
      ihv_gbm_trc(:,:) = ihv_gbm_trcu(:,:,j)
      ikill_trc(:,:) = ikill_trcu(:,:,j)
      trc_ncrops(:,:) = trcu_ncrops(:,:,j)
      idplt_trc(:,:) = idplt_trcu(:,:,j)
      iop_trc(:,:) = iop_trcu(:,:,j)
      trc_tnylda(:,:) = trcu_tnylda(:,:,j)

      tcmup_kgh = tucmup_kgh(j)
      tcmtot_kgh = tucmtot_kgh(j)

      trc_tfertn = trcu_tfertn(j)
      trc_tfertp = trcu_tfertp(j)
      trc_tcfrtn = trcu_tcfrtn(j)
      trc_tcfrtp = trcu_tcfrtp(j)
      tauto_napp = tuauto_napp(j)
      trc_anano3 = trcu_anano3(j)
      tauto_nyr = tuauto_nyr(j)
      tafrt_surface = tuafrt_surface(j)
      tauto_nstrs = tuauto_nstrs(j)
      trc_tauton = trcu_tauton(j)
      trc_tautop = trcu_tautop(j)

      tnfert = tunfert(j)
      trc_iafrttyp = trcu_iafrttyp(j)
      tncf = tuncf(j)
      trc_icfrt = trcu_icfrt(j)
      tiday_fert = tuiday_fert(j)
      trc_ndcfrt = trcu_ndcfrt(j)

      tfrt_surface(:,:) = tufrt_surface(:,:,j)
      tfrt_kg(:,:) = tufrt_kg(:,:,j)
      tcfrt_kg(:,:) = tucfrt_kg(:,:,j)
      phucf_trc(:,:) = phucf_trcu(:,:,j)
      phun_nocrop_trc(:,:) = phun_nocrop_trcu(:,:,j)
      phun_trc(:,:) = phun_trcu(:,:,j)

      trc_ifrttyp(:,:) = trcu_ifrttyp(:,:,j)
      tcfrt_id(:,:) = tucfrt_id(:,:,j)
      trc_icfert(:,:) = trcu_icfert(:,:,j)
      tifrt_freq(:,:) = tuifrt_freq(:,:,j)
      trc_ifert(:,:) = trcu_ifert(:,:,j)
      tfert_days(:,:) = tufert_days(:,:,j)

      case(2)
         tnro = trnro(j)
         tnrot = trnrot(j)
         tncut = trncut(j)
         ticr = tricr(j)
         ticnop = tricnop(j)
         tntil = trntil(j)
         !! initial the local variables
           tsol_st = trsol_st(:,j) 
           tsol_solp = trsol_solp(:,j) 
           tsol_orgp = trsol_orgp(:,j) 
           tsol_mp = trsol_mp(:,j) 
           tsol_actp = trsol_actp(:,j) 
           tsol_stap = trsol_stap(:,j) 
           tsol_fop = trsol_fop(:,j)

           tsol_n = trsol_n(:,j) 
           tsol_fon = trsol_fon(:,j) 
           tsol_mn = trsol_mn(:,j) 
           tsol_no3 = trsol_no3(:,j) 
           tsol_nh3 = trsol_nh3(:,j)

           tsol_rsd = trsol_rsd(:,j)
           tsol_mc = trsol_mc(:,j)
           tsol_aorgn = trsol_aorgn(:,j)
           tsol_orgn = trsol_orgn(:,j)
           tsol_cbn = trsol_cbn(:,j)
           
           tsol_volcr = trsol_volcr(:,j)

           tsol_sw = trsol_sw(j)

           trc_ha = (hru(j)%km*100)*trc_fr(j)*trcr_fr(j) 
           trc_slp = trcr_slp(j) 
           trc_sll = trcr_sll(j) 
           trc_n = trcr_n(j) 
           trc_cn = trcr_cn(j)
           trc_cn1 = trcr_cn1(j)
           trc_cn2 = trcr_cn2(j)
           trc_cn3 = trcr_cn3(j)
           tsci = trcr_sci(j)
           tsmx = trcr_smx(j)
           twrt(1) = trcr_wrt(1,j)
           twrt(2) = trcr_wrt(2,j)

           laiday_trc = laiday_trcr(j) 
           canmx_trc = canmx_trcr(j) 
           blai_trc = pldb(idplt_trcr(tnro,ticr,j))%blai 
           canstor_trc = canstor_trcr(j)

           trc_uslek = trcr_uslek(j)
           trc_p = trcr_p(j)
           tsol_cov = trsol_cov(j)
           idplt_trc(:,:)  = idplt_trcr(:,:,j)
           igro_trc = igro_trcr(j)
           trc_cht = trcr_cht(j)

           prec_trc = wst(ob(hru(j)%obj_no)%wst)%weat%precip 
           twtab = trwtab(j)

          trc_aird = trcr_aird(j)
          trc_qird = trcr_qird(j)
          trc_tankfr = trcr_tankfr(j)
          tnair = trnair(j)
          tnirr = trnirr(j)

          tauto_wstr(:,:) = trauto_wstr(:,:,j)
          tirr_mx(:,:) = trirr_mx(:,:,j)
          tirr_asq(:,:) = trirr_asq(:,:,j)
          tirr_sq(:,:) = trirr_sq(:,:,j)
          tirr_efm(:,:) = trirr_efm(:,:,j)
          tirr_amt(:,:) = trirr_amt(:,:,j)
          phuirr_nocrop_trc(:,:) = phuirr_nocrop_trcr(:,:,j)
          phuirr_trc(:,:) = phuirr_trcr(:,:,j)
          tirr_eff(:,:) = trirr_eff(:,:,j)

          twstrs_id(:,:) = trwstrs_id(:,:,j)
          trc_iir(:,:) = trcr_iir(:,:,j)
          trfqeo_30d = trrfqeo_30d(:,j)


      idorm_trc = idorm_trcr(j)
      trc_phuacc = trcr_phuacc(j)
      trc_bioms = trcr_bioms(j)
      trc_cyrmat = trcr_cyrmat(j)
      trc_rwt = trcr_rwt(j)
      trc_laimxfr = trcr_laimxfr(j)
      trc_olai = trcr_olai(j)
      thvstiadj = trhvstiadj(j)
      trc_pltfrn = trcr_pltfrn(j)
      trc_pltfrp = trcr_pltfrp(j)
      trc_pltn = trcr_pltn(j)
      trc_pltp = trcr_pltp(j)
      imgt_trc = imgt_trcr(j)
      trc_bioyrms = trcr_bioyrms(j)
      trc_yldanu = trcr_yldanu(j)
      tsumix = trsumix(j)
      trc_pltet = trcr_pltet(j)
      trc_pltpet = trcr_pltpet(j)
      trc_rd = trcr_rd(j)
      trc_laiyrmx = trcr_laiyrmx(j)

      trc_tillagef(:) = trcr_tillagef(:,j)

      trc_biotarg(:,:) = trcr_biotarg(:,:,j)
      phup_trc(:,:) = phup_trcr(:,:,j)
      phuho_trc(:,:) = phuho_trcr(:,:,j)
      phuk_trc(:,:) = phuk_trcr(:,:,j)
      phut_nocr_trc(:,:) = phut_nocr_trcr(:,:,j)
      phut_trc(:,:) = phut_trcr(:,:,j)
      trc_inilai(:,:) = trcr_inilai(:,:,j)
      trc_inibio(:,:) = trcr_inibio(:,:,j)
      trc_cnop(:,:) = trcr_cnop(:,:,j)
      trc_biohv(:,:) = trcr_biohv(:,:,j)
      trc_hitarg(:,:) = trcr_hitarg(:,:,j)
      trc_phuplt(:,:) = trcr_phuplt(:,:,j)
      trc_tnyld(:,:) = trcr_tnyld(:,:,j)
      trc_frharvk(:,:) = trcr_frharvk(:,:,j)
      trc_hiovr(:,:) = trcr_hiovr(:,:,j)
      trc_yldkg(:,:) = trcr_yldkg(:,:,j)
      tidtill(:,:) = tridtill(:,:,j)
      tharveff(:,:) = trharveff(:,:,j)
      phuh_trc(:,:) = phuh_trcr(:,:,j)
      iplant_trc(:,:) = iplant_trcr(:,:,j)
      ihv_trc(:,:) = ihv_trcr(:,:,j)
      ihvo_trc(:,:) = ihvo_trcr(:,:,j)
      ihv_gbm_trc(:,:) = ihv_gbm_trcr(:,:,j)
      ikill_trc(:,:) = ikill_trcr(:,:,j)
      trc_ncrops(:,:) = trcr_ncrops(:,:,j)
      idplt_trc(:,:) = idplt_trcr(:,:,j)
      iop_trc(:,:) = iop_trcr(:,:,j)
      trc_tnylda(:,:) = trcr_tnylda(:,:,j)

      tcmup_kgh = trcmup_kgh(j)
      tcmtot_kgh = trcmtot_kgh(j)

      trc_tfertn = trcr_tfertn(j)
      trc_tfertp = trcr_tfertp(j)
      trc_tcfrtn = trcr_tcfrtn(j)
      trc_tcfrtp = trcr_tcfrtp(j)
      tauto_napp = trauto_napp(j)
      trc_anano3 = trcr_anano3(j)
      tauto_nyr = trauto_nyr(j)
      tafrt_surface = trafrt_surface(j)
      tauto_nstrs = trauto_nstrs(j)
      trc_tauton = trcr_tauton(j)
      trc_tautop = trcr_tautop(j)

      tnfert = trnfert(j)
      trc_iafrttyp = trcr_iafrttyp(j)
      tncf = trncf(j)
      trc_icfrt = trcr_icfrt(j)
      tiday_fert = triday_fert(j)
      trc_ndcfrt = trcr_ndcfrt(j)

      tfrt_surface(:,:) = trfrt_surface(:,:,j)
      tfrt_kg(:,:) = trfrt_kg(:,:,j)
      tcfrt_kg(:,:) = trcfrt_kg(:,:,j)
      phucf_trc(:,:) = phucf_trcr(:,:,j)
      phun_nocrop_trc(:,:) = phun_nocrop_trcr(:,:,j)
      phun_trc(:,:) = phun_trcr(:,:,j)

      trc_ifrttyp(:,:) = trcr_ifrttyp(:,:,j)
      tcfrt_id(:,:) = trcfrt_id(:,:,j)
      trc_icfert(:,:) = trcr_icfert(:,:,j)
      tifrt_freq(:,:) = trifrt_freq(:,:,j)
      trc_ifert(:,:) = trcr_ifert(:,:,j)
      tfert_days(:,:) = trfert_days(:,:,j)

      case(3)
         tnro = tbnro(j)
         tnrot = tbnrot(j)
         tncut = tbncut(j)
         ticr = tbicr(j)
         ticnop = tbicnop(j)
         tntil = tbntil(j)
         !! initial the local variables
           tsol_st = tbsol_st(:,j) 
           tsol_solp = tbsol_solp(:,j) 
           tsol_orgp = tbsol_orgp(:,j) 
           tsol_mp = tbsol_mp(:,j) 
           tsol_actp = tbsol_actp(:,j) 
           tsol_stap = tbsol_stap(:,j) 
           tsol_fop = tbsol_fop(:,j)

           tsol_n = tbsol_n(:,j) 
           tsol_fon = tbsol_fon(:,j) 
           tsol_mn = tbsol_mn(:,j) 
           tsol_no3 = tbsol_no3(:,j) 
           tsol_nh3 = tbsol_nh3(:,j)

           tsol_rsd = tbsol_rsd(:,j)
           tsol_mc = tbsol_mc(:,j)
           tsol_aorgn = tbsol_aorgn(:,j)
           tsol_orgn = tbsol_orgn(:,j)
           tsol_cbn = tbsol_cbn(:,j)
           
           tsol_volcr = tbsol_volcr(:,j)

           tsol_sw = tbsol_sw(j)

           trc_ha = (hru(j)%km*100) * trc_fr(j) * trcb_fr(j) 
           trc_slp = trcb_slp(j) 
           trc_sll = trcb_sll(j) 
           trc_n = trcb_n(j) 
           trc_cn = trcb_cn(j)
           trc_cn1 = trcb_cn1(j)
           trc_cn2 = trcb_cn2(j)
           trc_cn3 = trcb_cn3(j)
           tsci = trcb_sci(j)
           tsmx = trcb_smx(j)
           twrt(1) = trcb_wrt(1,j)
           twrt(2) = trcb_wrt(2,j)

           laiday_trc = laiday_trcb(j) 
           canmx_trc = canmx_trcb(j) 
           blai_trc = pldb(idplt_trcb(tnro,ticr,j))%blai 
           canstor_trc = canstor_trcb(j)

           trc_uslek = trcb_uslek(j)
           trc_p = trcb_p(j)
           tsol_cov = tbsol_cov(j)
           idplt_trc(:,:)  = idplt_trcb(:,:,j)
           igro_trc = igro_trcb(j)
           trc_cht = trcb_cht(j)

           prec_trc = wst(ob(hru(j)%obj_no)%wst)%weat%precip 
           twtab = tbwtab(j)

          trc_aird = trcb_aird(j)
          trc_qird = trcb_qird(j)
          trc_tankfr = trcb_tankfr(j)
          tnair = tbnair(j)
          tnirr = tbnirr(j)

          tauto_wstr(:,:) = tbauto_wstr(:,:,j)
          tirr_mx(:,:) = tbirr_mx(:,:,j)
          tirr_asq(:,:) = tbirr_asq(:,:,j)
          tirr_sq(:,:) = tbirr_sq(:,:,j)
          tirr_efm(:,:) = tbirr_efm(:,:,j)
          tirr_amt(:,:) = tbirr_amt(:,:,j)
          phuirr_nocrop_trc(:,:) = phuirr_nocrop_trcb(:,:,j)
          phuirr_trc(:,:) = phuirr_trcb(:,:,j)
          tirr_eff(:,:) = tbirr_eff(:,:,j)

          twstrs_id(:,:) = tbwstrs_id(:,:,j)
          trc_iir(:,:) = trcb_iir(:,:,j)
          trfqeo_30d = tbrfqeo_30d(:,j)

      idorm_trc = idorm_trcb(j)
      trc_phuacc = trcb_phuacc(j)
      trc_bioms = trcb_bioms(j)
      trc_cyrmat = trcb_cyrmat(j)
      trc_rwt = trcb_rwt(j)
      trc_laimxfr = trcb_laimxfr(j)
      trc_olai = trcb_olai(j)
      thvstiadj = tbhvstiadj(j)
      trc_pltfrn = trcb_pltfrn(j)
      trc_pltfrp = trcb_pltfrp(j)
      trc_pltn = trcb_pltn(j)
      trc_pltp = trcb_pltp(j)
      imgt_trc = imgt_trcb(j)
      trc_bioyrms = trcb_bioyrms(j)
      trc_yldanu = trcb_yldanu(j)
      tsumix = tbsumix(j)
      trc_pltet = trcb_pltet(j)
      trc_pltpet = trcb_pltpet(j)
      trc_rd = trcb_rd(j)
      trc_laiyrmx = trcb_laiyrmx(j)

      trc_tillagef(:) = trcb_tillagef(:,j)

      trc_biotarg(:,:) = trcb_biotarg(:,:,j)
      phup_trc(:,:) = phup_trcb(:,:,j)
      phuho_trc(:,:) = phuho_trcb(:,:,j)
      phuk_trc(:,:) = phuk_trcb(:,:,j)
      phut_nocr_trc(:,:) = phut_nocr_trcb(:,:,j)
      phut_trc(:,:) = phut_trcb(:,:,j)
      trc_inilai(:,:) = trcb_inilai(:,:,j)
      trc_inibio(:,:) = trcb_inibio(:,:,j)
      trc_cnop(:,:) = trcb_cnop(:,:,j)
      trc_biohv(:,:) = trcb_biohv(:,:,j)
      trc_hitarg(:,:) = trcb_hitarg(:,:,j)
      trc_phuplt(:,:) = trcb_phuplt(:,:,j)
      trc_tnyld(:,:) = trcb_tnyld(:,:,j)
      trc_frharvk(:,:) = trcb_frharvk(:,:,j)
      trc_hiovr(:,:) = trcb_hiovr(:,:,j)
      trc_yldkg(:,:) = trcb_yldkg(:,:,j)
      tidtill(:,:) = tbidtill(:,:,j)
      tharveff(:,:) = tbharveff(:,:,j)
      phuh_trc(:,:) = phuh_trcb(:,:,j)
      iplant_trc(:,:) = iplant_trcb(:,:,j)
      ihv_trc(:,:) = ihv_trcb(:,:,j)
      ihvo_trc(:,:) = ihvo_trcb(:,:,j)
      ihv_gbm_trc(:,:) = ihv_gbm_trcb(:,:,j)
      ikill_trc(:,:) = ikill_trcb(:,:,j)
      trc_ncrops(:,:) = trcb_ncrops(:,:,j)
      idplt_trc(:,:) = idplt_trcb(:,:,j)
      iop_trc(:,:) = iop_trcb(:,:,j)
      trc_tnylda(:,:) = trcb_tnylda(:,:,j)

      tcmup_kgh = tbcmup_kgh(j)
      tcmtot_kgh = tbcmtot_kgh(j)

      trc_tfertn = trcb_tfertn(j)
      trc_tfertp = trcb_tfertp(j)
      trc_tcfrtn = trcb_tcfrtn(j)
      trc_tcfrtp = trcb_tcfrtp(j)
      tauto_napp = tbauto_napp(j)
      trc_anano3 = trcb_anano3(j)
      tauto_nyr = tbauto_nyr(j)
      tafrt_surface = tbafrt_surface(j)
      tauto_nstrs = tbauto_nstrs(j)
      trc_tauton = trcb_tauton(j)
      trc_tautop = trcb_tautop(j)

      tnfert = tbnfert(j)
      trc_iafrttyp = trcb_iafrttyp(j)
      tncf = tbncf(j)
      trc_icfrt = trcb_icfrt(j)
      tiday_fert = tbiday_fert(j)
      trc_ndcfrt = trcb_ndcfrt(j)

      tfrt_surface(:,:) = tbfrt_surface(:,:,j)
      tfrt_kg(:,:) = tbfrt_kg(:,:,j)
      tcfrt_kg(:,:) = tbcfrt_kg(:,:,j)
      phucf_trc(:,:) = phucf_trcb(:,:,j)
      phun_nocrop_trc(:,:) = phun_nocrop_trcb(:,:,j)
      phun_trc(:,:) = phun_trcb(:,:,j)

      trc_ifrttyp(:,:) = trcb_ifrttyp(:,:,j)
      tcfrt_id(:,:) = tbcfrt_id(:,:,j)
      trc_icfert(:,:) = trcb_icfert(:,:,j)
      tifrt_freq(:,:) = tbifrt_freq(:,:,j)
      trc_ifert(:,:) = trcb_ifert(:,:,j)
      tfert_days(:,:) = tbfert_days(:,:,j)

      end select

      else
      !! give the value back
      select case(tflag)
      case(1)
           tusol_st(:,j) = tsol_st 
           tusol_prk(:,j) = tsol_prk 
           tusol_flat(:,j) = tsol_flat

           tusol_solp(:,j) = tsol_solp 
           tusol_orgp(:,j) = tsol_orgp 
           tusol_mp(:,j) = tsol_mp 
           tusol_actp(:,j) = tsol_actp 
           tusol_stap(:,j) = tsol_stap 
           tusol_fop(:,j) = tsol_fop

           tusol_n(:,j) = tsol_n 
           tusol_fon(:,j) = tsol_fon 
           tusol_mn(:,j) = tsol_mn 
           tusol_no3(:,j) = tsol_no3 
           tusol_nh3(:,j) = tsol_nh3

           tusol_rsd(:,j) = tsol_rsd
           tusol_mc(:,j) = tsol_mc
           tusol_aorgn(:,j) = tsol_aorgn
           tusol_orgn(:,j) = tsol_orgn
           tusol_cbn(:,j) = tsol_cbn
           
           tusol_volcr(:,j) = tsol_volcr

           tusol_sw(j) = tsol_sw 
           tuwtab(j) = twtab
           trcu_cn(j) = trc_cn
           tunro(j) = tnro
           tunrot(j) = tnrot
           tuncut(j) = tncut
           tuicr(j) = ticr
           tuicnop(j) = ticnop
           tuntil(j) = tntil

           trcu_cn1(j) = trc_cn1
           trcu_cn2(j) = trc_cn2
           trcu_cn3(j) = trc_cn3
           trcu_sci(j) = tsci
           trcu_smx(j) = tsmx
           trcu_wrt(1,j) = twrt(1)
           trcu_wrt(2,j) = twrt(2)

           laiday_trcu(j) = laiday_trc
           canstor_trcu(j) = canstor_trc

          trcu_aird(j) = trc_aird
          trcu_qird(j) = trc_qird
          tunirr(j) = tnirr
          turfqeo_30d(:,j) = trfqeo_30d

      idorm_trcu(j) = idorm_trc
      trcu_phuacc(j) = trc_phuacc
      trcu_bioday(j) = trc_bioday
      trcu_bioms(j) = trc_bioms
      trcu_cyrmat(j) = trc_cyrmat
      trcu_rwt(j) = trc_rwt
      trcu_laimxfr(j) = trc_laimxfr
      trcu_cht(j) = trc_cht
      trcu_olai(j) = trc_olai
      tuhvstiadj(j) = thvstiadj
      trcu_pltfrn(j) = trc_pltfrn
      trcu_pltfrp(j) = trc_pltfrp
      trcu_pltn(j) = trc_pltn
      trcu_pltp(j) = trc_pltp
      tupplnt(j) = tpplnt
      tunplnt(j) = tnplnt
      trcu_fixn(j) = trc_fixn
      igro_trcu(j) = igro_trc
      imgt_trcu(j) = imgt_trc
      trcu_bioyrms(j) = trc_bioyrms
      trcu_yldanu(j) = trc_yldanu
      tusumix(j) = tsumix
      trcu_pltet(j) = trc_pltet
      trcu_pltpet(j) = trc_pltpet
      trcu_rd(j) = trc_rd
      trcu_laiyrmx(j) = trc_laiyrmx

      trcu_tillagef(:,j) = trc_tillagef(:)

      trcu_biotarg(:,:,j) = trc_biotarg(:,:)
      phup_trcu(:,:,j) = phup_trc(:,:)
      phuho_trcu(:,:,j) = phuho_trc(:,:)
      phuk_trcu(:,:,j) = phuk_trc(:,:)
      phut_nocr_trcu(:,:,j) = phut_nocr_trc(:,:)
      phut_trcu(:,:,j) = phut_trc(:,:)
      trcu_inilai(:,:,j) = trc_inilai(:,:)
      trcu_inibio(:,:,j) = trc_inibio(:,:)
      trcu_cnop(:,:,j) = trc_cnop(:,:)
      trcu_biohv(:,:,j) = trc_biohv(:,:)
      trcu_hitarg(:,:,j) = trc_hitarg(:,:)
      trcu_phuplt(:,:,j) = trc_phuplt(:,:)
      trcu_tnyld(:,:,j) = trc_tnyld(:,:)
      trcu_frharvk(:,:,j) = trc_frharvk(:,:)
      trcu_hiovr(:,:,j) = trc_hiovr(:,:)
      trcu_yldkg(:,:,j) = trc_yldkg(:,:)
      tuidtill(:,:,j) = tidtill(:,:)
      tuharveff(:,:,j) = tharveff(:,:)
      phuh_trcu(:,:,j) = phuh_trc(:,:)
      iplant_trcu(:,:,j) = iplant_trc(:,:)
      ihv_trcu(:,:,j) = ihv_trc(:,:)
      ihvo_trcu(:,:,j) = ihvo_trc(:,:)
      ihv_gbm_trcu(:,:,j) = ihv_gbm_trc(:,:)
      ikill_trcu(:,:,j) = ikill_trc(:,:)
      trcu_ncrops(:,:,j) = trc_ncrops(:,:)
      idplt_trcu(:,:,j) = idplt_trc(:,:)
      iop_trcu(:,:,j) = iop_trc(:,:)
      trcu_tnylda(:,:,j) = trc_tnylda(:,:)

      tustrsw(j) = tstrsw
      tustrsa(j) = tstrsa
      tustrsn(j) = tstrsn
      tustrsp(j) = tstrsp
      tustrstmp(j) = tstrstmp

      tuhmntl(j) = thmntl
      turwntl(j) = trwntl
      tuhmptl(j) = thmptl
      turmn2tl(j) = trmn2tl
      turmptl(j) = trmptl
      tuwdntl(j) = twdntl
      turoctl(j) = troctl
      turmp1tl(j) = trmp1tl
      tucmup_kgh(j) = tcmup_kgh
      tucmtot_kgh(j) = tcmtot_kgh

      trcu_cfertn = trc_cfertn
      trcu_cfertp = trc_cfertp
      trcu_auton = trc_auton
      trcu_autop = trc_autop
      trcu_fertn = trc_fertn
      trcu_fertp = trc_fertp

      trcu_tfertn(j) = trc_tfertn
      trcu_tfertp(j) = trc_tfertp
      trcu_tcfrtn(j) = trc_tcfrtn
      trcu_tcfrtp(j) = trc_tcfrtp
      tuauto_napp(j) = tauto_napp
      trcu_anano3(j) = trc_anano3
      tuauto_nyr(j) = tauto_nyr
      tuafrt_surface(j) = tafrt_surface
      tuauto_nstrs(j) = tauto_nstrs
      trcu_tauton(j) = trc_tauton
      trcu_tautop(j) = trc_tautop

      tunfert(j) = tnfert
      trcu_iafrttyp(j) = trc_iafrttyp
      tuncf(j) = tncf
      trcu_icfrt(j) = trc_icfrt
      tuiday_fert(j) = tiday_fert
      trcu_ndcfrt(j) = trc_ndcfrt

      tusol_cov(j) = tsol_cov

      tufrt_surface(:,:,j) = tfrt_surface(:,:)
      tufrt_kg(:,:,j) = tfrt_kg(:,:)
      tucfrt_kg(:,:,j) = tcfrt_kg(:,:)
      phucf_trcu(:,:,j) = phucf_trc(:,:)
      phun_nocrop_trcu(:,:,j) = phun_nocrop_trc(:,:)
      phun_trcu(:,:,j) = phun_trc(:,:)

      trcu_ifrttyp(:,:,j) = trc_ifrttyp(:,:)
      tucfrt_id(:,:,j) = tcfrt_id(:,:)
      trcu_icfert(:,:,j) = trc_icfert(:,:)
      tuifrt_freq(:,:,j) = tifrt_freq(:,:)
      trcu_ifert(:,:,j) = trc_ifert(:,:)
      tufert_days(:,:,j) = tfert_days(:,:)

      trcu_cnday = cnday_trc
      trcu_esday = tes_day
      trcu_epday = tep_day

      case(2)
           trsol_st(:,j) = tsol_st 
           trsol_prk(:,j) = tsol_prk 
           trsol_flat(:,j) = tsol_flat

           trsol_solp(:,j) = tsol_solp 
           trsol_orgp(:,j) = tsol_orgp 
           trsol_mp(:,j) = tsol_mp 
           trsol_actp(:,j) = tsol_actp 
           trsol_stap(:,j) = tsol_stap 
           trsol_fop(:,j) = tsol_fop

           trsol_n(:,j) = tsol_n 
           trsol_fon(:,j) = tsol_fon 
           trsol_mn(:,j) = tsol_mn 
           trsol_no3(:,j) = tsol_no3 
           trsol_nh3(:,j) = tsol_nh3

           trsol_rsd(:,j) = tsol_rsd
           trsol_mc(:,j) = tsol_mc
           trsol_aorgn(:,j) = tsol_aorgn
           trsol_orgn(:,j) = tsol_orgn
           trsol_cbn(:,j) = tsol_cbn
           
           trsol_volcr(:,j) = tsol_volcr

           trsol_sw(j) = tsol_sw 
           trwtab(j) = twtab
           trcr_cn(j) = trc_cn
           trnro(j) = tnro
           tunrot(j) = tnrot
           trncut(j) = tncut
           tricr(j) = ticr
           tricnop(j) = ticnop
           trntil(j) = tntil

           trcr_cn1(j) = trc_cn1
           trcr_cn2(j) = trc_cn2
           trcr_cn3(j) = trc_cn3
           trcr_sci(j) = tsci
           trcr_smx(j) = tsmx
           trcr_wrt(1,j) = twrt(1)
           trcr_wrt(2,j) = twrt(2)

           laiday_trcr(j) = laiday_trc
           canstor_trcr(j) = canstor_trc

          trcr_aird(j) = trc_aird
          trcr_qird(j) = trc_qird
          trnirr(j) = tnirr
          turfqeo_30d(:,j) = trfqeo_30d

      idorm_trcr(j) = idorm_trc
      trcr_phuacc(j) = trc_phuacc
      trcr_bioday(j) = trc_bioday
      trcr_bioms(j) = trc_bioms
      trcr_cyrmat(j) = trc_cyrmat
      trcr_rwt(j) = trc_rwt
      trcr_laimxfr(j) = trc_laimxfr
      trcr_cht(j) = trc_cht
      trcr_olai(j) = trc_olai
      trhvstiadj(j) = thvstiadj
      trcr_pltfrn(j) = trc_pltfrn
      trcr_pltfrp(j) = trc_pltfrp
      trcr_pltn(j) = trc_pltn
      trcr_pltp(j) = trc_pltp
      trpplnt(j) = tpplnt
      trnplnt(j) = tnplnt
      trcr_fixn(j) = trc_fixn
      igro_trcr(j) = igro_trc
      imgt_trcr(j) = imgt_trc
      trcr_bioyrms(j) = trc_bioyrms
      trcr_yldanu(j) = trc_yldanu
      trsumix(j) = tsumix
      trcr_pltet(j) = trc_pltet
      trcr_pltpet(j) = trc_pltpet
      trcr_rd(j) = trc_rd
      trcr_laiyrmx(j) = trc_laiyrmx

      trcr_tillagef(:,j) = trc_tillagef(:)

      trcr_biotarg(:,:,j) = trc_biotarg(:,:)
      phup_trcr(:,:,j) = phup_trc(:,:)
      phuho_trcr(:,:,j) = phuho_trc(:,:)
      phuk_trcr(:,:,j) = phuk_trc(:,:)
      phut_nocr_trcr(:,:,j) = phut_nocr_trc(:,:)
      phut_trcr(:,:,j) = phut_trc(:,:)
      trcr_inilai(:,:,j) = trc_inilai(:,:)
      trcr_inibio(:,:,j) = trc_inibio(:,:)
      trcr_cnop(:,:,j) = trc_cnop(:,:)
      trcr_biohv(:,:,j) = trc_biohv(:,:)
      trcr_hitarg(:,:,j) = trc_hitarg(:,:)
      trcr_phuplt(:,:,j) = trc_phuplt(:,:)
      trcr_tnyld(:,:,j) = trc_tnyld(:,:)
      trcr_frharvk(:,:,j) = trc_frharvk(:,:)
      trcr_hiovr(:,:,j) = trc_hiovr(:,:)
      trcr_yldkg(:,:,j) = trc_yldkg(:,:)
      tridtill(:,:,j) = tidtill(:,:)
      trharveff(:,:,j) = tharveff(:,:)
      phuh_trcr(:,:,j) = phuh_trc(:,:)
      iplant_trcr(:,:,j) = iplant_trc(:,:)
      ihv_trcr(:,:,j) = ihv_trc(:,:)
      ihvo_trcr(:,:,j) = ihvo_trc(:,:)
      ihv_gbm_trcr(:,:,j) = ihv_gbm_trc(:,:)
      ikill_trcr(:,:,j) = ikill_trc(:,:)
      trcr_ncrops(:,:,j) = trc_ncrops(:,:)
      idplt_trcr(:,:,j) = idplt_trc(:,:)
      iop_trcr(:,:,j) = iop_trc(:,:)
      trcr_tnylda(:,:,j) = trc_tnylda(:,:)
      
      trstrsw(j) = tstrsw
      trstrsa(j) = tstrsa
      trstrsn(j) = tstrsn
      trstrsp(j) = tstrsp
      trstrstmp(j) = tstrstmp

      trhmntl(j) = thmntl
      trrwntl(j) = trwntl
      trhmptl(j) = thmptl
      trrmn2tl(j) = trmn2tl
      trrmptl(j) = trmptl
      trwdntl(j) = twdntl
      trroctl(j) = troctl
      trrmp1tl(j) = trmp1tl
      trcmup_kgh(j) = tcmup_kgh
      trcmtot_kgh(j) = tcmtot_kgh

      trcr_cfertn = trc_cfertn
      trcr_cfertp = trc_cfertp
      trcr_auton = trc_auton
      trcr_autop = trc_autop
      trcr_fertn = trc_fertn
      trcr_fertp = trc_fertp

      trcr_tfertn(j) = trc_tfertn
      trcr_tfertp(j) = trc_tfertp
      trcr_tcfrtn(j) = trc_tcfrtn
      trcr_tcfrtp(j) = trc_tcfrtp
      trauto_napp(j) = tauto_napp
      trcr_anano3(j) = trc_anano3
      trauto_nyr(j) = tauto_nyr
      trafrt_surface(j) = tafrt_surface
      trauto_nstrs(j) = tauto_nstrs
      trcr_tauton(j) = trc_tauton
      trcr_tautop(j) = trc_tautop

      trnfert(j) = tnfert
      trcr_iafrttyp(j) = trc_iafrttyp
      trncf(j) = tncf
      trcr_icfrt(j) = trc_icfrt
      triday_fert(j) = tiday_fert
      trcr_ndcfrt(j) = trc_ndcfrt
      
      trsol_cov(j) = tsol_cov

      trfrt_surface(:,:,j) = tfrt_surface(:,:)
      trfrt_kg(:,:,j) = tfrt_kg(:,:)
      trcfrt_kg(:,:,j) = tcfrt_kg(:,:)
      phucf_trcr(:,:,j) = phucf_trc(:,:)
      phun_nocrop_trcr(:,:,j) = phun_nocrop_trc(:,:)
      phun_trcr(:,:,j) = phun_trc(:,:)

      trcr_ifrttyp(:,:,j) = trc_ifrttyp(:,:)
      trcfrt_id(:,:,j) = tcfrt_id(:,:)
      trcr_icfert(:,:,j) = trc_icfert(:,:)
      trifrt_freq(:,:,j) = tifrt_freq(:,:)
      trcr_ifert(:,:,j) = trc_ifert(:,:)
      trfert_days(:,:,j) = tfert_days(:,:)

      trcr_cnday = cnday_trc
      trcr_esday = tes_day
      trcr_epday = tep_day

      case(3)
           tbsol_st(:,j) = tsol_st 
           tbsol_prk(:,j) = tsol_prk 
           tbsol_flat(:,j) = tsol_flat

           tbsol_solp(:,j) = tsol_solp 
           tbsol_orgp(:,j) = tsol_orgp 
           tbsol_mp(:,j) = tsol_mp 
           tbsol_actp(:,j) = tsol_actp 
           tbsol_stap(:,j) = tsol_stap 
           tbsol_fop(:,j) = tsol_fop

           tbsol_n(:,j) = tsol_n 
           tbsol_fon(:,j) = tsol_fon 
           tbsol_mn(:,j) = tsol_mn 
           tbsol_no3(:,j) = tsol_no3 
           tbsol_nh3(:,j) = tsol_nh3

           tbsol_rsd(:,j) = tsol_rsd
           tbsol_mc(:,j) = tsol_mc
           tbsol_aorgn(:,j) = tsol_aorgn
           tbsol_orgn(:,j) = tsol_orgn
           tbsol_cbn(:,j) = tsol_cbn
           
           tbsol_volcr(:,j) = tsol_volcr

           tbsol_sw(j) = tsol_sw 
           tbwtab(j) = twtab
           trcb_cn(j) = trc_cn
           tbnro(j) = tnro
           tunrot(j) = tnrot
           tbncut(j) = tncut
           tbicr(j) = ticr
           tbicnop(j) = ticnop
           tbntil(j) = tntil

           trcb_cn1(j) = trc_cn1
           trcb_cn2(j) = trc_cn2
           trcb_cn3(j) = trc_cn3
           trcb_sci(j) = tsci
           trcb_smx(j) = tsmx
           trcb_wrt(1,j) = twrt(1)
           trcb_wrt(2,j) = twrt(2)

           laiday_trcb(j) = laiday_trc
           canstor_trcb(j) = canstor_trc

          trcb_aird(j) = trc_aird
          trcb_qird(j) = trc_qird
          tbnirr(j) = tnirr
          turfqeo_30d(:,j) = trfqeo_30d

      idorm_trcb(j) = idorm_trc
      trcb_phuacc(j) = trc_phuacc
      trcb_bioday(j) = trc_bioday
      trcb_bioms(j) = trc_bioms
      trcb_cyrmat(j) = trc_cyrmat
      trcb_rwt(j) = trc_rwt
      trcb_laimxfr(j) = trc_laimxfr
      trcb_cht(j) = trc_cht
      trcb_olai(j) = trc_olai
      tbhvstiadj(j) = thvstiadj
      trcb_pltfrn(j) = trc_pltfrn
      trcb_pltfrp(j) = trc_pltfrp
      trcb_pltn(j) = trc_pltn
      trcb_pltp(j) = trc_pltp
      tbpplnt(j) = tpplnt
      tbnplnt(j) = tnplnt
      trcb_fixn(j) = trc_fixn
      igro_trcb(j) = igro_trc
      imgt_trcb(j) = imgt_trc
      trcb_bioyrms(j) = trc_bioyrms
      trcb_yldanu(j) = trc_yldanu
      tbsumix(j) = tsumix
      trcb_pltet(j) = trc_pltet
      trcb_pltpet(j) = trc_pltpet
      trcb_rd(j) = trc_rd
      trcb_laiyrmx(j) = trc_laiyrmx

      trcb_tillagef(:,j) = trc_tillagef(:)

      trcb_biotarg(:,:,j) = trc_biotarg(:,:)
      phup_trcb(:,:,j) = phup_trc(:,:)
      phuho_trcb(:,:,j) = phuho_trc(:,:)
      phuk_trcb(:,:,j) = phuk_trc(:,:)
      phut_nocr_trcb(:,:,j) = phut_nocr_trc(:,:)
      phut_trcb(:,:,j) = phut_trc(:,:)
      trcb_inilai(:,:,j) = trc_inilai(:,:)
      trcb_inibio(:,:,j) = trc_inibio(:,:)
      trcb_cnop(:,:,j) = trc_cnop(:,:)
      trcb_biohv(:,:,j) = trc_biohv(:,:)
      trcb_hitarg(:,:,j) = trc_hitarg(:,:)
      trcb_phuplt(:,:,j) = trc_phuplt(:,:)
      trcb_tnyld(:,:,j) = trc_tnyld(:,:)
      trcb_frharvk(:,:,j) = trc_frharvk(:,:)
      trcb_hiovr(:,:,j) = trc_hiovr(:,:)
      trcb_yldkg(:,:,j) = trc_yldkg(:,:)
      tbidtill(:,:,j) = tidtill(:,:)
      tbharveff(:,:,j) = tharveff(:,:)
      phuh_trcb(:,:,j) = phuh_trc(:,:)
      iplant_trcb(:,:,j) = iplant_trc(:,:)
      ihv_trcb(:,:,j) = ihv_trc(:,:)
      ihvo_trcb(:,:,j) = ihvo_trc(:,:)
      ihv_gbm_trcb(:,:,j) = ihv_gbm_trc(:,:)
      ikill_trcb(:,:,j) = ikill_trc(:,:)
      trcb_ncrops(:,:,j) = trc_ncrops(:,:)
      idplt_trcb(:,:,j) = idplt_trc(:,:)
      iop_trcb(:,:,j) = iop_trc(:,:)
      trcb_tnylda(:,:,j) = trc_tnylda(:,:)
      
      tbstrsw(j) = tstrsw
      tbstrsa(j) = tstrsa
      tbstrsn(j) = tstrsn
      tbstrsp(j) = tstrsp
      tbstrstmp(j) = tstrstmp

      tbhmntl(j) = thmntl
      tbrwntl(j) = trwntl
      tbhmptl(j) = thmptl
      tbrmn2tl(j) = trmn2tl
      tbrmptl(j) = trmptl
      tbwdntl(j) = twdntl
      tbroctl(j) = troctl
      tbrmp1tl(j) = trmp1tl
      tbcmup_kgh(j) = tcmup_kgh
      tbcmtot_kgh(j) = tcmtot_kgh

      trcb_cfertn = trc_cfertn
      trcb_cfertp = trc_cfertp
      trcb_auton = trc_auton
      trcb_autop = trc_autop
      trcb_fertn = trc_fertn
      trcb_fertp = trc_fertp

      trcb_tfertn(j) = trc_tfertn
      trcb_tfertp(j) = trc_tfertp
      trcb_tcfrtn(j) = trc_tcfrtn
      trcb_tcfrtp(j) = trc_tcfrtp
      tbauto_napp(j) = tauto_napp
      trcb_anano3(j) = trc_anano3
      tbauto_nyr(j) = tauto_nyr
      tbafrt_surface(j) = tafrt_surface
      tbauto_nstrs(j) = tauto_nstrs
      trcb_tauton(j) = trc_tauton
      trcb_tautop(j) = trc_tautop

      tbnfert(j) = tnfert
      trcb_iafrttyp(j) = trc_iafrttyp
      tbncf(j) = tncf
      trcb_icfrt(j) = trc_icfrt
      tbiday_fert(j) = tiday_fert
      trcb_ndcfrt(j) = trc_ndcfrt
      
      tbsol_cov(j) = tsol_cov

      tbfrt_surface(:,:,j) = tfrt_surface(:,:)
      tbfrt_kg(:,:,j) = tfrt_kg(:,:)
      tbcfrt_kg(:,:,j) = tcfrt_kg(:,:)
      phucf_trcb(:,:,j) = phucf_trc(:,:)
      phun_nocrop_trcb(:,:,j) = phun_nocrop_trc(:,:)
      phun_trcb(:,:,j) = phun_trc(:,:)

      trcb_ifrttyp(:,:,j) = trc_ifrttyp(:,:)
      tbcfrt_id(:,:,j) = tcfrt_id(:,:)
      trcb_icfert(:,:,j) = trc_icfert(:,:)
      tbifrt_freq(:,:,j) = tifrt_freq(:,:)
      trcb_ifert(:,:,j) = trc_ifert(:,:)
      tbfert_days(:,:,j) = tfert_days(:,:)

      trcb_cnday = cnday_trc
      trcb_esday = tes_day
      trcb_epday = tep_day

      end select
      end if

      return
      end

      subroutine wattable_trc
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine predictes the soil water table depth on terrace segment.

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    pet_day     |mm H2O        |potential evapotranspiration on current day
!!    prec_trc    |mm H2O        |amount of water reaching soil surface in 
!!                               |terrace segment
!!    trcq        |mm H2O        |surface runoff generated on day in 
!!                               |terrace segment
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    twtab       |m             |water table depth from soil surface 
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    i30         |none          |30 days counter
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Max
!!    SWAT: 

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      implicit none

      integer :: i30
      real :: twtab_mn, twtab_mx, trfqeo_sum, teo_sum
      real :: w1, w2, wtl

           twtab_mn = 0. 
           twtab_mx = 2.5

              !! compute 30 day sums
              trfqeo_30d(nd_30) = prec_trc - trcq - pet_day 
              teo_30d(nd_30) = pet_day 
              trfqeo_sum = 0. 
              teo_sum = 0.

              do i30 = 1, 30
              trfqeo_sum = trfqeo_sum + trfqeo_30d(i30) 
              teo_sum = teo_sum + teo_30d(i30)
              end do

              if (teo_sum > 1.e-4) then
              w2 = trfqeo_sum / teo_sum
              else
              w2 = 0.
              end if

              !! compute water table
              w1 = amin1 (0.1,abs(w2))

              if (w2 > 0.) then
              wtl = twtab_mn
              else
              wtl = twtab_mx
              end if

              if (twtab < 1.e-6)  twtab = 0.0
              twtab = twtab - w1 * (twtab - wtl)      

      return
      end

      subroutine ysed_trc
     
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine predicts daily soil loss caused by water erosion
!!    using the modified universal soil loss equation

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    cvm(:)      |none          |natural log of USLE_C (the minimum value
!!                               |of the USLE C factor for the land cover)
!!    ihru        |none          |HRU number
!!    sno_hru(:)  |mm H2O        |amount of water in snow in HRU on current day
!!    tpeakr      |m^3/s         |peak runoff rate on terrace segment
!!    trc_ha      |ha            |terrace segment area in hectare
!!    trc_no(:)   |none          |numbers of terrace unit in HRU
!!    trcq        |mm H2O        |surface runoff of terrace segment
!!    tsol_cov    |kg/ha         |amount of residue on soil surface
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    trccla      |metric tons   |amount of clay sediment in terrace segment
!!    trclag      |metric tons   |amount of large aggregate sediment in terrace 
!!                               |segment
!!    trcsag      |metric tons   |amount of small aggregate sediment in terrace 
!!                               |segment
!!    trcsan      |metric tons   |amount of sand sediment in terrace segment
!!    trcsed      |metric tons   |daily soil loss caused by water erosion
!!    trcsil      |metric tons   |amount of silt sediment in terrace segment
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    c           |
!!    j           |none          |HRU number

!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Exp

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use parm; use terrace_parm; use basin_module
      use jrw_datalib_module
      implicit none
      integer :: j
      real :: c, xm, sin_sl, trc_ls, trc_mult, trc_cklsp

      j = 0
      j = ihru
      !if (iida == 137 .and. iyr == 2001) pause
      !! initialize variables
      c = 0.
      trc_cklsp = 0.
      trc_mult = 0.
      xm = 0.
      sin_sl = 0.

      xm = 0.6 * (1. - Exp(-35.835 * trc_slp))    
      sin_sl = Sin(Atan(trc_slp))
      trc_ls = (trc_sll / 22.128) ** xm * (65.41 * sin_sl * sin_sl      &
     &       + 4.56 * sin_sl + .065)
      trc_mult = soil(ihru)%phys(1)%rock * trc_uslek * trc_p * trc_ls * 11.8
        if (idplt_trc(tnro,ticr) > 0) then
          c = Exp((-.2231 - plcp(idplt_trc(tnro,ticr))%cvm) *                &
     &        Exp(-.00115 * tsol_cov) + plcp(idplt_trc(tnro,ticr))%cvm) 
        else
          if (tsol_cov > 1.e-4) then
            c = Exp(-.2231 * Exp(-.00115 * tsol_cov))
          else
            c = .8
          end if
	  end if
     
        trc_cklsp = c * trc_mult

       trcsed = (trcq * tpeakr * 10. * trc_ha / trc_no(j)) ** .56       &
     &                                                      * trc_cklsp
       trcsed = trcsed * trc_no(j)

      if (trcsed < 0.) trcsed = 0.

      !!adjust sediment yield for protection of snow cover
      if (sno_hru(j) > 0.) then
        if (sno_hru(j) > 100.) then
        trcsed = 0.
        else
        trcsed = trcsed / Exp(sno_hru(j) * 3. / 25.4)
        end if
      end if

	!!Particle size distribution of sediment yield
	  trcsan = trcsed * soil(j)%det_san    !! Sand yield
	  trcsil = trcsed * soil(j)%det_sil    !! Silt yield
	  trccla = trcsed * soil(j)%det_cla    !! Clay yield
	  trcsag = trcsed * soil(j)%det_sag    !! Small Aggregate yield
	  trclag = trcsed * soil(j)%det_lag    !! Large Aggregate yield

      return
      end

subroutine zero2_trc
    use terrace_parm
    use parm
         !! zero all var for terrace simulation
        !! reals
        trc_ha = 0.
        trc_slp = 0.
        trc_sll = 0.
        trc_n = 0.
        trc_cn = 0.
        trc_cn1 = 35.
        trc_cn2 = 0.
        trc_cn3 = 0.
        laiday_trc = 0.
        canmx_trc = 0.
        blai_trc = 0.
        canstor_trc = 0.
        trc_uslek = 0.
        trc_p = 0.
        tsol_cov = 0.
        idplt_trc = 0.
        trc_vmax = 0.
        trcq = 0.
        trcflwi = 0.
        tcanev = 0.
        ovflwi = 0.
        inftrc = 0.
        tes_day = 0.
        trc_sumsep = 0.
        tpeakr = 0.
        prec_trc = 0.
        twtab = 0.
        trcbtm = 0.
        trclatq = 0.
        trcsed = 0.
        trcsan = 0.
        trcsil = 0.
        trccla = 0.
        trcsag = 0.
        trclag = 0.
        trcqsolp = 0.
        tsedminpa = 0.
        tsedminps = 0.
        tsedorgp = 0.
        tsedorgn = 0.
        trcqno3 = 0.
        tlatno3 = 0.
        tsol_sw = 0.
        trcflwo_ov = 0.
        trcno3o_ov = 0.
        trcorgno_ov = 0.
        trcsolpo_ov = 0.
        trcpsedao_ov = 0.
        trcpsedso_ov = 0.
        trcorgpo_ov = 0.
        trcsedo_ov = 0.
        trcsano_ov = 0.
        trcsilo_ov = 0.
        trcclao_ov = 0.
        trcsago_ov = 0.
        trclago_ov = 0.
        trcno3o_dr = 0.
        trcorgno_dr = 0.
        trcsolpo_dr = 0.
        trcpsedao_dr = 0.
        trcpsedso_dr = 0.
        trcorgpo_dr = 0.
        trcsedo_dr = 0.
        trcsano_dr = 0.
        trcsilo_dr = 0.
        trcclao_dr = 0.
        trcsago_dr = 0.
        trclago_dr = 0.
        trcflwo_dr = 0.
        tep_max = 0.
        tep_day = 0.
        tsol_rd = 0.
        tfactor = 1.

        !! arrays
            !! dimension mhru
            trc_fr = 0.
            trc_sax = 0.
            trc_volx = 0.
            trc_htx = 0.
            trci_fr = 0.
            trc_vol = 0.
            trc_sed = 0.
            trc_san = 0.
            trc_sil = 0.
            trc_cla = 0.
            trc_sag = 0.
            trc_lag = 0.
            trc_pseds = 0.
            trc_pseda = 0.
            trc_orgp = 0.
            trc_orgn = 0.
            trc_no3 = 0.
            trc_no3s = 0.
            trc_nsed = 0.
            trc_stl = 0.
            trcsa_stl = 0.
            trcht_stl = 0.
            tsed_stl = 0.
            trc_stlr_co = 0.
            trc_solp = 0.
            trc_chl = 0.
            trc_chs = 0.
            trc_chtype = 0
            trc_wth = 0.
            trcu_fr = 0.
            turfqeo_30d = 0.
            tueo_30d = 0.
            tusol_sw = 0.
            trcu_p = 0.
            trcu_slp = 0.
            trcu_sll = 0.
            trcu_n = 0.
            trcu_cn = 0.
            trcu_cn1 = 35.
            trcu_cn2 = 0.
            trcu_cn3 = 0.
            trcu_sci = 0.
            trcu_smx = 0.
            trcu_wrt = 0.
            laiday_trcu = 0.
            canmx_trcu = 0.
            canstor_trcu = 0.
            trcu_uslek = 0.
            tusol_cov = 0.
            idplt_trcu = 0.
            tuwtab = 0.
            tunrot = 0
            trcr_fr = 0.
            trrfqeo_30d = 0.
            treo_30d = 0.
            trsol_sw = 0.
            trcr_p = 0.
            trcr_slp = 0.
            trcr_sll = 0.
            trcr_n = 0.
            trcr_cn = 0.
            trcr_cn1 = 35.
            trcr_cn2 = 0.
            trcr_cn3 = 0.
            trcr_sci = 0.
            trcr_smx = 0.
            trcr_wrt = 0.
            laiday_trcr = 0.
            canmx_trcr = 0.
            canstor_trcr = 0.
            trcr_uslek = 0.
            trsol_cov = 0.
            idplt_trcr = 0.
            trwtab = 0.
            tunrot = 0
            trcb_fr = 0.
            tbrfqeo_30d = 0.
            tbeo_30d = 0.
            tbsol_sw = 0.
            trcb_p = 0.
            trcb_slp = 0.
            trcb_sll = 0.
            trcb_n = 0.
            trcb_cn = 0.
            trcb_cn1 = 35.
            trcb_cn2 = 0.
            trcb_cn3 = 0.
            trcb_sci = 0.
            trcb_smx = 0.
            trcb_wrt = 0.
            laiday_trcb = 0.
            canmx_trcb = 0.
            canstor_trcb = 0.
            trcb_uslek = 0.
            tbsol_cov = 0.
            idplt_trcb = 0.
            tbwtab = 0.
            tunrot = 0
            trc_type = 1
            trcb_ht = 0.
            itrc1 = 0
            itrc2 = 0
            tdr_flag = 1
            trc_sumev = 0.
            trcyro = 0.
            trcaao = 0.
            trc_no = 0.
            !trc_hafrtl = 0.
            tsmx = 0.
            tsci = 0.
            twrt = 0.

            !! dimension mlyr
            tsol_st = 0.
            tsol_mp = 0.
            tsol_solp = 0.
            tsol_orgp = 0.
            tsol_actp = 0.
            tsol_stap = 0.
            tsol_fop = 0.
            tsol_n = 0.
            tsol_fon = 0.
            tsol_mn = 0.
            tsol_no3 = 0.
            tsol_nh3 = 0.
            tsol_prk = 0.
            tsol_flat = 0.
            tsol_rsd = 0.
            tsol_mc = 0.
            tsol_aorgn = 0.
            tsol_cbn = 0.
            tsol_orgn = 0.
            tsol_volcr = 0.
            !! dimension mlyr,mhru
            tusol_st = 0.
            tusol_prk = 0.
            tusol_flat = 0.
            tusol_solp = 0.
            tusol_orgp = 0.
            tusol_mp = 0.
            tusol_stap = 0.
            tusol_fop = 0.
            tusol_fon = 0.
            tusol_mn = 0.
            tusol_actp = 0.
            tusol_no3 = 0.
            tusol_nh3 = 0.
            tusol_n = 0.
            tusol_rsd = 0.
            tusol_mc = 0.
            tusol_aorgn = 0.
            tusol_orgn = 0.
            tusol_cbn = 0.
            tusol_volcr = 0.
            trsol_st = 0.
            trsol_prk = 0.
            trsol_flat = 0.
            trsol_solp = 0.
            trsol_orgp = 0.
            trsol_mp = 0.
            trsol_stap = 0.
            trsol_fop = 0.
            trsol_fon = 0.
            trsol_mn = 0.
            trsol_actp = 0.
            trsol_no3 = 0.
            trsol_nh3 = 0.
            trsol_n = 0.
            trsol_rsd = 0.
            trsol_mc = 0.
            trsol_aorgn = 0.
            trsol_orgn = 0.
            trsol_cbn = 0.
            trsol_volcr = 0.
            tbsol_st = 0.
            tbsol_prk = 0.
            tbsol_flat = 0.
            tbsol_solp = 0.
            tbsol_orgp = 0.
            tbsol_mp = 0.
            tbsol_stap = 0.
            tbsol_fop = 0.
            tbsol_fon = 0.
            tbsol_mn = 0.
            tbsol_actp = 0.
            tbsol_no3 = 0.
            tbsol_nh3 = 0.
            tbsol_n = 0.
            tbsol_rsd = 0.
            tbsol_mc = 0.
            tbsol_aorgn = 0.
            tbsol_orgn = 0.
            tbsol_cbn = 0.
            tbsol_volcr = 0.
      ! plant variables
       ! integer
       tnro = 1 
       tnrot = 1
       tncut = 1 
       ticr = 1 
       ticnop = 1 
       tntil = 1
       idorm_trc = 0
       ! real
       trc_phuplt = 0.
       trc_phuacc = 0.
       trc_bioday = 0.
       trc_bioms = 0.
       trc_cyrmat = 0
       trc_rwt = 0.
       trc_laimxfr = 0.
       trc_cht = 0.
       trc_olai = 0.
       thvstiadj = 0.
       tstrstmp = 0.
       trc_pltfrn = 0.
       trc_pltfrp = 0.
       tstrsn = 0.
       tuno3d = 0.
       trc_pltn = 0.
       trc_pltp = 0.
       tpplnt = 0.
       tnplnt = 0.
       tstrsp = 0.
       trc_fixn = 0.
       igro_trc = 0
       imgt_trc = 0.
       trc_bioyrms = 0.
       trc_yldanu = 0.
       tsumix = 0.
       trc_pltet = 0.
       trc_pltpet = 0.
       ! integer array
       iplant_trc = 0
       ihv_trc = 0
       ihvo_trc = 0
       ihv_gbm_trc = 0
       ikill_trc = 0
       trc_ncrops = 0
       idplt_trc = 0
       iop_trc = 0
       tuicnop = 1
       tunro = 1
       tuncut = 1
       tuicr = 1
       tuntil = 1
       ihvo_trcu = 0
       ihv_gbm_trcu = 0
       trcu_ncrops = 0
       ihv_trcu = 0
       iop_trcu = 0
       idplt_trcu = 0
       iplant_trcu = 0
       ikill_trcu = 0
       tricnop = 1
       trnro = 1
       trncut = 1
       tricr = 1
       trntil = 1
       ihvo_trcr = 0
       ihv_gbm_trcr = 0
       trcr_ncrops = 0
       ihv_trcr = 0
       iop_trcr = 0
       idplt_trcr = 0
       iplant_trcr = 0
       ikill_trcr = 0
       tbicnop = 1
       tbnro = 1
       tbncut = 1
       tbicr = 1
       tbntil = 1
       ihvo_trcb = 0
       ihv_gbm_trcb = 0
       trcb_ncrops = 0
       ihv_trcb = 0
       iop_trcb = 0
       idplt_trcb = 0
       iplant_trcb = 0
       ikill_trcb = 0
       ! real array
       trc_tillagef = 0.
       trc_biotarg = 0.
       phup_trc = 300.
       phuho_trc = 300.
       phuk_trc = 300.
       phut_nocr_trc = 300.
       phut_trc = 300.
       trc_inilai = 0.
       trc_inibio = 0.
       trc_cnop = 0.
       trc_biohv = 0.
       trc_hitarg = 0.
       trc_tnyld = 0.
       trc_frharvk = 0.
       trc_hiovr = 0.
       trc_yldkg = 0.
       trc_rd = 0.
       tidtill = 0.
       tharveff = 0.
       idorm_trcu = 0
       trcu_phuplt = 0.
       trcu_phuacc = 0.
       trcu_bioday = 0.
       trcu_cyrmat = 0
       trcu_rwt = 0.
       trcu_laimxfr = 0.
       trcu_cht = 0.
       trcu_olai = 0.
       trcu_pltfrn = 0.
       trcu_pltfrp = 0.
       imgt_trcu = 0.
       trcu_pltn = 0.
       trcu_pltp = 0.
       trcu_fixn = 0.
       igro_trcu = 0
       trcu_bioyrms = 0.
       trcu_yldanu = 0.
       trcu_bioms = 0.
       tusumix = 0.
       trcu_pltet = 0.
       trcu_pltpet = 0.
       trcu_rd = 0.
       tuhvstiadj = 0.
       tupplnt = 0.
       tunplnt = 0.
       trcu_tillagef = 0.
       trcu_biotarg = 0.
       phuho_trcu  = 300.
       phup_trcu = 300.
       phut_trcu = 300.
       phut_nocr_trcu = 300.
       phuk_trcu = 300.
       trcu_inilai = 0.
       trcu_inibio = 0.
       trcu_cnop = 0.
       trcu_biohv = 0.
       trcu_hitarg = 0.
       trcu_tnyld = 0.
       trcu_frharvk = 0.
       trcu_hiovr = 0.
       trcu_yldkg = 0.
       tuidtill = 0.
       tuharveff = 0.
       idorm_trcr = 0
       trcr_phuplt = 0.
       trcr_phuacc = 0.
       trcr_bioday = 0.
       trcr_cyrmat = 0
       trcr_rwt = 0.
       trcr_laimxfr = 0.
       trcr_cht = 0.
       trcr_olai = 0.
       trcr_pltfrn = 0.
       trcr_pltfrp = 0.
       imgt_trcr = 0.
       trcr_pltn = 0.
       trcr_pltp = 0.
       trcr_fixn = 0.
       igro_trcr = 0
       trcr_bioyrms = 0.
       trcr_yldanu = 0.
       trcr_bioms = 0.
       trsumix = 0.
       trcr_pltet = 0.
       trcr_pltpet = 0.
       trcr_rd = 0.
       trhvstiadj = 0.
       trpplnt = 0.
       trnplnt = 0.
       trcr_tillagef = 0.
       trcr_biotarg = 0.
       phuho_trcr  = 300.
       phup_trcr = 300.
       phut_trcr = 300.
       phut_nocr_trcr = 300.
       phuk_trcr = 300.
       trcr_inilai = 0.
       trcr_inibio = 0.
       trcr_cnop = 0.
       trcr_biohv = 0.
       trcr_hitarg = 0.
       trcr_tnyld = 0.
       trcr_frharvk = 0.
       trcr_hiovr = 0.
       trcr_yldkg = 0.
       tridtill = 0.
       trharveff = 0.
       idorm_trcb = 0
       trcb_phuplt = 0.
       trcb_phuacc = 0.
       trcb_bioday = 0.
       trcb_cyrmat = 0
       trcb_rwt = 0.
       trcb_laimxfr = 0.
       trcb_cht = 0.
       trcb_olai = 0.
       trcb_pltfrn = 0.
       trcb_pltfrp = 0.
       imgt_trcb = 0.
       trcb_pltn = 0.
       trcb_pltp = 0.
       trcb_fixn = 0.
       igro_trcb = 0
       trcb_bioyrms = 0.
       trcb_yldanu = 0.
       trcb_bioms = 0.
       tbsumix = 0.
       trcb_pltet = 0.
       trcb_pltpet = 0.
       trcb_rd = 0.
       tbhvstiadj = 0.
       tbpplnt = 0.
       tbnplnt = 0.
       trcb_tillagef = 0.
       trcb_biotarg = 0.
       phuho_trcb  = 300.
       phup_trcb = 300.
       phut_trcb = 300.
       phut_nocr_trcb = 300.
       phuk_trcb = 300.
       trcb_inilai = 0.
       trcb_inibio = 0.
       trcb_cnop = 0.
       trcb_biohv = 0.
       trcb_hitarg = 0.
       trcb_tnyld = 0.
       trcb_frharvk = 0.
       trcb_hiovr = 0.
       trcb_yldkg = 0.
       tbidtill = 0.
       tbharveff = 0.
       phuh_trc = 300.
       phuh_trcu = 300.
       phuh_trcr = 300.
       phuh_trcb = 300.
       trc_tnylda = 0.
       trcu_tnylda = 0.
       trcr_tnylda = 0.
       trcb_tnylda = 0.
       trcu_bioaams = 0.
       trcr_bioaams = 0.
       trcb_bioaams = 0.
       trcu_laiaamx = 0.
       trcr_laiaamx = 0.
       trcb_laiaamx = 0.
       trcu_yldaa = 0.
       trcr_yldaa = 0.
       trcb_yldaa = 0.
       trcu_yldn = 0.
       trcr_yldn = 0.
       trcb_yldn = 0.
       trcu_bioaahv = 0.
       trcr_bioaahv = 0.
       trcb_bioaahv = 0.
       tustrsw = 1.
       tustrsa = 1.
       tustrsn = 1.
       tustrsp = 1.
       tustrstmp = 1.
       trstrsw = 1.
       trstrsa = 1.
       trstrsn = 1.
       trstrsp = 1.
       trstrstmp = 1.
       tbstrsw = 1.
       tbstrsa = 1.
       tbstrsn = 1.
       tbstrsp = 1.
       tbstrstmp = 1.
       cnday_trc = 0.
       tuhmntl = 0.
       turwntl = 0.
       tuhmptl = 0.
       turmn2tl = 0.
       turmptl = 0.
       tuwdntl = 0.
       turoctl = 0.
       turmp1tl = 0.
       tucmup_kgh = 0.
       tucmtot_kgh = 0.
       trhmntl = 0.
       trrwntl = 0.
       trhmptl = 0.
       trrmn2tl = 0.
       trrmptl = 0.
       trwdntl = 0.
       trroctl = 0.
       trrmp1tl = 0.
       trcmup_kgh = 0.
       trcmtot_kgh = 0.
       tbhmntl = 0.
       tbrwntl = 0.
       tbhmptl = 0.
       tbrmn2tl = 0.
       tbrmptl = 0.
       tbwdntl = 0.
       tbroctl = 0.
       tbrmp1tl = 0.
       tbcmup_kgh = 0.
       tbcmtot_kgh = 0.

! fertilization variables(compute)
      trc_cfertn = 0.
      trc_cfertp = 0.
      trc_auton = 0.
      trc_autop = 0.
      trc_fertn = 0.
      trc_fertp = 0.

      trc_tfertn = 0.
      trc_tfertp = 0.
      trc_tcfrtn = 0.
      trc_tcfrtp = 0.
      tauto_napp = 0.
      trc_anano3 = 0.
      tauto_nyr = 0.
      tafrt_surface = 0.
      tauto_nstrs = 0.
      trc_tauton = 0.
      trc_tautop = 0.

      tnfert = 1
      trc_iafrttyp = 0
      tncf = 0
      trc_icfrt = 0
      tiday_fert = 0
      trc_ndcfrt = 0

      tfrt_surface = 0.
      tfrt_kg = 0.
      tcfrt_kg = 0.
      phucf_trc = 0.
      phun_nocrop_trc = 0.
      phun_trc = 0.

      trc_ifrttyp = 0
      tcfrt_id = 0
      trc_icfert = 0
      tifrt_freq = 0
      trc_ifert = 0
      tfert_days = 0

! fertilization variables(undisturbed)
      trcu_cfertn = 0.
      trcu_cfertp = 0.
      trcu_auton = 0.
      trcu_autop = 0.
      trcu_fertn = 0.
      trcu_fertp = 0.

      trcu_tfertn = 0.
      trcu_tfertp = 0.
      trcu_tcfrtn = 0.
      trcu_tcfrtp = 0.
      tuauto_napp = 0.
      trcu_anano3 = 0.
      tuauto_nyr = 0.
      tuafrt_surface = 0.
      tuauto_nstrs = -1.
      trcu_tauton = 0.
      trcu_tautop = 0.

      tunfert = 1
      trcu_iafrttyp = 0
      tuncf = 1
      trcu_icfrt = 0
      tuiday_fert = 0
      trcu_ndcfrt = 0

      tufrt_surface = 0.
      tufrt_kg = 0.
      tucfrt_kg = 0.
      phucf_trcu = 300.
      phun_nocrop_trcu = 300.
      phun_trcu = 300.

      trcu_ifrttyp = 0
      tucfrt_id = 0
      trcu_icfert = 0
      tuifrt_freq = 0
      trcu_ifert = 0
      tufert_days = 0

! fertilization variables(raiser)
      trcr_cfertn = 0.
      trcr_cfertp = 0.
      trcr_auton = 0.
      trcr_autop = 0.
      trcr_fertn = 0.
      trcr_fertp = 0.

      trcr_tfertn = 0.
      trcr_tfertp = 0.
      trcr_tcfrtn = 0.
      trcr_tcfrtp = 0.
      trauto_napp = 0.
      trcr_anano3 = 0.
      trauto_nyr = 0.
      trafrt_surface = 0.
      trauto_nstrs = -1.
      trcr_tauton = 0.
      trcr_tautop = 0.

      trnfert = 1
      trcr_iafrttyp = 0
      trncf = 1
      trcr_icfrt = 0
      triday_fert = 0
      trcr_ndcfrt = 0

      trfrt_surface = 0.
      trfrt_kg = 0.
      trcfrt_kg = 0.
      phucf_trcr = 300.
      phun_nocrop_trcr = 300.
      phun_trcr = 300.

      trcr_ifrttyp = 0
      trcfrt_id = 0
      trcr_icfert = 0
      trifrt_freq = 0
      trcr_ifert = 0
      trfert_days = 0

! fertilization variables(bed)
      trcb_cfertn = 0.
      trcb_cfertp = 0.
      trcb_auton = 0.
      trcb_autop = 0.
      trcb_fertn = 0.
      trcb_fertp = 0.

      trcb_tfertn = 0.
      trcb_tfertp = 0.
      trcb_tcfrtn = 0.
      trcb_tcfrtp = 0.
      tbauto_napp = 0.
      trcb_anano3 = 0.
      tbauto_nyr = 0.
      tbafrt_surface = 0.
      tbauto_nstrs = -1.
      trcb_tauton = 0.
      trcb_tautop = 0.

      tbnfert = 1
      trcb_iafrttyp = 0
      tbncf = 1
      trcb_icfrt = 0
      tbiday_fert = 0
      trcb_ndcfrt = 0

      tbfrt_surface = 0.
      tbfrt_kg = 0.
      tbcfrt_kg = 0.
      phucf_trcb = 300.
      phun_nocrop_trcb = 300.
      phun_trcb = 300.

      trcb_ifrttyp = 0
      tbcfrt_id = 0
      trcb_icfert = 0
      tbifrt_freq = 0
      trcb_ifert = 0
      tbfert_days = 0

      ! rainfall harvest tank storage variables
      trc_tvol = 0.
      trc_tvolx = 0.
      tnk_eff = 1.0
      trc_tnkmm = 0.
      tankirr = 0.
      ! irrigation variables(compute)
      trc_aird = 0.
      trc_qird = 0.
      trc_tankfr = 0.
      tnair = 1
      tnirr = 1
      tauto_wstr = 0.
      tirr_mx = 0.
      tirr_asq = 0.
      tirr_sq = 0.
      tirr_efm = 1.
      tirr_amt = 0.
      phuirr_nocrop_trc = 300.
      phuirr_trc = 300.
      tirr_eff = 1.
      twstrs_id = 0
      trc_iir = 0
      ! irrigation variables(undisturbed)
      trcu_aird = 0.
      trcu_qird = 0.
      trcu_tankfr = 0.
      tunair = 1
      tunirr = 1
      tuauto_wstr = 0.
      tuirr_mx = 0.
      tuirr_asq = 0.
      tuirr_sq = 0.
      tuirr_efm = 1.
      tuirr_amt = 0.
      phuirr_nocrop_trcu = 300.
      phuirr_trcu = 300.
      tuirr_eff = 1.
      tuwstrs_id = 0
      trcu_iir = 0
      tuiairr = 0
      phuai_trcu = 300.
      ! irrigation variables(raiser)
      trcr_aird = 0.
      trcr_qird = 0.
      trcr_tankfr = 0.
      trnair = 1
      trnirr = 1
      trauto_wstr = 0.
      trirr_mx = 0.
      trirr_asq = 0.
      trirr_sq = 0.
      trirr_efm = 1.
      trirr_amt = 0.
      phuirr_nocrop_trcr = 300.
      phuirr_trcr = 300.
      trirr_eff = 1.
      trwstrs_id = 0
      trcr_iir = 0
      triairr = 0
      phuai_trcr = 300.
      ! irrigation variables(bed)
      trcb_aird = 0.
      trcb_qird = 0.
      trcb_tankfr = 0.
      tbnair = 1
      tbnirr = 1
      tbauto_wstr = 0.
      tbirr_mx = 0.
      tbirr_asq = 0.
      tbirr_sq = 0.
      tbirr_efm = 1.
      tbirr_amt = 0.
      phuirr_nocrop_trcb = 300.
      phuirr_trcb = 300.
      tbirr_eff = 1.
      tbwstrs_id = 0
      trcb_iir = 0
      tbiairr = 0
      phuai_trcb = 300.
      trfqeo_30d = 0.
      teo_30d = 0.
      tstrsw = 1.
      tstrsa = 1.
      tstrsn = 1.
      tstrsp = 1.
      tstrstmp = 1.
      hru_fn = 0.
      hru_fp = 0.
      hru_yn = 0.
      hru_yp = 0.
      hru_rn = 0.
      hru_vn = 0.
      hru_fxn = 0.
      hru_dnt = 0.
      pre_wbal = 0.
      pre_nbal = 0.
      pre_pbal = 0.

      trcu_cnday = 0.
      trcr_cnday = 0.
      trcb_cnday = 0.
      trcu_esday = 0.
      trcr_esday = 0.
      trcb_esday = 0.
      trcu_epday = 0.
      trcr_epday = 0.
      trcb_epday = 0.
return
end


